use medipersona;

CREATE TABLE IF NOT EXISTS medipersona.cosmos_user(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(64) NOT NULL UNIQUE,
    firstName VARCHAR(64),
    middleName VARCHAR(64),
    lastName VARCHAR(64),
    password VARCHAR(256),
    phoneNumber VARCHAR(64),
    enabled BIT,
    username VARCHAR(64) NOT NULL UNIQUE,
    accountNonExpired BIT,
    accountNonLocked BIT,
    credentialsNonExpired BIT,
    imageData TEXT,
    createdOn DATETIME,
    roles VARCHAR(4096),
    roleGroups VARCHAR(4098)
);

DROP PROCEDURE IF EXISTS medipersona.create_cosmos_user;

DELIMITER $$

CREATE PROCEDURE medipersona.create_cosmos_user(
	OUT id BIGINT,
        IN email VARCHAR(64),
        IN firstName VARCHAR(64),
        IN middleName VARCHAR(64),
        IN lastName VARCHAR(64),
        IN password VARCHAR(256),
        IN phoneNumber VARCHAR(64),
        IN enabled BIT,
        IN username VARCHAR(64),
        IN accountNonExpired BIT,
        IN accountNonLocked BIT,
        IN credentialsNonExpired BIT,
        IN imageData VARCHAR(20480),
        IN createdOn DATETIME,
        IN roles VARCHAR(4098),
        IN roleGroups VARCHAR(4098)
)
BEGIN
	INSERT INTO medipersona.cosmos_user
        (
            email,
            firstName,
            middleName,
            lastName,
            password,
            phoneNumber,
            enabled,
            username,
            accountNonExpired,
            accountNonLocked,
            credentialsNonExpired,
            imageData,
            createdOn,
            roles,
            roleGroups
        )
	VALUES
	(
            email,
            firstName,
            middleName,
            lastName,
            password,
            phoneNumber,
            enabled,
            username,
            accountNonExpired,
            accountNonLocked,
            credentialsNonExpired,
            imageData,
            createdOn,
            roles,
            roleGroups
	);
        SET id = LAST_INSERT_ID();
       
        
END $$
DELIMITER ;

drop procedure if exists medipersona.update_cosmos_user;
DELIMITER %%

create procedure medipersona.update_cosmos_user(
         IN email VARCHAR(64),
        IN firstName VARCHAR(64),
        IN middleName VARCHAR(64),
        IN lastName VARCHAR(64),
        IN password VARCHAR(256),
        IN phoneNumber VARCHAR(64),
        IN enabled BIT,
        IN username VARCHAR(64),
        IN accountNonExpired BIT,
        IN accountNonLocked BIT,
        IN credentialsNonExpired BIT,
        IN imageData VARCHAR(20480),
        IN createdOn DATETIME,
        IN roles VARCHAR(4098),
        IN roleGroups VARCHAR(4098)
)

begin
    
    update medipersona.cosmos_user
   set 
       medipersona.cosmos_user.email= email,
       medipersona.cosmos_user.firstName=firstName,
            medipersona.cosmos_user.middleName=middleName,
            medipersona.cosmos_user.lastName=lastName,
            medipersona.cosmos_user.password=password,
            medipersona.cosmos_user.phoneNumber=phoneNumber,
            medipersona.cosmos_user.enabled=enabled,
            medipersona.cosmos_user.username=username,
            medipersona.cosmos_user.accountNonExpired=accountNonExpired,
            medipersona.cosmos_user.accountNonLocked=accountNonLocked,
            medipersona.cosmos_user.credentialsNonExpired=credentialsNonExpired,
            medipersona.cosmos_user.imageData=imageData,
            medipersona.cosmos_user.createdOn=createdOn,
            medipersona.cosmos_user.roles=roles,
            medipersona.cosmos_user.roleGroups=roleGroups
        

    where medipersona.cosmos_user.username=username;

end %%
delimiter ;


DROP PROCEDURE IF EXISTS medipersona.find_cosmos_user_by_username;
DELIMITER $$
CREATE PROCEDURE medipersona.find_cosmos_user_by_username
(
	IN username VARCHAR(64)
)
BEGIN
  SELECT *  FROM medipersona.cosmos_user  WHERE medipersona.cosmos_user.username = username;
END$$

DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.find_cosmos_user_by_email;
DELIMITER $$
CREATE PROCEDURE medipersona.find_cosmos_user_by_email
(
	IN email VARCHAR(64)
)
BEGIN
  SELECT *  FROM medipersona.cosmos_user  WHERE medipersona.cosmos_user.email = email;
END $$

DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.find_cosmos_user_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.find_cosmos_user_by_id
(
    IN id BIGINT
)
BEGIN
  SELECT *  FROM medipersona.cosmos_user  WHERE medipersona.cosmos_user.id=id ;
END$$

DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.delete_cosmos_user_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.delete_cosmos_user_by_id
(
    IN id BIGINT
)
BEGIN
  DELETE FROM medipersona.cosmos_user  WHERE medipersona.cosmos_user.id = id;
END$$

DELIMITER ;

DROP PROCEDURE IF EXISTS medipersona.find_all_cosmos_user;
DELIMITER $$
CREATE PROCEDURE medipersona.find_all_cosmos_user(
    in pageNum int,
    in pageSize int,
    out count int
)
BEGIN
    declare number_to_skip, temp_counter, counter int;

    set number_to_skip = pageSize *  pageNum ;
    select count(*) into counter from medipersona.cosmos_user;
    set count = counter;
    
    Prepare findStatement from 'select * from medipersona.cosmos_user order by medipersona.cosmos_user.id desc limit ?';
    prepare findStatement2 from 'select * from medipersona.cosmos_user limit ? offset ?';

    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;

            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
-- 
--         if(pageSize <= 0) then
--             Set @pageSize = counter ;
--         end if;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
END$$

DELIMITER ;

-- END COSMOS USER SCRIPTS

-- BEGIN ROLE SCRIPTS

CREATE TABLE IF NOT EXISTS medipersona.core_roles(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL UNIQUE,
    description VARCHAR(256),
    createdOn DATETIME
);

DROP PROCEDURE IF EXISTS medipersona.create_core_role;

DELIMITER $$

CREATE PROCEDURE medipersona.create_core_role(
	OUT id BIGINT,
        IN name VARCHAR(128),
        IN description VARCHAR(256),
        IN createdOn DATETIME
)
BEGIN
	INSERT INTO medipersona.core_roles
        (
            name,
            description,
            createdOn
        )
	VALUES
	(
            name,
            description,
            createdOn
	);
        SET id = LAST_INSERT_ID();
       
        
END $$
DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.delete_core_role_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.delete_core_role_by_id
(
	IN id BIGINT
)
BEGIN
  DELETE FROM medipersona.core_roles  WHERE medipersona.core_roles.id = id;
END$$
DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.find_core_role_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.find_core_role_by_id
(
	IN id BIGINT
)
BEGIN
  SELECT *  FROM medipersona.core_roles  WHERE medipersona.core_roles.id = id;
END$$
DELIMITER ;

DROP PROCEDURE IF EXISTS medipersona.find_core_role_by_name;
DELIMITER $$
CREATE PROCEDURE medipersona.find_core_role_by_name
(
	IN name varchar(128)
)
BEGIN
  SELECT *  FROM medipersona.core_roles  WHERE medipersona.core_roles.name = name;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS medipersona.update_core_role;
DELIMITER $$
CREATE PROCEDURE medipersona.update_core_role(
	IN id BIGINT,
        IN name VARCHAR(128),
        IN description VARCHAR(256)
)
BEGIN
	UPDATE medipersona.core_roles
        SET 
            medipersona.core_roles.name = name,
            medipersona.core_roles.description = description
        WHERE medipersona.core_roles.id = id;   
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS medipersona.find_all_core_role;

DELIMITER $$
CREATE PROCEDURE medipersona.find_all_core_role
(
    in pageNum int, 
    in pageSize int,
    out count int
)
BEGIN
  declare number_to_skip, temp_counter, counter int;
	
	set number_to_skip = pageSize *  pageNum;
	
	select count(*) into counter from medipersona.core_roles;
        SET count=counter;
  PREPARE ps1 FROM 'select * from medipersona.core_roles
                    limit ?';
	
   PREPARE ps2 FROM 'select * from medipersona.core_roles
                LIMIT ? OFFSET ?';
	
	if (number_to_skip >= counter) then
            set temp_counter = number_to_skip % count;
            set @pageSize = pageSize;
            EXECUTE ps1 USING @pageSize ;
            DROP PREPARE ps1; 
	else
            SET @pageSize = pageSize;
            SET @moffSet = number_to_skip;

            if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

            EXECUTE ps2 USING @pageSize,@moffSet ;
            DROP PREPARE ps2;   
	end if;
END $$
Delimiter ;
-- END ROLE SCRIPTS


--  BEGIN ROLEGROUP SCRIPTS


CREATE TABLE IF NOT EXISTS medipersona.core_role_groups(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL UNIQUE,
    description VARCHAR(256),
    createdOn DATETIME,
    roleIds VARCHAR(10240)
);

DROP PROCEDURE IF EXISTS medipersona.create_core_role_group;

DELIMITER $$

CREATE PROCEDURE medipersona.create_core_role_group(
	OUT id BIGINT,
        IN name VARCHAR(128),
        IN description VARCHAR(256),
        IN createdOn DATETIME,
        IN roleIds VARCHAR(10240)
)
BEGIN
	INSERT INTO medipersona.core_role_groups
        (
            name,
            description,
            createdOn,
            roleIds
        )
	VALUES
	(
            name,
            description,
            createdOn,
            roleIds
	);
        SET id = LAST_INSERT_ID();
       
        
END $$
DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.delete_core_role_group_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.delete_core_role_group_by_id
(   
    IN id BIGINT
)
BEGIN
  DELETE FROM medipersona.core_role_groups  WHERE medipersona.core_role_groups.id = id;
END$$

DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.find_core_role_group_by_id;
DELIMITER $$
CREATE PROCEDURE medipersona.find_core_role_group_by_id
(
    IN id BIGINT
)
BEGIN
  SELECT *  FROM medipersona.core_role_groups  WHERE medipersona.core_role_groups.id = id;
END $$

DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.find_core_role_group_by_name;
DELIMITER $$
CREATE PROCEDURE medipersona.find_core_role_group_by_name
(
    IN name varchar(64)
)
BEGIN
  SELECT *  FROM medipersona.core_role_groups  WHERE medipersona.core_role_groups.name = name;
END $$
DELIMITER ;

DROP PROCEDURE IF EXISTS medipersona.update_core_role_group;
DELIMITER $$
CREATE PROCEDURE medipersona.update_core_role_group(
	IN id BIGINT,
        IN name VARCHAR(128),
        IN description VARCHAR(256),
        IN roleIds VARCHAR(10240)
)
BEGIN

	UPDATE medipersona.core_role_groups
        SET core_role_groups.name = name,
            core_role_groups.description = description,
            core_role_groups.roleIds = roleIds
        WHERE core_role_groups.id = id ;
               
END $$
Delimiter;

DROP PROCEDURE IF EXISTS medipersona.find_all_core_role_group;
DELIMITER $$

CREATE PROCEDURE medipersona.find_all_core_role_group
(
    in pageNum int, 
    in pageSize int,
    out count int
)
BEGIN
  declare number_to_skip, temp_counter, counter int;
	
	set number_to_skip = pageSize *  pageNum;
	
	select count(*) into counter from medipersona.core_role_groups;
    SET count=counter;

  PREPARE ps1 FROM 'select * from medipersona.core_role_groups
                    limit ?';
	
   PREPARE ps2 FROM 'select * from medipersona.core_role_groups
                LIMIT ? OFFSET ?';
	
	if (number_to_skip >= count) then
            set temp_counter = number_to_skip % count;

            SET @pageSize = pageSize;

            EXECUTE ps1 USING @pageSize ;
            DROP PREPARE ps1; 
	else
            SET @pageSize = pageSize;
            SET @moffSet = number_to_skip;

            if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

            EXECUTE ps2 USING @pageSize,@moffSet ;
            DROP PREPARE ps2;   
	end if;
END $$

DELIMITER ;