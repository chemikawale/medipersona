<%-- 
    Document   : styles
    Created on : 24-Jul-2014, 12:07:01
    Author     : Adeyemi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="<c:url value="/css/bootstrap/bootstrap.css"/>" rel="stylesheet" />

<link href="<c:url value="/css/libs/dataTables.bootstrap.css"/>" rel="stylesheet" />
<!-- libraries -->
<link href="<c:url value="/css/libs/font-awesome.css"/>" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="<c:url value="/css/libs/nanoscroller.css"/>" type="text/css" />

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<c:url value="/css/compiled/layout.css" />">
<link rel="stylesheet" type="text/css" href="<c:url value="/css/compiled/elements.css" />">

<!-- this page specific styles -->
<link rel="stylesheet" href="<c:url value="/css/libs/datepicker.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/daterangepicker.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/bootstrap-timepicker.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/select2.css" />" type="text/css" />

	<!-- libraries -->
 <!-- <link href="<c:url value="/css/libs/jquery-ui-1.10.2.custom.css" />" rel="stylesheet" type="text/css" /> -->

	<!-- global styles -->

	<!-- this page specific styles -->
<link rel="stylesheet" href="<c:url value="/css/libs/fullcalendar.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/fullcalendar.print.css"/>" type="text/css" media="print" />
<link rel="stylesheet" href="<c:url value="/css/compiled/calendar.css"/>" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/css/libs/morris.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/daterangepicker.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/libs/jquery-jvectormap-1.2.2.css" />" type="text/css" />
	
	<!-- Favicon -->
	<link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>

	<!-- google font libraries -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

	<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->

<!-- Favicon -->
<link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>

<!-- google font libraries -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css" />