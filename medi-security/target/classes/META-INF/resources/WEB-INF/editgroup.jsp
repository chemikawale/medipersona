<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>MediPersona || EDIT ROLE GROUP</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../../css/basic.css">
    <link rel="stylesheet" href="../../css/general.css">
    <link rel="stylesheet" href="../../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
      <style>
                .role-list li{
                    list-style: none !important;
                    color: red;
                }
                
                .role-list li input[type='checkbox']{
                    font-size: 15px;
                    margin-right: 2%;
                }
            </style>
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
   
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
   <jsp:include page="/WEB-INF/sidebar.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../WEB-INF/navigation.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">Edit <span>Role Group</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>Role Group</span> Details</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              </ul>
            </div>
            <div class="l-box-body l-spaced">
            <c:url var="createrole" value="${baseRolePath}/creategroup" />
                     <form:form  role="form"  commandName="roleModel" action="" method="POST" class="form-horizontal">
                              <c:if test="${message != null && !message.isEmpty()}">
                                                    <div class="alert alert-warning">
                                                        <c:out value="${message}" />
                                                    </div>
                                                </c:if>
                              <c:if test="${success != null && ! success.isEmpty()}">
                                                    <div class="alert alert-success">
                                                        <c:out value="${success}" />
                                                    </div>
                                                </c:if>
                 <div class="form-group">
                   <label class="col-sm-3 control-label" for="name">ROLE GROUP NAME :</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control" id="name" name="name" path="name" type="text" placeholder="Role Name"  required="true"/>
                         <form:errors path="name" cssStyle="color : red;"/>       
                       </div>
                       </div>
                        
              <div class="form-group">
                  <label for="autogrowTextarea" class="col-sm-3 control-label">DESCRIPTION:</label>
                  <div class="col-sm-5">               
                      <form:textarea class="form-control" id="autogrowTextarea" name="description" path="description" rows="3"  placeholder="Description" required="true"/>
                      <form:errors path="description" cssStyle="color : red;"/>    
                  </div>
              </div>
                    <div class="form-group">
                                                    <label class="control-label" for="groupRoles">Choose Roles</label>
                                                    <ul class="role-list">
                                                        <form:checkboxes element="li" cssClass="checkbox-nice" id="groupRoles" path="groupRoles" items="${allroles}" />
                                                    </ul>
                                                </div>
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">Update</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
          
      </section>

    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../../js/basic/jquery.min.js"></script>
    <script src="../../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../../js/basic/modernizr.min.js"></script>
    <script src="../../js/basic/bootstrap.min.js"></script>
    <script src="../../js/shared/jquery.asonWidget.js"></script>
    <script src="../../js/plugins/plugins.js"></script>
    <script src="../../js/general.js"></script>
   
    <script src="../../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../../js/shared/classie.js"></script>
    <script src="../../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.autogrow.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.fancySelect.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.maskedinput.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.onoff.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.select2.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../../js/plugins/forms/elements/jquery.textareaCounter.plugin.js"></script>
    <script src="../../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../../js/calls/form.elements.js"></script>
    <script src="../../js/calls/part.header.1.js"></script>
    <script src="../../js/calls/part.sidebar.2.js"></script>
    <script src="../../js/calls/part.theme.setting.js"></script>
   
  
     <script>   
        $("#role").change(function(){
           var value = $(this).val();
           
           if(value === "Dynamic"){
               $("#permissions").show(600);
           }
           else{
               $("#permissions").hide();
           }
        });
        
    </script>
  </body>

</html>
       
  </body>

</html>