
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>  

<aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a></div>
        <div class="l-side-box">
          <!--Logo-->
          <div class="widget-logo logo-in-side">
              <h1>    <a href="${pageContext.request.contextPath}/medipersona/home"><span class="logo-default visible-default-inline-block"><img src="<c:url value="/img/logo.png"/>" alt="Proteus"></span><span class="logo-medium visible-compact-inline-block"><img src="img/logo_medium.png" alt="Proteus" title="PROTEUS"></span>
                      <span class="logo-small visible-collapsed-inline-block"><img src="<c:url value="/img/logo_small.png" />" alt="Proteus" title="PROTEUS"></span></a></h1>
          </div>
        </div>
        <!--Main Menu-->
        <div class="l-side-box">
          <!--MAIN NAVIGATION MENU-->
          <nav class="navigation">
            <ul data-ason-type="menu" class="ason-widget">                                     
                     <security:authorize access="isAuthenticated()">
              <li><a href="/medipersona/home"><i class="icon fa fa-dashboard"></i><span class="title">HOME</span></a></li>
              <li class="<c:out value='${role}' />"><a href="#"><i class="icon fa fa-user-secret"></i><span class="title">ROLES</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li >    <a href="<c:url value="${baseRolePath}/all" />" ><span class="title">ALL ROLES</span></a>
                  </li>
                  <li >  <a href="<c:url value="${baseRolePath}/create" />" ><span class="title">NEW ROLE</span></a>
                  </li>
                </ul>
              </li>    
                  </security:authorize>
                    <security:authorize access="isAuthenticated()">     
               <li class="<c:out value='${rolegroup}' />"><a href="#"><i class="icon fa fa-user-times"></i><span class="title">ROLE GROUPS</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="<c:url value="${baseRolePath}/allgroup" />" > <span class="title">ALL ROLE GROUPS</span></a>
                  </li>
                  <li > <a href="<c:url value="${baseRolePath}/creategroup" />" ><span class="title">NEW ROLE GROUP</span></a>
                  </li>
                  
                </ul>
              </li>
                </security:authorize>
             
            </ul>
          </nav>
        </div>
      </aside>