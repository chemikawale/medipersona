<%-- 
    Document   : js
    Created on : 24-Jul-2014, 13:13:08
    Author     : Adeyemi
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--[if lt IE 9]>
        <script src="<c:url value="/js/html5shiv.js" />"></script>
        <script src="<c:url value="/js/respond.min.js" />"></script>
<![endif]-->
<script src="<c:url value="/js/demo-skin-changer.js" />"></script> <!-- only for demo -->
	
<script src="<c:url value="/js/jquery.js" />"></script>
<script src="<c:url value="/js/jquery.dataTables.min.js" />"></script>
<script src="<c:url value="/js/dataTables.bootstrap.js" />"></script>
<script src="<c:url value="/js/bootstrap.js" />"></script>
<script src="<c:url value="/js/jquery.nanoscroller.min.js" />"></script>

<script src="<c:url value="/js/demo.js" />"></script> <!-- only for demo -->

<!-- this page specific scripts -->
<script src="<c:url value="/js/jquery-ui.custom.min.js" />"></script>
<script src="<c:url value="/js/fullcalendar.min.js" />"></script>
<script src="<c:url value="/js/jquery.slimscroll.min.js" />"></script>
<script src="<c:url value="/js/raphael-min.js" />"></script>
<script src="<c:url value="/js/morris.min.js" />"></script>
<script src="<c:url value="/js/moment.min.js" />"></script>
<script src="<c:url value="/js/daterangepicker.js" />"></script>
<script src="<c:url value="/js/bootstrap-datepicker.js" />"></script>
<script src="<c:url value="/js/jquery-jvectormap-1.2.2.min.js" />"></script>
<script src="<c:url value="/js/jquery-jvectormap-world-merc-en.js" />"></script>
<script src="<c:url value="/js/gdp-data.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.min.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.pie.min.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.stack.min.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.resize.min.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.time.min.js" />"></script>
<script src="<c:url value="/js/flot/jquery.flot.threshold.js" />"></script>
<script src="<c:url value="/js/select2.min.js" />"></script>
<script src="<c:url value="/js/jquery.numeric.js" />"></script>
<script src="<c:url value="/js/jquery.maskMoney.min.js" />"></script>
<!-- this page specific scripts -->


<!-- theme scripts -->
<script src="<c:url value="/js/scripts.js" />"></script>
