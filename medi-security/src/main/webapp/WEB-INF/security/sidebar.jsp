<%-- 
    Document   : sidebar
    Created on : 31-Jul-2014, 19:17:23
    Author     : Adeyemi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div id="nav-col">
    <section id="col-left" class="col-left-nano">
        <div id="col-left-inner" class="col-left-nano-content">
            <div id="user-left-box" class="clearfix hidden-sm hidden-xs">

                <div class="user-box">
                    <span class="name">
                        Welcome<br/>
                        <c:if test="${authentication != null}">
                            <%= request.getUserPrincipal().getName()%>
                        </c:if>
                    </span>
                    <span class="status">
                        <i class="fa fa-circle"></i> Online
                    </span>
                </div>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">	
                <ul class="nav nav-pills nav-stacked">
                    <%--<security:authorize access="hasRole('ROLE_MANAGER')">--%>
                    <li class="<c:out value="${home}" />">
                        <a href="${pageContext.request.contextPath}/index">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <%--</security:authorize>--%>
                    <security:authorize access="isAuthenticated()">
                    <li class="<c:out value='${role}' />">
                        <a href="" class="dropdown-toggle">
                            <i class="fa fa-users"></i>
                            <span>Roles</span>
                            <i class="fa fa-chevron-circle-right drop-icon"></i>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="<c:url value="/users/customers" />" >
                                    All Roles
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/users/customers/create" />" >
                                    New Role
                                </a>
                            </li>
                        </ul>
                    </li>
                    </security:authorize>
                    <security:authorize access="isAuthenticated()">
                    <li class="<c:out value='${rolegroup}' />">
                        <a href="#" class="dropdown-toggle">
                            <i class="fa fa-users"></i>
                            <span>Staff</span>
                            <i class="fa fa-chevron-circle-right drop-icon"></i>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="<c:url value="/security/role/all" />" > 
                                    All Role Groups
                                </a>
                            </li>
                            <li>
                                <a href="<c:url value="/security/role/allgroup" />" >
                                    New Role Group
                                </a>
                            </li>
                        </ul>
                    </li>
                    </security:authorize>
<!--                    <li class="<c:out value="${profile}" />">
                        <a href="<c:url value="/security/user/myprofile" />">
                            <i class="fa fa-user"></i>
                            <span>My Profile</span>
                        </a>
                    </li>-->
                    <li class="<c:out value="${profile}" /> ">
                        <a href="<c:url value="/users/changepassword" />">
                            <i class="fa fa-lock"></i>
                            <span>Change Password</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</div>