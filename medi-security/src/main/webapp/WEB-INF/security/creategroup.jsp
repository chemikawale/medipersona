<%-- 
    Document   : create
    Created on : 03-Aug-2014, 14:36:56
    Author     : Adeyemi
--%>

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Create Role Group</title>

        <!-- bootstrap -->
        <%--<jsp:include page="../../views/includes/styles.jsp" />--%>
        <link href="<c:url value="/css/bootstrap/bootstrap.css"/>" rel="stylesheet" />

        <link href="<c:url value="/css/libs/dataTables.bootstrap.css"/>" rel="stylesheet" />
        <!-- libraries -->
        <link href="<c:url value="/css/libs/font-awesome.css"/>" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" href="<c:url value="/css/libs/nanoscroller.css"/>" type="text/css" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/compiled/layout.css" />">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/compiled/elements.css" />">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="<c:url value="/css/libs/datepicker.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/daterangepicker.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/bootstrap-timepicker.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/select2.css" />" type="text/css" />

            <!-- libraries -->
     <!-- <link href="<c:url value="/css/libs/jquery-ui-1.10.2.custom.css" />" rel="stylesheet" type="text/css" /> -->

            <!-- global styles -->

            <!-- this page specific styles -->
        <link rel="stylesheet" href="<c:url value="/css/libs/fullcalendar.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/fullcalendar.print.css"/>" type="text/css" media="print" />
        <link rel="stylesheet" href="<c:url value="/css/compiled/calendar.css"/>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<c:url value="/css/libs/morris.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/daterangepicker.css" />" type="text/css" />
        <link rel="stylesheet" href="<c:url value="/css/libs/jquery-jvectormap-1.2.2.css" />" type="text/css" />
	
        <!-- Favicon -->
        <link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>

        <!-- google font libraries -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
                <script src="js/html5shiv.js"></script>
                <script src="js/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon -->
        <link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>

        <!-- google font libraries -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>
            <!--[if lt IE 9]>
                    <script src="js/html5shiv.js"></script>
                    <script src="js/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <%--<jsp:include page="/WEB-INF/views/includes/navigation.jsp" />--%>
        <div id="page-wrapper" class="container">
            <div class="row">
                <%--<jsp:include page="/WEB-INF/views/includes/sidebar.jsp" />--%>
                <div id="content-wrapper" style="min-height: 0px !important;">
                    <div class="row" style="margin-top: 5%">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="center-block" style="width: 50%">
                                    <c:url var="createrole" value="/security/role/creategroup" />
                                    <form:form commandName="roleModel" action="${createrole}" method="POST">
<!--                                        <header></header>-->
                                        <div class="main-box">
                                            <div class="main-box-body clearfix">
                                                <h1 style="text-align: center; padding: 5% 5% 0 0;">Create New Group</h1>
                                                <c:if test="${message != null && !message.isEmpty}">
                                                    <div class="alert alert-warning">
                                                        <c:out value="${message}" />
                                                    </div>
                                                </c:if>
                                                <c:if test="${success != null && !message.isEmpty}">
                                                    <div class="alert alert-success">
                                                        <c:out value="${success}" />
                                                    </div>
                                                </c:if>
                                                <div class="form-group">
                                                    <label class="control-label" for="name">Role Group Name</label>
                                                    <form:input class="form-control" id="name" name="name" path="name" type="text" required="true"/>
                                                    <form:errors path="name" cssStyle="color : red;"/>        
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label" for="description">Description</label>
                                                    <form:textarea class="form-control" path="description" required="true"/>
                                                    <form:errors path="description" cssStyle="color : red;"/>        
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="control-label" for="groupRoles">Choose Roles</label>
                                                    <ul class="role-list">
                                                        <form:checkboxes element="li" cssClass="checkbox-nice" id="groupRoles" path="groupRoles" items="${allroles}" />
                                                    </ul>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12" id="submit-pane">
                                                        <button type="submit" class="btn btn-success col-xs-12">Create</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/><br/>
                                        </div>
                                    </div>
                                </form:form>
                            </div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- global scripts -->
    
    <jsp:include page="../../views/includes/js.jsp" />
    <!-- this page specific inline scripts -->
    <script>
        $(function($) {
            //tooltip init
            $('#exampleTooltip').tooltip();

            //nice select boxes
            $('#sel2').select2();

            $('#sel2Multi').select2({
                placeholder: 'Select a Country',
                allowClear: true
            });

            //masked inputs
            $("#maskedDate").mask("99/99/9999");
            $("#maskedPhone").mask("(999) 999-9999");
            $("#maskedPhoneExt").mask("(999) 999-9999? x99999");
            $("#maskedTax").mask("99-9999999");
            $("#maskedSsn").mask("999-99-9999");

            $("#maskedProductKey").mask("a*-999-a999", {placeholder: " ", completed: function() {
                    alert("You typed the following: " + this.val());
                }});

            $.mask.definitions['~'] = '[+-]';
            $("#maskedEye").mask("~9.99 ~9.99 999");

            //datepicker
            $('#datepickerDate').datepicker({
                format: 'mm-dd-yyyy'
            });

            $('#datepickerDateComponent').datepicker();

            //daterange picker
            $('#datepickerDateRange').daterangepicker();

            //timepicker
            $('#timepicker').timepicker({
                minuteStep: 5,
                showSeconds: true,
                showMeridian: false,
                disableFocus: false,
                showWidget: true
            }).focus(function() {
                $(this).next().trigger('click');
            });

            //autocomplete simple
            $('#exampleAutocompleteSimple').typeahead({
                prefetch: <c:url value= "/data/countries.json" />,
                limit: 10
            });

            //autocomplete with templating
            $('#exampleAutocomplete').typeahead({
                name: 'twitter-oss',
                prefetch: <c:url value="/data/repos.json" />,
                template: [
                    '<p class="repo-language">{{language}} < /p>',
                    '<p class="repo-name">{{name}}</p>',
                    '<p class="repo-description">{{description}}</p>'
                ].join(''),
                engine: Hogan
            });
            //password strength meter
            $('#examplePwdMeter').pwstrength({
                label: '.pwdstrength-label'
            });
        });
        
        $("#permissions").hide();
        
        
        $("#role").change(function(){
           var value = $(this).val();
           
           if(value === "Dynamic"){
               $("#permissions").show(600);
           }
           else{
               $("#permissions").hide();
           }
        });
        
    </script>

</body>
</html>