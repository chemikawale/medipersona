<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">


        <title>Home- Merchant</title>

    <!-- Bootstrap Core CSS -->
        <jsp:include page="/WEB-INF/views/includes/styles.jsp" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <jsp:include page="/WEB-INF/views/includes/navigation.jsp" />
        
        <div id="page-wrapper" class="container">
            <div class="row">
                <jsp:include page="/WEB-INF/sidebar.jsp" />
                <div id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="clearfix">
                                        <h1 class="pull-left">Staff</h1>
                                        <div class="pull-right top-page-ui">
                                            <a href="<c:url value="/users/staff/create" />" class="btn btn-primary pull-right">
                                                <i class="fa fa-plus-circle fa-lg"></i> Add Staff
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-box no-header clearfix">
                                        <div class="main-box-body clearfix">
                                            <div class="table-responsive">
                                                <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                                                <table class="table user-list">
                                                    <thead>
                                                        <tr>
                                                            <th><span>User / Email</span></th>
                                                            <th><span>Full Name</span></th>
                                                            <th><span>Created</span></th>
                                                            <th class="text-center"><span>Status</span></th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                        <tbody>
                                                           <c:forEach items="${allstaff}" var="staff"> 
                                                            <tr>
                                                                <td>
                                                                    <a href="<c:url value="/users/staff/edit?username=${staff.email}" />" class="user-link">
                                                                        ${staff.email}
                                                                    </a>
                                                                    <!--                                                                        <span class="user-subhead">Admin</span>-->
                                                                </td>
                                                                <td>
                                                                    <c:out value="${staff.fullName}" />
                                                                </td>
                                                                <td>
                                                                    <c:out value="${staff.createdOn}" />
                                                                </td>
                                                                <td class="text-center">
                                                                    <span class="label label-default">Active</span>
                                                                </td>

                                                                <td style="width: 20%;">
                                                                   
                                                                    <a href="<c:url value="/users/staff/edit?username=${staff.email}"/>" class="table-link">
                                                                        <span class="fa-stack">
                                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                                            <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                                        </span>
                                                                    </a>
                                                                    
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../includes/js.jsp" />
        
        <script>
            $('.user-list').dataTable();
        </script>
</body>
</html>