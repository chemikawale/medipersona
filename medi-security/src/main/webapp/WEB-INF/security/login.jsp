<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <title>NIMC  - LOGIN</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->

    <link href="<c:url value="/css/basic.css"/>" type="text/css"  rel="stylesheet" />
    <link href="<c:url value="/css/general.css"/>" type="text/css"  rel="stylesheet" />
    <link href="<c:url value="/css/theme.css"/>" rel="stylesheet" type="text/css"   class="style-theme"/>
 
    <script src="<c:url value="/js/basic/respond.min.js" />" />
     <script src="<c:url value="/js/html5shiv.min.js" />" />
  
  </head>
  <body class="login-bg">
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"><img src="/img/logo.png" alt="Proteus"></h1>
          <!--Login Form-->
          <form:form id="loginForm" role="form" commandName="loginViewModel"  action="j_spring_security_check" class="login-form">
                          <c:choose>
                           <c:when test="${message !=null && !message.isEmpty()}">
                                   <div class="alert alert-warning">                                                
                                                ${message}
                                           </div>
                                  </c:when>
                            </c:choose>
            <div class="form-group">
              <form:input id="loginEmail" type="email" path="username" name="username" placeholder="Username" class="form-control" />
            </div>
            <div class="form-group">
              <form:input id="loginPassword" type="password" name="password" path="password" placeholder="Password" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-dark btn-block btn-login">Sign In</button>
           
            <div class="login-options"><a href="${pageContext.request.contextPath}/forgetpassword"  class="fl">FORGOT PASSWORD ?</a><a href="page-register.html" class="fr">SIGN UP</a></div>
          </form:form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="js/basic/jquery.min.js"></script>
    <script src="js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="js/basic/modernizr.min.js"></script>
    <script src="js/basic/bootstrap.min.js"></script>
    <script src="js/shared/jquery.asonWidget.js"></script>
    <script src="js/plugins/plugins.js"></script>
    <script src="js/general.js"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
      <script src="<c:url value="js/plugins/pageprogressbar/pace.min.js" />" />
 
    <!-- Specific-->
       <script src="js/plugins/forms/validation/jquery.validate.additional.min.js"></script>
          <script src="js/plugins/forms/validation/jquery.validate.min.js"></script>
             <script src="js/calls/page.login.js"></script>
             
    <script type="text/javascript">
      (function(i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function() {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-31818910-9', 'auto');
      ga('send', 'pageview');
      
    </script>
    <script>
      (function(f,b){
      	var c;
      	f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
      	f._hjSettings={hjid:46109, hjsv:4};
      	c=b.createElement("script");c.async=1;
      	c.src="//static.hotjar.com/c/hotjar-"+f._hjSettings.hjid+".js?sv="+f._hjSettings.hjsv;
      	b.getElementsByTagName("head")[0].appendChild(c);
      })(window,document);
    </script>
  </body>
</html>