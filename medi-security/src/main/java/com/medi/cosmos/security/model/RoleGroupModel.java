/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.model;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Adeyemi
 */
public class RoleGroupModel extends RoleGroup{
    
    private String[] groupRoles = new String[]{};

    /**
     * @return the groupRoles
     */
    public String[] getGroupRoles() {
        return groupRoles;
    }

    /**
     * @param groupRoles the groupRoles to set
     */
    public void setGroupRoles(String[] groupRoles) {
        this.groupRoles = groupRoles;
    }
    
    public RoleGroup getRoleGroup(){
        
        RoleGroup group = new RoleGroup();
        group.setName("ROLE_"+getName().trim().toUpperCase().replaceAll(" ", "_"));
        group.setCreatedOn(new Date());
        group.setDescription(getDescription());
        
        //List<Role> roleLists = Arrays.asList(roleGroupModel.getGroupRoles()).stream().map(x -> cosmosRoleService.getRole(x)).collect(Collectors.toList());
        
        return group;
    }
    
    public static RoleGroupModel fromRoleGroup(RoleGroup rg){
        
        if(rg == null){
            return null;
        }
        
        RoleGroupModel model = new RoleGroupModel();
        model.setName(rg.getName().trim().replaceAll("ROLE_", "").replaceAll("_", " ").trim());
        model.setId(rg.getId());
        model.setCreatedOn(rg.getCreatedOn());
        model.setDescription(rg.getDescription());
        
        List<String> groupRoles = rg.getRoles().stream().map( x -> x.getName()).collect(Collectors.toList());
        
        String[] roleList = new String[groupRoles.size()];
        
        int counter = 0;
        
        for(String s : groupRoles){
            roleList[counter++] = s;
        }
        
        model.setGroupRoles(roleList);
        
        return model;
    }
}
