/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author tosin.eniolorunda
 *
 * Uses ProviderManager
 * //http://docs.spring.io/spring-security/site/docs/3.0.x/apidocs/org/springframework/security/authentication/ProviderManager.html
 */
public class AggregatingAuthenticationManager extends ProviderManager implements CosmosAuthenticationProviderManager{

    @Autowired
    public AggregatingAuthenticationManager(List<AuthenticationProvider> providers) {
        super(providers);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return super.authenticate(authentication);
    }
}
