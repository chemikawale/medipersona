/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.model;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Adeyemi
 */
public class RequestHasher {
    
    private static final String UTF_8 = "UTF-8";
    private static final String MD5 = "MD5";
    private Date timeStamp;
    private String url;
    private byte[] hash;
    
    public RequestHasher(HttpServletRequest request) throws UnsupportedEncodingException, NoSuchAlgorithmException{
        
        this.url = request.getRequestURL().toString();
        
        StringBuilder builder = new StringBuilder();
        Iterator<Map.Entry<String, String[]>> it = request.getParameterMap().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String[]> pairs = it.next();
            builder.append(pairs.getKey());
            builder.append(Arrays.toString(pairs.getValue()));
        }
        
        this.hash = MessageDigest.getInstance(MD5).digest(builder.toString().getBytes(UTF_8));
    }

    /**
     * @return the timeStamp
     */
    public Date getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the timeStamp to set
     */
    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the hash
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(byte[] hash) {
        this.hash = hash;
    }
    
}
