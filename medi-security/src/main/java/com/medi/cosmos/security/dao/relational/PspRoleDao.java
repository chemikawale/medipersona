/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational;

import com.medi.cosmos.core.dao.relational.BeanPropertySqlParameterSourceFactory;
import com.medi.cosmos.core.dao.relational.PspBaseDao;
import com.medi.cosmos.core.dao.relational.RowCountMapper;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.model.Role;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tosineniolorunda
 */
@Repository
public class PspRoleDao extends PspBaseDao<Role> implements RoleDao {

//    private final String FIND_RESULT_NAME = "cosmos_role";
//    private final String FIND_RESULT_SET_NAME = "cosmos_roles";
    private SimpleJdbcCall pspFindByKey;
    
    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SimpleJdbcCall pspCreate = new SimpleJdbcCall(dataSource).withProcedureName("create_core_role");
        SimpleJdbcCall pspUpdate = new SimpleJdbcCall(jdbcTemplate).withProcedureName("update_core_role");
        SimpleJdbcCall pspDelete = new SimpleJdbcCall(jdbcTemplate).withProcedureName("delete_core_role_by_id");
        BeanPropertyRowMapper rowMapper = new BeanPropertyRowMapper(Role.class);
        rowMapper.setPrimitivesDefaultedForNullValue(true);
        SimpleJdbcCall pspFind = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_core_role_by_id").returningResultSet(FIND_RESULT_NAME, rowMapper);
        pspFindByKey = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_core_role_by_name").returningResultSet(FIND_RESULT_NAME, rowMapper);
        SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_all_core_role").returningResultSet(FIND_RESULT_SET_NAME, rowMapper);
        super.init(pspCreate, pspDelete, pspFind, pspFindAll, null, pspUpdate, new BeanPropertySqlParameterSourceFactory(), jdbcTemplate);
    }
    
     @Override
    public Role findByName(String name) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("name", name);
            Map<String, Object> m = pspFindByKey.execute(in);
            if (m == null) {
                return null;
            }
            List<Role> list = (List<Role>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by key failed", ex);
        }
    }


}