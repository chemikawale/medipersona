/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.model;

import com.medi.cosmos.core.logging.LogManager;
import com.medi.cosmos.core.model.IHaveId;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tosineniolorunda
 */
public class RoleGroup implements IHaveId, Serializable {

    private long id;
    private Date createdOn = new Date();
    private String name;
    private String description;
    private final Map<String, Role> roles = new HashMap<>();
    private static final Logger logger = LogManager.getLogger(RoleGroup.class);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Role> getRoles() {
        List<Role> roleList = new ArrayList<>();
        roleList.addAll(roles.values());
        return roleList;
    }

    public void addRole(Role role) {

        roles.putIfAbsent(role.getName(), role);
    }

    public void removeRole(Role role) {
        roles.remove(role.getName());
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public static String buildIdString(List<RoleGroup> roleGroups) {
        if (roleGroups == null || roleGroups.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        
        for (RoleGroup roleGroup : roleGroups) {
            sb.append(roleGroup.getId());
            sb.append(",");
        }        
        
        String ret = sb.substring(0, sb.length() - 1);
        logger.log(Level.INFO, "Inserted row group is {0}", ret);
        return ret;
    }

}
