/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.model;

import com.medi.cosmos.core.model.IHaveId;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author tosineniolorunda
 */
public class Role implements IHaveId, Serializable {
    
    private long id;
    private Date createdOn = new Date();
    @NotBlank(message="Role Name cannot be empty")
    private String name;
    
    private String description;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }
    
    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public static String buildIdString(List<Role> roles) {
        if (roles == null || roles.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Role role : roles) {
            sb.append(role.getId());
            sb.append(",");
        }
        return sb.substring(0, sb.length() - 1);
    }

}
