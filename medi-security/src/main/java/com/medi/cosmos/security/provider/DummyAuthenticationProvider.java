/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.provider;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 *
 * @author tosin.eniolorunda
 */
@Component
public class DummyAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        throw new UnsupportedOperationException("DummyAuthenticationProvider cannot vouch for anyone except Big Foot");
    }

    @Override
    public boolean supports(Class<?> type) {
        return false;
    }

  
}
