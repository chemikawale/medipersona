/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.provider;

import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author tosineniolorunda
 */
@Component
public class CosmosAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private CosmosUserDao cosmosUserDao;
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    public String saltPassword(String password){
        return passwordEncoder.encode(password);
    }

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        String name = a.getName();
        String password = a.getCredentials().toString();
        CosmosUser user;
        try {
            user = cosmosUserDao.findByUserName(name);
        } catch (CosmosDatabaseException ex) {
            throw new AuthenticationServiceException("There was a problem while checking cosmos user database", ex);
        }
        if (user == null){
            return null;
        }
        
        if(user.isEnabled() == false){
            throw new AuthenticationException("Account has been Deactivated") {};
        }
        
        if (passwordEncoder.matches(password, user.getPassword())){
             return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        }
        throw new BadCredentialsException("Wrong password");
    }

    @Override
    public boolean supports(Class<?> type) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(type);
    }

}
