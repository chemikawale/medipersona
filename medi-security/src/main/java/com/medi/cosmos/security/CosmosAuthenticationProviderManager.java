/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security;

import org.springframework.security.authentication.AuthenticationManager;

/**
 *
 * @author tosineniolorunda
 * Allows the specialization for cosmos Spring's authentication manager
 * for auto wiring since there can be many in the class path
 */
public interface CosmosAuthenticationProviderManager extends AuthenticationManager {
    
}
