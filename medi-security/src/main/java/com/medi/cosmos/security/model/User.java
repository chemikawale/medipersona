/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.model;

import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author tosin.eniolorunda
 */
public interface User extends UserDetails {

    public String getEmail();

    public void setEmail(String email);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);
    
    public String getMiddleName();    
    
    public void setMiddleName(String middleName);

    public void setPassword(String password);

    public String getPhoneNumber();

    public void setUsername(String username);
   
   
}
