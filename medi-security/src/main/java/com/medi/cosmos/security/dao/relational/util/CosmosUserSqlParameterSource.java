/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational.util;

import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.sql.Types;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

/**
 *
 * @author tosineniolorunda
 */
public class CosmosUserSqlParameterSource extends BeanPropertySqlParameterSource {

    private final CosmosUser cosmosUser;


    public CosmosUserSqlParameterSource(CosmosUser cosmosUser) {
        super(cosmosUser);
        this.cosmosUser = cosmosUser;
    }

    @Override
    public boolean hasValue(String string) { 
        if ("roles".equals(string.toLowerCase()) || "rolegroups".equals(string.toLowerCase())) {
            return true;
        }
        return super.hasValue(string);
    }

    @Override
    public Object getValue(String string) throws IllegalArgumentException {
        if ("roles".equals(string.toLowerCase())) {
            return Role.buildIdString(cosmosUser.getRoles());
        }
        if ("rolegroups".equals(string.toLowerCase())) {
            return RoleGroup.buildIdString(cosmosUser.getRoleGroups());
        }
        return super.getValue(string);
    }

    @Override
    public int getSqlType(String string) {
        if ("roles".equals(string) || "rolegroups".equals(string)) {
            return Types.VARCHAR;
        }
        return super.getSqlType(string);
    }

    @Override
    public String getTypeName(String string) {
        return super.getTypeName(string);
    }

}
