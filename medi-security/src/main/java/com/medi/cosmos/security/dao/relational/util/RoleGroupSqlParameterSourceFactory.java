/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.dao.relational.util;


import com.medi.cosmos.core.dao.relational.SqlParameterSourceFactory;
import com.medi.cosmos.security.model.RoleGroup;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author tosineniolorunda
 */
public class RoleGroupSqlParameterSourceFactory implements SqlParameterSourceFactory<RoleGroup>{

    @Override
    public SqlParameterSource getSqlParameterSource(RoleGroup model) {
        return new RoleGroupSqlParameterSource(model);
    }
    
}
