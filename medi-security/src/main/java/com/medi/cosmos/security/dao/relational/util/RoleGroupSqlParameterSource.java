/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational.util;

import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.sql.Types;
import java.util.List;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

/**
 *
 * @author tosineniolorunda
 */
public class RoleGroupSqlParameterSource extends BeanPropertySqlParameterSource {

    private final RoleGroup roleGroup;

    public RoleGroupSqlParameterSource(RoleGroup roleGroup) {
        super(roleGroup);
        this.roleGroup = roleGroup;
    }

    @Override
    public boolean hasValue(String string) {
        if ("roleids".equals(string)) {
            return true;
        }
        return super.hasValue(string);
    }

    @Override
    public Object getValue(String string) throws IllegalArgumentException {
        if ("roleids".equals(string)) {
            List<Role> roles = roleGroup.getRoles();
            if (roles == null || roles.isEmpty()) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            for (Role role : roles) {
                sb.append(role.getId());
                sb.append(",");
            }
            return sb.substring(0, sb.length() - 1);
        }
        return super.getValue(string);
    }

    @Override
    public int getSqlType(String string) {
        if ("roleids".equals(string)) {
            return Types.VARCHAR;
        }
        return super.getSqlType(string);
    }

    @Override
    public String getTypeName(String string) {
        return super.getTypeName(string);
    }

}
