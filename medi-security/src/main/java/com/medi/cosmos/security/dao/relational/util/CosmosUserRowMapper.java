/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational.util;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.logging.LogManager;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.dao.RoleGroupDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author tosineniolorunda
 */
public class CosmosUserRowMapper implements RowMapper<CosmosUser> {

    private final BeanPropertyRowMapper<CosmosUser> rowmrMapper = BeanPropertyRowMapper.newInstance(CosmosUser.class);
    private final RoleDao roleDao;
    private final RoleGroupDao roleGroupDao;
    private static final Logger logger = LogManager.getLogger(CosmosUserRowMapper.class);

    public CosmosUserRowMapper(RoleDao roleDao, RoleGroupDao roleGroupDao) {
        this.roleDao = roleDao;
        this.roleGroupDao = roleGroupDao;
    }

    @Override
    public CosmosUser mapRow(ResultSet rs, int i) throws SQLException {
        CosmosUser cosmosUser = rowmrMapper.mapRow(rs, i);
        String stringRoles = rs.getString("roles");
        if (stringRoles != null && !stringRoles.isEmpty()) {
            logger.log(Level.INFO, "Role is{0}", stringRoles);
            String[] sRoles = stringRoles.split(",");
            for (String sRole : sRoles) {
                Long roleId = Long.valueOf(sRole);
                Role role;
                try {
                    role = roleDao.find(roleId);
                    if (role != null) {
                        cosmosUser.getRoles().add(role);
                    }
                } catch (CosmosDatabaseException ex) {
                    Logger.getLogger(CosmosUserRowMapper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        String stringRoleGroups = rs.getString("roleGroups");
        if (stringRoleGroups != null) {
            logger.log(Level.INFO, "Role Groups is {0}", stringRoleGroups);
            String[] sRoleGroups = stringRoleGroups.split(",");
            for (String sRoleGroup : sRoleGroups) {
                Long sRoleGroupId = Long.valueOf(sRoleGroup);
                RoleGroup roleGroup;
                try {
                    roleGroup = roleGroupDao.find(sRoleGroupId);
                    if (roleGroup != null) {
                        cosmosUser.getRoleGroups().add(roleGroup);
                    }
                } catch (CosmosDatabaseException ex) {
                    Logger.getLogger(CosmosUserRowMapper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return cosmosUser;
    }

}
