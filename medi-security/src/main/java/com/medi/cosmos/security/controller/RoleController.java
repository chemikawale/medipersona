/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.instrumentation.SingleSubmit;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import com.medi.cosmos.security.model.RoleGroupModel;
import com.medi.cosmos.security.service.CosmosRoleService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Adeyemi
 */
//@Controller
@RequestMapping(value="/")
@SessionAttributes(value = "baseRolePath")
public class RoleController {
    
    @Autowired
    private CosmosRoleService cosmosRoleService;
    
    @Autowired
    private RoleDao roleDao;
    
    private final String baseRolePath;
    
    /**
     * 
     * @param baseRolePath
     */
    public RoleController(String baseRolePath) {
        this.baseRolePath = baseRolePath;
    }
    
    @RequestMapping(value="/create", method = RequestMethod.GET)
    public String createRole(ModelMap model){
        model.addAttribute("roleModel", new Role());
        setBaseRoleUrl(model);
        return "/create";
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @SingleSubmit
    public String createRole(@ModelAttribute("roleModel") @Valid Role role,BindingResult result, ModelMap model){
        if (result.hasErrors()) {
            model.addAttribute("message", "Please correct invalid fields");
            return "/create";
        }
        
        Role r = role;
        
        setBaseRoleUrl(model);
        r.setName("PERM_"+role.getName().toUpperCase().trim().replaceAll(" ", "_"));
        r.setCreatedOn(new Date());
        
        
        Role roleObject = cosmosRoleService.getRole(r.getName());
        
        if(roleObject != null){
            model.addAttribute("message", "Aww snap! Role already exists");
            return "/create";
        }
        
        Role rol = cosmosRoleService.createRole(r);
        
        if( rol == null){
            model.addAttribute("message", "Aww snap! Role creation failed");
        }
        else{
            model.addAttribute("success", "Role Created Successfully");
            
            r = new Role();
            
            model.addAttribute("roleModel", r);
        }
        
        return "/create";
    }
    
    @RequestMapping(value="/creategroup", method = RequestMethod.GET)
    public String createRoleGroup(ModelMap model){
        
        setBaseRoleUrl(model);
        model.addAttribute("roleModel", new RoleGroupModel());
        
        Map<String, String> allRoles = new HashMap<>();
        cosmosRoleService.getAllRoles().stream().forEach(x -> allRoles.put(x.getName(), trim(x.getName(), "PERM_").replaceAll("_", " ")));
        model.addAttribute("allroles", allRoles);
        
        return "/creategroup";
    }
    
    @RequestMapping(value="/creategroup", method = RequestMethod.POST)
    public String createRoleGroup(@ModelAttribute("roleModel") @Valid RoleGroupModel roleGroupModel,BindingResult result, ModelMap model){
        
        if (result.hasErrors()) {
            model.addAttribute("message", "Please correct invalid fields");
            return "/creategroup";
        }
        
        setBaseRoleUrl(model);
        
        
        RoleGroup rg = roleGroupModel.getRoleGroup();
        
        RoleGroup roleGroup = cosmosRoleService.getRoleGroup(rg.getName());
        
        if(roleGroup != null){
            model.addAttribute("message", "Aww snap! Role Group already exists");
            return "/creategroup";
        }
        
        Stream.of(roleGroupModel.getGroupRoles()).forEach(x -> rg.addRole(cosmosRoleService.getRole(x)));
        
////        rg.setName("ROLE_"+rg.getName().toUpperCase());
////        rg.setCreatedOn(new Date());
//        rg.setDescription(roleGroupModel.getDescription());
        
        RoleGroup rg1 = cosmosRoleService.createRoleGroup(rg);
        
        if(rg1 == null){
            model.addAttribute("message", "Aww snap! Role creation failed");
        }
        else{
            model.addAttribute("success", "Role Group Created Successfully");
            model.addAttribute("roleModel", new RoleGroupModel());
            Map<String, String> allRoles = new HashMap<>();
            cosmosRoleService.getAllRoles().stream().forEach(x -> allRoles.put(x.getName(), trim(x.getName(), "ROLE_").replaceAll("_", " ")));
            model.addAttribute("allroles", allRoles);
        }
        
        return "/creategroup";
    }
    
    @RequestMapping(value = "/all")
    public String allRoles(ModelMap model){
        
        List<Role> allroles = cosmosRoleService.getAllRoles();
        
        model.addAttribute("allroles", allroles);
        
        setBaseRoleUrl(model);
        
        return "/all";
    }
    
    @RequestMapping(value = "/allgroup")
    public String allRoleGroups(ModelMap model){
        
        
        List<RoleGroup> allroles = cosmosRoleService.getAllRoleGroups();
        
        model.addAttribute("allgroups", allroles);
        
        setBaseRoleUrl(model);
        
        return "/allgroup";
    }
    
    private String trim(String object, String param) {
        return object.replaceAll(param,"");
    }
    
    private void setBaseRoleUrl(ModelMap model){
        String path = (String)model.get("baseRolePath");
        
        if (path == null)
            model.addAttribute("baseRolePath", baseRolePath);
    }
    
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editRole(@PathVariable("id") long id,ModelMap model){
        
        try{
            Role role = roleDao.find(id);
            
            String name = role.getName();
            
            name = name.replaceAll("PERM_", "").replaceAll("_", " ").trim();
            
            role.setName(name);
            
            model.addAttribute("roleModel", role);
            
            return "/edit";
        }
        catch(CosmosDatabaseException cde){   
        }
                    
        return "redirect:/"+baseRolePath+"/all";
    }
    
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String editRole(@ModelAttribute("roleModel") @Valid Role role, BindingResult result, ModelMap model, RedirectAttributes redirect){
        
        if(result.hasErrors()){
            //model.addAttribute("error", "Pls check ");
            return "edit";
        }
        
        try{
            
            String name = role.getName();
            name = "PERM_"+name.trim().replaceAll(" ", "_");
            role.setName(name);
            roleDao.update(role);
            redirect.addFlashAttribute("success", "Role has been updated successfully");
            
            return "redirect:/"+baseRolePath+"/all";
            
        }catch(CosmosDatabaseException cde){
            model.addAttribute("error", cde.getMessage());
        }
        
        return "edit";
    }
    
    @RequestMapping(value = "/editgroup/{id}", method = RequestMethod.GET)
    public String editRoleGroup(@PathVariable("id") long rolegroupId, ModelMap modelMap){
        
        if(rolegroupId <= 0L)
            return null;
        
        RoleGroup roleGroup = cosmosRoleService.getRoleGroupById(rolegroupId);
        
        Map<String, String> allRoles = new HashMap<>();
        cosmosRoleService.getAllRoles().stream()
                .forEach(x -> allRoles.put(x.getName(), 
                        trim(x.getName(), "PERM_").replaceAll("_", " ")));
        
        modelMap.addAttribute("allroles", allRoles);
        
        RoleGroupModel roleGroupModel = RoleGroupModel.fromRoleGroup(roleGroup);
        
        modelMap.addAttribute("roleModel", roleGroupModel);
        
        return "editgroup";
    }
    
    @RequestMapping(value = "/editgroup/{id}", method = RequestMethod.POST)
    public String editRoleGroup(@ModelAttribute("roleModel") @Valid RoleGroupModel roleGroupModel,
            BindingResult result, ModelMap model, RedirectAttributes ra){
        
        if (result.hasErrors()) {
            model.addAttribute("message", "Please correct invalid fields");
            return "editgroup";
        }
        
        RoleGroup roleGroup = cosmosRoleService.getRoleGroupById(roleGroupModel.getId());
        
        roleGroup.getRoles().stream().forEach(x -> {
            roleGroup.removeRole(x);
        });
        
        Stream.of(roleGroupModel.getGroupRoles()).forEach(x -> roleGroup.addRole(cosmosRoleService.getRole(x)));
        
        boolean status = cosmosRoleService.updateRoleGroup(roleGroup);
        
        if(! status ){
            model.addAttribute("message", "Unable to update role group");
            return "editgroup";
        }
        
        ra.addFlashAttribute("success", "Role Group was updated successfully");
            
        return "redirect:/"+baseRolePath+"/allgroup";
    }
}
