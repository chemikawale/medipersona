/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational;

import com.medi.cosmos.core.dao.relational.PspBaseDao;
import com.medi.cosmos.core.dao.relational.RowCountMapper;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleGroupDao;
import com.medi.cosmos.security.dao.relational.util.RoleGroupRowMapper;
import com.medi.cosmos.security.dao.relational.util.RoleGroupSqlParameterSourceFactory;
import com.medi.cosmos.security.model.RoleGroup;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tosineniolorunda
 */
@Repository
public class PspRoleGroupDao extends PspBaseDao<RoleGroup> implements RoleGroupDao {

    private SimpleJdbcCall pspFindByName;
    
    @Autowired
    private PspRoleDao roleDao;
    
    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SimpleJdbcCall pspCreate = new SimpleJdbcCall(dataSource).withProcedureName("create_core_role_group");
        SimpleJdbcCall pspUpdate = new SimpleJdbcCall(jdbcTemplate).withProcedureName("update_core_role_group");
        SimpleJdbcCall pspDelete = new SimpleJdbcCall(jdbcTemplate).withProcedureName("delete_core_role_group_by_id");
        RoleGroupRowMapper rowMapper = new RoleGroupRowMapper(roleDao);
        SimpleJdbcCall pspFindById = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_core_role_group_by_id").returningResultSet(FIND_RESULT_NAME, rowMapper);
        pspFindByName = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_core_role_group_by_name").returningResultSet(FIND_RESULT_NAME, rowMapper);
        //SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_core_role_group_by_id").returningResultSet(FIND_RESULT_NAME, rowMapper);
        //SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_all_core_role_group").returningResultSet("count", new RowCountMapper()).returningResultSet(FIND_RESULT_SET_NAME, rowMapper);
        SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate).withProcedureName("find_all_core_role_group").returningResultSet(FIND_RESULT_SET_NAME, rowMapper);
        super.init(pspCreate, pspDelete, pspFindById, pspFindAll, pspFindByName, pspUpdate, new RoleGroupSqlParameterSourceFactory(), jdbcTemplate);
    }

    @Override
    public RoleGroup findByName(String name) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("name", name);
            Map<String, Object> m = pspFindByName.execute(in);
            if (m == null) {
                return null;
            }
            List<RoleGroup> list = (List<RoleGroup>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by key failed", ex);
        }
    }

}
