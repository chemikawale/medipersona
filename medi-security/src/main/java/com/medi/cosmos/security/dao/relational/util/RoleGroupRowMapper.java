/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational.util;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author tosineniolorunda
 */
public class RoleGroupRowMapper implements RowMapper<RoleGroup> {

    private final RoleDao roleDao;
    private final BeanPropertyRowMapper<RoleGroup> beanPropertyRowMapper;

    public RoleGroupRowMapper(RoleDao roleDao) {
        beanPropertyRowMapper = new BeanPropertyRowMapper<>(RoleGroup.class);
        this.roleDao = roleDao;
    }

    @Override
    public RoleGroup mapRow(ResultSet rs, int i) throws SQLException {
        RoleGroup roleGroup = beanPropertyRowMapper.mapRow(rs, i);
        String roleIds = rs.getString("roleIds");
        if (roleIds != null) {
            String[] idList = roleIds.split(",");
            for (String sId : idList) {
                long id = Long.parseLong(sId);
                try {
                    Role role = roleDao.find(id);
                    if (role != null) {
                        roleGroup.addRole(role);
                    }
                } catch (CosmosDatabaseException ex) {
                    Logger.getLogger(RoleGroupRowMapper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return roleGroup;
    }

}
