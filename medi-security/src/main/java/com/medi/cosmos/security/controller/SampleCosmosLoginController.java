/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.controller;


import com.medi.cosmos.security.controller.viewmodel.LoginViewModel;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author tosineniolorunda
 */
@RequestMapping("/")
public class SampleCosmosLoginController {
    
    @Value("${jdbc.username}")
    private String dbUsername;
    @Value("${jdbc.password}")
    private String dbPassword;
    
    private final String loginViewPath;
    private final String loggedInViewPath;
    private final String loginErrorMessage;
    private final CosmosAuthenticationProvider cosmosAuthenticationProvider;

    
    public SampleCosmosLoginController(String loginErrorMessage, String loginViewPath, String loggedInViewPath, CosmosAuthenticationProvider cosmosAuthenticationProvider){
        this.loggedInViewPath = loggedInViewPath;
        this.loginErrorMessage= loginErrorMessage;
        this.loginViewPath= loginViewPath;
        this.cosmosAuthenticationProvider = cosmosAuthenticationProvider;
    }
    
    public SampleCosmosLoginController(CosmosAuthenticationProvider cosmosAuthenticationProvider){
        this.loggedInViewPath ="/index";
        this.loginErrorMessage = "Invalid Username/Password";
        this.loginViewPath = "/login";
        this.cosmosAuthenticationProvider = cosmosAuthenticationProvider;
        
//        Logger.getLogger(SampleCosmosLoginController.class.getName()).log(Level.WARNING, "DBUsername: "+ dbUsername);
//        Logger.getLogger(SampleCosmosLoginController.class.getName()).log(Level.WARNING, "DBpassword: "+ dbPassword);
//        Logger.getLogger(SampleCosmosLoginController.class.getName()).log(Level.WARNING, "DBAlias: "+ keyAlias
    }
    
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, @RequestParam(value = "loginError", defaultValue = "false", required = false) boolean showLoginError){
        model.addAttribute("loginViewModel", new LoginViewModel());
        if (showLoginError){
            model.addAttribute("message", loginErrorMessage);
        }
        
//        Logger.getLogger(SampleCosmosLoginController.class.getName()).log(Level.WARNING, "DBUsername: "+ dbUsername);
//        Logger.getLogger(SampleCosmosLoginController.class.getName()).log(Level.WARNING, "DBpassword: "+ dbPassword);
        
        return loginViewPath;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute("loginViewModel") @Valid LoginViewModel loginViewModel, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("message", "Please correct invalid fields");
            return loggedInViewPath;
        }
        Authentication auth = cosmosAuthenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(loginViewModel.getUsername(), loginViewModel.getPassword()));
        
        if (auth == null) {
            return "redirect:/login?loginError=true";
        }
        
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.getAuthentication();
        
        return loggedInViewPath;
    }
    
}
