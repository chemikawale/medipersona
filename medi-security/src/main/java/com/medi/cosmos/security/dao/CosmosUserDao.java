/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao;

import com.medi.cosmos.core.dao.Dao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.model.CosmosUser;

/**
 *
 * @author tosineniolorunda
 */
public interface CosmosUserDao extends Dao<CosmosUser> {

    public CosmosUser findByUserName(String userName) throws CosmosDatabaseException;

    public CosmosUser findByEmail(String email) throws CosmosDatabaseException;
}
