/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.instrumentation;

import com.medi.cosmos.security.model.RequestHasher;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionCreatedEvent;
import org.springframework.stereotype.Service;

/**
 *
 * @author Adeyemi
 */
@Service
public class SessionCreatedListener implements ApplicationListener<HttpSessionCreatedEvent> {

//    @Autowired
//    private SessionRegistry sessionRegistry;
    
    private final static String TOKEN_KEY = "_sessioN_ToKen_";
    
    private final static int NO_OF_TOKEN = 10;
    
    @Override
    public void onApplicationEvent(HttpSessionCreatedEvent e) {
        e.getSession().setAttribute(TOKEN_KEY, new LinkedHashMap<String, RequestHasher>(){

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, RequestHasher> eldest) {
                return size() > NO_OF_TOKEN;
            }
        });
    }
    
    public static String getTokenKey(){
        return TOKEN_KEY;
    }
    
}
