/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational.util;

import com.medi.cosmos.core.dao.relational.SqlParameterSourceFactory;
import com.medi.cosmos.security.model.CosmosUser;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author tosineniolorunda
 */
public class CosmosUserSqlParameterSourceFactory implements SqlParameterSourceFactory<CosmosUser> {

    @Override
    public SqlParameterSource getSqlParameterSource(CosmosUser model) {
        return new CosmosUserSqlParameterSource(model);
    }

}
