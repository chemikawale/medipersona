/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao.relational;

import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.core.dao.relational.PspBaseDao;
import com.medi.cosmos.core.dao.relational.RowCountMapper;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.dao.RoleGroupDao;
import com.medi.cosmos.security.dao.relational.util.CosmosUserRowMapper;
import com.medi.cosmos.security.dao.relational.util.CosmosUserSqlParameterSourceFactory;
import com.medi.cosmos.security.model.CosmosUser;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tosineniolorunda
 */
@Repository
public class PspCosmosUserDao extends PspBaseDao<CosmosUser> implements CosmosUserDao {

    private SimpleJdbcCall pspFindUserByEmail;
    private SimpleJdbcCall pspFindUserByUsername,pspUpdateCosmoUser;
    
//    private final String FIND_RESULT_NAME = "cosmos_user";
//    private final String FIND_RESULT_SET_NAME = "cosmos_user_result";
    
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private RoleGroupDao roleGroupDao;

   
    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplateObject = new JdbcTemplate(dataSource);
        SimpleJdbcCall pspCreateCall = new SimpleJdbcCall(dataSource).withProcedureName("create_cosmos_user");
        SimpleJdbcCall pspUpdateCall = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("update_cosmos_user");
        SimpleJdbcCall pspDeleteCall = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("delete_cosmos_user_by_id");
        RowMapper rowMapper = new CosmosUserRowMapper(roleDao, roleGroupDao);
       // pspUpdateCosmoUser= new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("update_cosmos_user");
        pspFindUserByEmail = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("find_cosmos_user_by_email").returningResultSet(FIND_RESULT_NAME, rowMapper);
        pspFindUserByUsername = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("find_cosmos_user_by_username").returningResultSet(FIND_RESULT_NAME, rowMapper);
        SimpleJdbcCall pspFindCall = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("find_cosmos_user_by_id").returningResultSet(FIND_RESULT_NAME, rowMapper);
        SimpleJdbcCall pspFindAllCall = new SimpleJdbcCall(jdbcTemplateObject).withProcedureName("find_all_cosmos_user").returningResultSet(FIND_RESULT_SET_NAME, rowMapper);
        super.init(pspCreateCall, pspDeleteCall, pspFindCall, pspFindAllCall, null, pspUpdateCall, new CosmosUserSqlParameterSourceFactory(), jdbcTemplateObject);
    }


    @Override
    public CosmosUser findByUserName(String userName) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("username", userName);
            Map<String, Object> m = pspFindUserByUsername.execute(in);
            if (m == null) {
                return null;
            }
            List<CosmosUser> list = (List<CosmosUser>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by id failed", ex);
        }
    }

    @Override
    public CosmosUser findByEmail(String email) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("email", email);
            Map<String, Object> m = pspFindUserByEmail.execute(in);
            if (m == null) {
                return null;
            }
            List<CosmosUser> list = (List<CosmosUser>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by id failed", ex);
        }
    }

}
