/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.model;

/**
 *
 * @author tosineniolorunda
 */
public class RoleConstants {

    public static String ROLE_HOSPITAL_CLERK = "ROLE_HOSPITAL_CLERK";
    public static String ROLE_HOSPITAL_CUSTOMER = "ROLE_HOSPITAL_CUSTOMER";
    public static String ROLE_HOSPITAL_SUPERVISOR = "ROLE_HOSPITAL_SUPERVISOR";
    public static String ROLE_HOSPITAL_MANAGER = "ROLE_HOSPITAL_MANAGER";
     public static String ROLE_HOSPITAL_ADMIN = "ROLE_HOSPITAL_ADMIN";

    public final static String[] customer = {"HOSPITAL_CUSTOMER"};

    public final static String[] clerk = {"CREATE_CUSTOMER","HOSPITAL_MANAGER", "AUTH_CUSTOMER", "EDIT_CUSTOMER",
        "CREATE_TRANSACTION", "INIT_TRANSACTION", "LINK_CARDS"};

    public final static String[] supervisor = {"CREATE_STAFF","HOSPITAL_MANAGER", "AUTH_STAFF",
        "CREATE_CUSTOMER", "AUTH_CUSTOMER", "EDIT_STAFF", "EDIT_CUSTOMER",
        "LINK_CARDS", "EDIT_TRANSACTION", "INIT_TRANSACTION"};

    public final static String[] manager = {"CREATE_STAFF","HOSPITAL_MANAGER", "AUTH_STAFF",
        "CREATE_CUSTOMER", "AUTH_CUSTOMER", "EDIT_STAFF", "EDIT_CUSTOMER",
        "LINK_CARDS", "VIEW_TRANSACTION", "EDIT_TRANSACTION", "AUTH_TRANSACTION", "INIT_TRANSACTION"};

    public final static String[] admin = {"CREATE_CARD_BATCH", "HOSPITAL_MANAGER", "UPLOAD_CARD_BATCH"};

}