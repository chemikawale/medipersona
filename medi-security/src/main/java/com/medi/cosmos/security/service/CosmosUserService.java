/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.service;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.logging.LogManager;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.ChangePassword;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.ResetPasswordModel;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author tosineniolorunda
 */
@Component
public class CosmosUserService {

    @Autowired
    private CosmosUserDao cosmosUserDao;

    private static final Logger logger = LogManager.getLogger(CosmosUserService.class);
    
    @Autowired
    private CosmosAuthenticationProvider authenticationProvider;
    
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    
    public boolean createCosmosUser(CosmosUser cosmosUser, boolean  encrypted) {
        try {
            
            String encryptedPassword = null;
            //This is because the password was already salted before was
            //stored in the database before activating the user
            //so salting another time means that we are salting twice
            
            encryptedPassword = cosmosUser.getPassword();
            
            if(encrypted == false)
                 encryptedPassword = authenticationProvider.saltPassword(cosmosUser.getPassword());
            
            cosmosUser.setPassword(encryptedPassword);
            
            cosmosUserDao.create(cosmosUser);
            return true;
        } catch (CosmosDatabaseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateCosmosUser(CosmosUser cosmosUser) {
        try {
            cosmosUserDao.update(cosmosUser);
            return true;
        } catch (CosmosDatabaseException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean changeCosmosUserPassword(ChangePassword changePassword, boolean isPasswordEncrypted){
        try{
            CosmosUser cosmosUser = cosmosUserDao.findByUserName(changePassword.getUsername());
            if(passwordEncoder.matches(changePassword.getOldpassword(), cosmosUser.getPassword())){
                
                String newPassword = changePassword.getNewpassword();
          
                if(! isPasswordEncrypted)
                    newPassword = authenticationProvider.saltPassword(newPassword);
                
                cosmosUser.setPassword(newPassword);
                cosmosUserDao.update(cosmosUser);
                
                return true;
            }
        }
        catch(CosmosDatabaseException ex){
        }
        
        return false;
    }
    
    public boolean resetCosmosUserPassword(ResetPasswordModel passwordModel, boolean isPasswordEncrypted){
        try{
            CosmosUser cosmosUser = cosmosUserDao.findByUserName(passwordModel.getUsername());
            String newPassword = passwordModel.getNewpassword();
            
            if(! isPasswordEncrypted)
                newPassword = authenticationProvider.saltPassword(newPassword);
            
            cosmosUser.setPassword(newPassword);
            
            return cosmosUserDao.update(cosmosUser);
        }
        catch(CosmosDatabaseException ex){
        }
        
        return false;
    }

}
