/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.model;

import com.medi.cosmos.core.model.IHaveId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosUser implements User, IHaveId {

    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private String password;
    private String phoneNumber;
    private boolean enabled;
    private String username;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private long id;
    private Date createdOn;
    private String imageData;
//    private String tempRole;
    private List<Role> roles = new ArrayList<>();
    private final List<RoleGroup> roleGroups = new ArrayList<>();

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getMiddleName() {
        return middleName;
    }

    @Override
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
//        
//        for (String role : getUserRoles()) {
//            grantedAuthorities.add(new SimpleGrantedAuthority(role));
//        }
//        return grantedAuthorities;
//    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
         //grantedAuthorities = new ArrayList<>();
//        for (Role role : getAllRoles()) {
//            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
//        }
        List<GrantedAuthority> grantedAuthorities =  getAllRoles().stream().map(
                x -> new SimpleGrantedAuthority(x)).collect(Collectors.toList());
        
        return grantedAuthorities;
    }
    
    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

//    public List<String> getUserRoles() {
//        List<String> roleMap = new ArrayList<>();
//        if (tempRole.equalsIgnoreCase(RoleConstants.ROLE_HOSPITAL_CLERK)) {
//            return Arrays.asList(RoleConstants.clerk);
//        } else if (tempRole.equalsIgnoreCase(RoleConstants.ROLE_HOSPITAL_CUSTOMER)) {
//            return Arrays.asList(RoleConstants.customer);
//        } else if (tempRole.equalsIgnoreCase(RoleConstants.ROLE_HOSPITAL_MANAGER)) {
//            return Arrays.asList(RoleConstants.manager);
//        } else if (tempRole.equalsIgnoreCase(RoleConstants.ROLE_HOSPITAL_SUPERVISOR)) {
//            return Arrays.asList(RoleConstants.supervisor);
//        } else if (tempRole.equalsIgnoreCase(RoleConstants.ROLE_HOSPITAL_ADMIN)) {
//            return Arrays.asList(RoleConstants.admin);
//        }
//        return roleMap;
//    }
    
    public List<String> getAllRoles() {
        //Map<String, String> roleMap = new HashMap<>();
        
        List<String> roleList = new ArrayList<>();
        
//        roles.stream().forEach((role) -> {
//            roleMap.put(role.getName(), role);
//        });
//        
//        roleGroups.stream().forEach((roleGroup) -> {
//            roleGroup.getRoles().stream().forEach((role) -> {
//                roleMap.put(role.getName(), role);
//            });
//        });
//        
//        List<Role> roleList = new ArrayList<>();
        
//        roles.stream().forEach( x -> { roleList.add(x.getName()); });
        
        roleGroups.stream().forEach(x -> { 
            roleList.add(x.getName()); 
            
            x.getRoles().stream().filter(y -> !roleList.contains(y.getName()))
                    .forEach(y -> {
                        roleList.add(y.getName());
                    });
        });
        
        
        
        //roleList.addAll(roleMap.values());
        
        return roleList;
    }

    public List<Role> getRoles() {
        return roles;
    }

     public void clearRoles() {
         
        roles = new ArrayList<>();
        
    }

    
    public List<RoleGroup> getRoleGroups() {
        return roleGroups;
    }

    public void addRole(Role role) {

        for (Role r : roles) {
            if (r.getName().equalsIgnoreCase(role.getName())) {
                return;
            }
        }

        roles.add(role);
    }

    public void addRoleGroup(RoleGroup roleGroup) {

        for (RoleGroup r : roleGroups) {
            if (r.getName().equalsIgnoreCase(roleGroup.getName())) {
                return;
            }
        }
        
        roleGroups.add(roleGroup);
        
        //This was commented because we found out that if the number of a role group was modified
        // we then have to modify the functionality of every person which is not good enough
//        roleGroup.getRoles().stream().forEach(y -> addRole(y));  
    }
    
    public CosmosUser removeRoleGroup(RoleGroup roleGroup){
        
//        List<Role> groupRoles = roleGroup.getRoles();
//        
//        for(int i = 0; i < groupRoles.size(); i++){
//            
//            for(int j =0; j < roles.size() ; j++){
//                
//               long idval = roles.get(j).getId();
//               
//                if( idval == groupRoles.get(i).getId()){
//                    roles.remove(roles.get(j));
//                }
//            }
//            
//        }
        
        for(int i = 0; i < roleGroups.size(); i++){
            if(roleGroups.get(i).getId() == roleGroup.getId()){
                roleGroups.remove(roleGroups.get(i));
            }
        }
        
        return this;
    }
}
