/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.service;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.RoleDao;
import com.medi.cosmos.security.dao.RoleGroupDao;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Adeyemi
 */
@Component
public class CosmosRoleService {
    
    Logger logger = Logger.getLogger("Role Service");
    @Autowired
    private RoleDao roleDao;
    
    @Autowired
    private RoleGroupDao roleGroupDao;
   
    
    public Role createRole(Role rol){
        try {
            Role role = roleDao.create(rol);
            
            return role;
            
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(CosmosRoleService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public RoleGroup createRoleGroup(RoleGroup roleGroup){
        try{
            RoleGroup rg = roleGroupDao.create(roleGroup);
            
            return rg;
        }catch(CosmosDatabaseException ex){
            
        }
        
        return null;
    }
    
    public List<Role> getAllRoles(){
        
        try{
            
            List<Role>  roles = roleDao.findAll();
            
            return roles;
        }
        catch(CosmosDatabaseException ex){
            
        }
        
        return null;
    }
    
    public List<RoleGroup> getAllRoleGroups(){
        
        try{
            
            List<RoleGroup>  roleGroups = roleGroupDao.findAll();
            
            return roleGroups;
        }
        catch(CosmosDatabaseException ex){
        }
        
        return null;
    }
    
    public Role getRole(String roleName){
        
        try{
            Role role = roleDao.findByName(roleName);
            
            return role;
        }
        catch(CosmosDatabaseException cde){
            logger.log(Level.SEVERE, roleName, "Unable to get the role with name role");
        }
        
        return null;
    }
    
    public RoleGroup getRoleGroup(String roleGroupName){
        
        try{
            RoleGroup roleGroup = roleGroupDao.findByName(roleGroupName);
            
            return roleGroup;
        }
        catch(CosmosDatabaseException cde){
            logger.log(Level.SEVERE, roleGroupName, "Unable to get the role with name role");
        }
        
        return null;
    }
    
    public RoleGroup getRoleGroupById(long roleGroupId){
        
        try{
            RoleGroup roleGroup = roleGroupDao.find(roleGroupId);
            
            return roleGroup;
        }
        catch(CosmosDatabaseException cde){
            logger.log(Level.SEVERE, roleGroupId+"", "Unable to get the role with name role");
        }
        
        return null;
    }
    
    public boolean updateRoleGroup(RoleGroup roleGroup){
        
        try {
//            RoleGroup rg = roleGroupDao.find(roleGroup.getId());
            
            if(roleGroup == null)
                return false;
            
//            rg.setDescription(roleGroup.getDescription());
//            rg.setName(roleGroup.getName());
//            
//            
//            
//            roleGroup.getRoles().stream().forEach((r) -> {
//                rg.addRole(r);
//            });
            
            return roleGroupDao.update(roleGroup);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(CosmosRoleService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
}
