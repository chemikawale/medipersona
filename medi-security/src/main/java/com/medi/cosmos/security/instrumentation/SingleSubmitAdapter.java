/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.instrumentation;

import com.medi.cosmos.security.model.RequestHasher;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author Adeyemi
 */
@Component
public class SingleSubmitAdapter extends HandlerInterceptorAdapter {
    
    private static final String BY_PASS = "GET";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        if(!request.getMethod().equalsIgnoreCase(BY_PASS) && handler != null 
                && handler instanceof HandlerMethod && ((HandlerMethod) handler).getMethodAnnotation(SingleSubmit.class) != null)
        {
            ConcurrentHashMap<String, RequestHasher> hashMap = (ConcurrentHashMap<String, RequestHasher>) request.getSession()
                    .getAttribute(SessionCreatedListener.getTokenKey());
            
            RequestHasher hasher = new RequestHasher(request);
            RequestHasher previousHasher = hashMap.get(request.getRequestURL().toString());
            
            if(previousHasher != null && Arrays.equals(hasher.getHash(), previousHasher.getHash())){
                return false;
            }
            else
                hashMap.put(request.getRequestURL().toString(), hasher);
        }
        
        return super.preHandle(request, response, handler); //To change body of generated methods, choose Tools | Templates.
    }
    
}
