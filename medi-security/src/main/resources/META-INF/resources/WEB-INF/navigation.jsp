
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="authenticated" var="authentication"/> 
<header class="l-header l-header-1 t-header-1">
          <div class="navbar navbar-ason">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#ason-navbar-collapse" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="index-2.html" class="navbar-brand widget-logo"><span class="logo-default-header"><img src="img/logo_dark.png" alt="Proteus"></span></a>
              </div>
              <div id="ason-navbar-collapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                 
               
                 
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li>
                    <!-- Profile Widget-->
                    <div class="widget-profile profile-in-header">
                      <button type="button" data-toggle="dropdown" class="btn dropdown-toggle"><span class="name">Seyi Daniels</span><img src="<c:url value="/img/profile/profile.jpg"/>"> </button>
                      <ul role="menu" class="dropdown-menu">
                        <li><a href="page-profile.html"><i class="fa fa-user"></i>Profile</a></li>                       
                        <li class="power">
                            <a> <c:choose>   
                                        <c:when test="${authentication != null}">
                                            <c:url var="logoutUrl" value="/logout" />
                                            <form action="${logoutUrl}" method="post" id="logout-form">                                               
                                               <i class="fa fa-power-off"></i> <input type="submit" class="btn btn-link" value="Log Out" />
                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                            </form>
                                        </c:when>
                                    </c:choose>
                            </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                 
                </ul>
              </div>
            </div>
          </div>
        </header>