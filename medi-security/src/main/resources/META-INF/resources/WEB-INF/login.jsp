<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
    <title>MediPersona || LOGIN</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link href="<c:url value="/css/basic.css"/>" rel="stylesheet" />
    <link href="<c:url value="/css/general.css"/>" rel="stylesheet" />
    <link href="<c:url value="/css/theme.css"/>" rel="stylesheet"  class="style-theme"/>

 

  
  </head>
  <body class="login-bg">
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"><img src="<c:url value="/img/logo.png"/>" alt="Proteus"></h1>
          <!--Login Form-->
          <form:form id="loginForm" role="form" commandName="loginViewModel"  action="j_spring_security_check" class="login-form">
                          <c:choose>
                           <c:when test="${message !=null && !message.isEmpty()}">
                                   <div class="alert alert-warning">                                                
                                                ${message}
                                           </div>
                                  </c:when>
                            </c:choose>
            <div class="form-group">
              <form:input id="loginEmail" type="text" path="username" name="username" placeholder="Username" class="form-control" />
            </div>
            <div class="form-group">
              <form:input id="loginPassword" type="password" name="password" path="password" placeholder="Password" class="form-control"/>
            </div>
            <button type="submit" class="btn btn-dark btn-block btn-login">Sign In</button>
            <div class="login-social">
              <div class="l-span-md-12">
                <div class="tor"><span>- OR -</span></div>
              </div>
              <div class="l-span-md-12 register-sign-in"><a href="/phr/persona/signup" class="btn btn-primary btn-block btn-login-register">Sign Up</a></div>
              
            </div>
            <div class="login-options"><a href="${pageContext.request.contextPath}/forgotpassword"  class="fl">FORGOT PASSWORD ?</a></div>
          </form:form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
         
    <script src="<c:url value="/js/basic/jquery.min.js"/>" />
    <script src="<c:url value="/js/basic/jquery-migrate.min.js"/>" />
    <!-- General-->
    <script src="<c:url value="/js/basic/jquery-migrate.min.js"/>" />
    <script src="<c:url value="/js/basic/modernizr.min.js"/>" />
    <script src="<c:url value="/js/basic/bootstrap.min.js"/>" />
    <script src="<c:url value="/js/shared/jquery.asonWidget.js"/>" />
    <script src="<c:url value="/js/plugins/plugins.js"/>" />
    <script src="<c:url value="/js/general.js"/>" />
    
     
      <script src="<c:url value="/js/plugins/pageprogressbar/pace.min.js" />" />
 
    <!-- Specific-->
       <script src="<c:url value="/js/plugins/forms/validation/jquery.validate.additional.min.js" />" />
       <script src="<c:url value="/js/plugins/forms/validation/jquery.validate.min.js"/>" />
       <script src="<c:url value="/js/calls/page.login.js"/>" />
             

  </body>
</html>