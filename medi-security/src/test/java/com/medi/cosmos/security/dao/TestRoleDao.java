/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao;

import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.relational.PspRoleDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.Role;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author tosineniolorunda
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
@Component
public class TestRoleDao {
    
    @Autowired
    private PspRoleDao roleDao;
    @Autowired
    private TestUserDao testUserDao;
    @Autowired
    private CosmosUserDao cosmosUserDao;
    
    private Role getTestRole() {
        Role role = new Role();
        role.setDescription("___role_that_Cannot_be replciated");
        role.setName("chuck_norris");
        return role;
    }
    
    public Role createRole() throws CosmosDatabaseException {
        Role role = getTestRole();
        role = roleDao.create(role);
        return role;
    }
    
    @Test
    public void testCreate() throws CosmosDatabaseException {
        Role role = createRole();
        Assert.assertNotNull(role);
        CosmosUser cosmosUser = testUserDao.getTestuser();
        cosmosUser.addRole(role);
        //cosmosUser = cosmosUserDao.create(cosmosUser);
        Assert.assertNotNull(cosmosUser);
        CosmosUser newCosmosUser = cosmosUserDao.find(cosmosUser.getId());
//        Assert.assertEquals(newCosmosUser.getRoles().get(0).getName(), role.getName());
        
    }
    
    @Test
    public void testFindById() throws CosmosDatabaseException {
        Role role = getTestRole();
        role = roleDao.create(role);
        role = roleDao.find(role.getId());
        Assert.assertNotNull(role);
    }
    
    @Test
    public void testRoleUpdate() throws CosmosDatabaseException {
        String updateName = "mrs_chuck_norris";
        Role role = getTestRole();
        role = roleDao.create(role);
        role.setName(updateName);
        Assert.assertTrue(roleDao.update(role));
        role = roleDao.find(role.getId());
        Assert.assertNotNull(role);
        Assert.assertEquals(role.getName(), updateName);
    }
    
    @Test
    public void testRoleFindByName() throws CosmosDatabaseException{
        Role role = getTestRole();
        roleDao.create(role);
        Role role1 = roleDao.findByName(role.getName());
        Assert.assertNotNull(role1);
        
    }
    
    @Test
    public void FindAll() throws CosmosDatabaseException{
        Role role = getTestRole();
        roleDao.create(role);
        List<Role> roles = roleDao.findAll();
        Assert.assertNotNull(roles);
        Assert.assertNotEquals(roles.size(), 0);
    }
    
    @Test
    public void testRoleDelete() throws CosmosDatabaseException {
        Role role = getTestRole();
        role = roleDao.create(role);
        Assert.assertTrue(roleDao.delete(role));
        Assert.assertFalse(roleDao.delete(role));
        role = roleDao.find(role.getId());
        Assert.assertNull(role);
    }
    
    @After
    public void after() {
        JdbcTestUtils.deleteFromTableWhere(roleDao.getJdbcTemplate(), "core_roles", "description = '" + getTestRole().getDescription() + "'");
    }
    
}
