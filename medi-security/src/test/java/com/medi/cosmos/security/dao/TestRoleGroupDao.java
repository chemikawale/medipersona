/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao;

import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.relational.PspRoleGroupDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.Role;
import com.medi.cosmos.security.model.RoleGroup;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author tosineniolorunda
 */
@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class TestRoleGroupDao {

    @Autowired
    private TestRoleDao testRoleDao;
    @Autowired
    private PspRoleGroupDao roleGroupDao;
    @Autowired
    private TestUserDao testUserDao;
    @Autowired
    private CosmosUserDao cosmosUserDao;
    
    @Before
    public void before(){
        testUserDao.deleteTestUser();
        after();
    }

    private RoleGroup getTestRoleGroup(List<Role> roles) {
        RoleGroup roleGroup = new RoleGroup();
        roleGroup.setDescription("__test role group the is bad ass");
        roleGroup.setName("__test_role_group");
        if (roles != null) {
            for (Role role : roles) {
                roleGroup.addRole(role);
            }
        }
        return roleGroup;
    }

    private RoleGroup createRoleGroup() throws CosmosDatabaseException {
        Role role = testRoleDao.createRole();
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        RoleGroup roleGroup = getTestRoleGroup(roles);
        roleGroup = roleGroupDao.create(roleGroup);
        return roleGroup;
    }

    @Test
    public void testCreate() throws CosmosDatabaseException {
        RoleGroup roleGroup = createRoleGroup();
        Assert.assertNotNull(roleGroup);
        CosmosUser cosmosUser = testUserDao.getTestuser();
        cosmosUser.addRoleGroup(roleGroup);
        cosmosUser = cosmosUserDao.create(cosmosUser);
        Assert.assertNotNull(cosmosUser);
        CosmosUser newCosmosUser = cosmosUserDao.find(cosmosUser.getId());
//        Assert.assertEquals(newCosmosUser.getRoleGroups().get(0).getName(), roleGroup.getName());
//        Assert.assertEquals(newCosmosUser.getRoleGroups().get(0).getRoles().get(0).getName(), roleGroup.getRoles().get(0).getName());
    }

    @Test
    public void testFind() throws CosmosDatabaseException {
        RoleGroup roleGroup = createRoleGroup();
        roleGroup = roleGroupDao.find(roleGroup.getId());
        Assert.assertNotNull(roleGroup);
    }

    @Test
    public void testDelete() throws CosmosDatabaseException {
        RoleGroup roleGroup = createRoleGroup();
        Assert.assertTrue(roleGroupDao.delete(roleGroup));
        Assert.assertFalse(roleGroupDao.delete(roleGroup));
    }

    @Test
    public void testUpdate() throws CosmosDatabaseException {
        String updateString = "__thank you Lord__";
        RoleGroup roleGroup = createRoleGroup();
        roleGroup.setDescription(updateString);
        Assert.assertTrue(roleGroupDao.update(roleGroup));
        Assert.assertEquals(updateString, roleGroup.getDescription());
    }
    
    @Test
    public void testRoleFindByName() throws CosmosDatabaseException{
        RoleGroup roleGroup = createRoleGroup();
        RoleGroup roleGroup1 = roleGroupDao.findByName(roleGroup.getName());
        Assert.assertNotNull(roleGroup1);
        Assert.assertEquals(roleGroup.getName(), roleGroup1.getName());
    }
    
    @Test
    public void FindAll() throws CosmosDatabaseException{
        createRoleGroup();
        List<RoleGroup> roleGroups = roleGroupDao.findAll();
        Assert.assertNotNull(roleGroups);
        Assert.assertNotEquals(roleGroups.size(), 0);
    }
    
    @After
    public void after() {
        testUserDao.deleteTestUser();
        testRoleDao.after();
        JdbcTestUtils.deleteFromTableWhere(roleGroupDao.getJdbcTemplate(), "core_role_groups", "name = '" + getTestRoleGroup(null).getName() + "'");
    }
}
