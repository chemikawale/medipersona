/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.security.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.CosmosAuthenticationProviderManager;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import com.medi.cosmos.security.service.CosmosUserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author tosineniolorunda
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class TestUserAuthentication {
    
    @Autowired
    private TestUserDao testUserDao;
    @Autowired
    private CosmosAuthenticationProviderManager cosmosAuthenticationProviderManager;
    @Autowired
    private CosmosAuthenticationProvider cosmosAuthenticationProvider;
    @Autowired
    private CosmosUserService cosmosUserService;
    
    @Before
    public void begin(){
        testUserDao.begin();
    }
    
    @Test
    public void testAuthentication() throws CosmosDatabaseException{
        CosmosUser cosmosUser = testUserDao.getTestuser();
        //cosmosUser.setPassword(cosmosAuthenticationProvider.saltPassword(cosmosUser.getPassword()));
        //cosmosUser.setTempRole(RoleConstants.ROLE_HOSPITAL_MANAGER);
        Assert.assertTrue(cosmosUserService.createCosmosUser(cosmosUser, false));
        Authentication auth = cosmosAuthenticationProviderManager.authenticate(new UsernamePasswordAuthenticationToken(TestUserDao.TEST_USER_NAME, TestUserDao.TEST_CLEAR_PWD));
        Assert.assertNotNull(auth);
    }
    
    @After
    public void tearDown(){
        testUserDao.tearDown();
    }
    
}
