/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.security.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.relational.PspCosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import java.util.List;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author tosineniolorunda
 */
@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class TestUserDao {

    public static final String TEST_USER_NAME = "enmitos";
    public static final String TEST_CLEAR_PWD = "password";

    @Autowired
    private PspCosmosUserDao cosmosUserDao;

    public void deleteTestUser() {
        JdbcTestUtils.deleteFromTableWhere(cosmosUserDao.getJdbcTemplate(), "cosmos_user", "email = '" + getTestuser().getEmail() + "'");
    }

    @Before
    public void begin() {
        deleteTestUser();
    }

    public CosmosUser getTestuser() {
        CosmosUser cosmosUser = new CosmosUser();
        cosmosUser.setAccountNonExpired(true);
        cosmosUser.setAccountNonLocked(true);
        cosmosUser.setEnabled(true);
        cosmosUser.setCredentialsNonExpired(true);
        cosmosUser.setEmail("enmitosatyahoo.co.uk");
        cosmosUser.setFirstName("Tosin");
        cosmosUser.setMiddleName("W0sebolatAn!");
        cosmosUser.setLastName("Eniolorunda");
        cosmosUser.setPassword(TEST_CLEAR_PWD);
        cosmosUser.setPhoneNumber("07059087168");
        cosmosUser.setUsername(TEST_USER_NAME);
        return cosmosUser;
    }

    public CosmosUser createTestUser() throws CosmosDatabaseException {
        CosmosUser testUser = getTestuser();
        return cosmosUserDao.create(testUser);
    }

    @Test
    public void testCreateUser() throws CosmosDatabaseException {
        Assert.assertNotNull(createTestUser());
    }

    @Test
    public void testFindUserByUserName() throws CosmosDatabaseException {
        CosmosUser testUser = getTestuser();
        cosmosUserDao.create(testUser);
        testUser = cosmosUserDao.findByUserName(getTestuser().getUsername());
        Assert.assertNotNull(testUser);
    }

    @Test
    public void testFindUserByEmail() throws CosmosDatabaseException {
        CosmosUser testUser = getTestuser();
        cosmosUserDao.create(testUser);
        testUser = cosmosUserDao.findByEmail(getTestuser().getEmail());
        Assert.assertNotNull(testUser);
    }

    @Test
    public void testFindUserById() throws CosmosDatabaseException {
        System.out.println("find cosmos_user by id");
        CosmosUser testUser = createTestUser();
        System.out.println("expectedid : "+ testUser.getId());
        testUser = cosmosUserDao.find(testUser.getId());
        Assert.assertNotNull(testUser);
    }
    
    @Test
    public void testFindAll() throws CosmosDatabaseException {
        CosmosUser testUser = getTestuser();
        cosmosUserDao.create(testUser);
        List<CosmosUser> cosmosUsers = cosmosUserDao.findAll();
        
        Assert.assertNotNull(cosmosUsers);
        Assert.assertNotSame(cosmosUsers.size(), 0);
    }

    @Test
    public void testDeleteUserById() throws CosmosDatabaseException {
        CosmosUser testUser = getTestuser();
        cosmosUserDao.create(testUser);
        Assert.assertTrue(cosmosUserDao.delete(testUser));
    }

    @After
    public void tearDown() {
        deleteTestUser();
    }

}
