use medipersona;

CREATE TABLE IF NOT EXISTS vital_signs(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    readingId VARCHAR(64) NOT NULL UNIQUE,
    username VARCHAR(64) NOT NULL,    
    diastolic VARCHAR(64),
    systolic VARCHAR(64),    
    temperature VARCHAR(64),
    weight VARCHAR(64),    
    height VARCHAR(64),
    respiration VARCHAR(64),    
    bmi VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL
);



DROP PROCEDURE IF EXISTS medipersona.add_vital_signs;

DELIMITER $$

CREATE PROCEDURE medipersona.add_vital_signs(
	OUT id BIGINT,
        IN readingId VARCHAR(64),
        IN username VARCHAR(64) ,
        IN diastolic VARCHAR(64),
         IN systolic VARCHAR(64),
        IN temperature VARCHAR(64) ,
        IN weight VARCHAR(64),
         IN height VARCHAR(64),
        IN respiration VARCHAR(64) ,
        IN bmi VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.vital_signs
        (
        readingId ,
        username ,
        diastolic ,
        systolic ,
        temperature ,
        weight ,
        height ,
        respiration ,
        bmi ,
        createdOn    
        )
	VALUES
	(
              readingId ,
            username ,
            diastolic ,
            systolic ,
            temperature ,
            weight ,
            height ,
            respiration ,
            bmi ,
            createdOn    
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_vital_signs_by_username;
DELIMITER $$
-- 
 CREATE PROCEDURE medipersona.find_vital_signs_by_username(
 in pageNum int,
 in pageSize int,
 in username varchar(64),
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.vital_signs where vital_signs.username  = username ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.vital_signs where vital_signs.username  = ?  ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.vital_signs where vital_signs.username  = ? ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;

            SET @username = username ;
-- 
-- 
             EXECUTE findStatement USING @username,@pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @username,@pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.get_all_vital_signs;
DELIMITER $$
-- 
 CREATE PROCEDURE medipersona.get_all_vital_signs(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.vital_signs ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.vital_signs ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.vital_signs ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
-- 
-- 
             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;

CREATE TABLE IF NOT EXISTS medipersona.medi_review_request(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL,
    reviewcode VARCHAR(64) NULL UNIQUE,
    email VARCHAR(64),
    phonenumber VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL ,
    expiryDate DATETIME NULL DEFAULT NULL ,
    enabled BIT  ,
    isUsed BIT
    
   );


DROP PROCEDURE IF EXISTS medipersona.log_review_request;

DELIMITER $$

CREATE PROCEDURE medipersona.log_review_request(
	OUT id BIGINT,
        IN username VARCHAR(64) ,
        IN reviewcode VARCHAR(64),
        IN email VARCHAR(64), 
        IN phonenumber VARCHAR(64) ,
        IN createdOn DATETIME,
        IN expiryDate DATETIME,
        IN enabled BIT,
        IN isUsed BIT
        )
BEGIN
	INSERT INTO medipersona.medi_review_request
        (
        username ,
        reviewcode ,
        email,
        phonenumber,
        createdOn,
        expiryDate,
        enabled,
        isUsed
        )
	VALUES
	(
         username ,
        reviewcode ,
        email,
        phonenumber,
        createdOn,
        expiryDate,
        enabled,
        isUsed
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_review_request_by_username;
DELIMITER $$
 CREATE PROCEDURE medipersona.get_review_request_by_username(
 in pageNum int,
 in pageSize int,
 in username varchar(64),
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.medi_review_request where medi_review_request.username = username ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_review_request where medi_review_request.username = ? ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_review_request where medi_review_request.username = ? ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
              SET @username = username ;
-- 
-- 
             EXECUTE findStatement USING @username ,@pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(@pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @username ,@pageSize,@moffSet ;

         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;






DROP PROCEDURE IF EXISTS medipersona.get_active_review_by_code;
DELIMITER $$

CREATE PROCEDURE medipersona.get_active_review_by_code(
    IN reviewcode VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_review_request where medipersona.medi_review_request.reviewcode = reviewcode and enabled = true ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.update_used_review_code;
DELIMITER $$

CREATE PROCEDURE medipersona.update_used_review_code(
    IN reviewCode VARCHAR(64)
)
BEGIN
 UPDATE   medipersona.medi_review_request
 
            SET medi_review_request.isUsed = true

        WHERE medi_review_request.reviewCode = reviewCode;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.deprecate_review_request;
DELIMITER $$

CREATE PROCEDURE medipersona.deprecate_review_request()

BEGIN 

  UPDATE 
        medipersona.medi_review_request

           set 

           medipersona.medi_review_request.enabled = false

           where 
        
            TIMESTAMPDIFF(HOUR,medi_review_request.expiryDate,now()) > '0.1';
    
END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.get_all_review_requests;
DELIMITER $$

 CREATE PROCEDURE medipersona.get_all_review_requests(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.medi_review_request ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_review_request ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_review_request ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
-- 
-- 
             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;




