use medipersona;

 CREATE TABLE IF NOT EXISTS medipersona.blood_details(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL UNIQUE,
    bloodGroup VARCHAR(64),
    rhesusD VARCHAR(64),
    genotype VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL   
   );

DROP PROCEDURE IF EXISTS medipersona.add_blood_details;

DELIMITER $$

CREATE PROCEDURE medipersona.add_blood_details(
	OUT id BIGINT,
        IN username VARCHAR(64) ,
        IN bloodGroup VARCHAR(64),
        IN rhesusD VARCHAR(64),
        IN genotype VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.blood_details
        (
        username ,
        bloodGroup ,
        rhesusD,
        genotype,
        createdOn
        )
	VALUES
	(
        username ,
        bloodGroup ,
        rhesusD,
        genotype,
        createdOn
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.find_blood_details_byUsername;
DELIMITER $$

CREATE PROCEDURE medipersona.find_blood_details_byUsername(
    IN username VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.blood_details where blood_details.username = username ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.delete_blood_details_byUsername;
DELIMITER $$

CREATE PROCEDURE medipersona.delete_blood_details_byUsername(
    IN username VARCHAR(64)
)
BEGIN

DELETE FROM medipersona.blood_details where blood_details.username = username ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_blood_details;
DELIMITER //

CREATE PROCEDURE medipersona.get_all_blood_details(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.blood_details ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.blood_details ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.blood_details ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;


            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;




 CREATE TABLE IF NOT EXISTS medipersona.allergy_categories(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    categoryId VARCHAR(64) NOT NULL UNIQUE,
    categoryName VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_allergy_category;

DELIMITER $$

CREATE PROCEDURE medipersona.add_allergy_category(
	OUT id BIGINT,
        IN categoryId VARCHAR(64) ,
        IN categoryName VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.allergy_categories
        (
        categoryId ,
        categoryName ,
        createdOn  
        )
	VALUES
	(
        categoryId ,
        categoryName ,
        createdOn   
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_allergy_categories;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_allergy_categories()
BEGIN

SELECT * FROM medipersona.allergy_categories order by categoryName ASC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_allergy_category_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_allergy_category_ById(
    IN categoryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.allergy_categories where allergy_categories.categoryId = categoryId ;

END $$

DELIMITER;



CREATE TABLE IF NOT EXISTS medipersona.allergies(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    entryId VARCHAR(64) NOT NULL UNIQUE,
    username VARCHAR(64),
    categoryId VARCHAR(64),
    started VARCHAR(64),
    allergicTo VARCHAR(64),
    reaction VARCHAR(64),
    location VARCHAR(64),
    active BIT,
    severity VARCHAR(64),
    comment TEXT,
    createdOn DATETIME NULL DEFAULT NULL   
   );

DROP PROCEDURE IF EXISTS medipersona.add_user_allergy;

DELIMITER $$

CREATE PROCEDURE medipersona.add_user_allergy(
	OUT id BIGINT,
        IN entryId VARCHAR(64) ,
        IN username VARCHAR(64),
        IN categoryId VARCHAR(64),
        IN started VARCHAR(64),
        IN allergicTo VARCHAR(64) ,
        IN reaction VARCHAR(64),
        IN location VARCHAR(64),
        IN active BIT,
        IN severity VARCHAR(64),
        IN comment TEXT,
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.allergies
        (
        entryId ,
        username ,
        categoryId,
        started,
        allergicTo,
        reaction ,
        location ,
        active,
        severity,
        comment,
        createdOn
        )
	VALUES
	(
       entryId ,
        username ,
        categoryId,
        started,
        allergicTo,
        reaction ,
        location ,
        active,
        severity,
        comment,
        createdOn
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.find_allergies_byUsername;
DELIMITER //

CREATE PROCEDURE medipersona.find_allergies_byUsername(
in pageNum int,
in pageSize int,
in username VARCHAR(64),
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.allergies where allergies.username = username ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.allergies where allergies.username = ? ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.allergies where allergies.username = ? ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;

            SET @username = username ;


            EXECUTE findStatement USING @username, @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        SET @username = username;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @username, @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.find_allergy_byId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_allergy_byId(
    IN entryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.allergies where allergies.entryId = entryId ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.delete_allergy_byId;
DELIMITER $$

CREATE PROCEDURE medipersona.delete_allergy_byId(
    IN entryId VARCHAR(64)
)
BEGIN

DELETE FROM medipersona.allergies where allergies.entryId = entryId ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_allergies;
DELIMITER //

CREATE PROCEDURE medipersona.get_all_allergies(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.allergies ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.allergies ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.allergies ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;


            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;