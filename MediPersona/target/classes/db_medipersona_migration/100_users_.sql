use medipersona;

CREATE TABLE IF NOT EXISTS vital_signs(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    readingId VARCHAR(64) NOT NULL UNIQUE,
    username VARCHAR(64) NOT NULL,    
    diastolic VARCHAR(64),
    systolic VARCHAR(64),    
    temperature VARCHAR(64),
    weight VARCHAR(64),    
    height VARCHAR(64),
    pulse VARCHAR(64),
    respiration VARCHAR(64),    
    bmi VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL
);



DROP PROCEDURE IF EXISTS medipersona.add_vital_signs;

DELIMITER $$

CREATE PROCEDURE medipersona.add_vital_signs(
	OUT id BIGINT,
        IN readingId VARCHAR(64),
        IN username VARCHAR(64) ,
        IN diastolic VARCHAR(64),
        IN systolic VARCHAR(64),
        IN temperature VARCHAR(64) ,
        IN weight VARCHAR(64),
         IN height VARCHAR(64),
         IN pulse VARCHAR(64),
        IN respiration VARCHAR(64) ,
        IN bmi VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.vital_signs
        (
        readingId ,
        username ,
        diastolic ,
        systolic ,
        temperature ,
        weight ,
        height ,
        pulse,
        respiration ,
        bmi ,
        createdOn    
        )
	VALUES
	(
            readingId ,
            username ,
            diastolic ,
            systolic ,
            temperature ,
            weight ,
            height ,
            pulse,
            respiration ,
            bmi ,
            createdOn    
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_vital_signs_by_username;
DELIMITER //

CREATE PROCEDURE medipersona.find_vital_signs_by_username(
In username varchar(64),
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.vital_signs where vital_signs.username = username ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.vital_signs where vital_signs.username = ? ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.vital_signs where vital_signs.username = ? ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;
            SET @username = username ;


            EXECUTE findStatement USING @username,@pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
           SET @username = username ;
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @username, @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.all_hmo_paginated;
DELIMITER //

CREATE PROCEDURE medipersona.all_hmo_paginated(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.medi_hmo ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.medi_hmo ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.medi_hmo ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;


            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;




DROP PROCEDURE IF EXISTS medipersona.get_all_vital_signs;

DELIMITER $$

 CREATE PROCEDURE medipersona.get_all_vital_signs(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;

     select count(*) into counter from medipersona.vital_signs ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.vital_signs ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.vital_signs ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
             SET @pageSize = pageSize ;

             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;

         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;

CREATE TABLE IF NOT EXISTS medipersona.medi_review_request(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(64) NOT NULL,
    reviewcode VARCHAR(64) NULL UNIQUE,
    email VARCHAR(64),
    phonenumber VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL ,
    expiryDate DATETIME NULL DEFAULT NULL ,
    enabled BIT  ,
    isUsed BIT,
    status BIT,
    role VARCHAR(64),
    firstName VARCHAR(64),
    lastName VARCHAR(64)
    
   );


DROP PROCEDURE IF EXISTS medipersona.log_review_request;

DELIMITER $$

CREATE PROCEDURE medipersona.log_review_request(
	OUT id BIGINT,
        IN username VARCHAR(64) ,
        IN reviewcode VARCHAR(64),
        IN email VARCHAR(64), 
        IN phonenumber VARCHAR(64) ,
        IN createdOn DATETIME,
        IN expiryDate DATETIME,
        IN enabled BIT,
        IN isUsed BIT,
        IN status BIT,
        IN role VARCHAR(64),
        IN firstName VARCHAR(64),
        IN lastName VARCHAR(64)
        )
BEGIN
	INSERT INTO medipersona.medi_review_request
        (
        username ,
        reviewcode ,
        email,
        phonenumber,
        createdOn,
        expiryDate,
        enabled,
        isUsed,
        status,
        role,
        firstName,
        lastName
        )
	VALUES
	(
         username ,
        reviewcode ,
        email,
        phonenumber,
        createdOn,
        expiryDate,
        enabled,
        isUsed,
        status,
        role,
        firstName,
        lastName
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_review_request_by_username;
DELIMITER $$
 CREATE PROCEDURE medipersona.get_review_request_by_username(
 in pageNum int,
 in pageSize int,
 in username varchar(64),
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.medi_review_request where medi_review_request.username = username and medi_review_request.enabled = true ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_review_request where medi_review_request.username = ? and medi_review_request.enabled = true ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_review_request where medi_review_request.username = ? and medi_review_request.enabled = true ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
              SET @username = username ;
-- 
-- userDao.logReviewRequest(getReview());
             EXECUTE findStatement USING @username ,@pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
 SET @username = username ;
--         
        if(@pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @username ,@pageSize,@moffSet ;

         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.count_user_actions;
DELIMITER $$

CREATE PROCEDURE medipersona.count_user_actions(
    IN username VARCHAR(64),
    OUT vitalCount int,
    OUT reviewCount int,
    OUT requestCount int,
    OUT usedRequestCount int
)
BEGIN
SET @username = username;
SELECT count(*) into vitalCount from medipersona.vital_signs where medipersona.vital_signs.username = @username;
SELECT count(*) into requestCount from medipersona.medi_review_request where medipersona.medi_review_request.username = @username  and medipersona.medi_review_request.enabled = true;
SELECT count(*) into usedRequestCount from medipersona.medi_review_request where ( medipersona.medi_review_request.username = @username and medipersona.medi_review_request.isUsed  = true ) and medipersona.medi_review_request.enabled = true;
SELECT count(*) into reviewCount from medipersona.reviews where medipersona.reviews.username = @username;


END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_recent_vital_signs_by_username;
DELIMITER $$

CREATE PROCEDURE medipersona.find_recent_vital_signs_by_username(
    IN username VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.vital_signs where medipersona.vital_signs.username = username   ORDER BY createdOn DESC LIMIT 5;

END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.find_last_ten_vital_signs_for_user;
DELIMITER $$

CREATE PROCEDURE medipersona.find_last_ten_vital_signs_for_user(
    IN username VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.vital_signs where medipersona.vital_signs.username = username   ORDER BY createdOn DESC LIMIT 10;

END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.get_active_review_by_code;
DELIMITER $$

CREATE PROCEDURE medipersona.get_active_review_by_code(
    IN reviewcode VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_review_request where (medipersona.medi_review_request.reviewcode = reviewcode and medi_review_request.status = true )and medi_review_request.enabled = true ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.update_used_review_code;
DELIMITER $$

CREATE PROCEDURE medipersona.update_used_review_code(
    IN reviewCode VARCHAR(64)
)
BEGIN
 UPDATE   medipersona.medi_review_request
 
            SET medi_review_request.isUsed = true

        WHERE medi_review_request.reviewCode = reviewCode;

END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.delete_user_request;
DELIMITER $$

CREATE PROCEDURE medipersona.delete_user_request(
    IN reviewCode VARCHAR(64)
)
BEGIN
 UPDATE   medipersona.medi_review_request
 
            SET medi_review_request.enabled = false

        WHERE medi_review_request.reviewCode = reviewCode;

END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.deprecate_review_request;
DELIMITER $$

CREATE PROCEDURE medipersona.deprecate_review_request()

BEGIN 

  UPDATE 
        medipersona.medi_review_request

           set 

           medipersona.medi_review_request.enabled = false,
           medipersona.medi_review_request.status = false

           where 
        
            TIMESTAMPDIFF(HOUR,medi_review_request.expiryDate,now()) > '0.1';
    
END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.get_all_review_requests;
DELIMITER $$

 CREATE PROCEDURE medipersona.get_all_review_requests(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.medi_review_request ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_review_request ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_review_request ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
-- 
-- 
             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;




CREATE TABLE IF NOT EXISTS user_admins(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    firstName VARCHAR(64) NOT NULL ,
    lastName VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL,
    lastEdit DATETIME NULL DEFAULT NULL,
    username VARCHAR(64),
    password VARCHAR(64),
    email VARCHAR(64),
    phoneNumber VARCHAR(64),
    countryId VARCHAR(64),
    enabled BIT
    
 
    
);

DROP PROCEDURE IF EXISTS medipersona.register_user_admin;

DELIMITER $$

CREATE PROCEDURE medipersona.register_user_admin(
	OUT id BIGINT,
     
    firstName VARCHAR(64),
    lastName VARCHAR(64),
    createdOn DATETIME ,
    lastEdit DATETIME ,
    username VARCHAR(64),
    password VARCHAR(64),
    email VARCHAR(64),
    phoneNumber VARCHAR(64),
    countryId VARCHAR(64),
    enabled BIT 
        )
BEGIN
	INSERT INTO medipersona.user_admins
        (
         
            firstName ,
            lastName,
            createdOn ,
            lastEdit,
            username ,
            password,
            email ,
            phoneNumber ,
            countryId,
            enabled
        )
	VALUES
	(
              
            firstName ,
            lastName,
            createdOn ,
            lastEdit,
            username ,
            password,
            email ,
            phoneNumber ,
            countryId,
            enabled
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.delete_user_admin_byusername;
DELIMITER $$

CREATE PROCEDURE medipersona.delete_user_admin_byusername(
    IN username VARCHAR(64)
)
BEGIN

DELETE FROM medipersona.user_admins where medipersona.user_admins.username = username;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.update_user_admin;

DELIMITER $$

CREATE PROCEDURE medipersona.update_user_admin(
	OUT id BIGINT,
    firstName VARCHAR(64),
    lastName VARCHAR(64),
    lastEdit DATETIME ,
    username VARCHAR(64),
    email VARCHAR(64),
    phoneNumber VARCHAR(64)
        )
BEGIN
	UPDATE  medipersona.user_admins

             SET
                medipersona.user_admins.firstName = firstName,
                medipersona.user_admins.lastName = lastName,
                medipersona.user_admins.lastEdit = lastEdit,
                medipersona.user_admins.email = email,
                medipersona.user_admins.phoneNumber = phoneNumber

           WHERE medipersona.user_admins.username = username;
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_user_admin_byusername;
DELIMITER $$

CREATE PROCEDURE medipersona.find_user_admin_byusername(
    IN username VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.user_admins where medipersona.user_admins.username = username;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_review_request_by_email;
DELIMITER $$

CREATE PROCEDURE medipersona.get_review_request_by_email(
 in pageNum int,
 in pageSize int,
 in email varchar(64),
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;
-- 
     select count(*) into counter from medipersona.medi_review_request where medi_review_request.email = email and medi_review_request.enabled = true ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_review_request where medi_review_request.email = ? and medi_review_request.enabled = true ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_review_request where medi_review_request.email = ? and medi_review_request.enabled = true ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
              SET @email = email ;
-- 
-- userDao.logReviewRequest(getReview());
             EXECUTE findStatement USING @email ,@pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
         SET @email = email ;
--         
        if(@pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @email ,@pageSize,@moffSet ;

         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;




CREATE TABLE IF NOT EXISTS medications(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    medicationId VARCHAR(64) NOT NULL UNIQUE,
    username VARCHAR(64) NOT NULL,    
    diagnosis VARCHAR(64),
    prescription TEXT,    
    prescriptionNote TEXT,
    medication TEXT,    
    startDate DATETIME NULL DEFAULT NULL,
    endDate DATETIME NULL DEFAULT NULL,
    createdOn DATETIME NULL DEFAULT NULL
);



DROP PROCEDURE IF EXISTS medipersona.add_medication;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medication(
	OUT id BIGINT,
        IN medicationId VARCHAR(64),
        IN username VARCHAR(64) ,
        IN diagnosis VARCHAR(64),
        IN prescription TEXT,
        IN prescriptionNote TEXT ,
        IN medication TEXT,
        IN startDate DATETIME,
        IN endDate DATETIME,
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medications
        (
        medicationId ,
        username ,
        diagnosis ,
        prescription ,
        prescriptionNote ,
        medication ,
        startDate ,
        endDate,
        createdOn    
        )
	VALUES
	(
             medicationId ,
        username ,
        diagnosis ,
        prescription ,
        prescriptionNote ,
        medication ,
        startDate ,
        endDate,
        createdOn 
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_medications_by_username;
DELIMITER //

CREATE PROCEDURE medipersona.find_medications_by_username(
In username varchar(64),
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.medications where medications.username = username ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.medications where medications.username = ? ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.medications where medications.username = ? ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;
            SET @username = username ;


            EXECUTE findStatement USING @username,@pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
           SET @username = username ;
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @username, @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.get_all_medications;
DELIMITER //

CREATE PROCEDURE medipersona.get_all_medications(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.medications;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.medications ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.medications ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;
                      EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
          
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING  @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;