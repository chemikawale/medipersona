use medipersona;

CREATE TABLE IF NOT EXISTS reviewer(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    firstName VARCHAR(64) NOT NULL ,
    lastName VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL,
    lastEdit DATETIME NULL DEFAULT NULL,
    username VARCHAR(64),
    password VARCHAR(64),
    email VARCHAR(64),
    phoneNumber VARCHAR(64),
    countryId VARCHAR(64),
    enabled BIT
   
    
);

DROP PROCEDURE IF EXISTS medipersona.register_reviewer;

DELIMITER $$

CREATE PROCEDURE medipersona.register_reviewer(
	OUT id BIGINT,
     
    firstName VARCHAR(64),
    lastName VARCHAR(64),
    createdOn DATETIME ,
    lastEdit DATETIME ,
    username VARCHAR(64),
    password VARCHAR(64),
    email VARCHAR(64),
    phoneNumber VARCHAR(64),
    countryId VARCHAR(64),
    enabled BIT 
        )
BEGIN
	INSERT INTO medipersona.reviewer
        (
         
            firstName ,
            lastName,
            createdOn ,
            lastEdit,
            username ,
            password,
            email ,
            phoneNumber ,
            countryId,
            enabled
        )
	VALUES
	(
              
            firstName ,
            lastName,
            createdOn ,
            lastEdit,
            username ,
            password,
            email ,
            phoneNumber ,
            countryId,
            enabled
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.delete_user_admin_byusername;
DELIMITER $$

CREATE PROCEDURE medipersona.delete_user_admin_byusername(
    IN username VARCHAR(64)
)
BEGIN

DELETE FROM medipersona.reviewer where medipersona.reviewer.username = username;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_reviewer_byusername;
DELIMITER $$

CREATE PROCEDURE medipersona.find_reviewer_byusername(
    IN username VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.reviewer where medipersona.reviewer.username = username;

END $$

DELIMITER;
