<%-- 
    Document   : createvasitem
    Created on : Oct 19, 2015, 7:49:18 AM
    Author     : Oyewale
--%>


<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>MediPersona || ADD ALLERGY</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
   
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
   <jsp:include page="../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../views/includes/nav.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">Add <span> Allergy</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>Allergy</span> Details</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              </ul>
            </div>
            <div class="l-box-body l-spaced">
             
                     <form:form  role="form"  commandName="viewModel" action="addallergy" method="POST" class="form-horizontal">
        <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                    
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                 <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">CATEGORY: </label>
                  <div class="col-sm-5">
                    <form:select id="providerSelect"  class="form-control" path="categoryId" >
                          <option>Select Category</option>
                         <c:forEach items="${allAllergyCategories}" var="category"> 
                           
                             <form:option value="${category.categoryId}" label="${category.categoryName}" />
                           </c:forEach>
                    </form:select>
                  </div>
                 </div>
       
          <div class="form-group">
                   <label class="col-sm-3 control-label" for="allergicTo"> ALLERGIC TO:</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control" id="allergicTo" name="allergicTo" path="allergicTo" type="text" placeholder="ALLERGIC TO ?"  required="true"/>
                         <form:errors path="allergicTo" cssStyle="color : red;"/>       
                       </div>
                       </div>
                        <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">SEVERITY: </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="severity" >
                       
                       <option value="MILD" label="MILD" />
                        <option value="VERY MILD" label="VERY MILD" />
                        <option value="MODERATE" label="MODERATE" />
                         <option value="SEVERE" label="SEVERE" />
                    </select>
                  </div>
                 </div>
                        <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">LOCATION : </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="location" >
                       
                       <option value="SKIN" label="SKIN" />
                        <option value="SYSTEMIC" label="SYSTEMIC" />
                        <option value="ABDOMINAL / ANALPHYLACTIC" label="ABDOMINAL / ANALPHYLACTIC" />
                        
                          
                    </select>
                  </div>
                 </div>
                          <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">STARTED WHEN ? : </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="started" >
                       
                       <option value="CHIDHOOD" label="CHILDHOOD" />
                       <option value="TEENAGE" label="TEENAGE" />
                       <option value="ADULTHOOD" label="ADULTHOOD" />
                       <option value="UNKNOWN" label="UNKNOWN" />
                        
                          
                    </select>
                  </div>
                 </div>
                       
                       
                         <div class="form-group">
                   <label class="col-sm-3 control-label" for="reaction">REACTION :</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control" id="reaction" name="reaction" path="reaction" type="text" placeholder="REACTION " />
                         <form:errors path="reaction" cssStyle="color : red;"/>       
                       </div>
                       </div>
                                   <div class="form-group">
                  <label class="col-sm-3 control-label">STILL ACTIVE ?</label>
                  <div class="col-sm-5">
                    <div class="checkbox">
                      <label>
                          <form:checkbox  path="active" />
                      </label>
                    </div>
                   
                  </div>
                </div>
                      <div class="form-group">
                  <label for="autogrowTextarea" class="col-sm-3 control-label">COMMENT(s)</label>
                  <div class="col-sm-5">               
                      <form:textarea class="form-control" id="autogrowTextarea" name="comment" path="comment" rows="3"  placeholder=" COMMENT" required="true"/>
                      <form:errors path="comment" cssStyle="color : red;"/>    
                  </div>
                  </div>
                   
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">SUBMIT</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
           <jsp:include page="../views/includes/footer.jsp" />
      </section>

    </section>
<!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>

  </body>

</html>