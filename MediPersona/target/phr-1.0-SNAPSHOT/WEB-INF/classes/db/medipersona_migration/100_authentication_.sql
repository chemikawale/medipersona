use medipersona;

 CREATE TABLE IF NOT EXISTS authentication_tokens(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accessToken VARCHAR(256) NOT NULL UNIQUE,
    username VARCHAR(64),
    password VARCHAR(256) ,
    createdOn DATETIME NULL DEFAULT NULL 
    
);


DROP PROCEDURE IF EXISTS medipersona.add_token;

DELIMITER $$

CREATE PROCEDURE medipersona.add_token(
	OUT id BIGINT,
        IN accessToken VARCHAR(256) ,
        IN username VARCHAR(64),
        IN password VARCHAR(256),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.authentication_tokens
        (
        accessToken ,
        username ,
        password,
        createdOn  
        )
	VALUES
	(
        accessToken ,
        username ,
        password,
        createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_access_tokens;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_access_tokens()
BEGIN

SELECT * FROM medipersona.authentication_tokens order by createdOn DESC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_user_by_accessToken;
DELIMITER $$

CREATE PROCEDURE medipersona.find_user_by_accessToken(
 IN accessToken varchar(256)
)
BEGIN

SELECT * FROM medipersona.authentication_tokens where medipersona.authentication_tokens.accessToken = accessToken  ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_token_by_username;
DELIMITER $$

CREATE PROCEDURE medipersona.find_token_by_username(
 IN username varchar(256)
)
BEGIN

SELECT * FROM medipersona.authentication_tokens where medipersona.authentication_tokens.username = username  ;

END $$

DELIMITER;

