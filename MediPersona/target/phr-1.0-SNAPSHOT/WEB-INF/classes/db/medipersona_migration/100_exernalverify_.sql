use medipersona;

-- This section is for recovery links
create table if not exists medipersona.password_recovery_links(
    id bigint not null primary key auto_increment,
    username varchar(100) not null,
    link varchar(256) not null,
    createdOn datetime not null,
    expiry  datetime not null
);


drop procedure if exists medipersona.add_password_recovery_link ; 

delimiter %%
create procedure medipersona.add_password_recovery_link(
    out id bigint,
    username varchar(100),
    link varchar(256),
    createdOn datetime,
    expiry  datetime
)
begin
    
    insert into medipersona.password_recovery_links(
        username,
        link,
        createdOn,
        expiry
    )
    values(
        username,
        link,
        createdOn,
        expiry
    );
  
    SET id = LAST_INSERT_ID();

end %%
delimiter ;

drop procedure if exists medipersona.delete_password_recovery_link_by_link ;

delimiter %%

create procedure medipersona.delete_password_recovery_link_by_link (
    in link varchar(256)
)

begin
    
    delete from medipersona.password_recovery_links 
    where medipersona.password_recovery_links.`link` = link ;
    

end %%
delimiter ;

drop procedure if exists medipersona.delete_password_recovery_link_by_username ;

delimiter %%

create procedure medipersona.delete_password_recovery_link_by_username (
    in username varchar(256)
)

begin
    
    delete from medipersona.password_recovery_links 
    where medipersona.password_recovery_links.username = username ;
    

end %%
delimiter ;

drop procedure if exists medipersona.find_password_recovery_link_by_link ;

delimiter %%

create procedure medipersona.find_password_recovery_link_by_link (
    in link varchar(256)
)

begin
    
    select * from medipersona.password_recovery_links 
    where medipersona.password_recovery_links.`link` = link ;
    

end %%
delimiter ;

drop procedure if exists medipersona.find_password_recovery_link_by_username ;

delimiter %%

create procedure medipersona.find_password_recovery_link_by_username (
    in username varchar(256)
)

begin
    
    select * from medipersona.password_recovery_links 
    where medipersona.password_recovery_links.username = username ;
    

end %%
delimiter ;

drop procedure if exists medipersona.find_all_password_recovery_links ;

delimiter &&
create procedure medipersona.find_all_password_recovery_links (
    in pageNum int,
    in pageSize int,
    out count int
)
begin
    declare number_to_skip, temp_counter, counter int;

    set number_to_skip = pageSize *  pageNum ;
    select count(*) into counter from medipersona.password_recovery_links;
    set count = counter;
    
    Prepare findStatement from 'select * from medipersona.password_recovery_links order by medipersona.password_recovery_links.id desc limit ?';
    prepare findStatement2 from 'select * from medipersona.password_recovery_links limit ? offset ?';

    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;

            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if ;
end &&
delimiter ;
