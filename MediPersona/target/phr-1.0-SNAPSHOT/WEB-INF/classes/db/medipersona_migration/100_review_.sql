use medipersona;

CREATE TABLE IF NOT EXISTS reviews(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    reviewId VARCHAR(64) NOT NULL ,
    reviewer VARCHAR(64),
    username VARCHAR(64),
    diagnosis VARCHAR(64),
    recommendation TEXT,
    comments TEXT,
    rating VARCHAR(64),
    createdOn DATETIME NULL DEFAULT NULL,
    lastEdit DATETIME NULL DEFAULT NULL
    
    
);

DROP PROCEDURE IF EXISTS medipersona.log_review;

DELIMITER $$

CREATE PROCEDURE medipersona.log_review(
    OUT id BIGINT,
    IN reviewId VARCHAR(64),
    IN reviewer VARCHAR(64),
    IN username VARCHAR(64),
    IN diagnosis VARCHAR(64),
    IN recommendation TEXT,
    IN comments TEXT,
    IN rating VARCHAR(64),
    IN createdOn DATETIME,
    IN lastEdit DATETIME

        )
BEGIN
	INSERT INTO medipersona.reviews
        (
       reviewId ,
     reviewer ,
     username ,
     diagnosis,
     recommendation,
     comments ,
     rating,
     createdOn,
     lastEdit
        )
	VALUES
	(
      reviewId ,
     reviewer ,
     username ,
     diagnosis,
     recommendation,
     comments ,
     rating,
     createdOn,
     lastEdit
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.find_reviews_byusername;
DELIMITER //

CREATE PROCEDURE medipersona.find_reviews_byusername(
in pageNum int,
in pageSize int,
in username VARCHAR(64),
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.reviews where reviews.username = username ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.reviews where reviews.username = ? ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.reviews where reviews.username = ? ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;

            SET @username = username ;


            EXECUTE findStatement USING @username, @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        SET @username = username;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @username, @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.find_reviews_byreviewer;
DELIMITER //

CREATE PROCEDURE medipersona.find_reviews_byreviewer(
in pageNum int,
in pageSize int,
in reviewer VARCHAR(64),
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.reviews where reviews.reviewer = reviewer ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.reviews where reviews.reviewer = ? ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.reviews where reviews.reviewer = ? ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;

            SET @reviewer = reviewer ;


            EXECUTE findStatement USING @reviewer, @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        SET @reviewer = reviewer;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @reviewer, @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.get_all_reviews;
DELIMITER //

CREATE PROCEDURE medipersona.get_all_reviews(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.reviews ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.reviews  ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.reviews  ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;     

            EXECUTE findStatement USING  @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
     
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;


DROP PROCEDURE IF EXISTS medipersona.find_review_byReviewId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_review_byReviewId(
    IN reviewId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.reviews where medipersona.reviews.reviewId = reviewId;

END $$

DELIMITER;