
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>  

<aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a></div>
        <div class="l-side-box">
          <!--Logo-->
          <div class="widget-logo logo-in-side">
              <h1><a href="#"><span class=" visible-default-inline-block"><img src="<c:url value="/img/logo.png"/>" alt="MediPersona"></span><span class="visible-compact-inline-block"><img src="<c:url value="/img/logo_medium.png"/>" alt="MediPersona" title="MediPersona"></span>
                      <span class="visible-collapsed-inline-block"><img src="<c:url value="/img/logo_small.png" />" alt="Proteus" title="MediPersona"></span></a></h1>
          </div>
        </div>
        <!--Main Menu-->
        <div class="l-side-box">
          <!--MAIN NAVIGATION MENU-->
          
          <nav class="navigation">
              
                            
            
            <ul data-ason-type="menu" class="ason-widget">   
                
                       
                     <!--<li class="<c:out value="${requests}" />"><a href="/medipersona/reviewer/reviewrequests"><i class="icon fa fa-user-md"></i><span class="title">REVIEW REQUESTS</span></a></li>-->
                      <li class="<c:out value="${reviewcode}" />"><a href="/phr/reviewer/reviewcode"><i class="icon fa fa-compress"></i><span class="title"> VERIFY REVIEW CODE</span></a></li>
                      <li class="<c:out value="${profile}" />"><a href="/phr/reviewer/medicalprofile"><i class="icon fa fa-folder"></i><span class="title">MEDICAL PROFILE</span></a></li>
                      <li class="<c:out value="${review}" />"><a href="/phr/reviewer/review"><i class="icon fa fa-exchange"></i><span class="title">SUBMIT REVIEW</span></a></li>
                       <!--<li class="<c:out value="${reviews}" />"><a href="/medipersona/reviewer/reviews"><i class="icon fa fa-list-ul"></i><span class="title">ALL REVIEWS</span></a></li>-->
                      
                
                
          
              
               
            </ul>
          </nav>
       
        </div>
      </aside>