<%-- 
    Document   : styles
    Created on : 24-Jul-2014, 12:07:01
    Author     : Adeyemi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!--loading proteus main css files-->

<link href="<c:url value="/css/basic.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/general.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/theme.css"/>" rel="stylesheet" />

<!--loading proteus addons css files-->
<link href="<c:url value="/css/addons/fullcalendar.print.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/animate.min.css"/>" rel="stylesheet" />


<!--loading proteus addons font css files-->
<link href="<c:url value="/css/addons/fonts/simple-line.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/fonts/stroke-gap.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/fonts/icomoon.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/fonts/artill-clean-icons.css"/>" rel="stylesheet" />

<!--loading proteus addons theme css files-->
<link href="<c:url value="/css/addons/theme/jasny-bootstrap.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/theme/jquery-ui.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/theme/patternbolt.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/theme/select2.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/theme/summernote.css"/>" rel="stylesheet" />
<link href="<c:url value="/css/addons/theme/syntaxhighlighter.css"/>" rel="stylesheet" />


<!--<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />" type="text/css" />-->