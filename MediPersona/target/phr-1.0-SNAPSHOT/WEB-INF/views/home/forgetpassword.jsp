<!DOCTYPE html>
<html lang="en">
  +<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
    <title>MediPersona - Forgot Password</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../medipersona/css/basic.css">
    <link rel="stylesheet" href="../medipersona/css/general.css">
    <link rel="stylesheet" href="../medipersona/css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"><img src="../medipersona/img/logo.png" alt="MediPersona"></h1>
          <h2>Forgot your password ?</h2>
          <p> Enter your username, a password reset will be sent to your account email address.</p>
          <!--Form-->
          <form:form id="forgotForm" role="form" commandName="editModel"  action="forgotpassword" class="login-form" method="POST">
                              <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
            <div class="form-group">
                <form:input id="username" type="text" path="username" placeholder="USERNAME" class="form-control" required="true"/>
            </div>
            <button type="submit" class="btn btn-block btn-dark btn-login">Send Link</button>
            <div class="login-options">
              <p>Not a member ? &nbsp;<a href="/medipersona/persona/signup">SIGN UP</a></p>
            </div>
          </form:form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../medipersona/js/basic/jquery.min.js"></script>
    <script src="../medipersona/js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../medipersona/js/basic/modernizr.min.js"></script>
    <script src="../medipersona/js/basic/bootstrap.min.js"></script>
    <script src="../medipersona/js/shared/jquery.asonWidget.js"></script>
    <script src="../medipersona/js/plugins/plugins.js"></script>
    <script src="../medipersona/js/general.js"></script>
    <script src="../medipersona/js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../medipersona/js/plugins/forms/validation/jquery.validate.min.js"></script>
    <script src="../medipersona/js/plugins/forms/validation/jquery.validate.additional.min.js"></script>
    <script src="../medipersona/js/calls/page.forgot.js"></script>
  
  
  </body>
</html>