<%-- 
    Document   : banks
    Created on : Sep 8, 2015, 12:51:46 PM
    Author     : Oyewale
--%>

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>MediPersona || ALL COUNTRIES</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    

    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../../views/includes/nav.jsp" />
        <div class="l-page-header">
          <h2 class="l-page-title"><span>Registered</span> Countries</h2>
          <!--BREADCRUMB-->
          <ul class="breadcrumb t-breadcrumb-page">
            <li></li>
          
            <li class=" navbar-right btn btn-success"><a href="/medipersona/uti/addcountry">ADD COUNTRY</a></li>
          </ul>
        </div>
        <div class="l-spaced">
          <div class="l-row">
            <div class="l-box">
              <div class="l-box-header">
                <h2 class="l-box-title"><span>All Countries</span> </h2>
                <ul class="l-box-options">
                  <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                  <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                  <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                   </ul>
              </div>
              <div class="l-box-body">
                  <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
              <table id="dataTableId" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                     
                      <th>ID</th>
                      <th>NAME</th>                 
                      <th>DATE REGISTERED</th>
                       
                    </tr>
                  </thead>
                                 <tbody>
                                                      <c:forEach items="${allCountries}" var="country" > 
                                                               
                                                                    <tr>
                                                                        <td>
                                                                            <c:out value="${country.countryId}" />
                                                                        </td>
                                                                       
                                                                        <td>
                                                                            <c:out value="${country.countryName}" />
                                                                        </td>
                                                                                                                                           
                                                                        <td>
                                                                            <c:out value="${country.createdOn}" />
                                                                        </td>
                                                                    </tr>
                                                              
                                                            </c:forEach>
                                                        </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
  
  
    <jsp:include page="../../views/includes/footer.jsp" />
        </section>
    </section>
   
  <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>

  </body>
</html>