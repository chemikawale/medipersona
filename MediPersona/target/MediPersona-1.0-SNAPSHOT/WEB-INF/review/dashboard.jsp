<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title> MediPersona || DASHBOARD </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <style type="text/css">
        .dataTables_filter, .dataTables_info,.dataTables_length,.dataTables_paginate { display: none; }
    </style>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    
    <![endif]-->
  </head>
  <body>
     <% response.setIntHeader("Refresh", 100); %>
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        
        <jsp:include page="../views/includes/nav.jsp" />
   
       <div class="l-spaced">
          <div class="l-row">
            <div class="l-box">
              <div class="l-box-header">
                <h2 class="l-box-title"><span>User's Most Recent Vital Signs</span> Records </h2>
                <ul class="l-box-options">
                  <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                  <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                  <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                </ul>
              </div>
              <div class="l-box-body">
                  <table id="dataTableId" class="usertable" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th class="tb-col-1">DATE/TIME</th>
                      <th class="tb-col-2">DIASTOLIC</th>
                      <th class="tb-col-3">SYSTOLIC</th>
                      <th class="tb-col-4">TEMPERATURE</th>
                      <th class="tb-col-5">WEIGHT </th>
                      <th class="tb-col-6" >HEIGHT</th>
                      <th class="tb-col-7">PULSE</th>
                      <th class="tb-col-8" >RESPIRATION</th>
                      <th class="tb-col-9">B.M.I</th>
                                  
                      
                      
                    </tr>
                  </thead>
                               <tbody>
                   <c:forEach items="${vitalsigns}" var="vitalsign"> 
                       <tr>
                           <td class="tb-col-1">
                                                                    <c:out value="${vitalsign.createdOn}" />
                                                                </td> 
                                                      <td class="tb-col-2">
                                                                    <c:out value="${vitalsign.diastolic}" />
                                                                </td> 
                                                                   <td class="tb-col-3">
                                                                    <c:out value="${vitalsign.systolic}" />
                                                                </td> 
                                                                   <td class="tb-col-4">
                                                                    <c:out value="${vitalsign.temperature}" />
                                                                </td> 
                                                                <td class="tb-col-5">
                                                                    <c:out value="${vitalsign.weight}" />
                                                                </td>
                                                                   <td class="tb-col-6">
                                                                    <c:out value="${vitalsign.height}" />
                                                                </td> 
                                                                   <td class="tb-col-7">
                                                                    <c:out value="${vitalsign.pulse}" />
                                                                </td> 
                                                                   <td class="tb-col-8">
                                                                    <c:out value="${vitalsign.respiration}" />
                                                                </td> 
                                                                   <td class="tb-col-9">
                                                                    <c:out value="${vitalsign.bmi}" />
                                                                </td> 
                                                                  
                                                                
                                                                
                                                               
                                                    
                                                                        </tr>
                                                              </c:forEach>
                   
                      </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        
          <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">T.P.R Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="revenueChart"></div>
              </div>
            </div>
          </div>
        </div>
      
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">B.P Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="bpChart"></div>
              </div>
            </div>
          </div>
        </div>
         
         
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">WEIGHT - HEIGHT Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="whChart"></div>
              </div>
            </div>
          </div>
        </div>
      
     
        
    
          <jsp:include page="../views/includes/footer.jsp" />
      </section>
   
    </section>
   <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <script src="../js/shared/jquery-ui.min.js"></script>
    <script src="../js/shared/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
      // jQuery Ui and Bootstrap conflict workaround
      $.widget.bridge('uitooltip', $.ui.tooltip);
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->

    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/charts/c3/c3.min.js"></script>
    <script src="../js/plugins/charts/c3/d3.v3.min.js"></script>
    <script src="../js/plugins/charts/other/jquery.sparkline.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
 
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
  <script src="../js/calls/table.data.js"></script>
   <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/shared/raphael.min.js"></script>
    <script src="../js/plugins/charts/morris/morris.min.js"></script>

   
    <script type="text/javascript">
     $(document).ready(function () {

         var ctx = "${pageContext.request.contextPath}";                                    
        
        
        
        
  $.ajax({
                      url: ctx + "/user/medicaldata",     
                      error: function() {
                              console.log('Error occure while medical data data');
                              },
                      type: 'GET',
                       success: function(data) {
                          var medicaldata = data.medicaldata;
                        
                            var temperatures = [];
                            
                            var pulses = [];
                            var systolic = [];
                            var diastolic = [];
                            var respiration = [];
                            var datelogged = [];
                            var weight = [];
                            var height = [];
                            
                        for(var i = 0; i < 10; i++){
                                  var datalength = medicaldata.length;
                                  if(i < datalength){
                                  datelogged[i] = medicaldata[i].createdOn; 
                                  temperatures[i] = medicaldata[i].temperature; 
                                   weight[i] = medicaldata[i].weight; 
                                     height[i] = medicaldata[i].height; 
                                   pulses[i] = medicaldata[i].pulse; 
                                  systolic[i] = medicaldata[i].systolic; 
                                  diastolic[i] = medicaldata[i].diastolic; 
                                  respiration[i] = medicaldata[i].respiration; 
                              }else{
                                   datelogged[i] = 'DATE'; 
                                  temperatures[i] = 0; 
                                   pulses[i] = 0; 
                                  systolic[i] = 0; 
                                  diastolic[i] = 0; 
                                  respiration[i] = 0; 
                                    weight[i] = 0; 
                                  height[i] = 0; 
                              }
                              }
                          
                          
                          
	 var  revenueChart   = '#revenueChart';
         var color_1 = $('.c3-revenue-1').css('color'),
        color_2 = $('.c3-revenue-2').css('color'),
        color_3 = $('.c3-revenue-3').css('color'),
        color_4 = $('.c3-revenue-4').css('color'),
        color_5 = $('.c3-revenue-5').css('color');

        var chart = c3.generate({
        bindto: revenueChart,
        data: {
            x: 'x',
            columns: [
                ['x', datelogged[9],datelogged[8],datelogged[7],datelogged[6],datelogged[5],datelogged[4],datelogged[3],datelogged[2],datelogged[1],datelogged[0]],
                ['TEMPERATURE',temperatures[9],temperatures[8],temperatures[7],temperatures[6],temperatures[5],temperatures[4],temperatures[3],temperatures[2],temperatures[0],temperatures[0]],
                ['PULSE', pulses[9], pulses[8],pulses[7],pulses[6], pulses[5], pulses[4],pulses[3],pulses[2], pulses[1], pulses[0]],
                ['RESPIRATION', respiration[9],respiration[8], respiration[7], respiration[6],respiration[5],respiration[4],respiration[3], respiration[2], respiration[1],respiration[0]]
            ],
            type: 'area-spline'
        },
        axis : {
            x : {
                 type : 'category',
                tick: {
                    fit: false,
                    //format: "%e %b %y"
                    //format: function (x) { return x.getFullYear(); }
                    format: '%m/%Y' // format string is also available for timeseries data

                }
            },
            y : {
                tick: {
                    //format: d3.format("$,")
                    format: function (d) { return "" + d + " "; }
                }
            }
        },
        color: {
            pattern: [color_1, color_2, color_3]
        }
    });
    
    
    	 var  bpChart   = '#bpChart';
         var color_1 = $('.c3-revenue-1').css('color'),
        color_2 = $('.c3-revenue-2').css('color'),
        color_3 = $('.c3-revenue-3').css('color'),
        color_4 = $('.c3-revenue-4').css('color'),
        color_5 = $('.c3-revenue-5').css('color');

        var chart = c3.generate({
        bindto: bpChart,
        data: {
            x: 'x',
            columns: [
                ['x', datelogged[9],datelogged[8],datelogged[7],datelogged[6],datelogged[5],datelogged[4],datelogged[3],datelogged[2],datelogged[1],datelogged[0]],
                ['DIALOSTIC',diastolic[9],diastolic[8],diastolic[7],diastolic[6],diastolic[5],diastolic[4],diastolic[3],diastolic[2],diastolic[1],diastolic[0]],
                ['SYSTOLIC', systolic[9], systolic[8],systolic[7],systolic[6], systolic[5], systolic[4],systolic[3],systolic[2], systolic[1], systolic[0]]
               
            ],
            type: 'area-spline'
        },
        axis : {
            x : {
                 type : 'category',
                tick: {
                    fit: false,
                    //format: "%e %b %y"
                    //format: function (x) { return x.getFullYear(); }
                    format: '%m/%Y' // format string is also available for timeseries data

                }
            },
            y : {
                tick: {
                    //format: d3.format("$,")
                    format: function (d) { return "" + d + " "; }
                }
            }
        },
        color: {
            pattern: [color_1, color_2, color_3]
        }
    });

    var  whChart   = '#whChart';
         var color_1 = $('.c3-revenue-1').css('color'),
        color_2 = $('.c3-revenue-2').css('color'),
        color_3 = $('.c3-revenue-3').css('color'),
        color_4 = $('.c3-revenue-4').css('color'),
        color_5 = $('.c3-revenue-5').css('color');

        var chart = c3.generate({
        bindto: whChart,
        data: {
            x: 'x',
            columns: [
                ['x', datelogged[9],datelogged[8],datelogged[7],datelogged[6],datelogged[5],datelogged[4],datelogged[3],datelogged[2],datelogged[1],datelogged[0]],
                ['WEIGHT',weight[9],weight[8],weight[7],weight[6],weight[5],weight[4],weight[3],weight[2],weight[1],weight[0]],
                ['HEIGHT', height[9], height[8],height[7],height[6], height[5], height[4],height[3],height[2], height[1], height[0]]
               
            ],
            type: 'area-spline'
        },
        axis : {
            x : {
                 type : 'category',
                tick: {
                    fit: false,
                    //format: "%e %b %y"
                    //format: function (x) { return x.getFullYear(); }
                    format: '%m/%Y' // format string is also available for timeseries data

                }
            },
            y : {
                tick: {
                    //format: d3.format("$,")
                    format: function (d) { return "" + d + " "; }
                }
            }
        },
        color: {
            pattern: [color_1, color_2, color_3]
        }
    });

   
  
  
  

                    }
       });
        
          
});


    </script>
  </body>

</html>