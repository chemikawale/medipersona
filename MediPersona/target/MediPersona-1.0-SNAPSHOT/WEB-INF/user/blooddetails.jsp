<%-- 
    Document   : createvasitem
    Created on : Oct 19, 2015, 7:49:18 AM
    Author     : Oyewale
--%>


<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>MediPersona || UPDATE BLOOD DETAILS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
   
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
   <jsp:include page="../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../views/includes/nav.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">Update <span> Blood Details</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>Blood Details</span> Details</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              </ul>
            </div>
            <div class="l-box-body l-spaced">
             
                     <form:form  role="form"  commandName="viewModel" action="blooddetails" method="POST" class="form-horizontal">
        <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                    
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                 <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">BLOOD GROUP: </label>
                  <div class="col-sm-5">
                    <form:select id="providerSelect"  class="form-control" path="bloodGroup" >
                       
                       <option value="O-" label="O-" />
                        <option value="O+" label="O+" />
                        <option value="AB" label="AB" />
                          
                    </form:select>
                  </div>
                 </div>
        
          <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">RHESUS D: </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="rhesusD" >
                       
                       <option value="O-" label="O-" />
                        <option value="O+" label="O+" />
                        <option value="AB" label="AB" />
                          
                    </select>
                  </div>
                 </div>
                <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">GENOTYPE: </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="genotype" >
                       
                       <option value="AA" label="AA" />
                       <option value="AS" label="AS" />
                       <option value="AB" label="AB" />
                       <option value="SC" label="SC" />
                       <option value="SS" label="SS" />
                       <option value="AC" label="AC" />
                          
                    </select>
                  </div>
                 </div>
       
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">UPDATE</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
           <jsp:include page="../views/includes/footer.jsp" />
      </section>

    </section>
<!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>
    <script type="text/javascript">
               
                var ctx = "${pageContext.request.contextPath}";
           $(document).ready(function() {
              
             dropDown = $('#providerSelect').change(function(){
                 var countryId = $(this).val();  
                $.ajax({
                   url:  ctx + '/uti/states?countryId='+countryId,
                   type: 'GET',
                   contentType:"application/json"
               }).done(function(data){
                       var content = "";                   
                                        
                       for(var i = 0; i < data.length; i++){
                           var d = data[i];
                           
                           content += "<option value="+d.stateId+">"+d.stateName+"</option>";
                       }                    
                       
                       $('#stateId').html(content);
                   

             });
           });
           
    });
           
           
               
               
    </script>
  </body>

</html>