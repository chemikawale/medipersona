<%-- 
    Document   : resetpassword
    Created on : 02-Aug-2014, 00:06:12
    Author     : Adeyemi
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PAYPAD || CHANGE PASSWORD</title>

    <!-- Bootstrap Core CSS -->
    <jsp:include page="../views/includes/styles.jsp" />
</head>

<body>
    <jsp:include page="../views/includes/navigation.jsp" />
        
        <div id="page-wrapper" class="container">
            <div class="row">
                <jsp:include page="../views/includes/sidebar.jsp" />

                <div id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-offset-2 col-lg-9">
                                    <div class="clearfix">
                                        <h1>Change Password</h1> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-9 col-lg-offset-2 ">
                                    <div class="main-box no-header clearfix">
                                        <div class="main-box-body clearfix">
                                           
                                            <form:form cssClass="col-lg-8 col-lg-offset-2 form-horizontal" commandName="viewModel" action="changepassword" method="post">
                                                <fieldset>
                                                    <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                                                    <div class="col-lg-12 form-group">
                                                        <form:label path="username" cssClass="col-sm-5 control-label">Username</form:label> 
                                                        <div class="col-sm-6">
                                                        <form:input path="username" cssClass="form-control" readonly="true" required="true" placeholder="username" alt="username" autocomplete="off"></form:input>
                                                        <form:errors path="username" cssStyle="color : red;"/>  
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <form:label path="password" cssClass="col-sm-5 control-label">New Password</form:label>
                                                        <div class="col-sm-6">
                                                        <form:password path="password" required="true" cssClass="form-control" placeholder="password" autocomplete="off"/>
                                                            <form:errors path="password" cssStyle="color : red;"/>  
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <form:label path="confirmpassword" cssClass="col-sm-5 control-label">Confirm New Password</form:label>
                                                        <div class="col-sm-6">
                                                            <form:password path="confirmpassword" required="true" cssClass="form-control" placeholder="confirm password" autocomplete="off"/>
                                                            <form:errors path="password" cssStyle="color : red;"/>  
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="col-sm-offset-5 col-sm-7" id="user-create-btn-div" >
                                                            <input type="submit" class="btn btn-lg btn-primary col-lg-8" id="submit" value="Change Password" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<jsp:include page="../views/includes/footer.jsp" />