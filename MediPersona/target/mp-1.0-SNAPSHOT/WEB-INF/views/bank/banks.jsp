<%-- 
    Document   : banks
    Created on : Sep 8, 2015, 12:51:46 PM
    Author     : Oyewale
--%>

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>PAYPAD || ALL BANKS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    

    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../../views/includes/nav.jsp" />
        <div class="l-page-header">
          <h2 class="l-page-title"><span>Registered</span> Banks</h2>
         
        </div>
        <div class="l-spaced">
          <div class="l-row">
            <div class="l-box">
              <div class="l-box-header">
                <h2 class="l-box-title"><span>All Banks</span> </h2>
                <ul class="l-box-options">
                  <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                  <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                  <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                   </ul>
              </div>
              <div class="l-box-body">
                  <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
              <table id="dataTableId" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                     
                      <th>ID</th>
                      <th>NAME</th>                 
                      <th>ADDRESS</th>
                      <th>DATE REGISTERED</th>
                       
                    </tr>
                  </thead>
                                 <tbody>
                                                      <c:forEach items="${allBanks}" var="bank" > 
                                                               
                                                                    <tr>
                                                                        <td>
                                                                            <c:out value="${bank.bankId}" />
                                                                        </td>
                                                                       
                                                                        <td>
                                                                            <c:out value="${bank.bankName}" />
                                                                        </td>
                                                                        <td>
                                                                            <c:out value="${bank.address}" />
                                                                        </td>
                                                                       
                                                                        <td>
                                                                            <c:out value="${bank.createdOn}" />
                                                                        </td>
                                                                    </tr>
                                                              
                                                            </c:forEach>
                                                        </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
  
  
    <jsp:include page="../../views/includes/footer.jsp" />
        </section>
    </section>
   
  <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>
    <script type="text/javascript">
      (function(i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function() {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-31818910-9', 'auto');
      ga('send', 'pageview');
      
    </script>
    <script>
      (function(f,b){
      	var c;
      	f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
      	f._hjSettings={hjid:46109, hjsv:4};
      	c=b.createElement("script");c.async=1;
      	c.src="//static.hotjar.com/c/hotjar-"+f._hjSettings.hjid+".js?sv="+f._hjSettings.hjsv;
      	b.getElementsByTagName("head")[0].appendChild(c);
      })(window,document);
    </script>
  </body>
</html>