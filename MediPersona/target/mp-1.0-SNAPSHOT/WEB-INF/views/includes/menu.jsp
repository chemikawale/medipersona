
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>  

<aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a></div>
        <div class="l-side-box">
          <!--Logo-->
          <div class="widget-logo logo-in-side">
              <h1><a href="index-2.html"><span class="logo-default visible-default-inline-block"><img src="<c:url value="/img/logo.png"/>" alt="Proteus"></span><span class="logo-medium visible-compact-inline-block"><img src="<c:url value="/img/logo_medium.png"/>" alt="Proteus" title="PROTEUS"></span>
                      <span class="logo-small visible-collapsed-inline-block"><img src="<c:url value="/img/logo_small.png" />" alt="Proteus" title="PROTEUS"></span></a></h1>
          </div>
        </div>
        <!--Main Menu-->
        <div class="l-side-box">
          <!--MAIN NAVIGATION MENU-->
          <nav class="navigation">
            <ul data-ason-type="menu" class="ason-widget">                                     
                    
              <li><a href="/paypad/home/index"><i class="icon fa fa-dashboard"></i><span class="title">DASHBOARD</span></a></li>
              <li class="<c:out value="${bank}" />"><a href="#"><i class="icon fa fa-user-secret"></i><span class="title">BANK</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/bank/allbanks"><span class="title">ALL BANKS</span></a>
                  </li>
                  <li ><a href="/paypad/bank/createbank"><span class="title">ADD BANK</span></a>
                  </li>
                  <li ><a href="/paypad/bank/allbankadmins"><span class="title">ALL BANK ADMINS</span></a>
                  </li>
                  <li ><a href="/paypad/bank/createbankadmin"><span class="title">ADD BANK ADMIN</span></a>
                  </li>
                </ul>
              </li>    
                   
               <li class="<c:out value="${merchant}" />"><a href="#"><i class="icon fa fa-user-times"></i><span class="title">MERCHANT</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/merchant/allmerchants"><span class="title">ALL MERCHANT</span></a>
                  </li>
                  <li ><a href="/paypad/merchant/createmerchant"><span class="title">ADD MERCHANT</span></a>
                  </li>
                  <li ><a href="/paypad/merchant/rechargemerchant"><span class="title">RECHARGE MERCHANT</span></a>
                  </li>
                  <li ><a href="/paypad/merchant/createmerchantadmin"><span class="title">ADD MERCHANT ADMIN</span></a>
                  </li>
                    <li ><a href="/paypad/merchant/alladmins"><span class="title">ALL MERCHANT ADMIN</span></a>
                  </li>
                   <li ><a href="/paypad/posaccount/createpos"><span class="title">ADD POS ACCOUNT</span></a>
                  </li>
                    <li ><a href="/paypad/posaccount/allaccounts"><span class="title">ALL POS ACCOUNTS</span></a>
                  </li>
                </ul>
              </li>
              
               <li class="<c:out value="${support}" />"><a href="#"><i class="icon fa fa-user-times"></i><span class="title">SUPPORT</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/support/createsupport"><span class="title">ADD SUPPORT STAFF</span></a>
                  </li>
                  <li ><a href="/paypad/support/allsupportstaffs"><span class="title">ALL SUPPORT STAFFS</span></a>
                  </li>
                  
                </ul>
              </li>
              <li class="<c:out value="${supers}" />" ><a href="#"><i class="icon fa fa-user-times"></i><span class="title"> SUPER MERCHANT</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/super/createsupermerchant"><span class="title">ADD SUPER MERCHANT</span></a>
                  </li>
                  <li ><a href="/paypad/super/allsupermerchants"><span class="title">ALL SUPER MERCHANTS</span></a>
                  </li>
                  <li ><a href="/paypad/super/createsuperadmin"><span class="title">ADD SUPER MERCHANT ADMIN</span></a>
                  </li>
                    <li ><a href="/paypad/super/allsuperadmins"><span class="title">ALL SUPER MERCHANT ADMIN</span></a>
                  </li>
                   <li ><a href="/paypad/super/createsupermember"><span class="title">ADD SUPER MEMBER</span></a>
                  </li>
                  
                </ul>
              </li>
              
               <li class="<c:out value="${terminal}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">TERMINALS</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/terminal/allterminals"><span class="title">ALL TERMINALS</span></a>
                  </li>
                  <li ><a href="/paypad/terminal/createterminal"><span class="title">REGISTER TERMINAL</span></a>
                  </li>
                </ul>
              </li>
                <li class="<c:out value="${messageActive}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">IN-APP MESSAGING</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/message/sendall"><span class="title">ALL MERCHANTS</span></a>
                  </li>
                  <li ><a href="/paypad/message/sendmerchant"><span class="title">SINGLE MERCHANTS</span></a>
                  </li>
                   <li ><a href="/paypad/message/sendone"><span class="title">SINGLE USER</span></a>
                  </li>
                  </li>
                   <li ><a href="/paypad/message/sentmessages"><span class="title">SENT MESSAGES</span></a>
                  </li>
                </ul>
              </li>
               <li class="<c:out value="${vas}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">VAS</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/paypad/vas/createcategory"><span class="title">CREATE CATEGORY</span></a>
                  </li>
                  <li ><a href="/paypad/vas/allcategory"><span class="title">ALL CATEGORY</span></a>
                  </li>
                      <li><a href="/paypad/vas/createitem"><span class="title">CREATE  ITEM</span></a>
                  </li>
                  <li ><a href="/paypad/vas/allitems"><span class="title">ALL  ITEMS</span></a>
                  </li>
                  <li ><a href="/paypad/vas/createprovider"><span class="title">ADD PROVIDER</span></a>
                  </li>
                  <li ><a href="/paypad/vas/allprovider"><span class="title">ALL  PROVIDERS</span></a>
                  </li>
                   <li ><a href="/paypad/vas/syncprovider"><span class="title">SYNC. PROVIDERS</span></a>
                  </li>
                    <li ><a href="/paypad/vas/attachprovider"><span class="title">ATTACH PROVIDERS</span></a>
                  </li>
                   </li>
                    <li ><a href="/paypad/vas/attachedproviders"><span class="title">ATTACHED PROVIDERS</span></a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </aside>