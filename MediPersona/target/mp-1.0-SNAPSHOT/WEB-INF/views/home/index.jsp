<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>PAYPAD || DASHBOARD </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../../views/includes/nav.jsp" />
          <!-- Row 1 - Info Widgets-->
        <div class="l-row l-spaced-horizontal l-spaced-top">
          <!-- User Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-1 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#user-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="user-info" class="widget-info-details">
                    <div class="info-data open">
                      <h4>1577</h4>
                      <hr>
                      <p><span>Total</span> Customers
                      </p>
                      <div class="progress progress-no-border progress-mini">
                        <div role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;" class="progress-bar"><span class="sr-only">75% Complete</span></div>
                      </div>
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-1-spark-1-c"></div>
                      <div id="customerChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Orders Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom l-clear-sm">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-2 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#order-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="order-info" class="widget-info-details">
                    <div class="info-data open">
                      <h4>2332</h4>
                      <hr>
                      <p><span>Total</span> Orders
                      </p>
                      <div class="progress progress-no-border progress-mini">
                        <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;" class="progress-bar"><span class="sr-only">50% Complete</span></div>
                      </div>
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-2-spark-1-c"></div>
                      <div class="hide info-t-2-spark-2-c"></div>
                      <div class="hide info-t-2-spark-3-c"></div>
                      <div id="ordersChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Reports Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-3 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#report-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="report-info" class="widget-info-details">
                    <div class="info-data open">
                      <h4>4253</h4>
                      <hr>
                      <p><span>Total</span> Reports
                      </p>
                      <div class="progress progress-no-border progress-mini">
                        <div role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;" class="progress-bar"><span class="sr-only">20% Complete</span></div>
                      </div>
                    </div>
                    <div class="info-chart">
                      <div class="pt-10">
                        <div class="hide info-t-3-spark-1-c"></div>
                        <div id="reportsChart"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Payout Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-4 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#payout-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="payout-info" class="widget-info-details">
                    <div class="info-data open">
                      <h4>$32768</h4>
                      <hr>
                      <p><span>Total</span> Payouts
                      </p>
                      <div class="progress progress-no-border progress-mini">
                        <div role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;" class="progress-bar"><span class="sr-only">65% Complete</span></div>
                      </div>
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-4-spark-1-c"></div>
                      <div id="paymentChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     
                  <!-- Row 2 - Stats, Orders, Social-->
        <div class="l-row l-spaced-horizontal">
          <!-- Stats Widget-->
          <div class="l-col-lg-3 l-col-md-4 l-spaced-bottom">
            <div class="widget-stats t-stats-1">
              <ul class="widget-options is-options-right">
                <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-stats" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-stats-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-stats" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
              </ul>
              <div class="widget-stats-info">
                <h4 class="widget-header">Stats</h4>
                <div class="l-row">
                  <div class="l-col-xs-8 stats-data"><span>27826</span>
                    <div>subscribers</div>
                  </div>
                  <div class="l-col-xs-4 stats-change"><span class="text-success">+12%</span>
                    <div>this week</div>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-xs-8 stats-data"><span>12475</span>
                    <div>visitors</div>
                  </div>
                  <div class="l-col-xs-4 stats-change"><span class="text-danger">-8%</span>
                    <div>this week</div>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-xs-8 stats-data"><span>4235</span>
                    <div>comments</div>
                  </div>
                  <div class="l-col-xs-4 stats-change"><span>+25</span>
                    <div>this week</div>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-xs-8 stats-data"><span>728</span>
                    <div>posts</div>
                  </div>
                  <div class="l-col-xs-4 stats-change"><span>+47</span>
                    <div>this week</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Orders Widget-->
          <div class="l-col-lg-6 l-col-md-8 l-clear-md l-spaced-bottom">
            <div class="widget-latest-orders t-latest-orders-1">
              <ul class="widget-options is-options-left">
                <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-latest-orders" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-latest-orders-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-latest-orders" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
              </ul>
              <div class="widget-latest-orders-info">
                <h4 class="widget-header">Latest Orders  </h4>
                  <div class="weekly"><span class="text-success">+5%</span> this week
                  </div>
              
                <table class="table table-hover table-order-header">
                  <thead>
                    <tr>
                      <th class="tb-col-1">#</th>
                      <th class="tb-col-2">Customer Name</th>
                      <th class="tb-col-3">Number</th>
                      <th class="tb-col-4">Average Amount</th>
                      <th class="tb-col-5">Total Amount</th>
                    </tr>
                  </thead>
                </table>
                <div class="table-order-body">
                  <div data-ason-type="scrollbar" data-ason-max-height="200" class="ason-widget">
                    <table class="table table-hover">
                      <tbody>
                        <tr>
                          <td class="tb-col-1">1</td>
                          <td class="tb-col-2">Evelyn Elliott</td>
                          <td class="tb-col-3">2</td>
                          <td class="tb-col-4">$2647</td>
                          <td class="tb-col-5">$5294</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">2</td>
                          <td class="tb-col-2">Austin Powell</td>
                          <td class="tb-col-3">1</td>
                          <td class="tb-col-4">$840</td>
                          <td class="tb-col-5">$840</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">3</td>
                          <td class="tb-col-2">Scott Ward</td>
                          <td class="tb-col-3">4</td>
                          <td class="tb-col-4">$124</td>
                          <td class="tb-col-5">$496</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">4</td>
                          <td class="tb-col-2">Sara Perkins</td>
                          <td class="tb-col-3">3</td>
                          <td class="tb-col-4">$520</td>
                          <td class="tb-col-5">$1560</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">5</td>
                          <td class="tb-col-2">Karen Edwards</td>
                          <td class="tb-col-3">2</td>
                          <td class="tb-col-4">$250</td>
                          <td class="tb-col-5">$500</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">6</td>
                          <td class="tb-col-2">Beverly Hoffman</td>
                          <td class="tb-col-3">10</td>
                          <td class="tb-col-4">$85</td>
                          <td class="tb-col-5">$850</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">7</td>
                          <td class="tb-col-2">Jason Wallace</td>
                          <td class="tb-col-3">6</td>
                          <td class="tb-col-4">$541</td>
                          <td class="tb-col-5">$3246</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">8</td>
                          <td class="tb-col-2">Nathan Alvarez</td>
                          <td class="tb-col-3">12</td>
                          <td class="tb-col-4">$57</td>
                          <td class="tb-col-5">$684</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">9</td>
                          <td class="tb-col-2">Cheryl Carter</td>
                          <td class="tb-col-3">8</td>
                          <td class="tb-col-4">$745</td>
                          <td class="tb-col-5">$5960</td>
                        </tr>
                        <tr>
                          <td class="tb-col-1">10</td>
                          <td class="tb-col-2">Ryan Hudson</td>
                          <td class="tb-col-3">2</td>
                          <td class="tb-col-4">$500</td>
                          <td class="tb-col-5">$1000</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="l-row">
                  <div class="l-col-xs-6"><a href="#" class="view-all"><i class="fa fa-angle-double-down"></i> View All</a></div>
                  <div class="l-col-xs-6">
                    <div class="total">Total:<span>$20430</span></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Social Widget-->
          <div class="l-col-lg-3 l-col-md-12 l-spaced-bottom">
            <div class="l-row">
              <div class="l-col-lg-12 l-col-sm-4">
                <div class="widget-social social-in-box t-social-facebook">
                  <div class="social-icon"><i class="fa fa-facebook"></i></div>
                  <div class="social-info"><span>76547</span> Likes
                  </div>
                </div>
              </div>
              <div class="l-col-lg-12 l-col-sm-4">
                <div class="widget-social social-in-box t-social-twitter">
                  <div class="social-icon"><i class="fa fa-twitter"></i></div>
                  <div class="social-info"><span>34756</span> Followers
                  </div>
                </div>
              </div>
              <div class="l-col-lg-12 l-col-sm-4">
                <div class="widget-social social-in-box t-social-google-plus">
                  <div class="social-icon"><i class="fa fa-google-plus"></i></div>
                  <div class="social-info"><span>54785</span> Followers
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
                          <!-- Row 3 - Revenue Widget-->
        <div class="l-row l-spaced-horizontal">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">Annual Revenue</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="revenueChart"></div>
              </div>
            </div>
          </div>
        </div>
                 <!-- Row 4 - Todo and Maps-->
        <div class="l-row l-spaced-horizontal">
          <div class="l-col-xl-3 l-col-lg-4 l-col-md-5 l-spaced-bottom">
            <!-- Todo Widget-->
            <div class="widget-todo">
              <ul class="widget-options is-options-right">
                <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-todo" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-todo-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-todo" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
              </ul>
              <div class="widget-todo-info">
                <h4 class="widget-header">Todo List</h4>
                <ul data-ason-type="draggable" class="todo-list todocheckbo ason-widget">
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-danger-2">
                      <input type="checkbox" value=""><span class="todo-text">Commit latest changes</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-danger-2">
                      <input type="checkbox" value=""><span class="todo-text">Finish documentation</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-warning-2">
                      <input type="checkbox" value=""><span class="todo-text">Update website</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-info-2">
                      <input type="checkbox" value=""><span class="todo-text">Prepare presentation</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-dark-2">
                      <input type="checkbox" value=""><span class="todo-text">Go to gym</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                  <li class="todo-item ui-drag-item"><a href="#" class="ui-drag-handle"><i class="fa fa-bars"></i></a>
                    <label class="cb-checkbox cb-checkbox-dark-2">
                      <input type="checkbox" value=""><span class="todo-text">Book plain tickets</span>
                    </label><a href="#" data-ason-type="delete" data-ason-target=".todo-item" data-ason-content="true" data-ason-animation="fadeOut" class="delete-todo ason-widget pull-right"></a>
                  </li>
                </ul>
                <div class="todo-input">
                  <div class="input-group">
                    <input type="text" placeholder="Add task..." class="form-control"><span class="input-group-btn">
                      <button type="button" class="btn btn-dark"><i class="fa fa-plus"></i></button></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="l-col-xl-9 l-col-lg-8 l-col-md-7 l-spaced-bottom">
            <div class="widget-members t-members-1">
              <ul class="widget-options is-options-right">
                <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-members" data-ason-loader-class="spinner duo" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-members-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-members" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
              </ul>
              <div class="widget-members-info">
                <h4 class="widget-header">Members</h4>
                <div class="table-responsive">
                  <table class="todocheckbo table table-hover table-striped table-order-header">
                    <thead>
                      <tr>
                        <th class="tb-col-0">
                          <label id="members-all" class="cb-checkbox cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </th>
                        <th class="tb-col-1">#</th>
                        <th class="tb-col-2">Name</th>
                        <th class="tb-col-3">Position</th>
                        <th class="tb-col-4">Status</th>
                        <th class="tb-col-5">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">1</td>
                        <td class="tb-col-2">Kelly Garcia</td>
                        <td class="tb-col-3">Designer</td>
                        <td class="tb-col-4">
                          <div class="label label-danger">Offline</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">2</td>
                        <td class="tb-col-2">Jerry Nelson</td>
                        <td class="tb-col-3">Member</td>
                        <td class="tb-col-4">
                          <div class="label label-success">Online</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">3</td>
                        <td class="tb-col-2">Harold Ellis</td>
                        <td class="tb-col-3">Programmer</td>
                        <td class="tb-col-4">
                          <div class="label label-danger">Offline</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">4</td>
                        <td class="tb-col-2">Michael Hart</td>
                        <td class="tb-col-3">Co-founder</td>
                        <td class="tb-col-4">
                          <div class="label label-danger">Offline</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">5</td>
                        <td class="tb-col-2">Madison Hart</td>
                        <td class="tb-col-3">Member</td>
                        <td class="tb-col-4">
                          <div class="label label-success">Online</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                      <tr class="members-row">
                        <td class="tb-col-0">
                          <label class="cb-checkbox">
                            <input type="checkbox" value="">
                          </label>
                        </td>
                        <td class="tb-col-1">6</td>
                        <td class="tb-col-2">Kelly Oliver</td>
                        <td class="tb-col-3">CEO</td>
                        <td class="tb-col-4">
                          <div class="label label-success">Online</div>
                        </td>
                        <td class="tb-col-5"><a href="#" class="btn btn-dark">Edit</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>      
          <jsp:include page="../../views/includes/footer.jsp" />
      </section>
   
    </section>
   <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <script src="../js/shared/jquery-ui.min.js"></script>
    <script src="../js/shared/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
      // jQuery Ui and Bootstrap conflict workaround
      $.widget.bridge('uitooltip', $.ui.tooltip);
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/charts/c3/c3.min.js"></script>
    <script src="../js/plugins/charts/c3/d3.v3.min.js"></script>
    <script src="../js/plugins/charts/other/jquery.sparkline.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/dashboard.2.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script type="text/javascript">
      (function(i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function() {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-31818910-9', 'auto');
      ga('send', 'pageview');
      
    </script>
    <script>
      (function(f,b){
      	var c;
      	f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
      	f._hjSettings={hjid:46109, hjsv:4};
      	c=b.createElement("script");c.async=1;
      	c.src="//static.hotjar.com/c/hotjar-"+f._hjSettings.hjid+".js?sv="+f._hjSettings.hjsv;
      	b.getElementsByTagName("head")[0].appendChild(c);
      })(window,document);
    </script>
  </body>
    
</html>