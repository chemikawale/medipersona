<%-- 
    Document   : createbank
    Created on : Sep 8, 2015, 11:45:46 AM
    Author     : Oyewale
--%>

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title>PAYPAD || SEND MESSAGE TO ONE </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
   
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
   <jsp:include page="../../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../../views/includes/nav.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">SEND  <span>Message</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>Message</span> Details</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                </ul>
            </div>
            <div class="l-box-body l-spaced">
             
                     <form:form  role="form"  commandName="viewModel" action="sendone" method="POST" class="form-horizontal">
        <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
        
        <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">MERCHANT : </label>
                  <div class="col-sm-5">
                    <form:select id="merchantSelect" class="form-control" path="merchantId" >
                         <c:forEach items="${allMerchants}" var="merchant"> 
                             <form:option value="${merchant.merchantId}" label="${merchant.businessName}" />
                           </c:forEach>
                    </form:select>
                  </div>
                </div>
        
          <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">POS ACCOUNT USERNAME: </label>
                  <div class="col-sm-5">
                      <select id="messageReciever" name="messageReceiver" class="form-control"  >
                        
                    </select>
                  </div>
                </div>
        
                 <div class="form-group">
                   <label class="col-sm-3 control-label" for="messageTitle">TITLE :</label>
                       <div class="col-sm-5">                
                           <form:input class="form-control" id="messageTitle" name="messageTitle" path="messageTitle" type="text" placeholder="MESSAGE TITLE"  required="true" maxlength="20"/>
                         <form:errors path="messageTitle" cssStyle="color : red;"/>       
                       </div>
                       </div>
                 <div class="form-group">
                  <label for="autogrowTextarea" class="col-sm-3 control-label">BODY</label>
                  <div class="col-sm-5">               
                      <form:textarea class="form-control" id="autogrowTextarea" name="messageBody" path="messageBody" rows="3"  placeholder="MESSAGE BODY" required="true"/>
                      <form:errors path="messageBody" cssStyle="color : red;"/>    
                  </div>
                  </div>
                    
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">SEND</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
           <jsp:include page="../../views/includes/footer.jsp" />
      </section>

    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    <script type="text/javascript">
      var paceSemiGeneral = { restartOnPushState: false };
      if (typeof paceSpecific != 'undefined'){
      	var paceOptions = $.extend( {}, paceSemiGeneral, paceSpecific );
      	paceOptions = paceOptions;
      }else{
      	paceOptions = paceSemiGeneral;
      }
      
    </script>
    <script src="/js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.autogrow.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.fancySelect.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.maskedinput.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.onoff.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.select2.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.textareaCounter.plugin.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/form.elements.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script type="text/javascript">
      (function(i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function() {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m)
      })(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
      ga('create', 'UA-31818910-9', 'auto');
      ga('send', 'pageview');
      
    </script>
    <script>
        var ctx = "${pageContext.request.contextPath}";
           $(document).ready(function() {
              
             dropDown = $('#merchantSelect').change(function(){
                 var merchantId = $(this).val();  
                $.ajax({
                   url:  ctx + '/message/getmerchantposaccounts?merchantId='+merchantId,
                   type: 'GET',
                   contentType:"application/json"
               }).done(function(data){
                       var content = "";                   
                                        
                       for(var i = 0; i < data.length; i++){
                           var d = data[i];
                           
                           content += "<option value="+d.username+">"+d.username+"</option>";
                       }                    
                       
                       $('#messageReceiver').html(content);
                   

             });
           });
           
    });
           
           
    </script>
  </body>

</html>