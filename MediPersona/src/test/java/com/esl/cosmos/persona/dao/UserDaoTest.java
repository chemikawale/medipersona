///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.esl.cosmos.persona.dao;
//
//
//import com.medi.cosmos.core.model.Page;
//import com.medi.cosmos.persona.dao.UserDao;
//import com.medi.cosmos.persona.dataview.model.ViewCustomVitalSigns;
//import com.medi.cosmos.persona.dataview.model.ViewMedication;
//import com.medi.cosmos.persona.dataview.model.ViewReviewRequest;
//import com.medi.cosmos.persona.dataview.model.ViewUser;
//import com.medi.cosmos.persona.dataview.model.ViewVitalSigns;
//import com.medi.cosmos.persona.model.CountOutput;
//import com.medi.cosmos.persona.model.Medication;
//import com.medi.cosmos.persona.model.ReviewRequest;
//import com.medi.cosmos.persona.model.UpdateUser;
//import com.medi.cosmos.persona.model.UserModel;
//import com.medi.cosmos.persona.model.VitalSigns;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.jdbc.JdbcTestUtils;
//
///**
// *
// * @author Oyewale
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
//public class UserDaoTest {
//    @Autowired
//    private UserDao userDao;
//    
//    public UserDaoTest() {
//    }
//    public void deleteTestUser() {
//      JdbcTestUtils.deleteFromTableWhere(userDao.getJdbcTemplate(), "vital_signs", "readingId = '" + getVitalSigns().getReadingId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(userDao.getJdbcTemplate(), "medi_review_request", "reviewcode = '" + getRequest().getReviewcode()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(userDao.getJdbcTemplate(), "user_admins", "username = '" + getUser().getUsername()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(userDao.getJdbcTemplate(), "medications", "medicationId = '" + getMedication().getMedicationId()+ "'");
//     
//    }
//    @Before
//    public void setUp() {
//       deleteTestUser();
//       System.out.println("calling @ before");
//    }
//    
//    @After
//    public void tearDown() {
//            deleteTestUser();
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    public VitalSigns getVitalSigns(){
//      VitalSigns getSigns = new VitalSigns();
//      getSigns.setBmi("23.3");
//      getSigns.setCreatedOn(new Date());
//      getSigns.setDiastolic("73");
//      getSigns.setHeight("1.3");
//      getSigns.setPulse("22.3");
//      getSigns.setReadingId("3434");
//      getSigns.setRespiration("343");
//      getSigns.setSystolic("34");
//      getSigns.setTemperature("43");
//      getSigns.setUsername("admins");
//      getSigns.setWeight("88");
//
//    
//       return getSigns;
//    }
//
//    public UserModel getUser(){
//         UserModel user = new UserModel();
//          user.setCreatedOn(new Date());
//          user.setCountryId("2722");
//          user.setEmail("oye@email.com");
//          user.setEnabled(true);
//          user.setFirstName("Adekilekun");
//          user.setLastName("Abayomi");
//          user.setPassword("numerics");
//          user.setPhoneNumber("08080719926");
//          user.setUsername("chemika");
//     return user;
//    }
//    
//    
//    public UpdateUser getUpdateUser(){
//    UpdateUser user = new UpdateUser();
//    
//          user.setLastEdit(new Date());
//          user.setEmail("oye@oye.com");
//          user.setFirstName("Adebola");
//          user.setLastName("Ayinke");
//          user.setPhoneNumber("08139578794");
//          user.setUsername("chemika");
//    return user;
//    }
//    
//     
//    public ReviewRequest getRequest(){
//    ReviewRequest request = new ReviewRequest();
//      request.setCreatedOn(new Date());
//        request.setEmail("danday006@gmail.com");
//        request.setEnabled(true);
//        request.setExpiryDate(new Date());
//        request.setPhonenumber("08139578794");
//        request.setReviewcode("23434322c");
//        request.setUsername("ädmin");
//        request.setStatus(true);
//        request.setRole("Medical Doctor");
//        request.setFirstName("Adeola");
//        request.setLastName("Adebola");
//      return request;
//    }
//     
//    public Medication getMedication(){
//    Medication medicalData = new Medication();
//     medicalData.setCreatedOn(new Date());
//     medicalData.setDiagnosis("HEAD ACHE");
//     medicalData.setEndDate(new Date());
//     medicalData.setMedication("PARACETAMOL");
//     medicalData.setMedicationId("9748749504");
//     medicalData.setPrescription("Twice A Day");
//     medicalData.setPrescriptionNote("Try to take it with hot water");
//     medicalData.setUsername("admin");
//     medicalData.setStartDate(new Date());
//   return medicalData;  
//    }
//    /**
//     * Test of logVitalSigns method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testLogVitalSigns() throws Exception {
//        System.out.println("logVitalSigns");   
//        VitalSigns result = userDao.logVitalSigns(getVitalSigns());
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findVitalSignsByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindVitalSignsByUsername() throws Exception {
//        System.out.println("findVitalSignsByUsername");
//        String username = getVitalSigns().getUsername();
//        userDao.logVitalSigns(getVitalSigns());
//        int start = 0;
//        int length = 10;
//        Page<ViewVitalSigns> result = userDao.findVitalSignsByUsername(username,start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllLoggedVitalSigns method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllLoggedVitalSigns() throws Exception {
//        System.out.println("getAllLoggedVitalSigns");
//        int start = 0;
//        int length = 10;
//        userDao.logVitalSigns(getVitalSigns());
//        Page<ViewVitalSigns> result = userDao.getAllLoggedVitalSigns(start, length);
//        assertNotNull(result);
//    }
//
//
//
//    /**
//     * Test of logReviewRequest method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testLogReviewRequest() throws Exception {
//        System.out.println("logReviewRequest");
//        ReviewRequest model = getRequest();      
//        ReviewRequest result = userDao.logReviewRequest(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findReviewRequestByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewRequestByUsername() throws Exception {
//        System.out.println("findReviewRequestByUsername");
//        String username = getRequest().getUsername();
//       userDao.logReviewRequest(getRequest());
//        int start = 0;
//        int length = 10;
//        Page<ViewReviewRequest> result = userDao.findReviewRequestsByUsername(username,start,length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findActiveReviewRequestByReviewCode method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindActiveReviewRequestByReviewCode() throws Exception {
//        System.out.println("findActiveReviewRequestByReviewCode");
//        String reviewCode = getRequest().getReviewcode();
//       userDao.logReviewRequest(getRequest());
//        ViewReviewRequest result = userDao.findActiveReviewRequestByReviewCode(reviewCode);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of deprecateReviewRequest method, of class UserDao.
//     */
//    @Test
//    public void testDeprecateReviewRequest() throws Exception {
//        System.out.println("deprecateReviewRequest");
//      userDao.logReviewRequest(getRequest());
//        userDao.deprecateReviewRequests();
//        
//    }
//
//    /**
//     * Test of getAllReviewRequest method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllReviewRequest() throws Exception {
//        System.out.println("getAllReviewRequest");
//        int start = 0;
//        int length = 10;
//        userDao.logReviewRequest(getRequest());
//        Page<ViewReviewRequest> result = userDao.getAllReviewRequest(start, length);
//        assertNotNull(result);
//    }
//
//  
//    /**
//     * Test of findReviewRequestsByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewRequestsByUsername() throws Exception {
//        System.out.println("findReviewRequestsByUsername");
//        String username = getRequest().getUsername();
//        int start = 0;
//        int length = 10;
//      userDao.logReviewRequest(getRequest());
//        Page<ViewReviewRequest> result = userDao.findReviewRequestsByUsername(username, start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of UpdateUsedReviewCode method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testUpdateUsedReviewCode() throws Exception {
//        System.out.println("UpdateUsedReviewCode");
//        String reviewCode = getRequest().getReviewcode();
//       userDao.logReviewRequest(getRequest());
//        boolean result = userDao.UpdateUsedReviewCode(reviewCode);
//        assertTrue(result);
//    }
//
//    /**
//     * Test of createUserAdmin method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateUserAdmin() throws Exception {
//        System.out.println("createUserAdmin");
//        UserModel model = getUser();
//        UserModel result = userDao.createUserAdmin(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of deleteUserAdminByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeleteUserAdminByUsername() throws Exception {
//        System.out.println("deleteUserAdminByUsername");
//        String username = getUser().getUsername();
//         userDao.createUserAdmin(getUser());
//         boolean result = userDao.deleteUserAdminByUsername(username);
//        assertTrue(result);
//    }
//
//    /**
//     * Test of findRecentVitalSignsByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindRecentVitalSignsByUsername() throws Exception {
//        System.out.println("findRecentVitalSignsByUsername");
//        String username = getVitalSigns().getUsername();
//          userDao.logVitalSigns(getVitalSigns());
//        List<ViewVitalSigns> result = userDao.findRecentVitalSignsByUsername(username);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getUserActivityCount method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetUserActivityCount() throws Exception {
//        System.out.println("getUserActivityCount");
//        userDao.logReviewRequest(getRequest());
//        CountOutput result = userDao.getUserActivityCount(getRequest().getUsername());
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of deleteReviewRequest method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeleteReviewRequest() throws Exception {
//        System.out.println("deleteReviewRequest");
//        userDao.logReviewRequest(getRequest());
//        userDao.deleteReviewRequest(getRequest().getReviewcode());
//       
//    }
//
//    /**
//     * Test of deprecateReviewRequests method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeprecateReviewRequests() throws Exception {
//        System.out.println("deprecateReviewRequests");
//        userDao.logReviewRequest(getRequest());
//        userDao.deprecateReviewRequests();
//      }
//
//    /**
//     * @throws java.lang.Exception
//     * Test of updateUserAdmin method, of class UserDao.
//     */
//    @Test
//    public void testUpdateUserAdmin() throws Exception {
//        System.out.println("updateUserAdmin");
//        userDao.createUserAdmin(getUser());
//        UpdateUser model = getUpdateUser();
//        UpdateUser result = userDao.updateUserAdmin(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findUserByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindUserByUsername() throws Exception {
//        System.out.println("findUserByUsername");
//        userDao.createUserAdmin(getUser());
//        String username = getUser().getUsername();
//        ViewUser result = userDao.findUserByUsername(username);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findLastTenVitalSignsByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindLastTenVitalSignsByUsername() throws Exception {
//        System.out.println("findLastTenVitalSignsByUsername");
//        String username = getVitalSigns().getUsername();
//         userDao.logVitalSigns(getVitalSigns());
//        List<ViewCustomVitalSigns> result = userDao.findLastTenVitalSignsByUsername(username);
//        assertNotNull(result);
//    }
//    /**
//     * Test of saveMedication method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testSaveMedication() throws Exception {
//        System.out.println("saveMedication");
//        Medication model = getMedication();
//        Medication result = userDao.saveMedication(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findMedicationsByUsername method, of class UserDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindMedicationsByUsername() throws Exception {
//        System.out.println("findMedicationsByUsername");
//        String username = getMedication().getUsername();
//        int start = 0;
//        int length = 10;
//        userDao.saveMedication(getMedication());
//        Page<ViewMedication> result = userDao.findMedicationsByUsername(username, start, length);
//        assertNotNull(result);
//    }
//
//
//  
//    
//}
