///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.esl.cosmos.persona.dao;
//
//
//import com.medi.cosmos.core.model.Page;
//import com.medi.cosmos.persona.dao.ReviewDao;
//import com.medi.cosmos.persona.dataview.model.ViewReview;
//import com.medi.cosmos.persona.model.ReviewModel;
//import java.util.Date;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.jdbc.JdbcTestUtils;
//
///**
// *
// * @author Oyewale
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
//public class ReviewDaoTest {
//    @Autowired
//    private ReviewDao reviewDao;
//    public ReviewDaoTest() {
//    }
//    public void deleteTestUser() {
//       JdbcTestUtils.deleteFromTableWhere(reviewDao.getJdbcTemplate(), "reviews", "reviewId = '" + getReview().getReviewId()+ "'");
//     
//    }
//    @Before
//    public void setUp() {
//       deleteTestUser();
//       System.out.println("calling @ before");
//    }
//    
//    @After
//    public void tearDown() {
//            deleteTestUser();
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//     
//    public ReviewModel getReview(){
//    ReviewModel review = new ReviewModel();
//     review.setComments("Try to give yourself to at least 10 hours of good sleep per day");
//     review.setCreatedOn(new Date());
//     review.setDiagnosis("HeadAche");
//     review.setLastEdit(new Date());
//     review.setRating("5");
//     review.setRecommendation("Try to use take to dose of anti-malaria");
//     review.setReviewId("93933");
//     review.setReviewer("Ädebola Kunloye");
//     review.setUsername("admin");
//    return review;
//    }
//    
//    /**
//     * Test of logReview method, of class ReviewDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testLogReview() throws Exception {
//        System.out.println("logReview");
//        ReviewModel model = getReview();
//         ReviewModel result = reviewDao.logReview(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findReviewsForUsername method, of class ReviewDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewsForUsername() throws Exception {
//        System.out.println("findReviewsForUsername");
//        int start = 0;
//        int length = 10;
//        reviewDao.logReview(getReview());
//        String username = getReview().getUsername();
//        Page<ViewReview> result = reviewDao.findReviewsForUsername(start, length, username);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findReviewerReviews method, of class ReviewDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewerReviews() throws Exception {
//        System.out.println("findReviewerReviews");
//        int start = 0;
//        int length = 10;
//        String reviewer = getReview().getReviewer();
//        reviewDao.logReview(getReview());
//        Page<ViewReview> result = reviewDao.findReviewerReviews(start, length, reviewer);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllReviews method, of class ReviewDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllReviews() throws Exception {
//        System.out.println("getAllReviews");
//        int start = 0;
//        int length = 10;
//        reviewDao.logReview(getReview());
//        Page<ViewReview> result = reviewDao.getAllReviews(start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findReviewById method, of class ReviewDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewById() throws Exception {
//        System.out.println("findReviewById");
//        String reviewId = getReview().getReviewId();
//        reviewDao.logReview(getReview());
//        ViewReview result = reviewDao.findReviewById(reviewId);
//        assertNotNull(result);
//    }
//
//    
//}
