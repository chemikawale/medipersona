///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.esl.cosmos.persona.dao;
//
//import com.medi.cosmos.persona.dao.ReviewerDao;
//import com.medi.cosmos.persona.dataview.model.ViewReviewer;
//import com.medi.cosmos.persona.model.ReviewerModel;
//import java.util.Date;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.jdbc.JdbcTestUtils;
//
///**
// *
// * @author Oyewale
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
//public class ReviewerDaoTest {
//    @Autowired
//    private ReviewerDao reviewerDao;
//    
//    public ReviewerDaoTest() {
//    }
//    public void deleteTestUser() {
//       JdbcTestUtils.deleteFromTableWhere(reviewerDao.getJdbcTemplate(), "reviewer", "username = '" + getReviewer().getUsername()+ "'");
//     
//    }
//    @Before
//    public void setUp() {
//       deleteTestUser();
//       System.out.println("calling @ before");
//    }
//    
//    @After
//    public void tearDown() {
//            deleteTestUser();
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    public ReviewerModel getReviewer(){
//         ReviewerModel user = new ReviewerModel();
//          user.setCreatedOn(new Date());
//          user.setCountryId("2722");
//          user.setEmail("oye@email.com");
//          user.setEnabled(true);
//          user.setFirstName("Adekilekun");
//          user.setLastName("Abayomi");
//          user.setPassword("numerics");
//          user.setUsername("chemika");
//     return user;
//    }
//
//    /**
//     * Test of createReviewer method, of class ReviewerDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateReviewer() throws Exception {
//        System.out.println("createReviewer");
//        ReviewerModel model = getReviewer();
//        ReviewerModel result = reviewerDao.createReviewer(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findReviewerByUsername method, of class ReviewerDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindReviewerByUsername() throws Exception {
//        System.out.println("findReviewerByUsername");
//        String username = getReviewer().getUsername();
//        reviewerDao.createReviewer(getReviewer());
//        ViewReviewer result = reviewerDao.findReviewerByUsername(username);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of deleteReviewerByUsername method, of class ReviewerDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeleteReviewerByUsername() throws Exception {
//        System.out.println("deleteReviewerByUsername");
//        String username = getReviewer().getUsername();
//        boolean result = reviewerDao.deleteReviewerByUsername(username);
//        assertNotNull(result);
//    }
//
//    
//}
