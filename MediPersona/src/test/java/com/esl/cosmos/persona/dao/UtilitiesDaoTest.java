///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.esl.cosmos.persona.dao;
//
//import com.medi.cosmos.core.model.Page;
//import com.medi.cosmos.persona.dao.UtilitiesDao;
//import com.medi.cosmos.persona.dataview.model.ViewAssignedPlan;
//import com.medi.cosmos.persona.dataview.model.ViewCategory;
//import com.medi.cosmos.persona.dataview.model.ViewCountry;
//import com.medi.cosmos.persona.dataview.model.ViewFeedBack;
//import com.medi.cosmos.persona.dataview.model.ViewHmo;
//import com.medi.cosmos.persona.dataview.model.ViewHmoPlan;
//import com.medi.cosmos.persona.dataview.model.ViewMediFacility;
//import com.medi.cosmos.persona.dataview.model.ViewState;
//import com.medi.cosmos.persona.model.AssignHmoPlan;
//import com.medi.cosmos.persona.model.Category;
//import com.medi.cosmos.persona.model.Country;
//import com.medi.cosmos.persona.model.FeedBack;
//import com.medi.cosmos.persona.model.Hmo;
//import com.medi.cosmos.persona.model.HmoPlan;
//import com.medi.cosmos.persona.model.MediFacility;
//import com.medi.cosmos.persona.model.MediState;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.jdbc.JdbcTestUtils;
//
///**
// *
// * @author Oyewale
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
//
//public class UtilitiesDaoTest {
//    
//    @Autowired 
//    private UtilitiesDao utilitiesDao;
//    
//    public UtilitiesDaoTest() {
//    }
//    
//                
//    public void deleteTestUser() {
//      JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_countries", "countryId = '" + getCountry().getCountryId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_states", "stateId = '" + getState().getStateId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_categories", "categoryId = '" + getCategory().getCategoryId()+ "'");
//    JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_feedback", "feedBackId = '" + getFeedBack().getFeedBackId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_facilities", "facilityId = '" + getMedFacility().getFacilityId()+ "'");
//  JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_hmo", "hmoId = '" + getHmo().getHmoId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "medi_hmo_plan", "planId = '" + getHmoPlan().getPlanId()+ "'");
//    JdbcTestUtils.deleteFromTableWhere(utilitiesDao.getJdbcTemplate(), "assigned_hmo_plan", "assignId = '" + getAssign().getAssignId()+ "'");
//  
//    }
//   
//    @Before
//    public void setUp() {
//       deleteTestUser();
//       System.out.println("calling @ before");
//    }
//    
//    @After
//    public void tearDown() {
//            deleteTestUser();
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    public Country getCountry(){
//         Country country = new Country();
//            country.setCountryId("12345");
//            country.setCountryName("Ibinu Nation");
//            country.setCreatedOn(new Date());           
//         return country;
//    
//    }
//
//    public MediState getState(){
//    MediState state = new MediState();
//        state.setCountryId("12345");
//        state.setCreatedOn(new Date());
//        state.setStateId("11111");
//        state.setStateName("LAGELU");
//    return state;
//    }
//    
//     public Category getCategory(){
//      Category toCategory = new Category();
//       toCategory.setCategoryId("2323");
//       toCategory.setCategoryName("PHARMACOLOGY");
//       toCategory.setCreatedOn(new Date());
//        return toCategory;
//                
//     }
//    
//      public FeedBack getFeedBack(){
//       FeedBack toFeed = new FeedBack();
//       toFeed.setBody("The UI is really not intuitive");
//       toFeed.setCreatedOn(new Date());
//       toFeed.setFeedBackId("243434");
//       toFeed.setStatus(false);
//       toFeed.setTitle("UI error");
//       
//       return toFeed;    
//      
//      }
//      
//      public MediFacility getMedFacility(){
//           MediFacility toFacility = new MediFacility();
//            toFacility.setAddress("Olodo, Ibadan");
//            toFacility.setCategoryId("12132000");
//            toFacility.setContactPerson("adeyanJu Ibukun");
//            toFacility.setCountryId("343400");
//            toFacility.setCreatedOn(new Date());
//            toFacility.setFacilityName("OLODO HEATH CENTER");
//            toFacility.setLatitude("13.288899");
//            toFacility.setLongitude("33.83939330");
//            toFacility.setServices("Clinic, Diagnostics");
//            toFacility.setStateId("2324s");
//            toFacility.setVerified(false);
//            toFacility.setFacilityId("ywy999");
//           return toFacility;                   
//                  
//      
//      }
//      
//      public Hmo getHmo(){
//      Hmo hmo = new Hmo();
//      hmo.setAddress("yaba, Sabo, Lagos");
//      hmo.setCountryId("1244");
//      hmo.setCreatedOn(new Date());
//      hmo.setEmail("oye@oye.com");
//      hmo.setHmoId("2434");
//      hmo.setHmoName("MEDIALE");
//      
//      return hmo;
//      }
//      
//      public HmoPlan getHmoPlan(){
//       HmoPlan plan = new HmoPlan();
//            plan.setCreatedOn(new Date());
//            plan.setDescription("The highest plan");
//            plan.setPlanId("3434");
//            plan.setPlanName("PREMIUM");
//       return plan;
//      }
//      
//      public AssignHmoPlan getAssign(){
//       AssignHmoPlan assign = new AssignHmoPlan();
//        assign.setAssignId("243343");
//        assign.setCreatedOn(new Date());
//        assign.setHmoId(getHmo().getHmoId());
//        assign.setHmoName(getHmo().getHmoName());
//        assign.setPlanId(getHmoPlan().getPlanId());
//        assign.setPlanName(getHmoPlan().getPlanName());
//      return assign;
//      }
//    /**
//     * Test of createCountry method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateCountry() throws Exception {
//        System.out.println("createCountry");
//        Country model = getCountry();
//        Country result = utilitiesDao.createCountry(model);
//        assertNotNull(result);
//        
//    }
//
//    /**
//     * Test of getAllMediCountries method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllMediCountries() throws Exception {
//        System.out.println("getAllMediCountries");
//        utilitiesDao.createCountry(getCountry());
//        List<ViewCountry> expResult = null;
//        List<ViewCountry> result = utilitiesDao.getAllMediCountries();
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findCountryById method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindCountryById() throws Exception {
//        System.out.println("findCountryById");
//       utilitiesDao.createCountry(getCountry());
//   
//        ViewCountry result = utilitiesDao.findCountryById(getCountry().getCountryId());
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of createState method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateState() throws Exception {
//        System.out.println("createState");
//        MediState model = getState();
//        MediState result = utilitiesDao.createState(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediStates method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllMediStates() throws Exception {
//        System.out.println("getAllMediStates");
//      utilitiesDao.createState(getState());
//        List<ViewState> result = utilitiesDao.getAllMediStates();
//        assertNotNull(result);
//        
//    }
//
//    /**
//     * Test of findStatesByCountryId method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindStatesByCountryId() throws Exception {
//        System.out.println("findStatesByCountryId");
//        String countryId = getState().getCountryId();
//       utilitiesDao.createState(getState());
//        List<ViewState> result = utilitiesDao.findStatesByCountryId(countryId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findStateById method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindStateById() throws Exception {
//        System.out.println("findStateById");
//        utilitiesDao.createState(getState());
//        String stateId = getState().getStateId();
//        ViewState result = utilitiesDao.findStateById(stateId);
//        assertNotNull(result);
//    }
//    /**
//     * Test of createCategory method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateCategory() throws Exception {
//        System.out.println("createCategory");
//        Category model = getCategory();       
//        Category result = utilitiesDao.createCategory(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediCategories method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllMediCategories() throws Exception {
//        System.out.println("getAllMediCategories");
//        Category model = getCategory();       
//         utilitiesDao.createCategory(model);
//        List<ViewCategory> result = utilitiesDao.getAllMediCategories();
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findCategoryById method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindCategoryById() throws Exception {
//        System.out.println("findCategoryById");
//        String categoryId = getCategory().getCategoryId();
//        Category model = getCategory();       
//       utilitiesDao.createCategory(model);
//        ViewCategory result = utilitiesDao.findCategoryById(categoryId);
//        assertNotNull(result);
//    }
//
//
//    /**
//     * Test of logFee
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testLogFeedBack() throws Exception {
//        System.out.println("logFeedBack");
//        FeedBack model = getFeedBack(); 
//        FeedBack result = utilitiesDao.logFeedBack(model);
//        assertNotNull(result);
//                
//    }
//
//    /**
//     * Test of getAllLoggedFeedBack method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllLoggedFeedBack() throws Exception {
//        System.out.println("getAllLoggedFeedBack");
//        int start = 0;
//        int length = 10;
//        utilitiesDao.logFeedBack(getFeedBack());
//        Page<ViewFeedBack> result = utilitiesDao.getAllLoggedFeedBack(start, length);
//        assertNotNull(result);
//    }
//
//   
//
//    /**
//     * Test of createMediFacility method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testCreateMediFacility() throws Exception {
//        System.out.println("createMediFacility");
//        MediFacility model = getMedFacility();       
//        MediFacility result = utilitiesDao.createMediFacility(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediFacilities method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllMediFacilities() throws Exception {
//        System.out.println("getAllMediFacilities");
//        int start = 0;
//        int length = 10;
//        utilitiesDao.createMediFacility(getMedFacility());
//        Page<ViewMediFacility> result = utilitiesDao.getAllMediFacilities(start, length);
//        assertNotNull(result);
//    }
//
//   
//    /**
//     * Test of createHmoPlan method, of class UtilitiesDao.
//     */
//    @Test
//    public void testCreateHmoPlan() throws Exception {
//        System.out.println("createHmoPlan");
//        HmoPlan model = getHmoPlan();
//         HmoPlan result = utilitiesDao.createHmoPlan(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediHmoPlans method, of class UtilitiesDao.
//     */
//    @Test
//    public void testGetAllMediHmoPlans() throws Exception {
//        System.out.println("getAllMediHmoPlans");
//       utilitiesDao.createHmoPlan(getHmoPlan());
//        List<ViewHmoPlan> result = utilitiesDao.getAllMediHmoPlans();
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findHmoPlanById method, of class UtilitiesDao.
//     */
//    @Test
//    public void testFindHmoPlanById() throws Exception {
//        System.out.println("findHmoPlanById");
//        String planId = getHmoPlan().getPlanId();
//       utilitiesDao.createHmoPlan(getHmoPlan());
//        ViewHmoPlan result = utilitiesDao.findHmoPlanById(planId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of createHmo method, of class UtilitiesDao.
//     */
//    @Test
//    public void testCreateHmo() throws Exception {
//        System.out.println("createHmo");
//        Hmo model = getHmo();       
//        Hmo result = utilitiesDao.createHmo(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediHmos method, of class UtilitiesDao.
//     */
//    @Test
//    public void testGetAllMediHmos() throws Exception {
//        System.out.println("getAllMediHmos");
//        int start = 0;
//        int length = 10;
//        utilitiesDao.createHmo(getHmo());
//        Page<ViewHmo> result = utilitiesDao.getAllMediHmos(start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllMediHmo method, of class UtilitiesDao.
//     */
//    @Test
//    public void testGetAllMediHmo() throws Exception {
//        System.out.println("getAllMediHmo");
//            utilitiesDao.createHmo(getHmo());
//        List<ViewHmo> result = utilitiesDao.getAllMediHmo();
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findHmoById method, of class UtilitiesDao.
//     */
//    @Test
//    public void testFindHmoById() throws Exception {
//        System.out.println("findHmoById");
//        String hmoId = getHmo().getHmoId();
//       utilitiesDao.createHmo(getHmo());
//        ViewHmo result = utilitiesDao.findHmoById(hmoId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findHmoByCountryId method, of class UtilitiesDao.
//     */
//    @Test
//    public void testFindHmoByCountryId() throws Exception {
//        System.out.println("findHmoByCountryId");
//        String countryId = getHmo().getCountryId();
//        utilitiesDao.createHmo(getHmo());
//        
//        ViewHmo result = utilitiesDao.findHmoByCountryId(countryId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of AssignHmoPlan method, of class UtilitiesDao.
//     */
//    @Test
//    public void testAssignHmoPlan() throws Exception {
//        System.out.println("AssignHmoPlan");
//        AssignHmoPlan model = getAssign();
//         AssignHmoPlan result = utilitiesDao.AssignHmoPlan(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findAssignedPlanByHmoId method, of class UtilitiesDao.
//     */
//    @Test
//    public void testFindAssignedPlanByHmoId() throws Exception {
//        System.out.println("findAssignedPlanByHmoId");
//        String hmoId = getAssign().getHmoId();
//       utilitiesDao.AssignHmoPlan(getAssign());
//        List<ViewHmo> result = utilitiesDao.findAssignedPlanByHmoId(hmoId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findAssignedPlanByPlanId method, of class UtilitiesDao.
//     */
//    @Test
//    public void testFindAssignedPlanByPlanId() throws Exception {
//        System.out.println("findAssignedPlanByPlanId");
//        String planId = getAssign().getPlanId();
//        utilitiesDao.AssignHmoPlan(getAssign());
//        List<ViewHmo> result = utilitiesDao.findAssignedPlanByPlanId(planId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllAssignedHmoPlan method, of class UtilitiesDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllAssignedHmoPlan() throws Exception {
//        System.out.println("getAllAssignedHmoPlan");
//        int start = 0;
//        int length = 10;
//       utilitiesDao.AssignHmoPlan(getAssign());
//        Page<ViewAssignedPlan> result = utilitiesDao.getAllAssignedHmoPlan(start, length);
//        assertNotNull(result);
//    }
//
//    
//}
