///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.esl.cosmos.persona.dao;
//
//
//import com.medi.cosmos.core.model.Page;
//import com.medi.cosmos.persona.dao.MedicalDataDao;
//import com.medi.cosmos.persona.dataview.model.ViewAllergy;
//import com.medi.cosmos.persona.dataview.model.ViewAllergyCategory;
//import com.medi.cosmos.persona.dataview.model.ViewBloodDetails;
//import com.medi.cosmos.persona.model.Allergy;
//import com.medi.cosmos.persona.model.AllergyCategory;
//import com.medi.cosmos.persona.model.BloodDetails;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.jdbc.JdbcTestUtils;
//
///**
// *
// * @author Oyewale
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
//public class MedicalDataDaoTest {
//    
//    @Autowired
//    private MedicalDataDao medicalDao;
//    
//    public MedicalDataDaoTest() {
//    }
//    
//              
//    public void deleteTestUser() {
//      JdbcTestUtils.deleteFromTableWhere(medicalDao.getJdbcTemplate(), "blood_details", "username = '" + getBloodDetails().getUsername()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(medicalDao.getJdbcTemplate(), "allergy_categories", "categoryId = '" + getAllergyCategory().getCategoryId()+ "'");
//      JdbcTestUtils.deleteFromTableWhere(medicalDao.getJdbcTemplate(), "allergies", "entryId = '" + getAllergy().getEntryId()+ "'");
//   
//    }
//   
//    @Before
//    public void setUp() {
//       deleteTestUser();
//       System.out.println("calling @ before");
//    }
//    
//    @After
//    public void tearDown() {
//            deleteTestUser();
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//
//    public BloodDetails getBloodDetails(){
//         BloodDetails details = new BloodDetails();
//            details.setBloodGroup("O+");
//            details.setGenotype("AA");
//            details.setCreatedOn(new Date());
//            details.setRhesusD("AB");
//            details.setUsername("admin");
//         return details;
//    
//    }
//    
//    public AllergyCategory getAllergyCategory(){
//     AllergyCategory category = new AllergyCategory();
//      category.setCategoryId("2344");
//      category.setCategoryName("CATEGORY ONE");
//      category.setCreatedOn(new Date());
//    return category;
//    }
// 
// public Allergy getAllergy(){
// Allergy allergy = new Allergy();
//    allergy.setActive(true);
//    allergy.setAllergicTo("HOT DRINK");
//    allergy.setCategoryId("2344");
//    allergy.setComment("Sometime it does not happen");
//    allergy.setCreatedOn(new Date());
//    allergy.setEntryId("1222");
//    allergy.setLocation("upper jaw");
//    allergy.setReaction("Heartburn");
//    allergy.setSeverity("MILD");
//    allergy.setStarted("NOT KNOWN");
//    allergy.setUsername("admin");
// return allergy;
// }
//    
//    /**
//     * Test of logBloodDetails method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testLogBloodDetails() throws Exception {
//        System.out.println("logBloodDetails");
//        BloodDetails model = getBloodDetails();
//        BloodDetails result = medicalDao.logBloodDetails(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findBloodDetailsByUsername method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindBloodDetailsByUsername() throws Exception {
//        System.out.println("findBloodDetailsByUsername");
//        String username = getBloodDetails().getUsername();
//        medicalDao.logBloodDetails(getBloodDetails());
//        ViewBloodDetails result = medicalDao.findBloodDetailsByUsername(username);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of deleteBloodDetailsByUsername method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeleteBloodDetailsByUsername() throws Exception {
//        System.out.println("deleteBloodDetailsByUsername");
//        String username = getBloodDetails().getUsername();
//          medicalDao.logBloodDetails(getBloodDetails());
//        boolean result = medicalDao.deleteBloodDetailsByUsername(username);
//        assertTrue(result);
//    }
//
//    /**
//     * Test of getAllBloodDetails method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllBloodDetails() throws Exception {
//        System.out.println("getAllBloodDetails");
//        int start = 0;
//        int length = 10;
//          medicalDao.logBloodDetails(getBloodDetails());
//        Page<ViewBloodDetails> result = medicalDao.getAllBloodDetails(start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of addAllergyCategory method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testAddAllergyCategory() throws Exception {
//        System.out.println("addAllergyCategory");
//        AllergyCategory model = getAllergyCategory();
//       AllergyCategory result = medicalDao.addAllergyCategory(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findAllergyCategoryById method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindAllergyCategoryById() throws Exception {
//        System.out.println("findAllergyCategoryById");
//        String categoryId = getAllergyCategory().getCategoryId();
//        medicalDao.addAllergyCategory(getAllergyCategory());
//        ViewAllergyCategory result = medicalDao.findAllergyCategoryById(categoryId);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllAllergyCategories method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllAllergyCategories() throws Exception {
//        System.out.println("getAllAllergyCategories");
//        medicalDao.addAllergyCategory(getAllergyCategory());
//        List<ViewAllergyCategory> result = medicalDao.getAllAllergyCategories();
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of getAllAlllergies method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllAllergies() throws Exception {
//        System.out.println("getAllAlllergies");
//        int start = 0;
//        int length = 10;
//        medicalDao.addAllergy(getAllergy());
//        Page<ViewAllergy> result = medicalDao.getAllAllergies(start, length);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of addAllergy method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testAddAllergy() throws Exception {
//        System.out.println("addAllergy");
//        Allergy model = getAllergy();
//        Allergy result = medicalDao.addAllergy(model);
//        assertNotNull(result);
//    }
//
//    /**
//     * Test of findAllergyByEntryId method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testFindAllergyByEntryId() throws Exception {
//        System.out.println("findAllergyByEntryId");
//        String entryId = getAllergy().getEntryId();
//        medicalDao.addAllergy(getAllergy());
//         ViewAllergy result = medicalDao.findAllergyByEntryId(entryId);
//        assertNotNull(result);
//    }
//
//   
//    /**
//     * Test of deleteAllergyByEntryId method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testDeleteAllergyByEntryId() throws Exception {
//        System.out.println("deleteAllergyByEntryId");
//        String entryId = getAllergy().getEntryId();
//        medicalDao.addAllergy(getAllergy());
//        boolean result = medicalDao.deleteAllergyByEntryId(entryId);
//        assertTrue(result);
//    }
//
// 
//
//    /**
//     * Test of getAllUserAllergies method, of class MedicalDataDao.
//     * @throws java.lang.Exception
//     */
//    @Test
//    public void testGetAllUserAllergies() throws Exception {
//        System.out.println("getAllUserAllergies");
//        String username = getAllergy().getUsername();
//         int start = 0;
//        int length = 10;
//        medicalDao.addAllergy(getAllergy());
//         Page<ViewAllergy> result = medicalDao.getAllUserAllergies(start, length,username);
//        assertNotNull(result);
//    }
//
//    
//}
