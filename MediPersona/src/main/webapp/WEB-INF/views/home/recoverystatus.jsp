<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from themes.loxdesign.net/proteus/themes/admin/default/theme/page-error-405.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Jun 2015 09:48:30 GMT -->
<head>
    <title>Proteus - Pages - Error - 405</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
      <link rel="stylesheet" href="../medipersona/css/basic.css">
    <link rel="stylesheet" href="../medipersona/css/general.css">
    <link rel="stylesheet" href="../medipersona/css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../medipersona/css/addons/theme/patternbolt.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--Main Content-->
    <section class="error-container error-405-container pb-pattern o-lines-light">
      <div class="error-405">
        <div class="center-outer">
          <div class="center-inner">
            <div class="center-content error-405-content">
              <h1 class="font-bold">Password <span>Recovery</span></h1>
              <hr>
              <h2>Recovery Status !</h2>
              <hr>
              <div class="info">
                   <c:choose>
                                <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                <c:choose>
                                      <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
          
              </div><a href="https://medipersona.com/login" class="btn btn-lg btn-primary mr"><i class="fa fa-home mr-5"></i>Login</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="js/basic/jquery.min.js"></script>
    <script src="js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="js/basic/modernizr.min.js"></script>
    <script src="js/basic/bootstrap.min.js"></script>
    <script src="js/shared/jquery.asonWidget.js"></script>
    <script src="js/plugins/plugins.js"></script>
    <script src="js/general.js"></script>   
    <script src="js/plugins/pageprogressbar/pace.min.js"></script>
</html>