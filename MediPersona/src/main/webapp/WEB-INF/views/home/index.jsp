<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title> MediPersona || DASHBOARD </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <style type="text/css">
        .dataTables_filter, .dataTables_info,.dataTables_length,.dataTables_paginate { display: none; }
    </style>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    
    <![endif]-->
  </head>
  <body>
     <% response.setIntHeader("Refresh", 100); %>
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        
        <jsp:include page="../../views/includes/nav.jsp" />
          <!-- Row 1 - Info Widgets-->
        <div class="l-row l-spaced-horizontal l-spaced-top">
          <!-- User Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-1 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#user-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="user-info" class="widget-info-details">
                    <div class="info-data open">
                        <h3><span>${activityCount.vitalCount}</span></h3>
                      <hr>
                      <p><span>Vital</span> Signs
                      </p>
                      
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-1-spark-1-c"></div>
                      <div id="customerChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Orders Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom l-clear-sm">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-2 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#order-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="order-info" class="widget-info-details">
                    <div class="info-data open">
                        <h3><span id="alltranx"></span>${activityCount.requestCount}</h3>
                      <hr>
                      <p><span>Review</span> Requests
                      </p>
                     
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-2-spark-1-c"></div>
                      <div class="hide info-t-2-spark-2-c"></div>
                      <div class="hide info-t-2-spark-3-c"></div>
                      <div id="ordersChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Reports Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-3 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#report-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="report-info" class="widget-info-details">
                    <div class="info-data open">
                     <h3> <span id="allmerchants">${activityCount.usedRequestCount}</span></h3>
                      <hr>
                      <p><span>Used</span> Requests
                      </p>
                      
                    </div>
                    <div class="info-chart">
                      <div class="pt-10">
                        <div class="hide info-t-3-spark-1-c"></div>
                        <div id="reportsChart"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
              
          
          <!-- Payout Widget Info-->
          <div class="l-col-md-3 l-col-sm-6 l-spaced-bottom">
            <div data-ason-type="draggable" class="widget-info-wrapper ason-widget">
              <div class="widget-info-refresh-helper">
                <div class="widget-info t-info-4 ui-drag-item">
                  <ul class="widget-options is-options-right">
                    <li class="option-main-item"><a href="#" class="ui-drag-handle"><i class="fa fa-arrows"></i></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-info-refresh-helper" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target="#payout-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                    <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-info-wrapper" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                    <li>
                      <ul>
                        <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="details-btn"><i class="fa fa-file"></i></a></li>
                        <li class="option-sub-item"><a href="#" class="chart-btn"><i class="fa fa-line-chart"></i></a></li>
                      </ul>
                    </li>
                  </ul>
                  <div id="payout-info" class="widget-info-details">
                    <div class="info-data open">
                      <h3><span id="totalall">${activityCount.reviewCount}</span></h3>
                      <hr>
                      <p><span>Review</span> Comments
                      </p>
                     
                    </div>
                    <div class="info-chart">
                      <div class="hide info-t-4-spark-1-c"></div>
                      <div id="paymentChart"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
       <div class="l-spaced">
          <div class="l-row">
            <div class="l-box">
              <div class="l-box-header">
                <h2 class="l-box-title"><span>Five Recent Vital Signs</span> Records </h2>
                <ul class="l-box-options">
                  <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                  <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                  <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                </ul>
              </div>
              <div class="l-box-body">
                  <table id="dataTableId" class="usertable" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th class="tb-col-1">DATE/TIME</th>
                      <th class="tb-col-2">DIASTOLIC</th>
                      <th class="tb-col-3">SYSTOLIC</th>
                      <th class="tb-col-4">TEMPERATURE</th>
                      <th class="tb-col-5">WEIGHT </th>
                      <th class="tb-col-6" >HEIGHT</th>
                      <th class="tb-col-7">PULSE</th>
                      <th class="tb-col-8" >RESPIRATION</th>
                      <th class="tb-col-9">B.M.I</th>
                                  
                      
                      
                    </tr>
                  </thead>
                               <tbody>
                   <c:forEach items="${vitalsigns}" var="vitalsign"> 
                       <tr>
                           <td class="tb-col-1">
                                                                    <c:out value="${vitalsign.createdOn}" />
                                                                </td> 
                                                      <td class="tb-col-2">
                                                                    <c:out value="${vitalsign.diastolic}" />
                                                                </td> 
                                                                   <td class="tb-col-3">
                                                                    <c:out value="${vitalsign.systolic}" />
                                                                </td> 
                                                                   <td class="tb-col-4">
                                                                    <c:out value="${vitalsign.temperature}" />
                                                                </td> 
                                                                <td class="tb-col-5">
                                                                    <c:out value="${vitalsign.weight}" />
                                                                </td>
                                                                   <td class="tb-col-6">
                                                                    <c:out value="${vitalsign.height}" />
                                                                </td> 
                                                                   <td class="tb-col-7">
                                                                    <c:out value="${vitalsign.pulse}" />
                                                                </td> 
                                                                   <td class="tb-col-8">
                                                                    <c:out value="${vitalsign.respiration}" />
                                                                </td> 
                                                                   <td class="tb-col-9">
                                                                    <c:out value="${vitalsign.bmi}" />
                                                                </td>                                                           
                                                                                                                               
                                                               
                                                    
                                                                        </tr>
                                                              </c:forEach>
                   
                      </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      
     
        
    
          <jsp:include page="../../views/includes/footer.jsp" />
      </section>
   
    </section>
   <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <script src="../js/shared/jquery-ui.min.js"></script>
    <script src="../js/shared/jquery.ui.touch-punch.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->

    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/charts/c3/c3.min.js"></script>
    <script src="../js/plugins/charts/c3/d3.v3.min.js"></script>
    <script src="../js/plugins/charts/other/jquery.sparkline.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
 
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
  <script src="../js/calls/table.data.js"></script>
   <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/shared/raphael.min.js"></script>
    <script src="../js/plugins/charts/morris/morris.min.js"></script>

    </script>
  </body>

</html>