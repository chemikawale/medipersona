<%-- 
    Document   : styles
    Created on : 24-Jul-2014, 12:07:01
    Author     : Oyewale
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--loading proteus main js files-->

  <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="<c:url value="/js/basic/jquery.min.js" />"></script>
    <script src="<c:url value="js/basic/jquery-migrate.min.js" />"></script>
    <!-- General-->
    <script src="<c:url value="/js/basic/modernizr.min.js" />"></script>
    <script src="<c:url value="/basic/bootstrap.min.js" />"></script>
    <script src="<c:url value="/js/shared/jquery.asonWidget.js" />"></script>
    <script src="<c:url value="/js/plugins/plugins.js" />"></script>
    <script src="<c:url value="/js/general.js" />"></script>

    <script src="<c:url value="/js/plugins/pageprogressbar/pace.min.js" />"></script>
    <!-- Specific-->
    <script src="<c:url value="/js/shared/classie.js" />"></script>
    <script src="<c:url value="/js/shared/perfect-scrollbar.min.js" />"></script>
    <script src="<c:url value="/js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js" />"></script>
    <script src="<c:url value="/js/plugins/forms/elements/jquery.checkBo.min.js" />"></script>
    <script src="<c:url value="/js/plugins/forms/elements/jquery.switchery.min.js" />"></script>
    <script src="<c:url value="/js/plugins/table/jquery.dataTables.min.js" />"></script>
    <script src="<c:url value="/js/plugins/tooltip/jquery.tooltipster.min.js" />"></script>
    <script src="<c:url value="/js/calls/part.header.1.js" />"></script>
    <script src="<c:url value="/js/calls/part.sidebar.2.js" />"></script>
    <script src="<c:url value="/js/calls/part.theme.setting.js" />"></script>
    <script src="<c:url value="/js/calls/table.data.js" />"></script>