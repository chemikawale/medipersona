
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>  

<aside id="sb-left" class="l-sidebar l-sidebar-1 t-sidebar-1">
        <!--Switcher-->
        <div class="l-side-box"><a href="#" data-ason-type="sidebar" data-ason-to-sm="sidebar" data-ason-target="#sb-left" class="sidebar-switcher switcher t-switcher-side ason-widget"><i class="fa fa-bars"></i></a></div>
        <div class="l-side-box">
          <!--Logo-->
          <div class="widget-logo logo-in-side">
              <h1><a href="#"><span class="visible-default-inline-block"><img src="<c:url value="/img/logo.png"/>" alt="phr"></span><span class="visible-compact-inline-block"><img src="<c:url value="/img/logo_medium.png"/>" alt="phr" title="phr"></span>
                      <span class="visible-collapsed-inline-block"><img src="<c:url value="/img/logo_small.png" />" alt="phr" title="phr"></span></a></h1>
          </div>
        </div>
        <!--Main Menu-->
        <div class="l-side-box">
          <!--MAIN NAVIGATION MENU-->
          
          <nav class="navigation">
              
                            
            
            <ul data-ason-type="menu" class="ason-widget">   
                
                     
                   
                   <security:authorize access="hasRole('ROLE_USER')">
                       
                     <li class="<c:out value="${home}" />"><a href="/phr/user/home"><i class="icon fa fa-dashboard"></i><span class="title">DASHBOARD</span></a></li>
                      
                   </security:authorize>
                     
                      <security:authorize access="hasRole('ROLE_USER')">
                       
                    <li class="<c:out value="${vitalsigns}" />"><a href="#"><i class="icon fa fa-user-secret"></i><span class="title">VITAL SIGNS</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
    
                  <li ><a href="/phr/user/vitalsigns"><span class="title">ADD VITAL SIGNS</span></a>
                  </li>
                   <li ><a href="/phr/user/allvitalsigns"><span class="title">ALL VITAL SIGNS</span></a>
                  </li>
                </ul>
              </li>   
                   </security:authorize>
              
              
               <security:authorize access="hasRole('ROLE_USER')">
             <li class="<c:out value="${reviewrequest}" />"><a href="#"><i class="icon fa fa-user-md"></i><span class="title">REVIEW</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/phr/user/reviewrequest"><span class="title">SEND REQUEST</span></a>
                  </li>
                  <li ><a href="/phr/user/reviewrequests"><span class="title">ALL REQUESTS</span></a>
                  </li>
                  <li ><a href="/phr/user/reviews"><span class="title">ALL REVIEWS</span></a>
                  </li>
                </ul>
              </li>
                   </security:authorize>
                <security:authorize access="hasRole('ROLE_USER')">
                       
                     <li class="<c:out value="${charts}" />"><a href="/phr/user/medicalcharts"><i class="icon fa fa-dashboard"></i><span class="title">MEDICAL CHARTS</span></a></li>
                        <li class="<c:out value="${allergy}" />"><a href="#"><i class="icon fa fa-user-times"></i><span class="title">ALLERGY</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
    
                  <li ><a href="/phr/user/addallergy"><span class="title">ADD ALLERGY </span></a>
                  </li>
                   <li ><a href="/phr/user/allallergies"><span class="title">ALL ALLERGIES</span></a>
                  </li>
                </ul>
              </li>
              
                   <li class="<c:out value="${medication}" />"><a href="#"><i class="icon fa fa-medkit"></i><span class="title">MEDICATION</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>    
                  <li ><a href="/phr/user/addmedication"><span class="title">SAVE MEDICATION </span></a>
                  </li>
                   <li ><a href="/phr/user/allmedications"><span class="title">ALL MEDICATIONS</span></a>
                  </li>
                </ul>
              </li>
                       
                     <li class="<c:out value="${blooddetails}" />"><a href="/phr/user/blooddetails"><i class="icon fa fa-edit"></i><span class="title">UPDATE BLOOD DETAILS</span></a></li>
                      
                   </security:authorize>   
                     
                 <security:authorize access="hasRole('ROLE_USER')">
                       
                     <li class="<c:out value="${updateprofile}" />"><a href="/phr/user/updatedetails"><i class="icon fa fa-edit"></i><span class="title">UPDATE PROFILE</span></a></li>
                     
                     <li class="<c:out value="${feedback}" />"><a href="/phr/uti/logfeedback"><i class="icon fa fa-reply"></i><span class="title">LOG FEEDBACK</span></a></li>
                   
                     <li class="<c:out value="${changepassword}" />"><a href="/phr/pwd/changepassword"><i class="icon fa fa-edit"></i><span class="title">CHANGE PASSWORD</span></a></li>
                   
                   
                   </security:authorize>        
                     
                        <security:authorize access="hasRole('ROLE_MAIN_ADMIN') OR hasRole('ROLE_SUPPORT')">
<!--              <li class="<c:out value="${utilities}" />" ><a href="/phr/uti/allmedifacilities"><i class="icon fa fa-dashboard"></i><span class="title">MEDICAL FACILITIES</span></a></li>
                          
              <li class="<c:out value="${utilities}" />" ><a href="/phr/uti/addfacility"><i class="icon fa fa-dashboard"></i><span class="title">ADD PROVIDER</span></a></li>
                            
              <li class="<c:out value="${utilities}" />"><a href="/phr/uti/logfeedback"><i class="icon fa fa-dashboard"></i><span class="title">SEND FEEDBACK</span></a></li>
              
              <li class="<c:out value="${utilities}" />"><a href="/phr/user/reviewrequest"><i class="icon fa fa-dashboard"></i><span class="title">REQUEST REVIEW</span></a></li>
           -->
              
       
            
              
               <li class="<c:out value="${user}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">MEDICAL</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
    
                  <li ><a href="/phr/user/vitalsigns"><span class="title">ADD VITAL SIGNS</span></a>
                  </li>
                   <li ><a href="/phr/user/allvitalsigns"><span class="title">ALL VITAL SIGNS</span></a>
                  </li>
                </ul>
              </li>
              
                <li class="<c:out value="${allergy}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">ALLERGY</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
    
                  <li ><a href="/phr/uti/addallergycategory"><span class="title">ADD ALLERGY CATEGORY</span></a>
                  </li>
                   <li ><a href="/phr/uti/allergycategories"><span class="title">ALL ALLERGIES CATEGORIES</span></a>
                  </li>
                  <li ><a href="/phr/uti/allallergies"><span class="title">ALL ALLERGIES</span></a>
                  </li>
                </ul>
              </li>
              
             
                 <li class="<c:out value="${hmo}" />"><a href="#"><i class="icon fa fa-user-plus"></i><span class="title">HMO</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/phr/uti/addhmo"><span class="title">ADD HMO</span></a>  </li>
                  <li ><a href="/phr/uti/addhmoplan"><span class="title">ADD HMO PLAN</span></a>
                  <li><a href="/phr/uti/allhmos"><span class="title">ALL HMO</span></a>      </li>
                  <li ><a href="/phr/uti/allhmoplans"><span class="title">ALL HMO PLANS</span></a>
                  <li><a href="/phr/uti/assignplan"><span class="title">ASSIGN PLAN</span></a>                </li>
                  <li ><a href="/phr/uti/assignedplans"><span class="title">ALL ASSIGNED PLAN</span></a>
                  </li>
                </ul>
              </li>
              
              <li class="<c:out value="${utilities}" />"><a href="#"><i class="icon fa fa-user-secret"></i><span class="title">UTILITIES</span><span class="arrow"><i class="fa fa-angle-left"></i></span></a>
                <ul>
                  <li><a href="/phr/uti/addcountry"><span class="title">ADD COUNTRY</span></a>
                  </li>
                  <li ><a href="/phr/uti/allcountries"><span class="title">ALL COUNTRIES</span></a>
                  </li>
                  <li ><a href="/phr/uti/addstate"><span class="title">ADD STATE</span></a>
                  </li>
                  <li ><a href="/phr/uti/allstates"><span class="title">ALL STATES</span></a>
                  </li>
                   <li ><a href="/phr/uti/addcategory"><span class="title">ADD CATEGORY</span></a>
                  </li>
                  <li ><a href="/phr/uti/allcategories"><span class="title">ALL CATEGORIES</span></a>
                  </li>
                   <li ><a href="/phr/uti/allfeedbacks"><span class="title">ALL FEEDBACKS</span></a>
                  </li>
                </ul>
              </li>    
              <li class="<c:out value="${user}" />"><a href="/phr/user/changepassword"><i class="icon fa fa-dashboard"></i><span class="title">CHANGE PASSWORD</span></a></li>
              
               </security:authorize>
          
              
               
            </ul>
          </nav>
       
        </div>
      </aside>