<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
  
<head>
    <title> MediPersona || USER -- MEDICAL CHARTS </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
     <% response.setIntHeader("Refresh", 100); %>
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        
        <jsp:include page="../views/includes/nav.jsp" />
                   <div class="l-page-header">
          <h2 class="l-page-title">Medical Data <span> Charts</span></h2>
          
        </div>
      <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">TEMP.PUL.RESP Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="revenueChart"></div>
              </div>
            </div>
          </div>
        </div>
      
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">BLOOD.PRESSURE Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="bpChart"></div>
              </div>
            </div>
          </div>
        </div>
         
         
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">WEIGHT - HEIGHT Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="whChart"></div>
              </div>
            </div>
          </div>
        </div>
         
      
          <jsp:include page="../views/includes/footer.jsp" />
      </section>
   
    </section>
   <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <script src="../js/shared/jquery-ui.min.js"></script>
    <script src="../js/shared/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
      // jQuery Ui and Bootstrap conflict workaround
      $.widget.bridge('uitooltip', $.ui.tooltip);
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->

    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/charts/c3/c3.min.js"></script>
    <script src="../js/plugins/charts/c3/d3.v3.min.js"></script>
    <script src="../js/plugins/charts/other/jquery.sparkline.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
 
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>

    <script src="../js/shared/raphael.min.js"></script>
    <script src="../js/plugins/charts/morris/morris.min.js"></script>

   
    <script type="text/javascript">
     $(document).ready(function () {
     
        var ctx = "${pageContext.request.contextPath}";                    
        
         $.ajax({
                      url: ctx + "/user/medicaldata",     
                      error: function() {
                              console.log('Error occure while medical data data');
                              },
                      type: 'GET',
                       success: function(data) {
                          var medicaldata = data.medicaldata;
                         var datalength = medicaldata.length;
                         
                            var temperatures = [];                            
                            var pulses = [];
                            var systolic = [];
                            var diastolic = [];
                            var respiration = [];
                            var datelogged = [];
                            var weight = [];
                            var height = [];
                            
                        for(var i = 0; i < datalength; i++){
                                  datelogged[i] = medicaldata[i].createdOn; 
                                  temperatures[i] = medicaldata[i].temperature; 
                                  weight[i] = medicaldata[i].weight; 
                                  height[i] = medicaldata[i].height; 
                                  pulses[i] = medicaldata[i].pulse; 
                                  systolic[i] = medicaldata[i].systolic; 
                                  diastolic[i] = medicaldata[i].diastolic; 
                                  respiration[i] = medicaldata[i].respiration; 
                             }
                          
                          
                          
	 var  revenueChart   = '#revenueChart'; 
         var allTpr = [];      
     for ( i = 0; i < datalength; i++){              
      var tprData = {};
                   tprData['DATE'] = datelogged[datalength-i-1] ;
                   tprData['TEMPERATURE'] = temperatures[datalength-i-1];
	           tprData['PULSE'] = pulses[datalength-i-1];
		   tprData['RESPIRATION'] = respiration[datalength-i-1];
               allTpr.push(tprData);                                  
                }
   var chart = c3.generate({
       bindto: revenueChart,
        data: {
            json: allTpr,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','TEMPERATURE', 'PULSE','RESPIRATION']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });
    
    	 var  bpChart   = '#bpChart';
         var allBp = [];      
     for ( i = 0; i < datalength; i++){              
      var bpData = {};
                   bpData['DATE'] = datelogged[datalength-i-1] ;
                   bpData['DIASTOLIC'] = diastolic[datalength-i-1];
	           bpData['SYSTOLIC'] = systolic[datalength-i-1];
		   
               allBp.push(bpData);                                  
                }
                
   var chart = c3.generate({
       bindto: bpChart,
        data: {
            json: allBp,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','DIASTOLIC', 'SYSTOLIC']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });

     
    var  whChart   = '#whChart';
    var allWp = [];      
     for ( i = 0; i < datalength; i++){              
      var wpData = {};
                   wpData['DATE'] = datelogged[datalength-i-1] ;
                   wpData['WEIGHT'] = weight[datalength-i-1];
	           wpData['HEIGHT'] = height[datalength-i-1];
		   
               allWp.push(wpData);                                  
                }
                
   var chart = c3.generate({
       bindto: whChart,
        data: {
            json: allWp,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','HEIGHT', 'WEIGHT']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });
                      }
       });
        
          
});


    </script>
  </body>

</html>