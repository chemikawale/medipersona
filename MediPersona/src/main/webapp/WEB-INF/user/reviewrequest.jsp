<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>MediPersona || REVIEW REQUEST</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
   
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
     <jsp:include page="../views/includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../views/includes/nav.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">Request <span> Review</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
                <h2 class="l-box-title"><span> Make</span> Request</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                  </ul>
            </div>
            <div class="l-box-body l-spaced">
                <form:form  role="form"  commandName="viewModel" action="reviewrequest" method="POST" class="form-horizontal">
      <fieldset>
                                                    <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                   
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
          <div class="form-group">
              <label class="col-sm-3 control-label" for="role">ROLE:</label>
                       <div class="col-sm-5">  
                 
                    <select id="basicSelect" class="form-control" name="role" >
                             <option>SELECT ROLE</option>
                             <option value="MEDICAL DOCTOR">MEDICAL DOCTOR</option>
                             <option value="PERSONAL PHYSICIAN">PERSONAL PHYSICIAN</option>
                             <option value="NURSE">NURSE</option>
                             <option value="SCIENTIST">SCIENTIST</option>
                                                       
                    </select>
                       </div>
                </div>
            <div class="form-group">
                   <label class="col-sm-3 control-label" for="lastName">SURNAME:</label>
                       <div class="col-sm-5">  
              <input id="lastName" type="text" name="lastName" placeholder="Surname" class="form-control" required="true">
                       </div>
                       </div>
			  <div class="form-group">
                               <label class="col-sm-3 control-label" for="firstName">FIRST NAME:</label>
                       <div class="col-sm-5">                
              <input id="firstName" type="text" name="firstName" placeholder="First Name" class="form-control" required="true">
                       </div>
                       </div>
                <div class="form-group">
                   <label class="col-sm-3 control-label" for="email">EMAIL:</label>
                       <div class="col-sm-5">                
                         <input class="form-control" id="email" name="email"  type="email"  placeholder=" EMAIL ADDRESS OF THE REVIEWER" required="true"/>
                            
                       </div>
                </div> 
               <div class="form-group">
                   <label class="col-sm-3 control-label" for="addedTime">REVIEW DURATION:</label>
                       <div class="col-sm-5">                
                           <input class="form-control" id="addedTime" name="addedTime"  type="text"  placeholder=" FOR THE NEXT  ? HOURS" required="true" pattern="[0-9]{1,}" title="Hours in Numeric format"/>
                            
                       </div>
                </div> 
                              
               
                   
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">SEND</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
           <jsp:include page="../views/includes/footer.jsp" />
      </section>

    </section>
    <!-- ===== JS =====-->
    
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
   
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.autogrow.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.fancySelect.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.maskedinput.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.onoff.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.select2.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.textareaCounter.plugin.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/form.elements.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>

  </body>

</html>