<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>MediPersona || USER SIGN UP</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
   
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <body class="login-bg">
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    
    <!--SECTION-->
    <section class="l-main-container">
      <!--Main Content-->
      <div class="login-wrapper register-wrapper">
        <div class="login-container">
          <!--Logo-->
          <h1 class="login-logo"><img src="../img/logo.png" alt="Proteus"></h1>
          <!--Login Form-->
         <form:form  role="form"  commandName="viewModel" action="signup" method="POST" class="login-form">
              <fieldset>
                                                    <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
            <div class="form-group">
              <input id="username" type="text" name="username" placeholder="Preferred Username" class="form-control">
            </div>
			  <div class="form-group">
              <input id="lastName" type="text" name="lastName" placeholder="Surname" class="form-control" required="true">
            </div>
			  <div class="form-group">
              <input id="firstName" type="text" name="firstName" placeholder="First Name" class="form-control" required="true">
            </div>
			
			  <div class="form-group">
              <input id="phoneNumber" type="text" name="phoneNumber" placeholder="Phone Number" class="form-control" required="true">
            </div>
			
			
            <div class="form-group">
              <input id="email" type="email" name="email" placeholder="Email" class="form-control" required="true">
            </div>
              <div class="form-group">
                 
                    <select id="basicSelect" class="form-control" name="countryId" >
                        
                         <c:forEach items="${allCountries}" var="country"> 
                             <option value="${country.countryId}">${country.countryName}</option>
                           </c:forEach>
                    </select>
                  
                </div>
           
            <button type="submit" class="btn btn-dark btn-block btn-login">Sign Up</button>
            <div class="login-social">
              <div class="l-span-md-12">
                <div class="tor"><span>- OR -</span></div>
              </div>
              <div class="l-span-md-12 register-sign-in"><a href="/phr/login" class="btn btn-primary btn-block btn-login-register">Sign In</a></div>
              
            </div>
            <div class="login-options"></div>
         </form:form>
        </div>
      </div>
    </section>
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
    <!-- Semi general-->
    
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../js/plugins/forms/validation/jquery.validate.min.js"></script>
    <script src="../js/plugins/forms/validation/jquery.validate.additional.min.js"></script>
    <script src="../js/calls/page.register.js"></script>
    
 </html>