<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
  
<head>
    <title> MediPersona || MEDICAL PROFILE </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <style type="text/css">
        .dataTables_filter, .dataTables_info,.dataTables_length { display: none; }
    </style>
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    
    <![endif]-->
  </head>
  <body>
 
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
   
        <jsp:include page="../views/includes/reviewermenu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        
        <jsp:include page="../views/includes/reviewernav.jsp" />
   
        <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>${userfullname} 's </span> Medical Profile</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              </ul>
            </div>
            <div class="l-box-body l-spaced group">
              <!-- Vertical Tabs-->
              <div id="vertEasyRespTab">
                <ul class="resp-tabs-list">
                  <li>Vital Signs</li>
                  <li>Allergies</li>
                  <li>Blood Details</li>
                  <li>Medications</li>
                  <li>Medical Charts</li>
                </ul>
                <div class="resp-tabs-container">
                  <div>
                 <div class="l-box-body">
                <table id="myData" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th>SYSTOLIC</th>
                      <th>DIASTOLIC</th>                 
                      <th>WEIGHT</th>
                      <th>HEIGHT</th>
                      <th>TEMPERATURE</th>  
                      <th>PULSE</th>  
                      <th>RESPIRATION</th>  
                      <th>B.M.I</th>  
                      <th>DATE CREATED</th>  
                                 
                    </tr>
                  </thead>
                
                  <tbody>
                   
                  </tbody>
                </table>
              </div> 
                  </div>
                    <div>
                          <div class="l-box-body">
                <table id="myAllergy" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th>CATEGORY</th>
                      <th>ALLERGIC TO</th>                             
                      <th>REACTION</th>
                      <th>LOCATION</th>
                      <th>SEVERITY</th>  
                       <th>STARTED AT</th>  
                       <th>ACTIVE</th>  
                                  
                    </tr>
                  </thead>
                
                  <tbody>
                   
                  </tbody>
                </table>
              </div> 
                    </div>
                  <div>
                      <table class="table table-striped table-invoice">
              <thead>
               
              </thead>
              <tbody>
                <tr>
                  <td>BLOOD GROUP</td>
             
                  <td>${bloodDetails.bloodGroup}</td>
                </tr>
                <tr>
                  <td>GENOTYPE</td>
            
                  <td>${bloodDetails.genotype}</td>
                </tr>
                <tr>
                  <td>RHESUS D</td>
            
                  <td>${bloodDetails.rhesusD}</td>
                </tr>
              
              </tbody>
       
            </table>  
                  </div>
                
                
                  <div>
                 <div class="l-box-body">
                <table id="myMedications" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th>DIAGNOSIS</th>
                      <th>MEDICATION</th>                 
                      <th>PRESCRIPTION</th>
                      <th>NOTE</th>
                      <th>START DATE</th>  
                       <th>END DATE</th>  
                      <th>DATE PRESCRIBED</th>  
                      
                    </tr>
                  </thead>
                
                  <tbody>
                   
                  </tbody>
                </table>
              </div> 
                  </div>
                
                
                  <div>
                       <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">TEMP.PUL.RESP Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="revenueChart"></div>
              </div>
            </div>
          </div>
        </div>
      
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">BLOOD PRESSURE Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="bpChart"></div>
              </div>
            </div>
          </div>
        </div>
         
         
         <!-- Row 3 - Revenue Widget-->
        <div class="l-spaced">
          <!-- Revenue Widget-->
          <div class="widget-revenue l-spaced-bottom">
            <ul class="widget-options is-options-right">
              <li class="option-main-item"><a href="#" data-ason-type="refresh" data-ason-target=".widget-revenue" data-ason-loader-class="spinner line back-and-forth grow" data-ason-content="true" data-ason-duration="1100" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="toggle" data-ason-parent="false" data-ason-target=".widget-revenue-info" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
              <li class="option-main-item"><a href="#" data-ason-type="delete" data-ason-target=".widget-revenue" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
              <li><a href="#" class="toggler-btn"><i class="fa fa-circle-thin"></i></a></li>
            </ul>
            <div class="widget-revenue-info">
              <h4 class="widget-header">WEIGHT - HEIGHT Chart</h4>
              <div class="hide c3-revenue-1"></div>
              <div class="hide c3-revenue-2"></div>
              <div class="hide c3-revenue-3"></div>
              <div class="hide c3-revenue-4"></div>
              <div class="hide c3-revenue-5"></div>
              <div class="revenue-chart">
                <div id="whChart"></div>
              </div>
            </div>
          </div>
        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
      
        </div>
         <jsp:include page="../views/includes/footer.jsp" />
      </section>

 
    <!-- ===== JS =====-->
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>

    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
      <script src="../js/plugins/charts/c3/c3.min.js"></script>
    <script src="../js/plugins/charts/c3/d3.v3.min.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/tabs/jquery.easyResponsiveTabs.js"></script>
    <script src="../js/plugins/tabs/jquery.responsiveTabs.min.js"></script>
    <script src="../js/plugins/tabs/jquery.tabslet.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/ui.tabs.js"></script>
	   <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
	   <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>
  <script>
           
                var ctx = "${pageContext.request.contextPath}";
                 
                    $(document).ready(function() {
                                
                oTable = $('#myData').dataTable({
                   serverSide: true,
                    ajax: {
                        url: ctx + "/reviewer/getuservitalsigns",
                        type: 'GET'                    
                          },
                    columns:[
                         
                         {"data":"systolic"}, 
                         {"data":"diastolic"},   
                         {"data":"weight"},      
                         {"data":"height"},  
                         {"data":"temperature"}, 
                         {"data":"pulse"},   
                         {"data":"respiration"},      
                         {"data":"bmi"},  
                         {"data":"createdOn"}                                               
                        
                    ]
                });
               
                    oTable.fnDraw();
                    
                       aTable = $('#myAllergy').dataTable({
                   serverSide: true,
                    ajax: {
                        url: ctx + "/reviewer/getuserallergies",
                        type: 'GET'                    
                          },
                    columns:[
                         {"data":"categoryName"}, 
                         {"data":"allergicTo"},                         
                         {"data":"reaction"},      
                         {"data":"location"},   
                         {"data":"severity"},                         
                         {"data":"started"},      
                         {"data":"active", 
                           "render": function(data, type, row) {                             
                                if (data === true){ 
                                  return '<span class="label label-success">'+"TRUE"+'</span>';
                               }
                               else {
                                    return '<span class="label label-danger">'+"FALSE"+'</span>';
                               }
                            }
                         }
                        
                    ]
                });
               
                    aTable.fnDraw();
           
            mTable = $('#myMedications').dataTable({
                   serverSide: true,
                    ajax: {
                        url: ctx + "/reviewer/usermedications",
                        type: 'GET'                    
                          },
                    columns:[
                         
                         {"data":"diagnosis"}, 
                         {"data":"medication"},   
                         {"data":"prescription"},      
                         {"data":"prescriptionNote"},  
                         {"data":"startDate"}, 
                         {"data":"endDate"},   
                         {"data":"createdOn"}  
                                              
                         ]
                });
               
                    mTable.fnDraw();
       
       
            $.ajax({
                      url: ctx + "/reviewer/medicaldata",     
                      error: function() {
                              console.log('Error occur while medical data data');
                              },
                      type: 'GET',
                       success: function(data) {
                          var medicaldata = data.medicaldata;
                            var datalength = medicaldata.length;
                            
                            var temperatures = [];
                            var pulses = [];
                            var systolic = [];
                            var diastolic = [];
                            var respiration = [];
                            var datelogged = [];
                            var weight = [];
                            var height = [];
                            
                             for(var i = 0; i < datalength; i++){
                                  datelogged[i] = medicaldata[i].createdOn; 
                                  temperatures[i] = medicaldata[i].temperature; 
                                  weight[i] = medicaldata[i].weight; 
                                  height[i] = medicaldata[i].height; 
                                  pulses[i] = medicaldata[i].pulse; 
                                  systolic[i] = medicaldata[i].systolic; 
                                  diastolic[i] = medicaldata[i].diastolic; 
                                  respiration[i] = medicaldata[i].respiration; 
                             }
                          
                          
                          
	 var  revenueChart   = '#revenueChart'; 
         var allTpr = [];      
     for ( i = 0; i < datalength; i++){              
      var tprData = {};
                   tprData['DATE'] = datelogged[datalength-i-1] ;
                   tprData['TEMPERATURE'] = temperatures[datalength-i-1];
	           tprData['PULSE'] = pulses[datalength-i-1];
		   tprData['RESPIRATION'] = respiration[datalength-i-1];
               allTpr.push(tprData);                                  
                }
                
   var chart = c3.generate({
       bindto: revenueChart,
        data: {
            json: allTpr,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','TEMPERATURE', 'PULSE','RESPIRATION']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });
    
    	 var  bpChart   = '#bpChart';
         var allBp = [];      
     for ( i = 0; i < datalength; i++){              
      var bpData = {};
                   bpData['DATE'] = datelogged[datalength-i-1] ;
                   bpData['DIASTOLIC'] = diastolic[datalength-i-1];
	           bpData['SYSTOLIC'] = systolic[datalength-i-1];
		   
               allBp.push(bpData);                                  
                }
                
   var chart = c3.generate({
       bindto: bpChart,
        data: {
            json: allBp,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','DIASTOLIC', 'SYSTOLIC']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });

     
    var  whChart   = '#whChart';
    var allWp = [];      
     for ( i = 0; i < datalength; i++){              
      var wpData = {};
                   wpData['DATE'] = datelogged[datalength-i-1] ;
                   wpData['WEIGHT'] = weight[datalength-i-1];
	           wpData['HEIGHT'] = height[datalength-i-1];
		   
               allWp.push(wpData);                                  
                }
                
   var chart = c3.generate({
       bindto: whChart,
        data: {
            json: allWp,
            keys: {
                x: 'DATE', // it's possible to specify 'x' when category axis
                value: ['DATE','HEIGHT', 'WEIGHT']
            },
             type: 'area-spline'
        },
        axis: {
            x: {
                type: 'category'
            }
        }
    });
  
  
  

                    }
       });
           
            });
           
            
          
        </script>
  </body>
</html>