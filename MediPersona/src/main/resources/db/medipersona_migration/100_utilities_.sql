use medipersona;

 CREATE TABLE IF NOT EXISTS medipersona.medi_countries(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    countryId VARCHAR(64) NOT NULL UNIQUE,
    countryName VARCHAR(64) NOT NULL UNIQUE,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_country;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_country(
	OUT id BIGINT,
        IN countryId VARCHAR(64) ,
        IN countryName VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_countries
        (
        countryId ,
        countryName ,
        createdOn  
        )
	VALUES
	(
        countryId ,
        countryName ,
        createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_medi_countries;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_medi_countries()
BEGIN

SELECT * FROM medipersona.medi_countries order by countryName ASC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_medi_country_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_country_ById(
    IN countryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_countries where medi_countries.countryId = countryId ;

END $$

DELIMITER;


 CREATE TABLE IF NOT EXISTS medipersona.medi_states(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    countryId VARCHAR(64) ,
    stateId VARCHAR(64) NOT NULL UNIQUE,
    stateName VARCHAR(64) NOT NULL UNIQUE,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_state;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_state(
	OUT id BIGINT,
        IN countryId VARCHAR(64),
        IN stateId VARCHAR(64) ,
        IN stateName VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_states
        (
        countryId ,
        stateId,
        stateName ,
        createdOn  
        )
	VALUES
	(
         countryId ,
         stateId,
         stateName ,
         createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.get_all_medi_states;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_medi_states()
BEGIN

SELECT * FROM medipersona.medi_states order by stateName ASC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_medi_states_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_states_ById(
    IN stateId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_states where medi_states.stateId = stateId ;

END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.find_medi_states_ByCountryId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_states_ByCountryId(
    IN countryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_states where medi_states.countryId = countryId order by stateName ASC ;

END $$

DELIMITER;


 CREATE TABLE IF NOT EXISTS medipersona.medi_categories(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    categoryId VARCHAR(64) NOT NULL UNIQUE,
    categoryName VARCHAR(64) NOT NULL UNIQUE,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_category;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_category(
	OUT id BIGINT,
        IN categoryId VARCHAR(64) ,
        IN categoryName VARCHAR(64),
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_categories
        (
        categoryId ,
        categoryName ,
        createdOn  
        )
	VALUES
	(
         categoryId ,
        categoryName ,
        createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_medi_categories;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_medi_categories()
BEGIN

SELECT * FROM medipersona.medi_categories order by categoryName ASC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_medi_category_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_category_ById(
    IN categoryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_categories where medi_categories.categoryId = categoryId ;

END $$

DELIMITER;




CREATE TABLE IF NOT EXISTS medipersona.medi_feedback(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    feedBackId VARCHAR(64) NOT NULL UNIQUE,
    username VARCHAR(64) ,
    title VARCHAR(100),
    body TEXT,
    status BIT,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.log_medi_feedback;

DELIMITER $$

CREATE PROCEDURE medipersona.log_medi_feedback(
	OUT id BIGINT,
        IN feedBackId VARCHAR(64) ,
        IN username VARCHAR(64),
        IN title VARCHAR(100),
        IN body TEXT,
        IN status BIT,
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_feedback
        (
        feedBackId ,
        username,
        title ,
        body,
        status,
        createdOn
        )
	VALUES
	(
         feedBackId ,
         username,
         title ,
         body,
         status,
         createdOn
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.get_all_feedbacks;
DELIMITER //

CREATE PROCEDURE medipersona.get_all_feedbacks(
in pageNum int,
in pageSize int,
out count int
)
begin
    declare number_to_skip, temp_counter, counter int;
    set number_to_skip = pageSize *  pageNum ;

    select count(*) into counter from medipersona.medi_feedback ;
    set count = counter;
     prepare findStatement from 'SELECT  * FROM medipersona.medi_feedback ORDER BY createdOn DESC limit ?';
     prepare findStatement2 from 'SELECT * FROM medipersona.medi_feedback ORDER BY createdOn  DESC limit ? offset ? ';
   
    if (number_to_skip >= count) then

            set temp_counter = number_to_skip % counter;

            SET @pageSize = pageSize ;


            EXECUTE findStatement USING @pageSize ;
            DROP PREPARE findStatement; 
    else
        SET @pageSize = pageSize;
        SET @moffSet = number_to_skip;
        
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
            end if;

        EXECUTE findStatement2 USING @pageSize,@moffSet ;
        DROP PREPARE findStatement2;   
    end if;
end //
DELIMITER ;



 CREATE TABLE IF NOT EXISTS medipersona.medi_facilities(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        facilityId VARCHAR(64) NOT NULL UNIQUE,
        countryId VARCHAR(64) ,
        stateId VARCHAR(64) ,
        categoryId VARCHAR(64) ,
        longitude VARCHAR(64) ,
        latitude VARCHAR(64) ,
        facilityName VARCHAR(64) ,
        contactPerson VARCHAR(64) ,
        address TEXT,
        services TEXT ,
        verified BIT,
        createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_facility;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_facility(
	OUT id BIGINT,
        IN  facilityId VARCHAR(64),
        IN  countryId VARCHAR(64) ,
        IN  stateId VARCHAR(64) ,
        IN  categoryId VARCHAR(64) ,
        IN  longitude VARCHAR(64) ,
        IN  latitude VARCHAR(64) ,
        IN  facilityName VARCHAR(64) ,
        IN  contactPerson VARCHAR(64) ,
        IN  address TEXT,
        IN  services TEXT ,
        IN  verified BIT,
        IN  createdOn DATETIME      
        )
BEGIN
	INSERT INTO medipersona.medi_facilities
        (
         facilityId,
        countryId  ,
        stateId  ,
        categoryId  ,
        longitude ,
        latitude  ,
        facilityName ,
        contactPerson ,
        address ,
        services  ,
        verified ,
        createdOn
        )
	VALUES
	(
         facilityId,
        countryId  ,
        stateId  ,
        categoryId  ,
        longitude ,
        latitude  ,
        facilityName ,
        contactPerson ,
        address ,
        services  ,
        verified ,
        createdOn
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.all_medi_facilities;

DELIMITER $$

 CREATE PROCEDURE medipersona.all_medi_facilities(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;

     select count(*) into counter from medipersona.medi_facilities ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.medi_facilities ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.medi_facilities ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;

             SET @pageSize = pageSize ;

             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;


 CREATE TABLE IF NOT EXISTS medipersona.medi_hmo_plan(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    planId VARCHAR(64) NOT NULL UNIQUE,
    planName VARCHAR(64) ,
    description TEXT,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_hmo_plan;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_hmo_plan(
	OUT id BIGINT,
        IN planId VARCHAR(64) ,
        IN planName VARCHAR(64) ,
        IN description TEXT,
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_hmo_plan
        (
        planId ,
        planName ,
        description,
        createdOn  
        )
	VALUES
	(
          planId ,
        planName ,
        description,
        createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_medi_hmo_plans;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_medi_hmo_plans()
BEGIN

SELECT * FROM medipersona.medi_hmo_plan order by planName ASC ;

END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.find_medi_hmo_plan_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_hmo_plan_ById(
    IN planId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_hmo_plan where medi_hmo_plan.planId = planId ;

END $$

DELIMITER;



 CREATE TABLE IF NOT EXISTS medipersona.medi_hmo(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hmoId VARCHAR(64) NOT NULL UNIQUE,
    countryId VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL UNIQUE,
    hmoName VARCHAR(64) ,
    address TEXT,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.add_medi_hmo;

DELIMITER $$

CREATE PROCEDURE medipersona.add_medi_hmo(
	OUT id BIGINT,
        IN hmoId VARCHAR(64) ,
        IN countryId VARCHAR(64) ,
        IN hmoName VARCHAR(64) ,
        IN email VARCHAR(64) ,
        IN address TEXT,
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.medi_hmo
        (
        hmoId ,
        countryId,
        hmoName ,
        email,
        address,
        createdOn  
        )
	VALUES
	(
         hmoId ,
         countryId,
        hmoName ,
        email,
        address,
        createdOn  
        
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;


DROP PROCEDURE IF EXISTS medipersona.get_all_medi_hmo;
DELIMITER $$

CREATE PROCEDURE medipersona.get_all_medi_hmo()
BEGIN

SELECT * FROM medipersona.medi_hmo order by hmoName ASC ;

END $$

DELIMITER;



DROP PROCEDURE IF EXISTS medipersona.find_medi_hmo_ById;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_hmo_ById(
    IN hmoId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_hmo where medi_hmo.hmoId = hmoId ;

END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.find_medi_hmo_ByCountryId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_medi_hmo_ByCountryId(
    IN countryId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.medi_hmo where medi_hmo.countryId = countryId ;

END $$

DELIMITER;

CREATE TABLE IF NOT EXISTS medipersona.assigned_hmo_plan(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    assignId VARCHAR(64) NOT NULL UNIQUE,
    hmoId VARCHAR(64) ,
    hmoName VARCHAR(64) ,
    planId VARCHAR(64) ,
    planName VARCHAR(64) ,
    createdOn DATETIME NULL DEFAULT NULL   
   );


DROP PROCEDURE IF EXISTS medipersona.assign_hmo_to_plan;

DELIMITER $$

CREATE PROCEDURE medipersona.assign_hmo_to_plan(
	OUT id BIGINT,
        IN assignId VARCHAR(64) ,
        IN hmoId VARCHAR(64) ,
        IN hmoName VARCHAR(64) ,
          IN planId VARCHAR(64) ,
        IN planName VARCHAR(64) ,     
        IN createdOn DATETIME         
        )
BEGIN
	INSERT INTO medipersona.assigned_hmo_plan
        (
        assignId,
        hmoId ,
        hmoName ,
        planId,
        planName,
        createdOn  
        )
	VALUES
	(
        assignId,
        hmoId ,
        hmoName ,
        planId,
        planName,
        createdOn  
	);
        SET id = LAST_INSERT_ID();
       
END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.get_all_assigned_plan;

DELIMITER $$

 CREATE PROCEDURE medipersona.get_all_assigned_plan(
 in pageNum int,
 in pageSize int,
 out count int
 )
 begin
     declare number_to_skip, temp_counter, counter int;
     set number_to_skip = pageSize *  pageNum ;

     select count(*) into counter from medipersona.assigned_hmo_plan ;
     set count = counter;
      prepare findStatement from 'SELECT  * FROM medipersona.assigned_hmo_plan ORDER BY createdOn DESC limit ?';
      prepare findStatement2 from 'SELECT * FROM medipersona.assigned_hmo_plan ORDER BY createdOn  DESC limit ? offset ? ';

     if (number_to_skip >= count) then
 
             set temp_counter = number_to_skip % counter;
-- 
             SET @pageSize = pageSize ;
-- 
-- 
             EXECUTE findStatement USING @pageSize ;
             DROP PREPARE findStatement; 
     else
         SET @pageSize = pageSize;
         SET @moffSet = number_to_skip;
--         
        if(pageSize > 0) then
                SET @pageSize = pageSize;
            else
                Set @pageSize = counter;
             end if;
-- 
         EXECUTE findStatement2 USING @pageSize,@moffSet ;
         DROP PREPARE findStatement2;   
     end if;
  end $$
  DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.find_assigned_hmo_ByPlanId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_assigned_hmo_ByPlanId(
    IN planId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.assigned_hmo_plan where assigned_hmo_plan.planId = planId ;

END $$

DELIMITER;

DROP PROCEDURE IF EXISTS medipersona.find_assigned_hmo_ByHmoId;
DELIMITER $$

CREATE PROCEDURE medipersona.find_assigned_hmo_ByHmoId(
    IN hmoId VARCHAR(64)
)
BEGIN

SELECT * FROM medipersona.assigned_hmo_plan where assigned_hmo_plan.hmoId = hmoId ;

END $$

DELIMITER;



 