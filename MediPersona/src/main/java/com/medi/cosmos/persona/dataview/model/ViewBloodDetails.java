/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dataview.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewBloodDetails {
     private String bloodGroup;
    private String rhesusD;
    private String genotype;
    private String username;
    private String createdOn;

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRhesusD() {
        return rhesusD;
    }

    public void setRhesusD(String rhesusD) {
        this.rhesusD = rhesusD;
    }

    public String getGenotype() {
        return genotype;
    }

    public void setGenotype(String genotype) {
        this.genotype = genotype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreatedOn() {
        if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewBloodDetails.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    
    
}
