/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class WebUpdateBankAdmin {
     @NotNull(message = "Username field cannot be null")
     private String username;
    @NotNull(message = "BankId field cannot be null")
     private String bankId;
    @NotNull(message = "Roles field cannot be null")
    private String[] roles;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }
    
    
}
