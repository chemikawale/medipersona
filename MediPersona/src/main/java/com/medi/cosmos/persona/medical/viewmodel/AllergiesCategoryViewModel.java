/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.medical.viewmodel;

import com.medi.cosmos.persona.model.AllergyCategory;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class AllergiesCategoryViewModel {
     private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
     
    public AllergyCategory toCategory(){
     AllergyCategory category = new AllergyCategory();
      category.setCategoryId(Utilities.generateId(5));
      category.setCategoryName(categoryName.trim());
      category.setCreatedOn(Utilities.presentDateTime());
    return category;
    }
}
