/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 *
 * @author Oyewale
 */

@Component
public class RequestsSchedule {
    
    @Autowired
    private UserDao userDao;
    
  
    
    public void deprecateRequests() throws CosmosDatabaseException {
	   userDao.deprecateReviewRequests();
	}
    
    
}
