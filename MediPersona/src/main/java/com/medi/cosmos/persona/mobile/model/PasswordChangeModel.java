/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class PasswordChangeModel {
    
    @NotNull(message = "Username field cannot be null")
    private String username;
    @NotNull(message = "Bank ID field cannot be null")
    private String bankId;
    @NotNull(message = "Old Password field cannot be null")
    private String oldPassword;
    @NotNull(message = "New Password field cannot be null")
    private String newPassword;
     
   

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

   
 

}

