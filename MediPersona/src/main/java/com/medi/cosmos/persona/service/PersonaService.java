/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dataview.model.ViewBloodDetails;
import com.medi.cosmos.persona.model.Allergy;
import com.medi.cosmos.persona.model.BloodDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Oyewale
 */
@Component
public class PersonaService {
    @Autowired
    private MedicalDataDao medicalDao;
    
    public boolean logBloodData(BloodDetails blood) throws CosmosDatabaseException{
       
        ViewBloodDetails findBloodDetails = medicalDao.findBloodDetailsByUsername(blood.getUsername());
        
        if(findBloodDetails != null){
           medicalDao.deleteBloodDetailsByUsername(blood.getUsername());
        }
        BloodDetails logBloodDetails = medicalDao.logBloodDetails(blood);
        
        return logBloodDetails != null;
    
    }
    
    public boolean logAllergy(Allergy allergy) throws CosmosDatabaseException{
        Allergy addAllergy = medicalDao.addAllergy(allergy);
        return addAllergy != null;
    }
}
