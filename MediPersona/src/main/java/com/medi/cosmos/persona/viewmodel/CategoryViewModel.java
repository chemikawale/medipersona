/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.Category;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class CategoryViewModel {
     private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
      public Category toCategory(){
       Category toCategory = new Category();
        toCategory.setCategoryId(Utilities.generateId(5));
        toCategory.setCategoryName(categoryName.trim().toUpperCase());
        toCategory.setCreatedOn(Utilities.presentDateTime());
       return toCategory;
      }
}
