package com.medi.cosmos.persona.api.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.persona.dao.TokenUtils;
import com.medi.cosmos.persona.dao.UtilitiesDao;
import com.medi.cosmos.persona.dataview.model.ViewCountry;
import com.medi.cosmos.persona.mobile.model.AppLoginVerification;
import com.medi.cosmos.persona.mobile.model.UserRegister;
import com.medi.cosmos.persona.mobile.model.UserToken;
import com.medi.cosmos.persona.mobile.model.ViewUserToken;
import com.medi.cosmos.persona.model.UserModel;
import com.medi.cosmos.persona.service.NotificationService;
import com.medi.cosmos.persona.service.UserService;
import com.medi.cosmos.persona.utilities.UrlConstants;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


        

/**
 *
 * @author Oyewale
 */

 @CrossOriginResourceSharing
 @Controller
 @PropertySource(value={"classpath:server.properties"})
 public class PublicMobileController {

    @Autowired
    private CosmosUserDao cosmosUserDao;
     @Autowired
    private UserService userService;
       @Autowired 
    private  NotificationService notify;
       @Autowired
    private UtilitiesDao utiDao;
       @Autowired
    private TokenUtils tokenUtils;  
    
        
         
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
     @RequestMapping(value = UrlConstants.VERIFY_LOGIN, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> verifyUser(@RequestBody AppLoginVerification loginModel,BindingResult binding) throws CosmosDatabaseException{
       List responseDetails = new ArrayList();
        Map response = new HashMap();
               if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
         CosmosUser findByUserName = cosmosUserDao.findByUserName(loginModel.getUsername());
         if(findByUserName != null){
              boolean verifyPassword =  passwordEncoder.matches(loginModel.getPassword(),findByUserName.getPassword());
              if(verifyPassword){
                  String token = "";
                  //find if use has a token already
                  ViewUserToken findToken = tokenUtils.findTokenByUsername(loginModel.getUsername());
                  if(findToken != null){
                     token = findToken.getAccessToken();
                  }else{
                      token = Utilities.generateAccessToken(loginModel.getUsername());
                      UserToken userToken = new UserToken();
                      userToken.setAccessToken(token); 
                      userToken.setCreatedOn(new Date());
                      userToken.setPassword(loginModel.getPassword());
                      userToken.setUsername(loginModel.getUsername());
                      tokenUtils.SaveToken(userToken);
                  }
                    response.put("statusCode","00");
                     response.put("accessToken",token);
                    response.put("status","Authentication Successful");
                    response.put("response","Login Successful");
                    responseDetails.add(response);
              }else{
                    response.put("statusCode","02");
                    response.put("status","Authentication Failed");
                    response.put("response","Password incorrect");
                    responseDetails.add(response);
              }
         }
         else{
                response.put("statusCode","03");
                response.put("status","Invalid Authorization");
                response.put("response","User with the username does not exist");
                responseDetails.add(response);
         }
         Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
    
      
     @RequestMapping(value = UrlConstants.SIGN_UP_USER, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> SignUpUser(@RequestBody UserRegister userModel,BindingResult binding) throws CosmosDatabaseException{
       List responseDetails = new ArrayList();
        Map response = new HashMap();
               if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
         CosmosUser findByUserName = cosmosUserDao.findByUserName(userModel.getUsername());
         if(findByUserName != null){
             
                response.put("statusCode","02");
                response.put("status","Authentication Failed ");
                response.put("response","Your preferred username already exists");
                responseDetails.add(response);
         }
         else{
             
           CosmosUser findByEmail = cosmosUserDao.findByEmail(userModel.getEmail());
           
           if(findByEmail != null){
               
            response.put("statusCode","02");
                response.put("status","Authentication Failed ");
                response.put("response","Email address already exists");
                responseDetails.add(response);
           }
           else{
               UserModel toUser = userModel.toUser();           
                String userName = toUser.getUsername();
                String userEmail = toUser.getEmail();
                String userPassword = toUser.getPassword();
                String lastName = toUser.getLastName();          
        
         boolean createUser = userService.createUserAdmin(toUser);
      
            if(!createUser){
                response.put("statusCode","03");
                response.put("status","Registration Failed");
                response.put("response","An error occur while creating a user account");
                responseDetails.add(response);
            }
            else{
                response.put("statusCode","00");
                response.put("status","Registration Successful");
                response.put("response","User account created successfully, please check your email.");
                responseDetails.add(response);
                notify.SignupNotification(userName, userPassword, lastName, userEmail);
            }        
                       
         }}
         
         Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
    @RequestMapping(value = UrlConstants.GET_ALL_COUNTRIES, method = RequestMethod.GET)
    public @ResponseBody Map<String,List> getAllCountries() throws CosmosDatabaseException{
       List responseDetails = new ArrayList();
       Map response = new HashMap();
            List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
                    response.put("statusCode","00");
                    response.put("status","Successfull");                    
                    response.put("countries",allMediCountries);
                    responseDetails.add(response);
         Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
}
