/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.AssignHmoPlan;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class AssignHmoPlanViewModel {
    private String planId;
    private String hmoId;
    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getHmoId() {
        return hmoId;
    }

    public void setHmoId(String hmoId) {
        this.hmoId = hmoId;
    }

    
    public AssignHmoPlan toAssignPlan(){
     AssignHmoPlan toAssign = new AssignHmoPlan();
     toAssign.setAssignId(Utilities.generateNumericId(10));
     toAssign.setCreatedOn(new Date());
     toAssign.setHmoId(hmoId.trim());
     toAssign.setPlanId(planId.trim());

     
     return toAssign;
    }
}
