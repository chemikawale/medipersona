/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import com.medi.cosmos.persona.model.VitalSigns;
import com.medi.cosmos.persona.utilities.Utilities;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class VitalSignModel {
    @NotNull(message = "Username field cannot be null")
     private String username;
    @NotNull(message = "Systolic field cannot be null")
    private String systolic;
     @NotNull(message = "Diastolic field cannot be null")
    private String diastolic;
      @NotNull(message = "Weight field cannot be null")
    private String weight;
       @NotNull(message = "Height field cannot be null")
    private String height;
        @NotNull(message = "Temperature field cannot be null")
    private String temperature;
         @NotNull(message = "Pulse field cannot be null")
    private String pulse;
          @NotNull(message = "Respiration field cannot be null")
    private String respiration;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSystolic() {
        return systolic;
    }

    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    public String getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getRespiration() {
        return respiration;
    }

    public void setRespiration(String respiration) {
        this.respiration = respiration;
    }
    
    public VitalSigns toVitalSigns(){
       VitalSigns toSigns = new VitalSigns();
           
            toSigns.setUsername(username);
            toSigns.setCreatedOn(Utilities.presentDateTime());
            toSigns.setDiastolic(diastolic.trim());
            toSigns.setHeight(height.trim());
            toSigns.setPulse(pulse.trim());
            toSigns.setRespiration(respiration.trim());
            toSigns.setSystolic(systolic.trim());
            toSigns.setTemperature(temperature.trim());
            toSigns.setWeight(weight.trim());
            toSigns.setBmi(""+Utilities.calculateBmi(weight.trim(), height.trim()));
            toSigns.setReadingId(Utilities.generateId(10));
    
       return toSigns;
    }
 
     
}
