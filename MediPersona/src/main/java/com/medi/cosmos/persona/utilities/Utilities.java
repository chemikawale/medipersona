/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.utilities;

import com.medi.cosmos.security.model.RoleGroup;
import com.medi.cosmos.security.service.CosmosRoleService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Oyewale
 */
public class Utilities {
    @Autowired
    private CosmosRoleService cosmosRoleService; 
    
    public static String trim(String object, String param) {
        return object.substring(param.length() + object.indexOf(param));
    }

    public static void main(String[] arg) {
        try {
            System.out.println(getMaxDate(new Date()));// System.out.println(trim(RoleConstants.ROLE_HOSPITAL_CUSTOMER, "ROLE_"));
              System.out.println(getMinDate(new Date()));
        } catch (Exception ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String getExpiryTime(int expiryPeriod) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, expiryPeriod);
        String date = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss a").format(cal.getTime());
        return date;
    }

    public static String getFormatedTime() {
        String date = new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss a").format(Calendar.getInstance().getTime());
        return date;
    }
/** Formatting phone number to the standard format
  * @param phonenumber
 * @return formatted phone number
 */
    public static String formatPhone(String phonenumber) {
        String phone = "";
        phonenumber = phonenumber.replace("+", "").trim();

        if (!phonenumber.substring(0, 3).equalsIgnoreCase("234")) {
            if (phonenumber.substring(0, 1).equalsIgnoreCase("0")) {
                phone = "234" + phonenumber.substring(1);
            } else {
                phone = "234" + phonenumber;
            }

        } else if (phonenumber.substring(0, 3).equalsIgnoreCase("234")) {
            String fph = phonenumber.substring(3);
            if (fph.substring(0, 1).equalsIgnoreCase("0")) {
                phone = "234" + fph.substring(1);
            } else {
                phone = "234" + fph;
            }

        }
        return phone.trim();
    }

    /**
     * Validation of the length of phone number to be in the standard length of 11
     * @param phoneNumber
     * @return boolean
     */
    public static boolean validatePhoneNo(String phoneNumber){
        return phoneNumber.length() == 11;
     
    }
    
    public static String maskCCNumber(String ccnum) {
        int total = ccnum.length();
        int startlen = 6, endlen = 4;
        int masklen = total - (startlen + endlen);
        StringBuilder maskedbuf = new StringBuilder(ccnum.substring(0, startlen));
        for (int i = 0; i < masklen; i++) {
            maskedbuf.append('*');
        }
        maskedbuf.append(ccnum.substring(startlen + masklen, total));
        String masked = maskedbuf.toString();
        return masked;
    }

    public static String generateUniqueId(int IdLength) {
        String otp = new String();
        int otpSample = 0;
        for (int i = 0; i < IdLength; i++) {
            otp = otp + "9";
        }
        otpSample = Integer.parseInt(otp);
        SecureRandom prng;
        try {
            prng = SecureRandom.getInstance("NativePRNG "); //Number Generation Algorithm
            otp = Integer.toString(prng.nextInt(otpSample));
            otp = (otp.length() < IdLength) ? padleft(otp, IdLength, '0') : otp;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return otp;
    }

    private static String padleft(String s, int len, char c) { //Fill with some char or put some logic to make more secure
        s = s.trim();
        StringBuilder d = new StringBuilder(len);
        int fill = len - s.length();
        while (fill-- > 0) {
            d.append(c);
        }
        d.append(s);
        return d.toString();
    }

    public static String callURL(String myURL) {
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            if (urlConn != null) {
                urlConn.setReadTimeout(60 * 1000);
            }
            if (urlConn != null && urlConn.getInputStream() != null) {
                in = new InputStreamReader(urlConn.getInputStream(),
                        Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
        } catch (Exception e) {
            throw new RuntimeException("Exception while calling URL:" + myURL, e);
        }

        return sb.toString();
    }

    public static String hash(String x) throws Exception {
        java.security.MessageDigest digest = null;
        digest = java.security.MessageDigest.getInstance("SHA-1");
        digest.update(x.getBytes("UTF-8"));
        return bytesToHex(digest.digest());
    }

    public static String bytesToHex(byte[] b) {
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuilder buf = new StringBuilder();
        for (int j = 0; j < b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }
    
    
    public static Date getMaxDate(Date day) {
     Calendar cal=Calendar.getInstance();
      cal.setTime(day);
      cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
      cal.set(Calendar.MINUTE,      cal.getMaximum(Calendar.MINUTE));
      cal.set(Calendar.SECOND,      cal.getMaximum(Calendar.SECOND));
        return cal.getTime();
  }
     public static Date getMinDate(Date day) {
     Calendar cal=Calendar.getInstance();
      cal.setTime(day);
      cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
      cal.set(Calendar.MINUTE,      cal.getMinimum(Calendar.MINUTE));
      cal.set(Calendar.SECOND,      cal.getMinimum(Calendar.SECOND));
        return cal.getTime();
  }

    public static String generateNumericId(int idLength) {
        String otp = RandomStringUtils.randomNumeric(idLength);
        return otp;
    }
    
    /**  
     * Generating a random ID in alphanumeric format of specified length
     * @param length     
     * @return id   
     **/
       public static String generateId(int length) {
        String id = RandomStringUtils.randomNumeric(length);
        return id;
    }
    
 public static String toddMMyy(Date day){ 
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        // Use London's time zone to format the date in
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        
        String date = formatter.format(day);
        
        return date;
    }
 public static Date presentDateTime(){ 
    
         TimeZone.setDefault(TimeZone.getTimeZone("Africa/Lagos"));      
            return new Date();
    }
 

 
 public static String generateRandomID(int codeLength) {
       
        String id = RandomStringUtils.randomAlphanumeric(codeLength);
        return id;
    }
   
 public static String toMoney(String totalAmount){
    String total = "";
    if("".equals(totalAmount)){
       total = "0";
    }else{
         DecimalFormat formatter = new DecimalFormat("###,###,###.##");
             total=  formatter.format(Double.parseDouble(totalAmount));
    }
    return total;
 }
 public static Date stringToDate(String sDate){
    SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date formatDate = null;
       
        try {
            formatDate = formatObj.parse(sDate);
        } catch (ParseException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
            return formatDate;
   }
public static boolean isThisDateValid(String dateToValidate, String dateFromat){
 
		if(dateToValidate == null){
			return false;
		}
 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
 
		try {
 
			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			//System.out.println(date);
 
		} catch (ParseException e) {
 
			e.printStackTrace();
			return false;
		}
 
		return true;
	}


   public static String calculateBmi(String weight, String height){
    double bmi = 0.0;
    
    bmi = Double.parseDouble(weight.trim()) / (Double.parseDouble(height.trim()) * Double.parseDouble(height.trim())) ;
    DecimalFormat df = new DecimalFormat("#.00");
   return  df.format(bmi);
   
   }
   
   public static String dateToFormat(Date originalDate){
             
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
           return formatObj2.format(originalDate);
   
   }
         
       
     public static Date StringToDate(String date){
         Date format = null;
     if( date != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("MM-dd-yyyy");         
     
         try {
             format = formatObj.parse(date);
         } catch (ParseException ex) {
             Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
             return format;
    }
    
    public static String generateAccessToken (String username){
        String token = username;
         try{
            
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(username.getBytes("UTF-8"));
            
            byte[] bytes = digest.digest();
            
            java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
            
            token = encoder.encodeToString(bytes);
            
        }catch(NoSuchAlgorithmException nsae){
        
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        
      return token;
       }
}

