/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.utilities;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.logging.Level;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
//import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 *
 * @author Nippon
 */
public class RSAServiceUtil {

   // private static final transient Logger LOG = Logger.getLogger(RSAServiceUtil.class);
    private static Cipher ecipher;
    private Cipher dcipher;
    private KeyPair rsaKey;
    private static Key key;
    private static Cipher cipher;
    private static int mode; // 0 is encryption while 1 is decryption
    
    public RSAServiceUtil() {
        try {
            ecipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            dcipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

        } catch (Exception ex) {
           // LOG.error(ex.getMessage());
        }
    }

    public void openKeyStore(String keystoreFile, String keystorePassword,
            String alias, String aliasPassword) throws Exception {
        Resource resource = null;

        try {
            KeyStore ks = KeyStore.getInstance("jks");
            resource = new ClassPathResource(keystoreFile);

            ks.load(resource.getInputStream(), keystorePassword.toCharArray());
            rsaKey = this.get3DesKeyPair(ks, alias, aliasPassword.toCharArray());

            if (this.rsaKey != null) {
                ecipher.init(Cipher.ENCRYPT_MODE, this.rsaKey.getPrivate());
                dcipher.init(Cipher.DECRYPT_MODE, this.rsaKey.getPublic());
            } else {
                //LOG.error("RSA Key is null");
                throw new Exception("RSA Key is NULL");
            }

        } catch (Exception ex) {
            System.out.println(ex);
           // LOG.error("Unable to create keys: " + ex.getMessage());
            throw ex;
        }
    }

    private KeyPair get3DesKeyPair(KeyStore keystore, String alias, char[] password) {
        try {
            // Get private key
            Key key = keystore.getKey(alias, password);
            if (key instanceof PrivateKey) {
                java.security.cert.Certificate cert = keystore.getCertificate(alias);
                //X509Certificate xCert = (X509Certificate) cert;
                PublicKey publicKey = cert.getPublicKey();
                return new KeyPair(publicKey, (PrivateKey) key);
            }
        } catch (UnrecoverableKeyException e) {
         //   LOG.error(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
          //  LOG.error(e.getMessage());
        } catch (KeyStoreException e) {
            //LOG.error(e.getMessage());
        } catch (Exception ex) {
            //LOG.error(ex.getMessage());
        }
        return null;
    }

    public String RSACertEncrypt(byte[] str) {
        try {
            byte[] enc = ecipher.doFinal(str);
            return Base64.encode(enc);
        } catch (javax.crypto.BadPaddingException e) {
          //  LOG.error("Bad padding during RSA encryption :" + e);
        } catch (IllegalBlockSizeException e) {
            //LOG.error("Illegal block size during RSA encryption : " + e);
        }

        return null;
    }

    public byte[] RSACertDecrypt(String encrypted3DesKey) {
        try {
            // Decode base64 to get 
            byte[] dec = Base64.decode(encrypted3DesKey);
            byte[] ret = dcipher.doFinal(dec);
            return ret;
        } catch (javax.crypto.BadPaddingException e) {
           // LOG.error("Bad padding during RSA decryption :" + e);
        } catch (IllegalBlockSizeException e) {
            //LOG.error("Illegal block size during RSA decryption : " + e);
        }

        return null;

    }    
    
    public static String AESEncryption(String keyValue, String text){
        
        try{
            
//            if(!keyString.equalsIgnoreCase(keyValue)){
                key = getKey(keyValue);
                cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, key);
//            }
            
//            if(mode == 1){
//                cipher.init(Cipher.ENCRYPT_MODE, key);
//            }
            
            byte[] encryptedValue = cipher.doFinal(text.getBytes());
            
            java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
            
            mode = 0;
            
            return encoder.encodeToString(encryptedValue);
            
        }catch(NoSuchAlgorithmException | NoSuchPaddingException 
                | IllegalBlockSizeException | BadPaddingException 
                | InvalidKeyException algorithmException){
            
        }
        
        return null;
    }
    
    public static String AESDecryption(String keyValue, String encryptedText){
        
        try{
//             if(!keyString.equalsIgnoreCase(keyValue)){
//                
//            }
//             
//            if(mode == 0){
//                cipher.init(Cipher.DECRYPT_MODE, key);
//            }
            
            key = getKey(keyValue);
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            
            byte[] decryptedValue = cipher.doFinal(java.util.Base64.getDecoder().decode(encryptedText));
            
            return new String(decryptedValue);
            
        }catch(NoSuchAlgorithmException algorithmException){
            
        } catch (NoSuchPaddingException ex) {
            //java.util.logging.Logger.getLogger(SimpleXorApp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalBlockSizeException ex) {
            ///java.util.logging.Logger.getLogger(SimpleXorApp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            //java.util.logging.Logger.getLogger(SimpleXorApp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException ex) {
            //java.util.logging.Logger.getLogger(SimpleXorApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static SecretKey getKey(String key){
        
        MessageDigest messageDigest;
        
        try{
            byte[] tempKey = key.getBytes("UTF-8");
            messageDigest = MessageDigest.getInstance("SHA-256");
            tempKey = messageDigest.digest(tempKey);
            
            tempKey = Arrays.copyOf(tempKey, 16);
            //System.out.println("The new keys size "+ tempKey.length);
            
            SecretKey secretKey = new SecretKeySpec(tempKey,"AES");
            
            return  secretKey;
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException ex){
            return null;
        }
    }
    
    public static String sha256Hash(String pan){
        
        try{
            
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(pan.getBytes("UTF-8"));
            
            byte[] bytes = digest.digest();
            
            java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
            
            return encoder.encodeToString(bytes);
            
        }catch(NoSuchAlgorithmException nsae){
        
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(RSAServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static String passwordLinkGenerator(String email, String dateString){
        
        if(email == null)
            return null;
        
        if(email.isEmpty())
            return null;
        
        try{
            
            String value = email.concat(dateString);
            
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(value.getBytes("UTF-8"));
            
            byte[] bytes = digest.digest();
            
            java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();
            
            return encoder.encodeToString(bytes);
            
        }catch(NoSuchAlgorithmException nsae){
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(RSAServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
     public static String pbkdfHashing(String key, String text){
        try{
            
            PBEKeySpec spec = new PBEKeySpec(text.toCharArray(), key.getBytes(), 1000, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            
            byte[] encrytValue = skf.generateSecret(spec).getEncoded();
            
            return java.util.Base64.getEncoder().encodeToString(encrytValue);
            
        }catch(NoSuchAlgorithmException ex){
            
        } catch (InvalidKeySpecException ex) {
            java.util.logging.Logger.getLogger(RSAServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public static boolean pbkdfHashMatching(String encryptedPassword, String originalPassword, String key){
        try{
            
            byte[] ecPassword  = java.util.Base64.getDecoder().decode(encryptedPassword);
            
            PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), key.getBytes(), 1000, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            
            byte[] encrytValue = skf.generateSecret(spec).getEncoded();
            
            int diff = ecPassword.length ^ encrytValue.length;
            
            for(int i = 0; i < ecPassword.length && i < encrytValue.length; i++)
            {
                diff |= ecPassword[i] ^ encrytValue[i];
            }
            return diff == 0;
            
        }catch(NoSuchAlgorithmException ex){
            
        } catch (InvalidKeySpecException ex) {
            java.util.logging.Logger.getLogger(RSAServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    
    
}
