/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Oyewale
 */
public class PasswordValidator implements Validator {

	 
   @Override
   public boolean supports(Class<?> paramClass) {

	        return ChangePasswordViewModel.class.isAssignableFrom(paramClass);

    }
	
   @Override
   public void validate(Object obj, Errors errors) {

	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Please supply a password");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmpassword", "Please confirm your password");
	        ChangePasswordViewModel password = (ChangePasswordViewModel) obj;

	        if (!password.getPassword().equals(password.getConfirmpassword())) {

	            errors.rejectValue("confirmpassword", "Password and Confirm password not the same");

	        }
	    }
	}
