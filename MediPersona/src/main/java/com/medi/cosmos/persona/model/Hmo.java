/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.model;

import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class Hmo {
    private String hmoId;
    private String hmoName;
    private String address;
    private String email;
    private Date createdOn;
    private String countryId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getHmoId() {
        return hmoId;
    }

    public void setHmoId(String hmoId) {
        this.hmoId = hmoId;
    }

    public String getHmoName() {
        return hmoName;
    }

    public void setHmoName(String hmoName) {
        this.hmoName = hmoName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    
}
