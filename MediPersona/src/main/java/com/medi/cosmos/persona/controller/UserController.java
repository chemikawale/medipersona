/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dao.ReviewDao;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dataview.model.ViewAllergy;
import com.medi.cosmos.persona.dataview.model.ViewAllergyCategory;
import com.medi.cosmos.persona.dataview.model.ViewCustomAllergy;
import com.medi.cosmos.persona.dataview.model.ViewCustomReview;
import com.medi.cosmos.persona.dataview.model.ViewCustomVitalSigns;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.persona.dataview.model.ViewReview;
import com.medi.cosmos.persona.dataview.model.ViewReviewRequest;
import com.medi.cosmos.persona.dataview.model.ViewUser;
import com.medi.cosmos.persona.dataview.model.ViewVitalSigns;
import com.medi.cosmos.persona.medical.viewmodel.AllergyViewModel;
import com.medi.cosmos.persona.medical.viewmodel.BloodDetailsViewModel;
import com.medi.cosmos.persona.medical.viewmodel.MedicationViewModel;
import com.medi.cosmos.persona.model.Allergy;
import com.medi.cosmos.persona.model.BloodDetails;
import com.medi.cosmos.persona.model.CountOutput;
import com.medi.cosmos.persona.model.Medication;
import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.model.UpdateUser;
import com.medi.cosmos.persona.model.VitalSigns;
import com.medi.cosmos.persona.service.NotificationService;
import com.medi.cosmos.persona.service.PersonaService;
import com.medi.cosmos.persona.service.UserService;
import com.medi.cosmos.persona.utilities.PageResult;
import com.medi.cosmos.persona.viewmodel.ReviewRequestViewModel;
import com.medi.cosmos.persona.viewmodel.UpdateUserDetailsViewModel;
import com.medi.cosmos.persona.viewmodel.VitalSignViewModel;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.service.CosmosUserService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
     @Autowired
    private CosmosUserService cosmosUserService;
        @Autowired
    private CosmosUserDao cosmosDao;
         @Autowired
    private UserService userService;
        @Autowired 
    private  NotificationService notify;
           @Autowired
    private UserDao userDao;
             @Autowired
    private MedicalDataDao medicalDao;
              @Autowired
    private ReviewDao reviewDao;
               @Autowired
    private PersonaService personaService;
               
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        
    @RequestMapping(value = "/home", method = RequestMethod.GET)
     public  String UserHome(HttpServletRequest request, Model model,HttpSession session) throws CosmosDatabaseException {       
      
         String username =  (String) session.getAttribute("username");         
         List<ViewVitalSigns> recentSigns = userDao.findRecentVitalSignsByUsername(username);
         //CountOutput activityCount = userDao.getUserActivityCount(username);
         model.addAttribute("reviewCount", userDao.CountReviewByUsername(username));
         model.addAttribute("vitalCount", userDao.CountVitalSignsByUsername(username));
         model.addAttribute("usedRequestCount", userDao.CountUsedReviewRequestsByUsername(username));
         model.addAttribute("requestCount", userDao.CountReviewRequestsByUsername(username));
         model.addAttribute("vitalsigns", recentSigns);
         model.addAttribute("home", "active");
         
         CosmosUser findByUserName = cosmosDao.findByUserName(username);
          if(findByUserName != null){
          boolean result = passwordEncoder.matches("carepersona",findByUserName.getPassword());
          if(result){
              model.addAttribute("error","NOTICE !!! Please do well to change your password from the default one. Thanks");
             }
          }
         
         
         return "/user/index";
    }
                 
	     
    
    @RequestMapping(value = "/medicaldata", method = RequestMethod.GET)
    @ResponseBody
    public  Map<String,Object> UserMedicalChartsData(HttpServletRequest request, Model model,HttpSession session) throws CosmosDatabaseException {       
                     String username =  (String) session.getAttribute("username");
         List<ViewCustomVitalSigns> userLastTenVitalSigns = userDao.findLastTenVitalSignsByUsername(username);
                      
           Map<String,Object> chartsResponse = new HashMap<>();   
    
        chartsResponse.put("medicaldata",userLastTenVitalSigns);    
             
        
        return chartsResponse;
    }
         
      
     @RequestMapping(value = "/vitalsigns", method = RequestMethod.GET)
    public ModelAndView addVitalSigns(Model model) {     
        ModelAndView mav = new ModelAndView("/user/vitalsigns");
        mav.addObject("viewModel", new VitalSignViewModel());
         mav.addObject("vitalsigns", "active");
        return mav ;
    }

    @RequestMapping(value = "/vitalsigns", method = RequestMethod.POST)
    public String addNewVitalSigns(@ModelAttribute("viewModel") @Valid VitalSignViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
         String cusername = (String)session.getAttribute("username");
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new VitalSignViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/user/vitalsigns";
        }             
         VitalSigns toVitalSigns = viewModel.toVitalSigns();
         toVitalSigns.setUsername(cusername);
            boolean logSigns = userService.logVitalSigns(toVitalSigns);
            if(!logSigns){
                model.addAttribute("viewModel", new VitalSignViewModel());
                model.addAttribute("error", "Unable to add vital signs");
                return "/user/vitalsigns";
            } 
      model.addAttribute("success", "Vital Signs saved successfully");
          return "/user/vitalsigns";

    }
    
    
     @RequestMapping(value = {"/allvitalsigns"}, method = RequestMethod.GET)
    public String AllUserVitalSigns(Model model) throws CosmosDatabaseException {
        model.addAttribute("vitalsigns", "active");
        return "/user/allvitalsigns";
    }
    
     @RequestMapping(value = "/getuservitalsigns", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<ViewVitalSigns> allUserVitalSigns(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String cusername = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewVitalSigns> result;
             Page<ViewVitalSigns> page = userDao.findVitalSignsByUsername(cusername,start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
       
    
     @RequestMapping(value = "/reviewrequest", method = RequestMethod.GET)
    public ModelAndView requestReview(Model model) {     
        ModelAndView mav = new ModelAndView("/user/reviewrequest");
        mav.addObject("viewModel", new ReviewRequestViewModel());
         mav.addObject("reviewrequest", "active");
        return mav ;
    }

    @RequestMapping(value = "/reviewrequest", method = RequestMethod.POST)
    public String makeRequestReview(@ModelAttribute("viewModel") @Valid ReviewRequestViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
         String cusername = (String)session.getAttribute("username");
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new ReviewRequestViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/user/reviewrequest";
        }             
         ReviewRequest toReviewCode = viewModel.toReviewCode();
         toReviewCode.setUsername(cusername);
            boolean logSigns = userService.logReviewRequest(toReviewCode);
            if(!logSigns){
                model.addAttribute("viewModel", new ReviewRequestViewModel());
                model.addAttribute("error", "Unable to log request");
                return "/user/reviewrequest";
            }  
            //fetch and concatenate user's fullname
         CosmosUser findByUserName = cosmosDao.findByUserName(cusername);
         String userFullName = findByUserName.getLastName()+ " "+findByUserName.getFirstName();
            //concatenate reviewer full name
         String reviewer = toReviewCode.getLastName() + "  "+toReviewCode.getFirstName();
            notify.ReviewNotification(userFullName,reviewer,toReviewCode.getExpiryDate(), toReviewCode.getReviewcode(),toReviewCode.getEmail());
           model.addAttribute("success", "Review Request successfully");
          return "/user/reviewrequest";

    }
    
      @RequestMapping(value = {"/reviewrequests"}, method = RequestMethod.GET)
    public String AllUserReviewRequest(Model model) throws CosmosDatabaseException {
        model.addAttribute("reviewrequest", "active");
        return "/user/reviewrequests";
    }
    
//     @PreAuthorize("isAuthenticated")
     @RequestMapping(value = "/getuserreviewrequests", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewReviewRequest> allUserReviewRequest(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String cusername = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewReviewRequest> result;
             Page<ViewReviewRequest> page = userDao.findReviewRequestsByUsername(cusername,start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
       
    
   
    @RequestMapping(value = {"/deleterequest"}, method = RequestMethod.GET)    
    public String deleteRequests(@RequestParam("reviewCode") String reviewCode, Model model) throws CosmosDatabaseException  {
       
        userDao.deleteReviewRequest(reviewCode);
                
      return "redirect:/user/reviewrequests";
    }
    
      @RequestMapping(value = "/medicalcharts", method = RequestMethod.GET)
    public String allMedicalRecords(HttpServletRequest request, Model model) throws CosmosDatabaseException {
       model.addAttribute("charts", "active");
        return "/user/medicalcharts";
    }

      @RequestMapping(value = "/updatedetails", method = RequestMethod.GET)
    public String updateUserDetails(Model model,HttpSession session) throws CosmosDatabaseException {     
          String cusername = (String)session.getAttribute("username");
          ViewUser userDetails = userDao.findUserByUsername(cusername);         
          UpdateUser updateUser = new UpdateUser();
         
            updateUser.setEmail(userDetails.getEmail());
            updateUser.setFirstName(userDetails.getFirstName());
            updateUser.setLastName(userDetails.getLastName());
            updateUser.setPhoneNumber(userDetails.getPhoneNumber());
            updateUser.setUsername(userDetails.getUsername());
            model.addAttribute("viewModel", updateUser);
            
         model.addAttribute("updateprofile", "active");
        return "/user/updatedetails" ;
    }

    @RequestMapping(value = "/updatedetails", method = RequestMethod.POST)
    public String updateUser(@ModelAttribute("viewModel") @Valid UpdateUserDetailsViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
       
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new UpdateUserDetailsViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/user/updatedetails";
        }             
         UpdateUser toUpdate = viewModel.toUpdate();
         
            boolean logSigns = userService.updateUserAdmin(toUpdate);
            if(!logSigns){
                model.addAttribute("viewModel", new UpdateUserDetailsViewModel());
                model.addAttribute("error", "Unable to update your account");
                return "/user/updatedetails";
            } 
      model.addAttribute("success", "Data update successful");
          return "/user/updatedetails";

    }
      @RequestMapping(value = "/blooddetails", method = RequestMethod.GET)
    public ModelAndView addBloodDetails(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/user/blooddetails");
         mav.addObject("viewModel", new BloodDetailsViewModel());
         mav.addObject("blooddetails", "active");
        return mav ;
    }
    
  
    @RequestMapping(value = "/blooddetails", method = RequestMethod.POST)
    public String logBloodDetails(@ModelAttribute("viewModel") @Valid BloodDetailsViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
         
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new BloodDetailsViewModel());
            model.addAttribute("error", "Please correct invalid fields");
            return "/user/blooddetails";
        }    
            String cusername = (String)session.getAttribute("username");
            BloodDetails bloodDetails = viewModel.getBloodDetails();
            bloodDetails.setUsername(cusername);        
                  
            boolean logBloodData = personaService.logBloodData(bloodDetails);
       
          if(logBloodData){
                model.addAttribute("viewModel", new BloodDetailsViewModel());
                model.addAttribute("success", "Blood Data updated successfully");
                return "/user/blooddetails";
            } 
      
                model.addAttribute("viewModel", new BloodDetailsViewModel());
                model.addAttribute("error", "An error occur while updating your blood data");
                 return "/user/blooddetails";
        

    }
    
      
      @RequestMapping(value = "/addallergy", method = RequestMethod.GET)
    public ModelAndView addUserAllergy(Model model) throws CosmosDatabaseException {     
        
        ModelAndView mav = new ModelAndView("/user/addallergy");
        
         mav.addObject("viewModel", new AllergyViewModel());  
         List<ViewAllergyCategory> allAllergyCategories = medicalDao.getAllAllergyCategories();
         mav.addObject("allAllergyCategories",allAllergyCategories);
         mav.addObject("allergy", "active");
        
         return mav ;
    }
    
  
    @RequestMapping(value = "/addallergy", method = RequestMethod.POST)
    public String addNewUserAllergy(@ModelAttribute("viewModel") @Valid AllergyViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            
            model.addAttribute("viewModel", new AllergyViewModel());
            model.addAttribute("error", "Please correct invalid fields");
            
            return "/user/addallergy";
        }    
            String cusername = (String)session.getAttribute("username");
            Allergy toAllergy = viewModel.toAllergy();
            toAllergy.setUsername(cusername);        
                  
            boolean logAllergy = personaService.logAllergy(toAllergy);
       
          if(logAllergy){
           
                model.addAttribute("viewModel", new AllergyViewModel());
                model.addAttribute("success", "Allergy Details updated successfully");
                return "redirect:/user/allallergies";
            
          } 
      
                model.addAttribute("viewModel", new AllergyViewModel());
                model.addAttribute("error", "An error occur while updating your allergy details");
                 return "/user/addallergy";
        

    }
   
    
     @RequestMapping(value = {"/allallergies"}, method = RequestMethod.GET)
    public String AllUserAllergies(Model model) throws CosmosDatabaseException {
        model.addAttribute("allergy", "active");
        return "/user/allergies";
    }
    
     
     @RequestMapping(value = "/getuserallergies", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewCustomAllergy> PaginatedAllUseraAllergies(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String cusername = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomAllergy> result;
          Page<ViewAllergy> allUserAllergies = medicalDao.getAllUserAllergies(start, length, cusername);  
          List<ViewAllergy> allergies = allUserAllergies.getContent();
          List<ViewCustomAllergy> userAllergies = new ArrayList<>();
            if(allergies.size() > 0){
               for(ViewAllergy allergy : allergies){
                ViewCustomAllergy customAllergy = new ViewCustomAllergy();
                customAllergy.setActive(allergy.isActive());
                customAllergy.setAllergicTo(allergy.getAllergicTo());
                customAllergy.setCategoryId(allergy.getCategoryId());
                customAllergy.setCategoryName(medicalDao.findAllergyCategoryById(allergy.getCategoryId()).getCategoryName());
                customAllergy.setComment(allergy.getComment());
                customAllergy.setCreatedOn(allergy.getCreatedOn());
                customAllergy.setEntryId(allergy.getEntryId());
                customAllergy.setLocation(allergy.getLocation());
                customAllergy.setReaction(allergy.getReaction());
                customAllergy.setSeverity(allergy.getSeverity());
                customAllergy.setStarted(allergy.getStarted());
                        
                    userAllergies.add(customAllergy);
               
               
               }
            
            
            }
            result = new PageResult<>( userAllergies, allUserAllergies.getCount() , allUserAllergies.getCount());
            result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
        
       @RequestMapping(value = {"/deleteallergy"}, method = RequestMethod.GET)    
       public String deleteAllergy(@RequestParam("entryId") String entryId, Model model) throws CosmosDatabaseException  {
       
       
           medicalDao.deleteAllergyByEntryId(entryId);
                
      return "redirect:/user/allallergies";
    }
              
   @RequestMapping(value = {"/reviews"}, method = RequestMethod.GET)
    public String UserReviews(Model model,  HttpSession session) throws CosmosDatabaseException {
       
         model.addAttribute("reviews", "active");
        return "/user/reviews";
    }    
 @RequestMapping(value = "/userreviews", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewCustomReview> getUserReviews(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String username = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomReview> result;
          Page<ViewReview> allReviews = reviewDao.findReviewsForUsername(start, length, username);  
          List<ViewReview> content = allReviews.getContent();
          List<ViewCustomReview> userReviews = new ArrayList<>();
            if(content.size() > 0){
               for(ViewReview review : content){
                ViewCustomReview customReview = new ViewCustomReview();
                customReview.setComments(review.getComments());
                customReview.setDiagnosis(review.getDiagnosis());
                customReview.setUsername(review.getUsername());
                customReview.setComments(review.getComments());
                customReview.setCreatedOn(review.getCreatedOn());
                customReview.setRecommendation(review.getRecommendation());
                customReview.setReviewId(review.getReviewId());
                customReview.setReviewer(review.getReviewer());
                
                   ViewUser findUserByUsername = userDao.findUserByUsername(review.getUsername());
                customReview.setFullName(findUserByUsername.getLastName() +" "+findUserByUsername.getFirstName());
               
                        
                    userReviews.add(customReview);
               
               
               }
            
            
            }
            result = new PageResult<>( userReviews, allReviews.getCount() , allReviews.getCount());
            result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
        }      
        
         @RequestMapping(value = "/addmedication", method = RequestMethod.GET)
    public ModelAndView addMedication(Model model) {     
        ModelAndView mav = new ModelAndView("/user/addmedication");
        mav.addObject("viewModel", new MedicationViewModel());
         mav.addObject("medication", "active");
        return mav ;
    }

    @RequestMapping(value = "/addmedication", method = RequestMethod.POST)
    public String addNewMedication(@ModelAttribute("viewModel") @Valid MedicationViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws ParseException {
         String cusername = (String)session.getAttribute("username");
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new MedicationViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/user/addmedication";
        }             
         Medication toMedication = viewModel.toMedication();
         toMedication.setUsername(cusername);
            boolean logSigns = userService.saveMedication(toMedication);
            if(!logSigns){
                model.addAttribute("viewModel", new MedicationViewModel());
                model.addAttribute("error", "Unable to save medication");
                return "/user/addmedication";
            } 
      model.addAttribute("success", "Medication saved successfully");
          return "/user/addmedication";

    }
    
    
     @RequestMapping(value = {"/allmedications"}, method = RequestMethod.GET)
    public String AllUserMedications(Model model) throws CosmosDatabaseException {
        model.addAttribute("medication", "active");
        return "/user/allmedications";
    }
    
     @RequestMapping(value = "/usermedications", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewMedication> allUserMedications(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String cusername = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewMedication> result;
             Page<ViewMedication> page = userDao.findMedicationsByUsername(cusername,start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
       
    
}
