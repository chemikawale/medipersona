/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;

import com.medi.cosmos.notification.email.SmtpMailSender;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.persona.utilities.Utilities;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineFactory;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author Oyewale
 */
@Component
@PropertySource(value={"classpath:server.properties"})
public class NotificationService {
    
    private SmtpMailSender mailSender;
    private Properties properties;
    
    @Value(value ="${smtp.sender}")
    private String smtpSender;
    @Value(value = "${smtp.username}")
    private String smtpUsername;
   @Value(value = "${smtp.password}")
    private String smtpPassword;
   @Value(value = "${smtp.port}")
    private int smtpPort;
   @Value(value = "${smtp.host}")
    private String smtpHost;
   @Value("${sms.username}")
    private String smsUsername;
   @Value("${sms.password}")
    private String smsPassword;
     
   private VelocityEngine velocityEngine;

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }
   
    @PostConstruct
    public void init(){
        
        try{
           VelocityEngineFactory factory = new VelocityEngineFactory();
           Properties props = new Properties();
           props.put("resource.loader", "class");
           props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
           factory.setVelocityProperties(props);
           setVelocityEngine(factory.createVelocityEngine());
       }catch(IOException | VelocityException ex){
       }
     
        mailSender = new SmtpMailSender(smtpUsername, smtpPassword, smtpHost, smtpPort, true, true);
        
    }
       
     public void SignupNotification(String username, String password, String lastName,String receipentEmail)  {
       
                  try{
                 Map signupModel = new HashMap();
                signupModel.put("username",username.trim());  
                signupModel.put("password",password.trim());
                signupModel.put("lastName",lastName.trim().toUpperCase());                 
                            
                  String body = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "templates/signup.vm", "UTF-8", signupModel);
                  
             mailSender.sendHtmlMail(smtpSender,"MediPersona Account Creation",body,new String[]{receipentEmail});
          }catch(MessagingException messagingException){
        }
          
     }
     
      public void ResetPasswordNotification(String username,String link,String lastName,String receipentEmail)  {
       
                  try{
                      String resetLink = "https://medipersona.com/phr/forgotpassword/"+link;
                 Map passwordReset = new HashMap();
                 passwordReset.put("resetlink",resetLink);  
                 passwordReset.put("lastName",lastName.trim().toUpperCase());                 
                            
                  String body = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "templates/resetpassword.vm", "UTF-8", passwordReset);
                  
             mailSender.sendHtmlMail(smtpSender,"MediPersona Account Password Reset",body,new String[]{receipentEmail});
          }catch(MessagingException messagingException){
        }
          
     }
     
         public void ReviewNotification(String fullName,String reviewer,Date expiryTime, String reviewCode,String receipentEmail)  {
       
                  try{
                 Map reviewModel = new HashMap();
                reviewModel.put("reviewCode",reviewCode.trim());  
                reviewModel.put("fullName",fullName.trim());
                 reviewModel.put("reviewer",reviewer.trim());
                reviewModel.put("expiryTime",Utilities.dateToFormat(expiryTime));
                               
                            
                  String body = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "templates/review.vm", "UTF-8", reviewModel);
                  
             mailSender.sendHtmlMail(smtpSender,"MediPersona Profile Review Request",body,new String[]{receipentEmail});
          }catch(MessagingException messagingException){
        }
          
     }
  
          public void ReminderNotification(String emailAddress, String fullName, ViewMedication medication)  {
       
                  try{
                 Map reviewModel = new HashMap();
                reviewModel.put("fullName",fullName);  
                reviewModel.put("diagnosis",medication.getDiagnosis());
                 reviewModel.put("medication",medication.getMedication().trim());
                 reviewModel.put("prescriptionNote",medication.getPrescriptionNote().trim());
                  reviewModel.put("prescription",medication.getPrescription().trim());
                reviewModel.put("endDate",medication.getEndDate());                             
                            
                  String body = VelocityEngineUtils.mergeTemplateIntoString(
                velocityEngine, "templates/reminder.vm", "UTF-8", reviewModel);
                  
             mailSender.sendHtmlMail(smtpSender,"Medication Reminder",body,new String[]{emailAddress});
          }catch(MessagingException messagingException){
        }
          
     }
         
         
//    
         
//    public boolean accountSuperCreationSendNotification(String username, String password, String surname,String[] email) {
//        
//        try {
//            String headerString  = "<!DocType html><html><head>"
//                    + "</head><style>"
//                    + ".redcolor{ color: red; font-weight:bold; }"
//                    + "table{ width:100%; }"
//                    + "</style><body><table>";
//            String tailString = "</table></body><html/>";
//            
//            String bodyString = "<tr>Hello <b>"+ surname+"</b>, <br /></tr>"
//                    + "<tr>Your paypad super merchant account has been created successfully, <br />"
//                    + "The login details are as follows</tr>"
//                    + "<tr><td><strong>Username: </strong> &nbsp;&nbsp;&nbsp;&nbsp;   "+username+"</td></tr>"
//                    +"<tr><td><strong>Password: </strong>  &nbsp;&nbsp;&nbsp;&nbsp;   "+password+"</td></tr>"
//                    + "<tr></tr>"
//                    + "<tr><td colspan='2'>The password is a temporary one, pls changed it once you login successfully</td></tr>"
//                    + "You can login to www.paypad.com.ng </tr>"
//                     + "<tr><td colspan='2'><a href='https://www.paypad.com.ng'> Here </a></td></tr>"
//                    + "<tr></tr>"+ "<tr></tr>"
//                    + "<tr><td>Thanks<td></tr>"
//                    + "<tr></tr>"
//                    + "<tr><td>Powered by ESL</td></tr>";
//            String message = headerString + " "+bodyString + " "+ tailString;
//            mailSender.sendHtmlMail(smtpSender,"admin@paypad.com.ng","PAYPAD", message, email);
//            return true;
//          }catch(MessagingException messagingException){
//            return false; 
//        }
//    }
//    
//  
//    
    public void forgotPasswordNofication(String email, String link, String surname) {
        
        String headerString  = "<!DocType html><html><head>"
                + "</head><style>"
                + ".redcolor{ color: red; font-weight:bold; }"
                + "table{ width:100%; }"
                + "</style><body><table>";
        String tailString = "</table></body><html/>";
        try{
        String bodyString = "<tr>Hello <b>"+ surname+"</b>, <br /></tr>"
                + "<tr>We notice that you forgot your password, <br />"
                + "pls kindly use the link below to reset your password</tr>"
                + "<tr><td colspan='2'><a href='https://ibeji.paypad.com.ng/paypad/home/forgotpassword/"+link+"'>https://ibeji.paypad.com.ng/paypad/home/forgotpassword/"+link+"</td></tr>"
                + "<tr></tr>"
                +"<tr><td>If the link is not clickable pls copy it to the browser</td></tr>"
                + "<tr></tr>"
                + "<tr><td colspan='2'>The password reset link is only active for a period of time, "
                + "So endeavour to use the link on time</td></tr>"
                + "<tr></tr>"
                + "<tr><td>Thanks<td></tr>"
                + "<tr></tr>"
                + "<tr><td>Powered by ESL</td></tr>";
        String message = headerString + " "+bodyString + " "+ tailString;
        mailSender.sendHtmlMail(smtpSender,"admin@paypad.com.ng","PAYPAD", message , new String[]{email});
          }catch(MessagingException messagingException){
            return; 
        }
    }
    
//    public void resetPasswordNofication(String email, String surname){
//        
//        String headerString  = "<!DocType html><html><head>"
//                + "</head><style>"
//                + ".redcolor{ color: red; font-weight:bold; }"
//                + "table{ width:100%; }"
//                + "</style><body><table>";
//        String tailString = "</table></body><html/>";
//        try
//        {
//        String bodyString = "<tr>Hello <b>"+ surname+"</b>, <br /></tr>"
//                + "<tr></tr>"
//                + "<tr><td colspan='2'>Your password has been changed successfully</td></tr>"
//                + "<tr></tr>"
//                + "<tr><td>Thanks<td></tr>"
//                + "<tr></tr>"
//                + "<tr><td>Powered by ESL</td></tr>";
//        String message = headerString + " "+bodyString + " "+ tailString;
//        mailSender.sendHtmlMail(smtpSender,"admin@paypad.com.ng","PAYPAD", message , new String[]{email});
//         }catch(MessagingException messagingException){
//            return; 
//        }
//    }
//    
  
    
//    public boolean sendSms(String message, String phone){
//        
//        SmsRecipient smsRecipient = SmsRecipient.toSmsRecipient(phone, 3);
//        SmsMessage smsMessage = new SmsMessage("PAYPAD",smsRecipient);
//        smsMessage.setMessage(message);
//        MobileSmsProvider mobileSmsProvider = new MobileSmsProvider(smsUsername, smsPassword );
//        return mobileSmsProvider.send(smsMessage);
//   
//    }
//     public boolean sendCustomizedSms(String sender,String message, String phone){
//        
//        SmsRecipient smsRecipient = SmsRecipient.toSmsRecipient(phone, 3);
//        SmsMessage smsMessage = new SmsMessage(sender,smsRecipient);
//        smsMessage.setMessage(message);
//        MobileSmsProvider mobileSmsProvider = new MobileSmsProvider(smsUsername, smsPassword );
//        return mobileSmsProvider.send(smsMessage);
//   
//    }
     
}
