/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.Country;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class CountryViewModel {
    private String countryName;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    
    public Country toCountry(){
        
            Country toCountry = new Country();
               toCountry.setCountryId(Utilities.generateId(5));
               toCountry.setCountryName(countryName.trim().toUpperCase());
               toCountry.setCreatedOn(Utilities.presentDateTime());
            return toCountry;
            
    }
    
}
