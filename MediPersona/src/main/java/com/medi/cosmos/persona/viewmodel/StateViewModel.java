/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.MediState;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class StateViewModel {
      private String countryId;
      private String stateName;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
      
    public MediState toState(){
     MediState toState = new MediState();
        toState.setCountryId(countryId.trim());
        toState.setCreatedOn(Utilities.presentDateTime());
        toState.setStateId(Utilities.generateId(9));
        toState.setStateName(stateName.trim().toUpperCase());
        return toState;
     
    }
}
