/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.model;

/**
 *
 * @author Oyewale
 */
public class CountOutput {
    public int requestCount;
    public int vitalCount;
    public int usedRequestCount;
    public int reviewCount;

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public int getVitalCount() {
        return vitalCount;
    }

    public void setVitalCount(int vitalCount) {
        this.vitalCount = vitalCount;
    }

    public int getUsedRequestCount() {
        return usedRequestCount;
    }

    public void setUsedRequestCount(int usedRequestCount) {
        this.usedRequestCount = usedRequestCount;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }
    
}
