/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import com.medi.cosmos.persona.model.Medication;
import com.medi.cosmos.persona.utilities.Utilities;
import java.text.ParseException;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class MedicationModel {
    @NotNull(message = "Username field cannot be null")
    private String username;
    @NotNull(message = "Diagnosis field cannot be null")
    private String diagnosis;
    @NotNull(message = "Prescription field cannot be null")
    private String prescription;
    @NotNull(message = "Presciption Note field cannot be null")
    private String prescriptionNote;
    @NotNull(message = "Medication field cannot be null")
    private String medication;
    @NotNull(message = "Start Date field cannot be null")
    private String startDate;
    @NotNull(message = "End Date field cannot be null")
    private String endDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    
    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getPrescriptionNote() {
        return prescriptionNote;
    }

    public void setPrescriptionNote(String prescriptionNote) {
        this.prescriptionNote = prescriptionNote;
    }

   
    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public Medication toMedication() throws ParseException{
    
    Medication mediData = new Medication();
     
      mediData.setUsername(username);
      mediData.setPrescriptionNote(prescriptionNote.trim());
      mediData.setCreatedOn(Utilities.presentDateTime());
      mediData.setDiagnosis(diagnosis.trim());
      mediData.setMedication(medication.trim());
      mediData.setMedicationId(Utilities.generateRandomID(15)); 
      mediData.setPrescription(prescription.trim());
      mediData.setStartDate(Utilities.StringToDate(startDate.replaceAll("/","-").trim()));
      mediData.setEndDate(Utilities.StringToDate(endDate.replaceAll("/","-").trim()));
      
      
    return mediData;
    }
    
     
}
