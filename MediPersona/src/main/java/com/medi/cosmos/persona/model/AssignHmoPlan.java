/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.model;

import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class AssignHmoPlan {
      private String planId;
    private String hmoId;
    private String hmoName;
    private String planName;
    private Date createdOn;
    private String assignId;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getHmoId() {
        return hmoId;
    }

    public void setHmoId(String hmoId) {
        this.hmoId = hmoId;
    }

    public String getHmoName() {
        return hmoName;
    }

    public void setHmoName(String hmoName) {
        this.hmoName = hmoName;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getAssignId() {
        return assignId;
    }

    public void setAssignId(String assignId) {
        this.assignId = assignId;
    }
    
}
