/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.viewmodel.ChangePasswordViewModel;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import com.medi.cosmos.security.service.CosmosUserService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */
@Controller
@RequestMapping(value = "/pwd")
public class PasswordController {
    
    @Autowired
    private CosmosUserDao cosmosDao;
     @Autowired
    private CosmosAuthenticationProvider cosmosAuthenticationProvider;
       @Autowired
    private CosmosUserService cosmosUserService;
    
       @Autowired
       @Qualifier("passwordValidator")
       private Validator validator;
	    
       @InitBinder
       private void initBinder(WebDataBinder binder) {
	        binder.setValidator(validator);
	    }
            
             @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String UserChangePassword(HttpServletRequest request, Model model) {
        String username = request.getUserPrincipal().getName();
        ChangePasswordViewModel viewModel = new ChangePasswordViewModel();
        viewModel.setUsername(username);
        model.addAttribute("viewModel", viewModel);
        return "/user/changepassword";
    }
//
    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public String UserUpdatePassword(@ModelAttribute("viewModel") @Validated ChangePasswordViewModel viewModel, BindingResult result, RedirectAttributes rdat, Model model) {

        if (result.hasErrors()) {
           model.addAttribute("error", "Password and Confirm Password mis-match");
           return "/user/changepassword";
        }
        try {
            CosmosUser cu = cosmosDao.findByUserName(viewModel.getUsername());
            cu.setPassword(cosmosAuthenticationProvider.saltPassword(viewModel.getPassword()));            
            cosmosUserService.updateCosmosUser(cu);

        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }

        model.addAttribute("success", "password changed successfully ");
       return "/user/changepassword";
    }
    
   
}
