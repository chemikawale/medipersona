/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.ReviewModel;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class ReviewViewModel {
 
    private String username;
    private String diagnosis;
    private String comments;
    private String recommendation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }
    
    public ReviewModel getReview(){
      ReviewModel review = new ReviewModel();
        Date presentDateTime = Utilities.presentDateTime();
         review.setComments(comments.trim());
         review.setCreatedOn(presentDateTime);
         review.setDiagnosis(diagnosis.trim());
         review.setRecommendation(recommendation.trim());
         review.setReviewId(Utilities.generateId(15));
         review.setUsername(username.trim());
       
      return review;
     }
}
