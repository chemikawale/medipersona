/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.FeedBack;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
    public class FeedBackViewModel {
    private String title;
    private String body;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    
    public FeedBack toFeedBack(){
    
     FeedBack toFeed = new FeedBack();
      toFeed.setBody(body.trim());
      toFeed.setTitle(title.trim());
      toFeed.setCreatedOn(Utilities.presentDateTime());
      toFeed.setFeedBackId(Utilities.generateId(10));
      toFeed.setStatus(false);
      return toFeed;
      
    }
    
}
