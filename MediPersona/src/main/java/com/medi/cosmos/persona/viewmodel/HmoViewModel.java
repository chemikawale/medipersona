/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.Hmo;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class HmoViewModel {
    private String hmoName;
    private String address;
    private String email;
 private String countryId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
 
    public String getHmoName() {
        return hmoName;
    }

    public void setHmoName(String hmoName) {
        this.hmoName = hmoName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    public Hmo toHmo(){
     Hmo hmo = new Hmo();
     hmo.setAddress(address.trim());
     hmo.setCreatedOn(new Date());
     hmo.setEmail(email.trim());
     hmo.setHmoId(Utilities.generateNumericId(5));
     hmo.setHmoName(hmoName.trim());
     hmo.setCountryId(countryId.trim());
     return hmo;
    }
}
