/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.mobile.model.UserToken;
import com.medi.cosmos.persona.mobile.model.ViewUserToken;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class TokenUtils {
        private static final Logger logger = Logger.getLogger("TokenUtils Dao");
   
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    
     @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    }
    
        public UserToken SaveToken(UserToken model) throws CosmosDatabaseException {     
        
        try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_token");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to save token generated "+ exception.getMessage());
        }
    }
    
        public List<ViewUserToken> getAllTokens() throws CosmosDatabaseException{
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_access_tokens")
                            .returningResultSet("tokens", BeanPropertyRowMapper.newInstance(ViewUserToken.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewUserToken> tokens = (List<ViewUserToken>) result.get("tokens"); 
            
           return tokens;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching all access tokens "+exception.getMessage());
        }
       
      }
    public ViewUserToken findAccessToken(String accessToken) throws CosmosDatabaseException {       
        
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("accessToken",accessToken);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_user_by_accessToken")
                             .returningResultSet("token", BeanPropertyRowMapper.newInstance(ViewUserToken.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewUserToken> token = (List<ViewUserToken>) result.get("token"); 
            if(token.size() > 0){
           return token.get(0);
                   }else{
               return null;
            }}
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("unable to find user details by access token "+exception.getMessage());
        }
       
    }
        
    public boolean validateToken(String accessToken) throws CosmosDatabaseException {       
        
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("accessToken",accessToken);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_user_by_accessToken")
                             .returningResultSet("token", BeanPropertyRowMapper.newInstance(ViewUserToken.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewUserToken> token = (List<ViewUserToken>) result.get("token"); 
               return token.size() > 0;
}
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("unable to find user details by access token "+exception.getMessage());
        }
       
    }
        
    public ViewUserToken findTokenByUsername(String username) throws CosmosDatabaseException {       
        
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_token_by_username")
                             .returningResultSet("token", BeanPropertyRowMapper.newInstance(ViewUserToken.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewUserToken> token = (List<ViewUserToken>) result.get("token"); 
            if(token.size() > 0){
           return token.get(0);
                   }else{
               return null;
            }}
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("unable to find user details by username "+exception.getMessage());
        }
       
    }
       
     
        
          /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
