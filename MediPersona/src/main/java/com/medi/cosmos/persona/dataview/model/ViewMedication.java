/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dataview.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewMedication {
      private String medicationId;
    private String diagnosis;
    private String prescription;
    private String prescriptionNote;
    private String medication;
    private String startDate;
    private String endDate;
    private String createdOn;
    private String username;

    public String getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(String medicationId) {
        this.medicationId = medicationId;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getPrescriptionNote() {
        return prescriptionNote;
    }

    public void setPrescriptionNote(String prescriptionNote) {
        this.prescriptionNote = prescriptionNote;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getStartDate() {
               if( startDate != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(startDate);
        } catch (ParseException ex) {
            Logger.getLogger(ViewMedication.class.getName()).log(Level.SEVERE, null, ex);
        }
            
           // SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy");
        return formatObj2.format(format);}
         else 
             return startDate; 
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
         if( endDate != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(endDate);
        } catch (ParseException ex) {
            Logger.getLogger(ViewMedication.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            //SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy");
        return formatObj2.format(format);}
         else 
             return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCreatedOn() {
         if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewMedication.class.getName()).log(Level.SEVERE, null, ex);
        }
            
         //   SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
              SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy");
          
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    
    
}
