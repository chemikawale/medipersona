/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.medical.viewmodel;

import com.medi.cosmos.persona.model.Allergy;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class AllergyViewModel {
    private String categoryId;
    private String allergicTo;
    private String reaction;
    private String location;
    private String severity;
    private boolean active;
    private String started;
    private String comment;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getAllergicTo() {
        return allergicTo;
    }

    public void setAllergicTo(String allergicTo) {
        this.allergicTo = allergicTo;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public Allergy toAllergy(){
    Allergy allergy = new Allergy();
     allergy.setActive(active);
     allergy.setAllergicTo(allergicTo.trim());
     allergy.setCategoryId(categoryId.trim());
     allergy.setComment(comment);
     allergy.setEntryId(Utilities.generateId(10));
     allergy.setLocation(location.trim());
     allergy.setReaction(reaction.trim());
     allergy.setSeverity(severity.trim());
     allergy.setStarted(started.trim());
     allergy.setCreatedOn(Utilities.presentDateTime());
    return allergy;
    }
    
}
