package com.medi.cosmos.persona.api.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.persona.dataview.model.ViewUser;
import com.medi.cosmos.persona.dataview.model.ViewVitalSigns;
import com.medi.cosmos.persona.mobile.model.MedicationModel;
import com.medi.cosmos.persona.mobile.model.SendRequestModel;
import com.medi.cosmos.persona.mobile.model.UserVerificationModel;
import com.medi.cosmos.persona.mobile.model.VitalSignModel;
import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.service.NotificationService;
import com.medi.cosmos.persona.service.UserService;
import com.medi.cosmos.persona.utilities.UrlConstants;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author Oyewale
 */
 @CrossOriginResourceSharing(allowOrigins = "*")
 @RestController
 @PropertySource(value={"classpath:server.properties"})
 public class SecuredMobileController {

    @Autowired
    private CosmosUserDao cosmosUserDao;
    @Autowired
    private UserDao userDao;
    @Autowired 
    private  NotificationService notify;
      @Autowired
    private UserService userService;
    @Autowired
    private CosmosAuthenticationProvider cosmosAuthenticationProvider;
    
    
    private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
     
     
    @RequestMapping(value = UrlConstants.LOG_VITAL_SIGNS, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> LogVitalSigns(@RequestBody VitalSignModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException{
            List responseDetails = new ArrayList();
            Map response = new HashMap();
        if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        
        ViewUser findUserByUsername = userDao.findUserByUsername(requestModel.getUsername().trim());
         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
          if((findAdminByUserName != null)){  
             if(findUserByUsername != null){
                
                 boolean logVitalSigns = userService.logVitalSigns(requestModel.toVitalSigns());
                 if(logVitalSigns){
                   response.put("statusCode","00");
                    response.put("status","Successful");
                    response.put("vitalsign","Vital Signs logged successfully");
                    responseDetails.add(response);
                 }
                 else{
                     response.put("statusCode","02");
                    response.put("status","Error");
                    response.put("response","Unable to log vital signs successfully");
                    responseDetails.add(response);
        }             
             }
                else{                    
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username not valid");
                    responseDetails.add(response);
                    }
          }                 
         else{
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username does not exists or disabled");
                    responseDetails.add(response);      
         }
    
          Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
    @RequestMapping(value = UrlConstants.ALL_VITAL_SIGNS, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> AllVitalSigns(@RequestBody UserVerificationModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException{
            List responseDetails = new ArrayList();
            Map response = new HashMap();
        if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        
        ViewUser findUserByUsername = userDao.findUserByUsername(requestModel.getUsername().trim());
         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
          if((findAdminByUserName != null)){  
             if(findUserByUsername != null){
                 
                 //select the most rcent 10
                 List<ViewVitalSigns> recentVitalSigns = userDao.findRecentVitalSignsByUsername(requestModel.getUsername());  
                
                   response.put("statusCode","00");
                    response.put("status","Successful");
                    response.put("vitalsigns",recentVitalSigns);
                    responseDetails.add(response);
                
                 
             }
                else{                    
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username not valid");
                    responseDetails.add(response);
                    }
          }                 
         else{
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username does not exists or disabled");
                    responseDetails.add(response);      
         }
    
          Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
        
    
    @RequestMapping(value = UrlConstants.LOG_MEDICATION, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> LogMedication(@RequestBody MedicationModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException, ParseException{
            List responseDetails = new ArrayList();
            Map response = new HashMap();
        if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        
        ViewUser findUserByUsername = userDao.findUserByUsername(requestModel.getUsername().trim());
         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
          if((findAdminByUserName != null)){  
             if(findUserByUsername != null){
                
                 boolean logVitalSigns = userService.saveMedication(requestModel.toMedication());
                 if(logVitalSigns){
                   response.put("statusCode","00");
                    response.put("status","Successful");
                    response.put("medication","Medication logged successfully");
                    responseDetails.add(response);
                 }
                 else{
                     response.put("statusCode","02");
                    response.put("status","Error");
                    response.put("response","Unable to log medication successfully");
                    responseDetails.add(response);
        }             
             }
                else{                    
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username not valid");
                    responseDetails.add(response);
                    }
          }                 
         else{
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username does not exists or disabled");
                    responseDetails.add(response);      
         }
    
          Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
    @RequestMapping(value = UrlConstants.ALL_MEDICATIONS, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> AllMedications(@RequestBody UserVerificationModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException{
            List responseDetails = new ArrayList();
            Map response = new HashMap();
        if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        
        ViewUser findUserByUsername = userDao.findUserByUsername(requestModel.getUsername().trim());
         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
          if((findAdminByUserName != null)){  
             if(findUserByUsername != null){
                 
                 //select the most rcent 10
                 Page<ViewMedication> findMedications = userDao.findMedicationsByUsername(requestModel.getUsername(),0, 10);  
                
                   response.put("statusCode","00");
                    response.put("status","Successful");
                    response.put("medications",findMedications.getContent());
                    responseDetails.add(response);
                
                 
             }
                else{                    
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username not valid");
                    responseDetails.add(response);
                    }
          }                 
         else{
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username does not exists or disabled");
                    responseDetails.add(response);      
         }
    
          Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
     @RequestMapping(value = UrlConstants.SEND_REQUEST, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> SendReviewRequests(@RequestBody SendRequestModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException{
            List responseDetails = new ArrayList();
            Map response = new HashMap();
        if(binding.hasErrors()){
                    response.put("statusCode","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        
        ViewUser findUserByUsername = userDao.findUserByUsername(requestModel.getUsername().trim());
         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
          if((findAdminByUserName != null)){  
             if(findUserByUsername != null){
                 ReviewRequest toReviewCode = requestModel.toReviewCode();
                 boolean logRequest = userService.logReviewRequest(toReviewCode);
                 if(logRequest){
                      //fetch and concatenate user's fullname
         CosmosUser findByUserName = cosmosUserDao.findByUserName(requestModel.getUsername());
         String userFullName = findByUserName.getLastName()+ " "+findByUserName.getFirstName();
            //concatenate reviewer full name
         String reviewer = toReviewCode.getLastName() + "  "+toReviewCode.getFirstName();
         notify.ReviewNotification(userFullName,reviewer,toReviewCode.getExpiryDate(), toReviewCode.getReviewcode(),toReviewCode.getEmail());
  
                     
                   response.put("statusCode","00");
                    response.put("status","Successful");
                    response.put("response","Request sent successfully");
                    responseDetails.add(response);
                 }
                 else{
                     response.put("statusCode","02");
                    response.put("status","Error");
                    response.put("response","Unable to send request");
                    responseDetails.add(response);
        }             
             }
                else{                    
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username not valid");
                    responseDetails.add(response);
                    }
          }                 
         else{
                    response.put("statusCode","03");
                    response.put("status","Authorization Error");
                    response.put("response","Username does not exists or disabled");
                    responseDetails.add(response);      
         }
    
          Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
    
//    
//    
//    @RequestMapping(value = BankUrlConstant.UPDATE_BANK_ADMIN, method = RequestMethod.POST,consumes = "application/json")
//    public @ResponseBody Map<String,List> UpdateBankAdmin(@RequestBody WebUpdateBankAdmin requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException, WalletNotFoundExeption{
//            List responseDetails = new ArrayList();
//            Map response = new HashMap();
//        if(binding.hasErrors()){
//                    response.put("statusCode","01");
//                    String message = "";
//                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
//                    response.put("status","Error detected in request sent");
//                    response.put("response",message);
//                 responseDetails.add(response);
//         }     
//        
//         ViewBankAdmin findBankAdminByUsername = bankDao.findBankAdminByUsername(requestModel.getUsername().trim());
//         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
//          if((findAdminByUserName != null)){  
//             if(findBankAdminByUsername != null){
//             String bankId = findBankAdminByUsername.getBankId();             
//                ViewBank findBankById = bankDao.findBankById(bankId);
//                if(findBankById.getBankId().trim().equalsIgnoreCase(requestModel.getBankId())){
//                 
//                    boolean updateResponse = bankService.updateBankWebAdmin(requestModel.getUsername(), requestModel.getRoles(), true);
//                                  
//                    response.put("statusCode","00");
//                    response.put("status","Operation Successful");
//                    response.put("response",updateResponse);
//                    responseDetails.add(response);
//        }             
//                else{                    
//                    response.put("statusCode","03");
//                    response.put("status","Authorization Error");
//                    response.put("response","Bank mis-match");
//                    responseDetails.add(response);
//                    }
//                    }
//             else{                    
//                    response.put("statusCode","03");
//                    response.put("status","Authorization Error");
//                    response.put("response","Bank/Admins Details not found");
//                    responseDetails.add(response);
//                     }
//    }
//         else{
//                    response.put("statusCode","03");
//                    response.put("status","Authorization Error");
//                    response.put("response","Username does not exists or disabled");
//                    responseDetails.add(response);      
//         }
//    
//          Map<String,List> requestResponse = new HashMap<>();
//          requestResponse.put("requestResponse",responseDetails);
//          return requestResponse;
//    }
//    
//    
//
//    
//    
//      @RequestMapping(value = BankUrlConstant.TRANSACTION_SUMMARY, method = RequestMethod.POST,consumes = "application/json")
//    public @ResponseBody Map<String,List> TransactionSummary(@RequestBody BankVerificationModel requestModel,BindingResult binding) throws CosmosDatabaseException, IOException, FileNotFoundException, ClassNotFoundException, WalletNotFoundExeption{
//            List responseDetails = new ArrayList();
//            Map response = new HashMap();
//        if(binding.hasErrors()){
//                    response.put("statusCode","01");
//                    String message = "";
//                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
//                    response.put("status","Error detected in request sent");
//                    response.put("response",message);
//                 responseDetails.add(response);
//         }     
//        
//         ViewBankAdmin findBankAdminByUsername = bankDao.findBankAdminByUsername(requestModel.getUsername().trim());
//         CosmosUser findAdminByUserName = cosmosUserDao.findByUserName(requestModel.getUsername().trim());
//         
//           if((findAdminByUserName != null)){  
//             if(findBankAdminByUsername != null){
//             String bankId = findBankAdminByUsername.getBankId();             
//                ViewBank findBankById = bankDao.findBankById(bankId);
//                if(findBankById.getBankId().trim().equalsIgnoreCase(requestModel.getBankId())){
//                           
//                    DashboardTransactionDetails bankTransactionsSummary = bankCouchDao.getBankTransactionsSummary(requestModel.getBankId());
//               
//                     response.put("statusCode","00");
//                    response.put("status","Operation Successful");
//                    response.put("transactions",bankTransactionsSummary);
//                    responseDetails.add(response);
//        }                
//                 
//         else{                    
//                    response.put("statusCode","03");
//                    response.put("status","Authorization Error");
//                    response.put("response","Bank mis-match");
//                    responseDetails.add(response);
//                    
//         }}else{                    
//                    response.put("statusCode","03");
//                    response.put("status","Authentication Error");
//                    response.put("response","Bank details not found");
//                    responseDetails.add(response);
//                    
//         }
//    }
//         else{
//                    response.put("statusCode","03");
//                    response.put("status","Authorization Error");
//                    response.put("response","Username does not exists or disabled");
//                    responseDetails.add(response);      
//         }
//    
//          Map<String,List> requestResponse = new HashMap<>();
//          requestResponse.put("requestResponse",responseDetails);
//          return requestResponse;
//    }
//    
    
    
}
