/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.model.Medication;
import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.model.UpdateUser;
import com.medi.cosmos.persona.model.UserModel;
import com.medi.cosmos.persona.model.VitalSigns;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.RoleGroup;
import com.medi.cosmos.security.service.CosmosRoleService;
import com.medi.cosmos.security.service.CosmosUserService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Oyewale
 */
@Component
public class UserService {
     
    @Autowired
    private UserDao userDao;
     @Autowired
    private CosmosRoleService roleService;
      @Autowired
    private CosmosUserService cosmosUserService;
      @Autowired
      private CosmosUserDao cosmosDao;
  
       public boolean createUserAdmin(UserModel userDetails) throws CosmosDatabaseException {

        //create merchant admin
             UserModel createUserAdmin = userDao.createUserAdmin(userDetails);
            if(createUserAdmin != null){
             // create the cosmos account for the admin
               CosmosUser toCosmosUser = userDetails.toCosmosUser();
               RoleGroup roleGroup = roleService.getRoleGroup("ROLE_USER");
                toCosmosUser.addRoleGroup(roleGroup);
                boolean createCosmosUser = cosmosUserService.createCosmosUser(toCosmosUser,false);
                
                if(!createCosmosUser){
                    //try to delete the registered admin using username
                    userDao.deleteUserAdminByUsername(userDetails.getUsername());
                }
                return createCosmosUser;
            }
            else{
               return false;
             }
    }

        public boolean updateUserAdmin(UpdateUser userDetails) throws CosmosDatabaseException {

        //update merchant admin
        UpdateUser updateUser = userDao.updateUserAdmin(userDetails);
            if(updateUser != null){
             // update the cosmos account for the admin
             
            CosmosUser findByUserName = cosmosDao.findByUserName(userDetails.getUsername());
            findByUserName.setPhoneNumber(userDetails.getPhoneNumber());
            findByUserName.setLastName(userDetails.getLastName());
            findByUserName.setFirstName(userDetails.getFirstName());
            findByUserName.setEmail(userDetails.getEmail());
                  boolean createCosmosUser = cosmosUserService.updateCosmosUser(findByUserName);
                 return createCosmosUser;
               
            }
            else{
               return false;
             }
    }


       public boolean logVitalSigns(VitalSigns vitalsigns) {           
            
        VitalSigns logVitalSigns = null;
        try {
            logVitalSigns = userDao.logVitalSigns(vitalsigns);
        } catch (CosmosDatabaseException ex) {
            
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
             
             return logVitalSigns != null;

    }
         public boolean saveMedication(Medication medication) {           
            
        Medication saveMedication = null;
        try {
            saveMedication = userDao.saveMedication(medication);
        } catch (CosmosDatabaseException ex) {
            
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
             
             return saveMedication != null;

    }
         
         
      public boolean logReviewRequest(ReviewRequest request) {           
            
        ReviewRequest logReveiwRequest = null;
        try {
             logReveiwRequest = userDao.logReviewRequest(request);
        } catch (CosmosDatabaseException ex) {
            
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
             
             return logReveiwRequest != null;

    } 
}
