/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class UserVerificationModel {
     @NotNull(message = "Username field cannot be null")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
     
}
