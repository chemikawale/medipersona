/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.dao.Dao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.model.PasswordRecoveryLink;


/**
 *
 * @author Adeyemi
 */
public interface PasswordRecoveryLinkDao extends Dao<PasswordRecoveryLink> {
    
    public PasswordRecoveryLink findByLink(String link) throws CosmosDatabaseException;
    public PasswordRecoveryLink findByUsername(String username) throws CosmosDatabaseException;
    public boolean deleteByLink(String link) throws CosmosDatabaseException;
    public boolean deleteByUsername(String username) throws CosmosDatabaseException;
}