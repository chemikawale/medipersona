/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dataview.model.ViewReviewer;
import com.medi.cosmos.persona.model.ReviewerModel;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class ReviewerDao {
      private static final Logger logger = Logger.getLogger("Reviewer Dao");
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    
     @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    } 
    
    /**
     * create a reviewer profile 
     * @param model
     * @return
     * @throws CosmosDatabaseException 
     */
    
       public ReviewerModel createReviewer(ReviewerModel model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("register_reviewer");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                       
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create reviewer "+ exception.getMessage());
        }
    }
   
              
         /**
     * find a reviewer basic details
     * @param username
     * @return an object of the ViewReviewer class
     * @throws CosmosDatabaseException 
     */
         public ViewReviewer findReviewerByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_reviewer_byusername")
                            .returningResultSet("reviewer", BeanPropertyRowMapper.newInstance(ViewReviewer.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewer> reviewer = (List<ViewReviewer>) result.get("reviewer");         
                if(reviewer.size() > 0){
                   return reviewer.get(0);
                   }
                else{
                return null;
                }
           }
                   
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching reviewer data "+exception.getMessage());
        }
       
      } 
       /**
        * delete a reviewer profile
        * @param username
        * @return
        * @throws CosmosDatabaseException 
        */
    public boolean  deleteReviewerByUsername(String username) throws CosmosDatabaseException {       
        
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("delete_user_admin_byusername");
                            
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
        
             return true;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("unable to delete reviewer by username "+exception.getMessage());
        }
       
    }
    
    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

      /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
