/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.HmoPlan;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class HmoPlanViewModel {
    private String planName;
    private String description;

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public HmoPlan toHmoPlan(){
     HmoPlan plan = new HmoPlan();
        plan.setCreatedOn(new Date());
        plan.setDescription(description.trim());
        plan.setPlanId(Utilities.generateNumericId(5));
        plan.setPlanName(planName.trim());     
     return plan;
    }
}
