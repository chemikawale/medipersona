/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dataview.model.ViewCustomVitalSigns;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.persona.dataview.model.ViewReview;
import com.medi.cosmos.persona.dataview.model.ViewReviewRequest;
import com.medi.cosmos.persona.dataview.model.ViewUser;
import com.medi.cosmos.persona.dataview.model.ViewVitalSigns;
import com.medi.cosmos.persona.model.CountOutput;
import com.medi.cosmos.persona.model.Medication;
import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.model.UpdateUser;
import com.medi.cosmos.persona.model.UserModel;
import com.medi.cosmos.persona.model.VitalSigns;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class UserDao {
    private static final Logger logger = Logger.getLogger("Users Dao");
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    
    
    @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    } 
    
    /**
     * create a user admin to keep profile
     * @param model
     * @return
     * @throws CosmosDatabaseException 
     */
    
       public UserModel createUserAdmin(UserModel model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("register_user_admin");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                       
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create user admin "+ exception.getMessage());
        }
    }
   
        
    /**
     * create a user admin to keep profile
     * @param model
     * @return
     * @throws CosmosDatabaseException 
     */
    
       public UpdateUser updateUserAdmin(UpdateUser model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("update_user_admin");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                       
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to update user details "+ exception.getMessage());
        }
    }
   
           
         /**
     * find a user basic details
     * @param username
     * @return an object of the ViewUser class
     * @throws CosmosDatabaseException 
     */
         public ViewUser findUserByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_user_admin_byusername")
                            .returningResultSet("user", BeanPropertyRowMapper.newInstance(ViewUser.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewUser> user = (List<ViewUser>) result.get("user");         
                if(user.size() > 0){
                   return user.get(0);
                   }
                else{
                return null;
                }
           }
                   
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching user data "+exception.getMessage());
        }
       
      } 
       /**
        * delete a user profile
        * @param username
        * @return
        * @throws CosmosDatabaseException 
        */
    public boolean  deleteUserAdminByUsername(String username) throws CosmosDatabaseException {       
        
    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("delete_user_admin_byusername");
                            
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
        
             return true;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("unable to delete user admin by username "+exception.getMessage());
        }
       
    }
    
    /**
     * Logging of user's vital signs
     * @param model
     * @return  the logged model if successfully logged
     * @throws CosmosDatabaseException 
     */
    public VitalSigns logVitalSigns(VitalSigns model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_vital_signs");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
            
            System.out.println("Create: "+result.toString());
            
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, exception);
           
            return null;
        }
    }
   
    /**
     * finding vital signs of a particular user
     * @param username
     * @param start
     * @param length
     * @return a list of the VitalSigns object of the user
     * @throws CosmosDatabaseException 
     */
         public Page<ViewVitalSigns> findVitalSignsByUsername(String username,int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_vital_signs_by_username")
                            .returningResultSet("vitalsigns", BeanPropertyRowMapper.newInstance(ViewVitalSigns.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewVitalSigns> signs = (List<ViewVitalSigns>) result.get("vitalsigns");         
            Page<ViewVitalSigns> obj = new Page<>();
            obj.setContent(signs);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting user's vital signs "+exception.getMessage());
        }
       
      }
         
         /**
     * find the last 5 vital signs of a particular user
     * @param username
     * @return a list of the VitalSigns object of the user
     * @throws CosmosDatabaseException 
     */
         public List<ViewVitalSigns> findRecentVitalSignsByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_recent_vital_signs_by_username")
                            .returningResultSet("vitalsigns", BeanPropertyRowMapper.newInstance(ViewVitalSigns.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewVitalSigns> signs = (List<ViewVitalSigns>) result.get("vitalsigns");         
                if(signs.size() > 0){
                   return signs;
                   }
                else{
                return null;
                }
           }
                   
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting the recent five user's vital signs "+exception.getMessage());
        }
       
      }
         /**
     * find the last 10 vital signs of a particular user
     * @param username
     * @return a list of the VitalSigns object of the user
     * @throws CosmosDatabaseException 
     */
         public List<ViewCustomVitalSigns> findLastTenVitalSignsByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_last_ten_vital_signs_for_user")
                            .returningResultSet("vitalsigns", BeanPropertyRowMapper.newInstance(ViewCustomVitalSigns.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewCustomVitalSigns> signs = (List<ViewCustomVitalSigns>) result.get("vitalsigns");         
                if(signs.size() > 0){
                   return signs;
                   }
                else{
                return null;
                }
           }
                   
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting the recent five user's vital signs "+exception.getMessage());
        }
       
      }
         
         
        /**
         * retrieving all vital signs paginated
     * @param start
     * @param length
         * @return a paginated vital signs
         * @throws CosmosDatabaseException 
         */
         public Page<ViewVitalSigns> getAllLoggedVitalSigns(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_vital_signs")
                            .returningResultSet("vital_signs", BeanPropertyRowMapper.newInstance(ViewVitalSigns.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewVitalSigns> signs = (List<ViewVitalSigns>) result.get("vital_signs");         
            Page<ViewVitalSigns> obj = new Page<>();
            obj.setContent(signs);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all vital signs "+exception.getMessage());
        }
       
      }
         
    
    /**
     * Logging of user's review request
     * @param model
     * @return  the logged model if successfully logged
     * @throws CosmosDatabaseException 
     */
    public ReviewRequest logReviewRequest(ReviewRequest model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("log_review_request");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
            
            System.out.println("Create: "+result.toString());
            
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, exception);
           
            return null;
        }
    }
    /**
     * finding review requests of a particular user
     * @param username
     * @param start
     * @param length
     * @return the review request of the user
     * @throws CosmosDatabaseException 
     */
         public Page<ViewReviewRequest> findReviewRequestsByUsername(String username,int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_review_request_by_username")
                            .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests");         
            Page<ViewReviewRequest> obj = new Page<>();
            obj.setContent(requests);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review requests for a specific user "+exception.getMessage());
        }
       
      }
      public int CountVitalSignsByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("count_all_user_vital_signs")
                            .returningResultSet("vitalsigns", BeanPropertyRowMapper.newInstance(ViewVitalSigns.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewVitalSigns> vitalsigns = (List<ViewVitalSigns>) result.get("vitalsigns");         
             if(vitalsigns != null){
               return vitalsigns.size();
             }
             else{
             return 0;
             }
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all vital signs for a specific user "+exception.getMessage());
        }
       
      }    
         
 
         public int CountReviewRequestsByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("count_all_user_requests")
                            .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests");         
             if(requests != null){
               return requests.size();
             }
             else{
             return 0;
             }
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review requests for a specific user "+exception.getMessage());
        }
       
      }
         
           public int CountUsedReviewRequestsByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("count_all_user_used_requests")
                            .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests");         
             if(requests != null){
               return requests.size();
             }
             else{
             return 0;
             }
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review requests for a specific user "+exception.getMessage());
        }
       
      }
         
    public int CountReviewByUsername(String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("count_all_user_reviews")
                            .returningResultSet("reviews", BeanPropertyRowMapper.newInstance(ViewReview.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReview> reviews = (List<ViewReview>) result.get("reviews");         
             if(reviews != null){
               return reviews.size();
             }
             else{
             return 0;
             }
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review for a specific user "+exception.getMessage());
        }
       
      }
         
         /**
     * finding review requests of a particular reviewer using the reviewer's email address
     * @param email
     * @param start
     * @param length
     * @return the review request for the  reviewer
     * @throws CosmosDatabaseException 
     */
         public Page<ViewReviewRequest> findReviewRequestsForReviewer(String email,int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("email", email);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_review_request_by_email")
                            .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests");         
            Page<ViewReviewRequest> obj = new Page<>();
            obj.setContent(requests);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review requests for a particular reviewer "+exception.getMessage());
        }
       
      }
         /**
          * Counting the core users operations 
          * @param username
          * @return
          * @throws CosmosDatabaseException 
          */
          public CountOutput getUserActivityCount(String username) throws CosmosDatabaseException {
        try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
            
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("count_user_actions");
                       
            simpleJdbcCall.compile();
             Map<String, Object> m = simpleJdbcCall.execute(sqlParameterSource);
            CountOutput aCount = new CountOutput();              
            aCount.setRequestCount(Integer.parseInt(m.get("requestcount") == null ? "0": m.get("requestcount")+""));
            aCount.setReviewCount(Integer.parseInt(m.get("reviewcount")== null ? "0": m.get("reviewcount")+""));
            aCount.setUsedRequestCount(Integer.parseInt(m.get("usedrequestcount") == null ? "0": m.get("usedrequestcount")+""));
            aCount.setVitalCount(Integer.parseInt(m.get("vitalcount") == null ? "0": m.get("vitalcount")+""));
    
        return aCount;
        }
        catch(InvalidDataAccessApiUsageException ex){
            throw new CosmosDatabaseException("Unable to get all terminals status "+ex.getMessage());
        }
    }
         
   
      /**
     * finding active review request using review code
     * @param reviewCode
     * @return the review request of the review code
     * @throws CosmosDatabaseException 
     */
     public ViewReviewRequest findActiveReviewRequestByReviewCode(String reviewCode) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("reviewCode",reviewCode);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("get_active_review_by_code")
                             .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests"); 
            if (requests.size() > 0){
           return requests.get(0);
                   }else{
               return null;
            }
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find review request of review code "+ reviewCode+  exception.getMessage());
        }  
       
    }
       /**
     * delete a user review request, this will only be disabled as the data will still exists in the database 
     * @param reviewCode
     * @throws CosmosDatabaseException 
     */
     public void deleteReviewRequest(String reviewCode) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("reviewCode",reviewCode);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("delete_user_request");
                            
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
          
            
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find review request of review code "+ reviewCode+  exception.getMessage());
        }  
       
    }
     
       /**
     * update the status of a  review code
     * @param reviewCode
     * @return the boolean status of the update
     * @throws CosmosDatabaseException 
     */
     public boolean UpdateUsedReviewCode(String reviewCode) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("reviewCode",reviewCode);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("update_used_review_code");
                             
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            return result != null ;
         }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to update the status of the review code "+ reviewCode+  exception.getMessage());
        }  
       
    }
     
     
     
     public void deprecateReviewRequests() throws CosmosDatabaseException {
        try {

            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("medipersona")
                    .withProcedureName("deprecate_review_request");
                  
            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

           System.out.println("Old request deprecated  succesfully  "+result);
                       
          

       } catch (InvalidDataAccessApiUsageException exception) {
            throw new CosmosDatabaseException("unable to deprecate expired review requests " + exception.getMessage());
        }
    }

        /**
         * retrieving all review request paginated
     * @param start
     * @param length
         * @return a paginated review request
         * @throws CosmosDatabaseException 
         */
         public Page<ViewReviewRequest> getAllReviewRequest(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_review_requests")
                            .returningResultSet("requests", BeanPropertyRowMapper.newInstance(ViewReviewRequest.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReviewRequest> requests = (List<ViewReviewRequest>) result.get("requests");         
            Page<ViewReviewRequest> obj = new Page<>();
            obj.setContent(requests);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all review requests "+exception.getMessage());
        }
       
      }
         
         
    /**
     * Logging of user's medication
     * @param model
     * @return  the logged model if successfully logged
     * @throws CosmosDatabaseException 
     */
    public Medication saveMedication(Medication model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medication");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
            
            System.out.println("Create: "+result.toString());
            
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, exception);
           
            return null;
        }
    }
   
    /**
     * finding medications of a particular user
     * @param username
     * @param start
     * @param length
     * @return a list of the Medication object of the user
     * @throws CosmosDatabaseException 
     */
         public Page<ViewMedication> findMedicationsByUsername(String username,int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_medications_by_username")
                            .returningResultSet("medications", BeanPropertyRowMapper.newInstance(ViewMedication.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewMedication> medications = (List<ViewMedication>) result.get("medications");         
            Page<ViewMedication> obj = new Page<>();
            obj.setContent(medications);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting user's medications "+exception.getMessage());
        }
       
      }
         
         
          /**
     * Get all Active Medications
      * @return a list of the Medications that are still active
     * @throws CosmosDatabaseException 
     */
         public List<ViewMedication> fetchAllActiveMedications() throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("fetch_active_medications")
                            .returningResultSet("medications", BeanPropertyRowMapper.newInstance(ViewMedication.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewMedication> medications = (List<ViewMedication>) result.get("medications");   
            
            return medications;
                   }
           catch(InvalidDataAccessApiUsageException exception){
              throw new CosmosDatabaseException("an error occur while fecthing all active medications "+exception.getMessage());
            }
       
      }
     
   
    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

      /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

}
