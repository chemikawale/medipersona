/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.ReviewerDao;
import com.medi.cosmos.persona.model.ReviewerModel;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.model.RoleGroup;
import com.medi.cosmos.security.service.CosmosRoleService;
import com.medi.cosmos.security.service.CosmosUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Oyewale
 */
@Component
public class ReviewerService {
       @Autowired
    private ReviewerDao reviewerDao;
     @Autowired
    private CosmosRoleService roleService;
      @Autowired
    private CosmosUserService cosmosUserService;
      
      
            public boolean createReviewAccount(ReviewerModel reviewerDetails) throws CosmosDatabaseException {

           //create reviewer account
           ReviewerModel createReviewer = reviewerDao.createReviewer(reviewerDetails);
            if(createReviewer != null){
             // create the cosmos account for the admin
               CosmosUser toCosmosUser = createReviewer.toCosmosUser();
               RoleGroup roleGroup = roleService.getRoleGroup("ROLE_REVIEWER");
                toCosmosUser.addRoleGroup(roleGroup);
                boolean createCosmosUser = cosmosUserService.createCosmosUser(toCosmosUser,false);
                
                if(!createCosmosUser){
                    //try to delete the registered admin using username
                    reviewerDao.deleteReviewerByUsername(reviewerDetails.getUsername());
                }
                return createCosmosUser;
            }
            else{
               return false;
             }
    }
}
