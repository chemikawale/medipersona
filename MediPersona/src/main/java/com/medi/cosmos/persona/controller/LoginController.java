/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.security.controller.SampleCosmosLoginController;
import com.medi.cosmos.security.provider.CosmosAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author tosineniolorunda
 */
@Controller
@RequestMapping("/")
public class LoginController extends SampleCosmosLoginController {

    @Autowired
    public LoginController(CosmosAuthenticationProvider cosmosAuthenticationProvider) {
        super(cosmosAuthenticationProvider);
    }

}
