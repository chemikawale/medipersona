/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dataview.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewCustomMediFacility {
     private String countryId;
    private String stateId;
    private String categoryId;
    private String latitude;
    private String longitude;
    private String facilityName;
    private String address;
    private String contactPerson;
    private boolean verified;
    private String createdOn;
    private String services;
    private String facilityId;
    private String stateName;
    private String countryName;
    private String categoryName;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getCreatedOn() {
        if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewCustomMediFacility.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
}
