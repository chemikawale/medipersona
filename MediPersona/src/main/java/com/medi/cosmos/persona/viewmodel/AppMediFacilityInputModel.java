/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.MediFacility;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class AppMediFacilityInputModel {
    
    private String countryId;
    private String stateId;
    private String categoryId;
    private String latitude;
    private String longitude;
    private String facilityName;
    private String address;
    private String contactPerson;
    private String services;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }
    
   public MediFacility toFacility(){
    MediFacility facility = new MediFacility();
        facility.setCreatedOn(Utilities.presentDateTime());
        facility.setAddress(address.trim());
        facility.setCategoryId(categoryId.trim());
        facility.setContactPerson(contactPerson.trim());
        facility.setCountryId(countryId.trim());
        facility.setFacilityId(Utilities.generateId(10));
        facility.setLatitude(latitude.trim());
        facility.setLongitude(longitude.trim());
        facility.setFacilityName(facilityName.trim());
        facility.setServices(services.trim());
        facility.setStateId(stateId.trim());
        facility.setVerified(false);
        
   return facility;
   }
}
