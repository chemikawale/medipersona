/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.PasswordRecoveryLinkDao;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.model.PasswordRecoveryLink;
import com.medi.cosmos.persona.model.UpdateUser;
import com.medi.cosmos.persona.service.MedicationReminder;
import com.medi.cosmos.persona.service.NotificationService;
import com.medi.cosmos.persona.utilities.RSAServiceUtil;
import com.medi.cosmos.persona.viewmodel.ForgetPasswordModel;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.service.CosmosUserService;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import javax.servlet.http.HttpSession;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author ChemikaWale
 */
@Controller
 @RequestMapping(value = {"","/home", "/index", "/"})
public class HomeController {

    static final Logger logger = Logger.getLogger(HomeController.class.getName());

      @Autowired
    private CosmosUserService cosmosUserService;
      @Autowired
      private PasswordRecoveryLinkDao recoveryLinkDao;
      @Autowired
     private CosmosUserDao cosmoUserDao;
            @Autowired
      private UserDao userDao;
         @Autowired
      private NotificationService notifierService;     
     @Autowired
     private MedicationReminder reminder;
            
  //  private final BCryptPasswordEncoder tokenpasswordEncoder = new BCryptPasswordEncoder();
  
    @RequestMapping(value = {"", "/index", "/"})
    public String index(HttpServletRequest request, Model model, HttpSession session) throws CosmosDatabaseException, IOException { 
        //retrieving and storing the username inside a session variable for further usage
        session = request.getSession();
         String user =  request.getUserPrincipal().getName();
           session.setAttribute("username", user);
        CosmosUser findByUserName = cosmoUserDao.findByUserName(user);
        session.setAttribute("firstName", findByUserName.getFirstName().toUpperCase());
        // reminder.sendMedicationReminder();
               
      //  check for the role of the logged in user and re-direct appropriately
            if(request.isUserInRole("ROLE_USER")){
                    return "redirect:/user/home";                 

            }
 return "/views/home/index";  

    }
     
//     @PreAuthorize(value = "isAuthenticated()")
//    @RequestMapping(value = "/vitalsigndata", method = RequestMethod.GET)
//    @ResponseBody
//    public  Map<String,Object> UserVitalSigns(HttpSession session,HttpServletRequest request, Model model) throws CosmosDatabaseException {       
//         String cusername = (String)session.getAttribute("username");
//        Page<ViewVitalSigns> vitalsigns = userDao.findVitalSignsByUsername(cusername, 0, 10);
//            Map<String,Object> signsResponse = new HashMap<>();   
//            signsResponse.put("vitalsigns",vitalsigns);    
//        
//        return signsResponse;
//    }
//    
 
    /*
    Handling of forgot password, filtering the security to permit anonymous user to
    access the controller methods 
    */    
    
    @PreAuthorize(value = "isAnonymous()")
    @RequestMapping(value ="/forgotpassword", method = RequestMethod.GET)
    public String forgetPassword(ModelMap model){
        
        model.addAttribute("editModel", new ForgetPasswordModel());
      
        return "/views/home/forgetpassword";
    }
      @PreAuthorize(value = "isAnonymous()")
    @RequestMapping(value="/forgotpassword",  method = RequestMethod.POST)
    public String forgetPassword(@ModelAttribute("editModel") ForgetPasswordModel passwordModel, BindingResult result, ModelMap modelMap) throws CosmosDatabaseException{
        
        if(result.hasErrors()){
            modelMap.addAttribute("error", "Pls check all fields" );
            return "/views/home/forgetpassword";
        }
        
        CosmosUser  cosmosUser = cosmoUserDao.findByUserName(passwordModel.getUsername());
        
        if(cosmosUser == null){
            modelMap.addAttribute("error", "Record Not found in database");
            return "/views/home/forgetpassword";
        }
        
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        Date date = calendar.getTime();
        String link = RSAServiceUtil.passwordLinkGenerator(cosmosUser.getEmail(), date.toString());
        
        link = link.replaceAll("/", "_");
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        
        PasswordRecoveryLink  passwordRecoveryLink = new PasswordRecoveryLink();
        passwordRecoveryLink.setCreatedOn(date);
        passwordRecoveryLink.setLink(link);
        passwordRecoveryLink.setExpiry(calendar.getTime());
        passwordRecoveryLink.setUsername(cosmosUser.getUsername());
        
        try{
            
            PasswordRecoveryLink recoveryLink = recoveryLinkDao.findByUsername(cosmosUser.getUsername());
            
            if(recoveryLink != null)
                recoveryLinkDao.deleteByUsername(cosmosUser.getUsername());
            
            recoveryLinkDao.create(passwordRecoveryLink);
      notifierService.ResetPasswordNotification(cosmosUser.getUsername(), link,cosmosUser.getLastName(),cosmosUser.getEmail());
   
        }catch(CosmosDatabaseException  cde){
            modelMap.addAttribute("error", "unable to reset password at this time please try again later");
            return "/views/home/forgetpassword";
        }
        
        modelMap.addAttribute("success", "A password recovery link has been sent to the email");
        
        return "/views/home/forgetpassword";
    }
    
     @PreAuthorize(value = "isAnonymous()")
  @RequestMapping( value = "/forgotpassword/{link:.+}", method = RequestMethod.GET )
    public String forgetPassword(@PathVariable("link") String link, ModelMap modelMap, RedirectAttributes ra){
        
        if(link == null)
            return null;
        
        if(link.isEmpty())
            return null;
               
        try{
            
            PasswordRecoveryLink recoveryLink = recoveryLinkDao.findByLink(link);
            
            if(recoveryLink == null){
                modelMap.addAttribute("error", "No recovery link found! ");        
                return "/views/home/recoverystatus";
            }
            
            if(recoveryLink.getExpiry().before(Calendar.getInstance().getTime())){
                modelMap.addAttribute("error", "Recovery Link has expired.");
                return "/views/home/recoverystatus";
            }
            else{          
                String username = recoveryLink.getUsername();
                CosmosUser findByUserName = cosmoUserDao.findByUserName(username);
                findByUserName.setPassword("carepersona");
                boolean updateCosmosUser = cosmosUserService.updateCosmosUser(findByUserName);
              
                if(updateCosmosUser){
                UpdateUser update = new UpdateUser();
                update.setEmail(findByUserName.getEmail());
                update.setFirstName(findByUserName.getFirstName());
                update.setLastName(findByUserName.getLastName());
                update.setPhoneNumber(findByUserName.getPhoneNumber());
                update.setUsername(findByUserName.getUsername());
                userDao.updateUserAdmin(update);
                
                     modelMap.addAttribute("success","Password reset to the default carepersona successfully");
                     return "/views/home/recoverystatus";
                }else{
                     modelMap.addAttribute("error", "An error occur while updating and changing password");
                     return "/views/home/recoverystatus";
                }
                 
            }
            
        }catch(CosmosDatabaseException cde){
            logger.log(Level.INFO,"An error occur whil trying to utilize forgot password");
        }
        
       
        return null;
        
    }
//    }
//     @PreAuthorize(value = "isAnonymous()")
//     @RequestMapping(value = "/resetpassword", method = RequestMethod.GET)
//    public String resetPassword(@ModelAttribute("username") String username, ModelMap modelMap){
//        
//        if(username == null)
//            return null;
//        
//        if(username.isEmpty())
//            return null;
//        
//        PasswordViewModel viewModel = new PasswordViewModel();
//        viewModel.setUsername(username);
//        
//        modelMap.addAttribute("viewModel", viewModel);
//        
//        return "/views/home/resetpassword";
//        
//    }
//     @PreAuthorize(value = "isAnonymous()")
//    @RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
//    public String resetPassword(@ModelAttribute("viewModel") PasswordViewModel viewModel, 
//            BindingResult result, ModelMap model, HttpServletRequest servletRequest, RedirectAttributes rdat) throws CosmosDatabaseException {
//        
//        if(result.hasErrors()){
//            model.addAttribute("error", "Kindly check all fields");
//            return "/views/home/resetpassword";
//        }
//        
//        boolean status = false;
//        //retrieve the user role using cosmos 
//        CosmosUser cu = cosmoUserDao.findByUserName(viewModel.getUsername());
//           if (cu != null){
//              cosmosUserService.updateCosmosUser(cu); 
//           }
//                  
//           if(!status){
//            model.addAttribute("error", "Unable to reset password, pls try again later");
//            return "/views/home/resetpassword";
//        }
//        
//        rdat.addFlashAttribute("success", "Password has been reset to the default password successfully");
//              
//        return "/views/home/resetpassword";
//     
//    }

}
