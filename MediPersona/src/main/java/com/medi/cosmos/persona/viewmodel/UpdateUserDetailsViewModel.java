/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.UpdateUser;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class UpdateUserDetailsViewModel {
      private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String username;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
  public UpdateUser toUpdate(){
   UpdateUser update = new UpdateUser();
     update.setEmail(email.trim());
     update.setLastEdit(Utilities.presentDateTime());
     update.setFirstName(firstName.trim());
     update.setLastName(lastName.trim());
     update.setPhoneNumber(phoneNumber.trim());
     update.setUsername(username.trim());
   
   return update;
  }    
}
