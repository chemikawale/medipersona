/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.model;

import com.medi.cosmos.security.model.CosmosUser;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class ReviewRequest {
       private String  username ;
       private String  reviewcode ;
       private String  email;
       private String  phonenumber;
       private Date    createdOn ;
       private Date    expiryDate;
       private boolean enabled;
       private boolean isUsed;
       private boolean status;
       private String lastName;
       private String firstName;
       private String role;
      // private String password;

//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReviewcode() {
        return reviewcode;
    }

    public void setReviewcode(String reviewcode) {
        this.reviewcode = reviewcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
       
//    public CosmosUser toCosmos(){
//        
//    CosmosUser toCosmos = new CosmosUser();
//      toCosmos.setCreatedOn(createdOn);
//      toCosmos.setEmail(email);
//      toCosmos.setEnabled(false);
//      toCosmos.setFirstName(firstName);
//      toCosmos.setPassword(password);
//      toCosmos.setUsername(email);
//     toCosmos.setLastName(lastName);
//      
//    return toCosmos;
//    }
}
