/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.UserModel;
import com.medi.cosmos.persona.utilities.Utilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class SignUpViewModel {
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String username;
    private String countryId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
     public UserModel toUser(){
      UserModel toUser = new UserModel();
        Date presentDateTime = Utilities.presentDateTime();
            toUser.setCreatedOn(presentDateTime);
            toUser.setEmail(email.trim());
            toUser.setCountryId(countryId);
            toUser.setEnabled(true);
            toUser.setFirstName(firstName.trim());
            toUser.setLastName(lastName.trim());
            toUser.setLastEdit(presentDateTime);
            toUser.setPassword("carepersona");
            if(phoneNumber != null){
                toUser.setPhoneNumber(phoneNumber.trim());
            }
            toUser.setUsername(username.trim());
           
      return toUser;
     }
}
