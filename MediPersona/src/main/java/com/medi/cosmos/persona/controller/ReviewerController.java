/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dao.PasswordRecoveryLinkDao;
import com.medi.cosmos.persona.dao.ReviewDao;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dataview.model.ViewAllergy;
import com.medi.cosmos.persona.dataview.model.ViewBloodDetails;
import com.medi.cosmos.persona.dataview.model.ViewCustomAllergy;
import com.medi.cosmos.persona.dataview.model.ViewCustomReview;
import com.medi.cosmos.persona.dataview.model.ViewCustomVitalSigns;
import com.medi.cosmos.persona.dataview.model.ViewMedication;
import com.medi.cosmos.persona.dataview.model.ViewReview;
import com.medi.cosmos.persona.dataview.model.ViewReviewRequest;
import com.medi.cosmos.persona.dataview.model.ViewUser;
import com.medi.cosmos.persona.dataview.model.ViewVitalSigns;
import com.medi.cosmos.persona.model.ReviewModel;
import com.medi.cosmos.persona.utilities.PageResult;
import com.medi.cosmos.persona.viewmodel.ReviewViewModel;
import com.medi.cosmos.persona.viewmodel.VerifyReviewCodeViewModel;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import com.medi.cosmos.security.service.CosmosUserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */
@Controller
@RequestMapping(value = "/reviewer")
public class ReviewerController {
     static final Logger logger = Logger.getLogger(HomeController.class.getName());

      @Autowired
    private CosmosUserService cosmosUserService;
      @Autowired
      private PasswordRecoveryLinkDao recoveryLinkDao;
      @Autowired
     private CosmosUserDao cosmoUserDao;
      @Autowired
      private UserDao userDao;
       @Autowired
      private MedicalDataDao medicalDao;
        @Autowired
      private ReviewDao reviewDao;
//       @RequestMapping(value = {"", "/index", "/"})
//    public String index(HttpServletRequest request, Model model, HttpSession session) throws CosmosDatabaseException, IOException { 
//        //retrieving and storing the username inside a session variable for further usage
//        session = request.getSession();
//         String user =  request.getUserPrincipal().getName();
//           session.setAttribute("username", user);
//        CosmosUser findByUserName = cosmoUserDao.findByUserName(user);
//        session.setAttribute("email",findByUserName.getEmail());
//        session.setAttribute("firstName", findByUserName.getFirstName().toUpperCase());
//        
//      
////           logger.log(Level.INFO, "User with username: {0} logged in", user);       
////        //check for the role of the logged in user and re-direct appropriately
////       if(request.isUserInRole("ROLE_MERCHANT")){
////           return "redirect:/merchant/home";                 
////             }
////        if(request.isUserInRole("ROLE_STAFF")){
////           return "redirect:/staff/home";                 
////             }
////        if(request.isUserInRole("ROLE_SUPER_MERCHANT")){
////           return "redirect:/super/home";                 
////             }
////        if(request.isUserInRole("ROLE_BANK")){          
////             Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
////             CosmosUser findByUserName = cosmoUserDao.findByUserName(user);
////             SecurityContextLogoutHandler outt = new SecurityContextLogoutHandler();
////             outt.logout(request,null, authentication);
////            SecurityContextHolder.getContext().setAuthentication(null);
////        }
////           
////        int successTxn = transactionDao.getValidTransactionCount();
////        int failedTxn = transactionDao.getInValidTransactionCount();
////        int totaltxn = successTxn + failedTxn;
//      
////        
////        //calculating to amount made thus far (cash and card)
////         String totalAmount = Utility.toMoney(computeDao.getTransactionTotal());
////         String totalcash = Utility.toMoney(computeDao.getCashTransactionTotal());
////         String totalcard = Utility.toMoney(computeDao.getCardTransactionTotal());
////
////
////        model.addAttribute("transactions",recentTransactions);
////        model.addAttribute("alltxn", totaltxn);
////        model.addAttribute("successtxn", successTxn);
////        model.addAttribute("failedtxn", failedTxn);
////     
////        model.addAttribute("totalamount", totalAmount);
////        model.addAttribute("totalcash", totalcash);
////        model.addAttribute("totalcard", totalcard);
//    
//        model.addAttribute("home", "active");
//     return "redirect:/reviewer/reviewrequests";
//    }
//         @RequestMapping(value = {"/reviewrequests"}, method = RequestMethod.GET)
//    public String ReviewerReviewRequest(Model model) throws CosmosDatabaseException {
//        model.addAttribute("requests", "active");
//        return "/reviewer/reviewrequests";
//    }
//    
//     @PreAuthorize(value = "isAuthenticated()")
//     @RequestMapping(value = "/getreviewrequests", method = RequestMethod.GET)
//     @ResponseBody
//        public PageResult<ViewReviewRequest> allUserReviewRequest(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
//            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
//            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
//      String email = (String)session.getAttribute("email");
//         if(start > 0){
//            start = start / length ;
//        }
//     
//        try{
//        PageResult<ViewReviewRequest> result;
//             Page<ViewReviewRequest> page = userDao.findReviewRequestsForReviewer(email,start, length);  
//            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
//          result.setDraw(draw);
//            
//            return result;
//             }catch(CosmosDatabaseException cde){
//            return null;
//        }
//    }
       
        
     @RequestMapping(value = "/reviewcode" , method = RequestMethod.GET)
    public ModelAndView VerifyCodeUser(Model model) {     
        ModelAndView mav = new ModelAndView("/reviewer/verifycode");
        mav.addObject("viewModel", new VerifyReviewCodeViewModel());
          model.addAttribute("reviewcode", "active");
           return mav ;
    }
     @RequestMapping(value = "/reviewcode", method = RequestMethod.POST)
    public String VerifyCodeUser(@ModelAttribute("viewModel") @Valid VerifyReviewCodeViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new VerifyReviewCodeViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/reviewer/verifycode";
        }             
         String reviewCode = viewModel.getReviewCode().trim();
         ViewReviewRequest findRequest = userDao.findActiveReviewRequestByReviewCode(reviewCode);
         
          if(findRequest != null){              
         boolean codeUsed = userDao.UpdateUsedReviewCode(reviewCode);      
             if(codeUsed ){
              CosmosUser findUser = cosmoUserDao.findByUserName(findRequest.getUsername());
                session.setAttribute("userfullname",findUser.getLastName()+"  "+findUser.getFirstName());
                session.setAttribute("userpatient", findRequest.getUsername());
                session.setAttribute("reviewer", findRequest.getLastName()+ "  "+findRequest.getFirstName());
               
               model.addAttribute("success", "User's Account Set successfully and ready for review");
                return "redirect:/reviewer/medicalprofile";
            }
   
         else
             {
                model.addAttribute("viewModel", new VerifyReviewCodeViewModel());
                model.addAttribute("error", "An error occurred while processing user's details");
                 return "/reviewer/verifycode";
            } 
    }else{  model.addAttribute("viewModel", new VerifyReviewCodeViewModel());
                model.addAttribute("error", "Review Code have expired or Invalid");
                 return "/reviewer/verifycode";
            } 
    
    }
    
   @RequestMapping(value = {"/medicalprofile"}, method = RequestMethod.GET)
    public String UserMedicalProfile(Model model,  HttpSession session) throws CosmosDatabaseException {
         String cusername = (String)session.getAttribute("userpatient");
        
         ViewBloodDetails bloodDetails = medicalDao.findBloodDetailsByUsername(cusername);
         model.addAttribute("bloodDetails",bloodDetails);
        model.addAttribute("profile", "active");
        return "/reviewer/medicalprofile";
    }      
     
    @RequestMapping(value = "/getuservitalsigns", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<ViewVitalSigns> allUserVitalSigns(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      
            String cusername = (String)session.getAttribute("userpatient");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewVitalSigns> result;
             Page<ViewVitalSigns> page = userDao.findVitalSignsByUsername(cusername,start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
        
       @RequestMapping(value = "/getuserallergies", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewCustomAllergy> PaginatedAllUseraAllergies(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String cusername = (String)session.getAttribute("userpatient");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomAllergy> result;
          Page<ViewAllergy> allUserAllergies = medicalDao.getAllUserAllergies(start, length, cusername);  
          List<ViewAllergy> allergies = allUserAllergies.getContent();
          List<ViewCustomAllergy> userAllergies = new ArrayList<>();
            if(allergies.size() > 0){
               for(ViewAllergy allergy : allergies){
                ViewCustomAllergy customAllergy = new ViewCustomAllergy();
                customAllergy.setActive(allergy.isActive());
                customAllergy.setAllergicTo(allergy.getAllergicTo());
                customAllergy.setCategoryId(allergy.getCategoryId());
                customAllergy.setCategoryName(medicalDao.findAllergyCategoryById(allergy.getCategoryId()).getCategoryName());
                customAllergy.setComment(allergy.getComment());
                customAllergy.setCreatedOn(allergy.getCreatedOn());
                customAllergy.setEntryId(allergy.getEntryId());
                customAllergy.setLocation(allergy.getLocation());
                customAllergy.setReaction(allergy.getReaction());
                customAllergy.setSeverity(allergy.getSeverity());
                customAllergy.setStarted(allergy.getStarted());
                        
                    userAllergies.add(customAllergy);
               
               
               }
            
            
            }
            result = new PageResult<>( userAllergies, allUserAllergies.getCount() , allUserAllergies.getCount());
            result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
        }
           @RequestMapping(value = "/medicaldata", method = RequestMethod.GET)
    @ResponseBody
    public  Map<String,Object> UserMedicalChartsData(HttpServletRequest request, Model model,HttpSession session) throws CosmosDatabaseException {       
         
        String username =  (String) session.getAttribute("userpatient");
                     
         List<ViewCustomVitalSigns> userLastTenVitalSigns = userDao.findLastTenVitalSignsByUsername(username);
                      
           Map<String,Object> chartsResponse = new HashMap<>();   
    
        chartsResponse.put("medicaldata",userLastTenVitalSigns);
       
      
        
        
        return chartsResponse;
    }
    
      @RequestMapping(value = "/review", method = RequestMethod.GET)
    public ModelAndView getReview(Model model,HttpSession session) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/reviewer/review");
        
               String username =  (String) session.getAttribute("userpatient");
              
        
                String userfullname =  (String) session.getAttribute("userfullname");
            mav.addObject("userfullname", userfullname);
            mav.addObject("username", username);
          mav.addObject("viewModel", new ReviewViewModel());
         mav.addObject("review", "active");
        return mav ;
    }
    
  
    @RequestMapping(value = "/review", method = RequestMethod.POST)
    public String addNewUser(@ModelAttribute("viewModel") @Valid ReviewViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new ReviewViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/reviewer/review";
        }    
          String username =  (String) session.getAttribute("username");
           String reviewer =  (String) session.getAttribute("reviewer");
          ReviewModel review = viewModel.getReview();
          review.setReviewer(username);  
          review.setReviewer(reviewer);
       
         ReviewModel logReview = reviewDao.logReview(review);
      
            if(logReview != null){
                model.addAttribute("viewModel", new ReviewViewModel());
                model.addAttribute("success", "Review Logged successfully. Thanks");
                 return "/reviewer/review";
            } 
        
             model.addAttribute("viewModel", new ReviewViewModel());
             model.addAttribute("error", "An Error occur while trying to log your review");
           return "/reviewer/review";

    }
     
       
   @RequestMapping(value = {"/reviews"}, method = RequestMethod.GET)
    public String ReviewsReviews(Model model,  HttpSession session) throws CosmosDatabaseException {
       
         model.addAttribute("reviews", "active");
        return "/reviewer/reviews";
    }      
     
   @RequestMapping(value = "/getreviews", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewCustomReview> getAllReviews(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      String username = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomReview> result;
          Page<ViewReview> allReviews = reviewDao.findReviewerReviews(start, length, username);  
          List<ViewReview> content = allReviews.getContent();
          List<ViewCustomReview> userReviews = new ArrayList<>();
            if(content.size() > 0){
               for(ViewReview review : content){
                ViewCustomReview customReview = new ViewCustomReview();
                customReview.setComments(review.getComments());
                customReview.setDiagnosis(review.getDiagnosis());
                customReview.setUsername(review.getUsername());
                customReview.setComments(review.getComments());
                customReview.setCreatedOn(review.getCreatedOn());
                customReview.setRecommendation(review.getRecommendation());
                customReview.setReviewId(review.getReviewId());
                
                   ViewUser findUserByUsername = userDao.findUserByUsername(review.getUsername());
                customReview.setFullName(findUserByUsername.getLastName() +" "+findUserByUsername.getFirstName());
               
                        
                    userReviews.add(customReview);
               
               
               }
            
            
            }
            result = new PageResult<>( userReviews, allReviews.getCount() , allReviews.getCount());
            result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
        }
        
         @RequestMapping(value = "/usermedications", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewMedication> allReviewMedications(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
      
            String cusername = (String)session.getAttribute("username");
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewMedication> result;
             Page<ViewMedication> page = userDao.findMedicationsByUsername(cusername,start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
       
}