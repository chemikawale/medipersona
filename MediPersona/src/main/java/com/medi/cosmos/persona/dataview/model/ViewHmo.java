/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dataview.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewHmo {
      private String hmoId;
    private String hmoName;
    private String address;
    private String email;
    private String createdOn;
    private String countryId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getHmoId() {
        return hmoId;
    }

    public void setHmoId(String hmoId) {
        this.hmoId = hmoId;
    }

    public String getHmoName() {
        return hmoName;
    }

    public void setHmoName(String hmoName) {
        this.hmoName = hmoName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedOn() {
        if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewHmo.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }


    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    
    
}
