/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.api.authentication;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.TokenUtils;
import com.medi.cosmos.persona.mobile.model.ViewUserToken;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author Oyewale
 */
@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

//    @Autowired UserService userService;
    @Autowired TokenUtils tokenUtils;
    AuthenticationManager authManager;

    public AuthenticationTokenProcessingFilter(AuthenticationManager authManager) {
        this.authManager = authManager;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        @SuppressWarnings("unchecked")
          
       // Map<String, String[]> parms = request.getParameterMap("token");
              HttpServletResponse res=(HttpServletResponse) response;
              HttpServletRequest req = (HttpServletRequest) request;
              String method = req.getMethod();
  
              
        HttpServletRequest myRequest = (HttpServletRequest) request;
         if ("OPTIONS".equals(method)) {
                   res.setStatus(HttpStatus.OK.value());
                   res.addHeader("Access-Control-Allow-Origin", "*");
                    res.addHeader("Content-Type", "application/json");
                    res.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE,PUT");
               res.setHeader("Access-Control-Max-Age", Long.toString(60 * 60));
               res.setHeader("Access-Control-Allow-Credentials", "true");
               res.setHeader("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,token");
        
               } else {
        String token = myRequest.getHeader("token");
    
            try {
                
                // validate the token
                if (tokenUtils.validateToken(token)) {
                    // determine the user based on the (already validated) token
                    ViewUserToken userDetails = tokenUtils.findAccessToken(token);
                    // build an Authentication object with the user's info
                    UsernamePasswordAuthenticationToken authentication =
                            new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
                    // set the authentication into the SecurityContext
                    SecurityContextHolder.getContext().setAuthentication(authManager.authenticate(authentication));
                }
            } catch (CosmosDatabaseException ex) {
                Logger.getLogger(AuthenticationTokenProcessingFilter.class.getName()).log(Level.SEVERE, null, ex);
            }
      
        chain.doFilter(request, response);
    }
    }
}
