/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.model;

import com.medi.cosmos.security.model.CosmosUser;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class UserModel {
      private String firstName;
      private String lastName;
      private String email;
      private String phoneNumber;
      private String password;
      private boolean enabled;
      private Date createdOn;
      private String username;
      private Date lastEdit;
      private String countryId;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public Date getLastEdit() {
        return lastEdit;
    }

    public void setLastEdit(Date lastEdit) {
        this.lastEdit = lastEdit;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
      
     public CosmosUser toCosmosUser(){
       CosmosUser toCosmos = new CosmosUser();
        toCosmos.setEmail(email);
        toCosmos.setCreatedOn(createdOn);
        toCosmos.setEnabled(true);
        toCosmos.setFirstName(firstName);
        toCosmos.setPhoneNumber(phoneNumber);
        toCosmos.setUsername(username);
        toCosmos.setLastName(lastName);
        toCosmos.setPassword(password);
        
        
        return  toCosmos;
     
     }
}
