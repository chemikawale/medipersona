/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author Oyewale
 */
  @Controller
@RequestMapping(value = "/role")
@SessionAttributes(value = "baseRolePath")
public class RoleController extends com.medi.cosmos.security.controller.RoleController {
    public RoleController(){
     super("/role");
    }
}
