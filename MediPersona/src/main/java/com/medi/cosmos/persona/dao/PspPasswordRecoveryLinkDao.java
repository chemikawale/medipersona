/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.dao.relational.BeanPropertySqlParameterSourceFactory;
import com.medi.cosmos.core.dao.relational.PspBaseDao;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.model.PasswordRecoveryLink;

//import com.esl.cosmos.tribase.model.RecoveryQuestion;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Adeyemi
 */
@Repository
public class PspPasswordRecoveryLinkDao extends PspBaseDao<PasswordRecoveryLink>
    implements PasswordRecoveryLinkDao{
    
    SimpleJdbcCall pspFindByLink, pspFindByUsername, pspDeleteByUsername, pspDeleteByLink;
    
    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SimpleJdbcCall pspCreate = new SimpleJdbcCall(jdbcTemplate).withProcedureName("add_password_recovery_link");
        pspDeleteByUsername = new SimpleJdbcCall(jdbcTemplate).withProcedureName("delete_password_recovery_link_by_username");
        pspDeleteByLink = new SimpleJdbcCall(jdbcTemplate).withProcedureName("delete_password_recovery_link_by_link");
        
        
        
        pspFindByLink = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("find_password_recovery_link_by_link")
                .returningResultSet(FIND_RESULT_NAME, BeanPropertyRowMapper.newInstance(PasswordRecoveryLink.class));
        
        pspFindByUsername = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("find_password_recovery_link_by_username")
                .returningResultSet(FIND_RESULT_NAME, BeanPropertyRowMapper.newInstance(PasswordRecoveryLink.class));
        
        SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("find_all_password_recovery_links")
                .returningResultSet(FIND_RESULT_SET_NAME, BeanPropertyRowMapper.newInstance(PasswordRecoveryLink.class) );
        
        super.init(pspCreate, null, null, pspFindAll, null, null, new BeanPropertySqlParameterSourceFactory(), jdbcTemplate);
    }

    @Override
    public PasswordRecoveryLink findByLink(String link) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("link", link);
            Map<String, Object> m = pspFindByLink.execute(in);
            
            if (m == null) {
                return null;
            }
            List<PasswordRecoveryLink> list = (List<PasswordRecoveryLink>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
//            ex.printStackTrace();
            throw new CosmosDatabaseException("Find model by link failed", ex);
        }
    }

    @Override
    public boolean deleteByLink(String link) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("link", link);
            Map<String, Object> m = pspDeleteByLink.execute(in);
            if (m.size() <= 0) {
                return false;
            }
            String[] keys = m.keySet().toArray(new String[m.size()]);
            int ret = (Integer) m.get(keys[0]);
            
            return ret > 0;
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while delete model from database", ex);
        }
    }

    @Override
    public boolean deleteByUsername(String username) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("username", username);
            Map<String, Object> m = pspDeleteByUsername.execute(in);
            if (m.size() <= 0) {
                return false;
            }
            String[] keys = m.keySet().toArray(new String[m.size()]);
            int ret = (Integer) m.get(keys[0]);
            
            return ret > 0;
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while delete model from database", ex);
        }
    }

    /**
     *
     * @param username
     * @return
     * @throws CosmosDatabaseException
     */
    @Override
    public PasswordRecoveryLink findByUsername(String username) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue("username",username);
            Map<String, Object> m = pspFindByUsername.execute(in);
            
            if (m == null) {
                return null;
            }
            List<PasswordRecoveryLink> list = (List<PasswordRecoveryLink>) m.get(FIND_RESULT_NAME);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
//            ex.printStackTrace();
            throw new CosmosDatabaseException("Find model by link failed", ex);
        }
    }
    
}
