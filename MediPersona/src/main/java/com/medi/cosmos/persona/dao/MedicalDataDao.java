/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dataview.model.ViewAllergy;
import com.medi.cosmos.persona.dataview.model.ViewAllergyCategory;
import com.medi.cosmos.persona.dataview.model.ViewBloodDetails;
import com.medi.cosmos.persona.model.Allergy;
import com.medi.cosmos.persona.model.AllergyCategory;
import com.medi.cosmos.persona.model.BloodDetails;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class MedicalDataDao {
    private static final Logger logger = Logger.getLogger("Utilities Dao");
     
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    

    
    @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    }
          
     /**
     * adding user's blood details
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public BloodDetails logBloodDetails(BloodDetails model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_blood_details");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to log blood details "+ exception.getMessage());
        }
       
    }
    
    /**
     * finding user's blood details using username
     * @param username
     * @return an object of the BloodDetails Class or null if not found
     * @throws CosmosDatabaseException 
     */
     public ViewBloodDetails findBloodDetailsByUsername(String username) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_blood_details_byUsername")
                             .returningResultSet("blooddetails", BeanPropertyRowMapper.newInstance(ViewBloodDetails.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewBloodDetails> bloodDetails = (List<ViewBloodDetails>) result.get("blooddetails"); 
            if(bloodDetails.size() > 0){
           return bloodDetails.get(0);
                   }else{
               return null;
            }
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find blood details by username "+ exception.getMessage());
        }  
       
    }
    
      /**
     * delete user's blood details using username
     * @param username
     * @return true or false
     * @throws CosmosDatabaseException 
     */
     public boolean deleteBloodDetailsByUsername(String username) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("delete_blood_details_byUsername");
                            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            return true;
            
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to delete blood details by username "+ exception.getMessage());
        }  
       
    }
      /**
         * retrieving all blood details logged
     * @param start
     * @param length
         * @return a paginated blood details
         * @throws CosmosDatabaseException 
         */
         public Page<ViewBloodDetails> getAllBloodDetails(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_blood_details")
                            .returningResultSet("blood_details", BeanPropertyRowMapper.newInstance(ViewBloodDetails.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewBloodDetails> blood_details = (List<ViewBloodDetails>) result.get("blood_details");         
            Page<ViewBloodDetails> obj = new Page<>();
            obj.setContent(blood_details);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all blood details "+exception.getMessage());
        }
       
      }
         
    /**
     * adding am allergy category
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public AllergyCategory addAllergyCategory(AllergyCategory model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_allergy_category");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to add allergy category "+ exception.getMessage());
        }
       
    }
    
    /**
     * finding allergy category by category Id
     * @param categoryId
     * @return an object of the BloodDetails Class or null if not found
     * @throws CosmosDatabaseException 
     */
     public ViewAllergyCategory findAllergyCategoryById(String categoryId) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("categoryId",categoryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_allergy_category_ById")
                             .returningResultSet("category", BeanPropertyRowMapper.newInstance(ViewAllergyCategory.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAllergyCategory> category = (List<ViewAllergyCategory>) result.get("category"); 
            if(category.size() > 0 ){
           return category.get(0);
                   }else{
               return null;
            }
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find allergy category by ID "+ exception.getMessage());
        }  
       
    }
         /**
      * retrieved all created allergy Category
      * @return a list of created allergy category 
      */
   public List<ViewAllergyCategory> getAllAllergyCategories() throws CosmosDatabaseException{
  
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_allergy_categories")
                            .returningResultSet("categories", BeanPropertyRowMapper.newInstance(ViewAllergyCategory.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAllergyCategory> categories = (List<ViewAllergyCategory>) result.get("categories"); 
            
           return categories;
            }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error while fectching all allergy category "+ exception.getMessage());
        }        
       
      }
    
    /**
         * retrieving all users allergies
     * @param start
     * @param length
         * @return a paginated allergies
         * @throws CosmosDatabaseException 
         */
         public Page<ViewAllergy> getAllAllergies(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_allergies")
                            .returningResultSet("allergies", BeanPropertyRowMapper.newInstance(ViewAllergy.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAllergy> allergies = (List<ViewAllergy>) result.get("allergies");         
            Page<ViewAllergy> obj = new Page<>();
            obj.setContent(allergies);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all user allergies "+exception.getMessage());
        }
       
      }
         
    /**
     * adding a user allergy
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public Allergy addAllergy(Allergy model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_user_allergy");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to add user allergy "+ exception.getMessage());
        }
       
    }
    
    /**
     * finding allergy by entryId
     * @param entryId
     * @return an object of the BloodDetails Class or null if not found
     * @throws CosmosDatabaseException 
     */
     public ViewAllergy findAllergyByEntryId(String entryId) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("entryId",entryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_allergy_byId")
                             .returningResultSet("allergy", BeanPropertyRowMapper.newInstance(ViewAllergy.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAllergy> allergy = (List<ViewAllergy>) result.get("allergy"); 
            if(allergy.size() > 0){
           return allergy.get(0);
                   }else{
               return null;
            }
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find allergy ID "+ exception.getMessage());
        }  
       
    }
         /**
      * retrieved all user's allergies
     * @param start
     * @param length
     * @param username
      * @return a list of user's  allergy
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
      */
    public Page<ViewAllergy> getAllUserAllergies(int start,int length,String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("username",username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_allergies_byUsername")
                            .returningResultSet("allergies", BeanPropertyRowMapper.newInstance(ViewAllergy.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAllergy> allergies = (List<ViewAllergy>) result.get("allergies");         
            Page<ViewAllergy> obj = new Page<>();
            obj.setContent(allergies);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error while fetching user allergies "+ exception.getMessage());
        }        
       
      }
    
   /**
     * delete user's allergy entry by entryId
     * @param entryId
     * @return true or false
     * @throws CosmosDatabaseException 
     */
     public boolean deleteAllergyByEntryId(String entryId) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("entryId",entryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("delete_allergy_byId");
                            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            return true;
            
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to delete allergy by entryId "+ exception.getMessage());
        }  
       
    }
              /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
