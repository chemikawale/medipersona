/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dao.UtilitiesDao;
import com.medi.cosmos.persona.dataview.model.ViewCountry;
import com.medi.cosmos.persona.model.ReviewerModel;
import com.medi.cosmos.persona.model.UserModel;
import com.medi.cosmos.persona.service.NotificationService;
import com.medi.cosmos.persona.service.ReviewerService;
import com.medi.cosmos.persona.service.UserService;
import com.medi.cosmos.persona.viewmodel.ReviewerSignUpViewModel;
import com.medi.cosmos.persona.viewmodel.SignUpViewModel;
import com.medi.cosmos.security.dao.CosmosUserDao;
import com.medi.cosmos.security.model.CosmosUser;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */
@Controller
@RequestMapping(value = "/persona")
public class PersonaController {

        @Autowired
    private CosmosUserDao cosmosDao;
         @Autowired
    private UserService userService;
         @Autowired
    private ReviewerService reviewerService;
        @Autowired 
    private  NotificationService notify;
           @Autowired
    private UserDao userDao;
             @Autowired
    private UtilitiesDao utiDao;

      @Autowired
     private CosmosUserDao cosmoUserDao;
       @Autowired
     private MedicalDataDao medicalDao;
    
                       
      @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public ModelAndView addUser(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/public/signup");
               List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
           mav.addObject("allCountries", allMediCountries);
        mav.addObject("viewModel", new SignUpViewModel());
         mav.addObject("user", "active");
        return mav ;
    }
    
  
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String addNewUser(@ModelAttribute("viewModel") @Valid SignUpViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new SignUpViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/public/signup";
        }             
         UserModel toUser = viewModel.toUser();
          
          String userName = toUser.getUsername();
          String userEmail = toUser.getEmail();
          String userPassword = toUser.getPassword();
          String lastName = toUser.getLastName();
          
         CosmosUser findByEmail = cosmosDao.findByEmail(toUser.getEmail());
          if(findByEmail != null){
                model.addAttribute("viewModel", new SignUpViewModel());
                model.addAttribute("error", "Sign Up failed: Email address already exists");
                return "/public/signup";
            } 
         CosmosUser findByUserName = cosmosDao.findByUserName(toUser.getUsername());
             if(findByUserName != null){
                model.addAttribute("viewModel", new SignUpViewModel());
                model.addAttribute("error", "Sign Up failed: Username already exists");
                return "/public/signup";
            }
   
         boolean createUserAdmin = userService.createUserAdmin(toUser);
      
            if(!createUserAdmin){
                model.addAttribute("viewModel", new SignUpViewModel());
                model.addAttribute("error", "An Error occur while trying to complete your registration");
                return "/public/signup";
            } 
        
             notify.SignupNotification(userName, userPassword, lastName, userEmail);
            
            model.addAttribute("success", "Registration successful, Login credentials sent to your email address");
            return "/public/signup";

    }

    @RequestMapping(value = "/reviewer", method = RequestMethod.GET)
    public ModelAndView addReviewer(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/public/reviewer");
               List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
           mav.addObject("allCountries", allMediCountries);
        mav.addObject("viewModel", new ReviewerSignUpViewModel());
         mav.addObject("user", "active");
        return mav ;
    }
    
  
    @RequestMapping(value = "/reviewer", method = RequestMethod.POST)
    public String addNewReviewer(@ModelAttribute("viewModel") @Valid ReviewerSignUpViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new ReviewerSignUpViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/public/reviewer";
        }             
            ReviewerModel toReviewer = viewModel.toReviewer();
          
          String userName = toReviewer.getUsername();
          String userEmail = toReviewer.getEmail();
          String userPassword = toReviewer.getPassword();
          String lastName = toReviewer.getLastName();
          
         CosmosUser findByEmail = cosmosDao.findByEmail(toReviewer.getEmail());
          if(findByEmail != null){
                model.addAttribute("viewModel", new ReviewerSignUpViewModel());
                model.addAttribute("error", "Sign Up failed: Email address already exists");
                return "/public/reviewer";
            } 
         CosmosUser findByUserName = cosmosDao.findByUserName(toReviewer.getUsername());
             if(findByUserName != null){
                model.addAttribute("viewModel", new ReviewerSignUpViewModel());
                model.addAttribute("error", "Sign Up failed: Username already exists");
                return "/public/reviewer";
            }
   
         boolean createReviewer = reviewerService.createReviewAccount(toReviewer);
      
            if(!createReviewer){
                model.addAttribute("viewModel", new ReviewerSignUpViewModel());
                model.addAttribute("error", "An Error occur while trying to complete your registration");
                return "/public/reviewer";
            } 
        
             notify.SignupNotification(userName, userPassword, lastName, userEmail);
            
            model.addAttribute("success", "Registration successful, password sent to your email address");
            return "/public/reviewer";

    }
    
    
    
    
    
}
