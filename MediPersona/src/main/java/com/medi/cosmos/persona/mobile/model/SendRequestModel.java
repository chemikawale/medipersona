/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.model.VitalSigns;
import com.medi.cosmos.persona.utilities.Utilities;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Oyewale
 */
public class SendRequestModel {
    @NotNull(message = "Username field cannot be null")
    private String username;
    @NotNull(message = "Email field cannot be null")
    private String email;
    @NotNull(message = "Added Time field cannot be null")
    private String addedTime;
    @NotNull(message = "Role field cannot be null")
    private String role;
    @NotNull(message = "Last Name field cannot be null")
    private String lastName;
    @NotNull(message = "First NAme field cannot be null")
    private String firstName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

        public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }
      
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
  
    public ReviewRequest toReviewCode(){
        ReviewRequest toReview = new ReviewRequest();
         
        toReview.setUsername(username);
         toReview.setCreatedOn(Utilities.presentDateTime());
         toReview.setEmail(email.trim());
         toReview.setReviewcode(Utilities.generateNumericId(10));
         toReview.setEnabled(true);
         TimeZone.getTimeZone("Africa/Lagos");
          LocalDateTime plusHours = LocalDateTime.now().plusHours(Long.parseLong(addedTime.trim()));
          Date from = Date.from(plusHours.atZone(ZoneId.of("Africa/Lagos")).toInstant());
         toReview.setExpiryDate(from);
         toReview.setIsUsed(false);
         toReview.setStatus(true);
         toReview.setRole(role.trim());
         toReview.setLastName(lastName.trim());
         toReview.setFirstName(firstName.trim());
        // toReview.setPassword(Utilities.generateRandomID(13));
         return toReview;
    }
}
