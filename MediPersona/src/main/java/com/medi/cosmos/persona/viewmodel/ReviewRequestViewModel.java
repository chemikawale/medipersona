/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

import com.medi.cosmos.persona.model.ReviewRequest;
import com.medi.cosmos.persona.utilities.Utilities;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Oyewale
 */
public class ReviewRequestViewModel {

      private String email;
      private String addedTime;
      private String role;
      private String lastName;
      private String firstName;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddedTime() {
        return addedTime;
    }

    public void setAddedTime(String addedTime) {
        this.addedTime = addedTime;
    }
      
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
  
    public ReviewRequest toReviewCode(){
        ReviewRequest toReview = new ReviewRequest();
         
         toReview.setCreatedOn(Utilities.presentDateTime());
         toReview.setEmail(email.trim());
         toReview.setReviewcode(Utilities.generateNumericId(10));
         toReview.setEnabled(true);
         TimeZone.getTimeZone("Africa/Lagos");
          LocalDateTime plusHours = LocalDateTime.now().plusHours(Long.parseLong(addedTime.trim()));
          Date from = Date.from(plusHours.atZone(ZoneId.of("Africa/Lagos")).toInstant());
         toReview.setExpiryDate(from);
         toReview.setIsUsed(false);
         toReview.setStatus(true);
         toReview.setRole(role.trim());
         toReview.setLastName(lastName.trim());
         toReview.setFirstName(firstName.trim());
        // toReview.setPassword(Utilities.generateRandomID(13));
         return toReview;
    }
}
