/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.mobile.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewUserToken {
     private String username;
    private String password;
    private String accessToken;
    private String createdOn;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    public String getCreatedOn() {
      if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewUserToken.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    
    
}
