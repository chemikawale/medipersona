/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.service;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dao.UtilitiesDao;
import com.medi.cosmos.persona.model.AllergyCategory;
import com.medi.cosmos.persona.model.AssignHmoPlan;
import com.medi.cosmos.persona.model.Category;
import com.medi.cosmos.persona.model.Country;
import com.medi.cosmos.persona.model.FeedBack;
import com.medi.cosmos.persona.model.Hmo;
import com.medi.cosmos.persona.model.HmoPlan;
import com.medi.cosmos.persona.model.MediFacility;
import com.medi.cosmos.persona.model.MediState;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Oyewale
 */
@Component
public class UtilitiesService {
    
    @Autowired
    private UtilitiesDao utilitiesDao;
      @Autowired
    private MedicalDataDao medicalDao;
    
    /**
     * Adding country to the medi countries database
     * @param countryDetails
     * @return true or false 
     */
    public boolean addCountry(Country countryDetails) {
         
        Country createCountry = null;
        try {
            createCountry = utilitiesDao.createCountry(countryDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return createCountry != null;
       
    }
    
      /**
     * Adding a state to a medi country
     * @param stateDetails
     * @return true or false 
     */
    public boolean addState(MediState stateDetails) {
      
        MediState createState = null;
        try {
            createState = utilitiesDao.createState(stateDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
       return createState != null;
    }
        /**
     *log a feedback
     * @param details
     * @return true or false 
     */
    public boolean logFeedBack(FeedBack details) {
      
        FeedBack logFeedBack = null;
        try {
             logFeedBack = utilitiesDao.logFeedBack(details);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
       return logFeedBack != null;
    }
    
    
      /**
     * Adding a medi category
     * @param categoryDetails
     * @return true or false 
     */
    public boolean addCategory(Category categoryDetails) {
      
 Category createCategory = null;
        try {
           createCategory = utilitiesDao.createCategory(categoryDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
       return createCategory != null;
    }
    
    /**
     * Adding a medi facility
     * @param facilityDetails
     * @return true or false 
     */
    public boolean addFacility(MediFacility facilityDetails) {
      
     MediFacility createMediFacility = null;
        try {
          createMediFacility = utilitiesDao.createMediFacility(facilityDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
        }
           
       return createMediFacility != null;
    }
     /**
     * Adding a medi hm 
     * @param hmoDetails
     * @return true or false 
     */
    public boolean addHmo(Hmo hmoDetails) {
      
     Hmo createHmo = null;
        try {
        createHmo = utilitiesDao.createHmo(hmoDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
           
       return createHmo != null;
    }
     /**
     * Adding a medi hmo plan
     * @param planDetails
     * @return true or false 
     */
    public boolean addHmoPlan(HmoPlan planDetails) {
      
     HmoPlan createHmoPlan = null;
        try {
          createHmoPlan = utilitiesDao.createHmoPlan(planDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
           
       return createHmoPlan != null;
    }
    
    /**
     * Adding a medi allergy category
     * @param category
     * @return true or false 
     */
    public boolean addAllergyCategory(AllergyCategory category) {
      
     AllergyCategory addAllergyCategory = null;
        try {
          addAllergyCategory = medicalDao.addAllergyCategory(category);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
           
       return addAllergyCategory != null;
    }
    
    
     /**
     * Assign Hmo Plan
     * @param planDetails
     * @return true or false 
     */
    public boolean assignHmoPlan(AssignHmoPlan planDetails) {
      
     AssignHmoPlan assignHmoPlan = null;
        try {
          assignHmoPlan = utilitiesDao.AssignHmoPlan(planDetails);
        } catch (CosmosDatabaseException ex) {
            Logger.getLogger(UtilitiesService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
           
       return assignHmoPlan != null;
    }
}
