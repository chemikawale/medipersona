/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.persona.utilities;

/**
 *
 * @author ChemikaWale
 */
public class UrlConstants {
    
    //the end point for public APIs
    public static final String VERIFY_LOGIN = "/webapi/v1/authenticate";
    public static final String SIGN_UP_USER = "/webapi/v1/registeruser";
    public static final String GET_ALL_COUNTRIES = "/webapi/v1/allcountries";
    
    //the end point for protected APIs
    public static final String LOG_VITAL_SIGNS = "/mobileapi/v1/logvitalsigns";
    public static final String ALL_VITAL_SIGNS = "/mobileapi/v1/allvitalsigns";
    
    public static final String LOG_MEDICATION = "/mobileapi/v1/logmedication";
    public static final String ALL_MEDICATIONS = "/mobileapi/v1/allmedications";
    
     public static final String SEND_REQUEST = "/mobileapi/v1/sendrequest";
   
    
    public static final String ACTIVATE_TERMINAL = "/webapi/v2/activateterminal";
    public static final String SYNC_POS_ACCOUTS = "/webapi/v2/addmerchantposaccounts";
    public static final String FETCH_MERCHANT_ACCOUNTS = "/webapi/v2/fetchmerchantaccounts";
    public static final String FETCH_QT_BILLERS = "/webapi/v2/fetchbillerpayments";
          
    public static final String FETCH_MESSAGE = "/webapi/v2/fetchmessage";
    
      public static final String FETCH_VAS_CATEGORY = "/webapi/v2/vascategories/{username}";
      
      public static final String FETCH_VAS_ITEMS = "/webapi/v2/vasitems";
      public static final String MAKE_VAS_PAYMENT = "/webapi/v2/makevaspayment";
    //public static final String LOG_TRANSACTIONS = "/webapi/logtransactions";
      
          public static final String NEW_LOG_TRANSACTIONS = "/webapi/v2/logtransactions";
          
    

}
