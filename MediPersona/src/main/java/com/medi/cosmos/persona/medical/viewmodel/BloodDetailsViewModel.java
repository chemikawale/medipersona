/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.medical.viewmodel;

import com.medi.cosmos.persona.model.BloodDetails;
import com.medi.cosmos.persona.utilities.Utilities;

/**
 *
 * @author Oyewale
 */
public class BloodDetailsViewModel {
    private String bloodGroup;
    private String rhesusD;
    private String genotype;

    public String getGenotype() {
        return genotype;
    }

    public void setGenotype(String genotype) {
        this.genotype = genotype;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getRhesusD() {
        return rhesusD;
    }

    public void setRhesusD(String rhesusD) {
        this.rhesusD = rhesusD;
    }
   public BloodDetails getBloodDetails(){
    BloodDetails details = new BloodDetails();
      details.setBloodGroup(bloodGroup.trim());
      details.setCreatedOn(Utilities.presentDateTime());
      details.setGenotype(genotype.trim());
      details.setRhesusD(rhesusD.trim());
    return details;
   }   
}
