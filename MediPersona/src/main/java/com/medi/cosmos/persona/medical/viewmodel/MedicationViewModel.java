/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.medical.viewmodel;

import com.medi.cosmos.persona.model.Medication;
import com.medi.cosmos.persona.utilities.Utilities;
import java.text.ParseException;

/**
 *
 * @author Oyewale
 */
public class MedicationViewModel {
    private String diagnosis;
    private String prescription;
    private String prescriptionNote;
    private String medication;
    private String startDate;
    private String endDate;

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public String getPrescriptionNote() {
        return prescriptionNote;
    }

    public void setPrescriptionNote(String prescriptionNote) {
        this.prescriptionNote = prescriptionNote;
    }

   
    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public Medication toMedication() throws ParseException{
    
    Medication mediData = new Medication();
     
      mediData.setPrescriptionNote(prescriptionNote.trim());
      mediData.setCreatedOn(Utilities.presentDateTime());
      mediData.setDiagnosis(diagnosis.trim());
      mediData.setMedication(medication.trim());
      mediData.setMedicationId(Utilities.generateRandomID(15)); 
      mediData.setPrescription(prescription.trim());
      mediData.setStartDate(Utilities.StringToDate(startDate.replaceAll("/","-").trim()));
      mediData.setEndDate(Utilities.StringToDate(endDate.replaceAll("/","-").trim()));
      
      
    return mediData;
    }
    
}
