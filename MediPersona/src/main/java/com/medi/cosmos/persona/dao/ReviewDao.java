/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dataview.model.ViewAssignedPlan;
import com.medi.cosmos.persona.dataview.model.ViewReview;
import com.medi.cosmos.persona.model.ReviewModel;
import com.medi.cosmos.persona.model.ReviewerModel;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class ReviewDao {
       private static final Logger logger = Logger.getLogger("Review Dao");
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    
     @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    } 
    
    
     /**
     * log a review by a reviewer
     * @param model
     * @return
     * @throws CosmosDatabaseException 
     */
    
       public ReviewModel logReview(ReviewModel model) throws CosmosDatabaseException {       
        
     try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("log_review");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                       
            return model;   
        }
        catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to log review "+ exception.getMessage());
        }
    }
   
     /**
         * retrieving all user's reviews
     * @param start
     * @param length
     * @param username
         * @return a paginated reviews
         * @throws CosmosDatabaseException 
         */
         public Page<ViewReview> findReviewsForUsername(int start,int length,String username) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("username", username);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_reviews_byusername")
                            .returningResultSet("reviews", BeanPropertyRowMapper.newInstance(ViewReview.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReview> reviews = (List<ViewReview>) result.get("reviews");         
            Page<ViewReview> obj = new Page<>();
            obj.setContent(reviews);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching user's reviews by username "+exception.getMessage());
        }
       
      }
         
          /**
         * retrieving all reviewer's reviews
     * @param start
     * @param length
     * @param reviewer
         * @return a paginated reviews
         * @throws CosmosDatabaseException 
         */
         public Page<ViewReview> findReviewerReviews(int start,int length,String reviewer) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length).addValue("reviewer", reviewer);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("find_reviews_byreviewer")
                            .returningResultSet("reviews", BeanPropertyRowMapper.newInstance(ViewReview.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReview> reviews = (List<ViewReview>) result.get("reviews");         
            Page<ViewReview> obj = new Page<>();
            obj.setContent(reviews);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching reviewer's reviews "+exception.getMessage());
        }
       
      }
         
          /**
         * retrieving all reviews
     * @param start
     * @param length
   
         * @return a paginated reviews
         * @throws CosmosDatabaseException 
         */
         public Page<ViewReview> getAllReviews(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_reviews")
                            .returningResultSet("reviews", BeanPropertyRowMapper.newInstance(ViewReview.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReview> reviews = (List<ViewReview>) result.get("reviews");         
            Page<ViewReview> obj = new Page<>();
            obj.setContent(reviews);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while fetching all reviews "+exception.getMessage());
        }
       
      }
         
               /**
     * finding the details of a review by ID
     * @param reviewId    
     * @return an object of the Review Class
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public ViewReview findReviewById(String reviewId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("reviewId",reviewId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_review_byReviewId")
                             .returningResultSet("review", BeanPropertyRowMapper.newInstance(ViewReview.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewReview> review = (List<ViewReview>) result.get("review"); 
            if(review.size() > 0){
           return review.get(0);
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding review details by ID "+ exception.getMessage());
        }  
    }
    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

      /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
