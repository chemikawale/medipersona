/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.persona.dao.UtilitiesDao;
import com.medi.cosmos.persona.model.FeedBack;
import com.medi.cosmos.persona.model.UrlConstants;
import com.medi.cosmos.persona.viewmodel.FeedBackViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Oyewale
 */
@CrossOriginResourceSharing
@RestController
public class MobileAppApiController {
    @Autowired
    private UtilitiesDao utiDao;
    
      @RequestMapping(value = UrlConstants.LOG_FEEDBACK, method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody Map<String,List> logFeedBack(@RequestBody FeedBackViewModel logModel,BindingResult binding) throws CosmosDatabaseException{
       List responseDetails = new ArrayList();
        Map response = new HashMap();
               if(binding.hasErrors()){
                    response.put("status-code","01");
                    String message = "";
                    message = binding.getFieldErrors().stream().map((fe) -> fe.getDefaultMessage() +"\n").reduce(message, String::concat);
                    response.put("status","Error detected in request sent");
                    response.put("response",message);
                 responseDetails.add(response);
         }     
        FeedBack toFeedBack = logModel.toFeedBack();
        FeedBack logFeedBack = utiDao.logFeedBack(toFeedBack);
        if(logFeedBack != null)
            {
                response.put("status-code","00");
                response.put("status","Logged Successful");
                response.put("response","Feedback Logged Successful, Thanks");
                responseDetails.add(response);
         }
        else{
                response.put("status-code","01");
                response.put("status","Data log error");
                response.put("response","An internal error occured while trying to log feedbacks");
                responseDetails.add(response);
         }
         Map<String,List> requestResponse = new HashMap<>();
          requestResponse.put("requestResponse",responseDetails);
          return requestResponse;
    }
    
}
