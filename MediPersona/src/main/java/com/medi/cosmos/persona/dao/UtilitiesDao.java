/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dataview.model.ViewAssignedPlan;
import com.medi.cosmos.persona.dataview.model.ViewCategory;
import com.medi.cosmos.persona.dataview.model.ViewCountry;
import com.medi.cosmos.persona.dataview.model.ViewFeedBack;
import com.medi.cosmos.persona.dataview.model.ViewHmo;
import com.medi.cosmos.persona.dataview.model.ViewHmoPlan;
import com.medi.cosmos.persona.dataview.model.ViewMediFacility;
import com.medi.cosmos.persona.dataview.model.ViewState;
import com.medi.cosmos.persona.model.AssignHmoPlan;
import com.medi.cosmos.persona.model.Category;
import com.medi.cosmos.persona.model.Country;
import com.medi.cosmos.persona.model.FeedBack;
import com.medi.cosmos.persona.model.Hmo;
import com.medi.cosmos.persona.model.HmoPlan;
import com.medi.cosmos.persona.model.MediFacility;
import com.medi.cosmos.persona.model.MediState;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Oyewale
 */
@Repository
public class UtilitiesDao {
    
     private static final Logger logger = Logger.getLogger("Utilities Dao");
     
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    
    private SimpleJdbcCall simpleJdbcCall;
    

    
    @PostConstruct
    public void init(){
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    }
    
    /**
     * adding a medi country
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public Country createCountry(Country model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_country");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create country "+ exception.getMessage());
        }
       
    }
    
    
    /**
     * fetch all the previously added medi countries
     * @return a List of all added countries
     * @throws CosmosDatabaseException 
     */
    public List<ViewCountry> getAllMediCountries() throws CosmosDatabaseException{
    
          try{
              SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
          
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_medi_countries")
                            .returningResultSet("countries", BeanPropertyRowMapper.newInstance(ViewCountry.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewCountry> countries = (List<ViewCountry>) result.get("countries"); 
            
           return countries;
          }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occure while fetching all medi countries "+ exception.getMessage());
        }         
        
       
      }
    /**
     * finding a medi country using country ID
     * @param countryId
     * @return an object of the Country Class or null if not found
     * @throws CosmosDatabaseException 
     */
     public ViewCountry findCountryById(String countryId) throws CosmosDatabaseException {       
       
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("countryId",countryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_country_ById")
                             .returningResultSet("country", BeanPropertyRowMapper.newInstance(ViewCountry.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewCountry> country = (List<ViewCountry>) result.get("country"); 
            if(country != null){
           return country.get(0);
                   }else{
               return null;
            }
             }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find country by Id "+ exception.getMessage());
        }  
       
    }
    /**
     * add a state to a particular country
     * @param model
     * @return a replica of the supplied model if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public MediState createState(MediState model) throws CosmosDatabaseException {     
        
       try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_state");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create a medi state "+ exception.getMessage());
        }  
       
    }
     /**
      * retrieved all registered medi states
      * @return a list of registered state class 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
      */
   public List<ViewState> getAllMediStates() throws CosmosDatabaseException{
  
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_medi_states")
                            .returningResultSet("states", BeanPropertyRowMapper.newInstance(ViewState.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewState> states = (List<ViewState>) result.get("states"); 
            
           return states;
            }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error while fectching all medi states "+ exception.getMessage());
        }        
       
      }
    /**
     * Finding the states of a particular medi country using the country ID
     * @param countryId
     * @return a list of State Class 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public List<ViewState> findStatesByCountryId(String countryId) throws CosmosDatabaseException {       
        
    
        try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("countryId",countryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_states_ByCountryId")
                             .returningResultSet("states", BeanPropertyRowMapper.newInstance(ViewState.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewState> states = (List<ViewState>) result.get("states"); 
            if(states != null){
           return states;
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to find medi states by country Id "+ exception.getMessage());
        }  
           }   
     
     /**
      * Finding a registered state by the State ID
      * @param stateId
      * @return an object of State class or null if not available 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
      */
     public ViewState findStateById(String stateId) throws CosmosDatabaseException {       
     
         try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("stateId",stateId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_states_ById")
                             .returningResultSet("state", BeanPropertyRowMapper.newInstance(ViewState.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewState> state = (List<ViewState>) result.get("state"); 
            if(state != null){
           return state.get(0);
                   }else{
               return null;
            }
         }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding state by ID "+ exception.getMessage());
        }  
       
    }
     
     /**
     * adding a medi category
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public Category createCategory(Category model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_category");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create a medi category "+ exception.getMessage());
        }  
    }
    
    /**
     * fetch all the previously added medi categories
     * @return a List of all added categories 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public List<ViewCategory> getAllMediCategories() throws CosmosDatabaseException{
    
       try{   
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_medi_categories")
                            .returningResultSet("categories", BeanPropertyRowMapper.newInstance(ViewCategory.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewCategory> categories = (List<ViewCategory>) result.get("categories"); 
            
           return categories;
                   
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while fetching medi categories "+ exception.getMessage());
        }   
       
      }
    /**
     * finding a medi country using country ID
     * @param categoryId
     * @return an object of the Category Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public ViewCategory findCategoryById(String categoryId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("categoryId",categoryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_category_ById")
                             .returningResultSet("category", BeanPropertyRowMapper.newInstance(ViewCategory.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewCategory> category = (List<ViewCategory>) result.get("category"); 
            if(category != null){
           return category.get(0);
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding category by ID "+ exception.getMessage());
        }  
    }
     /**
      * Logging of feedback
      * @param model
      * @return a model of the logged feedback if successful
      * @throws CosmosDatabaseException 
      */
        public FeedBack logFeedBack(FeedBack model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("log_medi_feedback");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to log feedback "+ exception.getMessage());
        }
       
    }
        /**
         * retrieving all medi feed backs paginated
     * @param start
     * @param length
         * @return a paginated medi feedbacks
         * @throws CosmosDatabaseException 
         */
         public Page<ViewFeedBack> getAllLoggedFeedBack(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_feedbacks")
                            .returningResultSet("feedbacks", BeanPropertyRowMapper.newInstance(ViewFeedBack.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewFeedBack> feedbacks = (List<ViewFeedBack>) result.get("feedbacks");         
            Page<ViewFeedBack> obj = new Page<>();
            obj.setContent(feedbacks);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all medi feedbacks "+exception.getMessage());
        }
       
      }
         
         /**
      * adding a new medical facility to the repository
      * @param model
      * @return a model of the added medical facility
      * @throws CosmosDatabaseException 
      */
        public MediFacility createMediFacility(MediFacility model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_facility");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
      } catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create medi facility "+ exception.getMessage());
        }
       
    }
        /**
         * retrieving all registered medi facilities
     * @param start
     * @param length
         * @return a paginated medi feedbacks
         * @throws CosmosDatabaseException 
         */
         public Page<ViewMediFacility> getAllMediFacilities(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("all_medi_facilities")
                            .returningResultSet("facilities", BeanPropertyRowMapper.newInstance(ViewMediFacility.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewMediFacility> facilities = (List<ViewMediFacility>) result.get("facilities");         
            Page<ViewMediFacility> obj = new Page<>();
            obj.setContent(facilities);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all medi facilities "+exception.getMessage());
        }
       
      }
     
          /**
     * adding hmo plan
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public HmoPlan createHmoPlan(HmoPlan model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_hmo_plan");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create a medi hmo plan "+ exception.getMessage());
        }  
    }
    
    /**
     * fetch all the previously added medi hmo plan
     * @return a List of all added categories 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public List<ViewHmoPlan> getAllMediHmoPlans() throws CosmosDatabaseException{
    
       try{   
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_medi_hmo_plans")
                            .returningResultSet("hmoplans", BeanPropertyRowMapper.newInstance(ViewHmoPlan.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmoPlan> hmoplans = (List<ViewHmoPlan>) result.get("hmoplans"); 
            
           return hmoplans;
                   
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while fetching medi hmoplans "+ exception.getMessage());
        }   
       
      }
    /**
     * finding a medi plan using plan ID
     * @param planId
     * @return an object of the HmoPlan Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public ViewHmoPlan findHmoPlanById(String planId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("planId",planId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_hmo_plan_ById")
                             .returningResultSet("hmoplan", BeanPropertyRowMapper.newInstance(ViewHmoPlan.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmoPlan> hmoplan = (List<ViewHmoPlan>) result.get("hmoplan"); 
            if(hmoplan != null){
           return hmoplan.get(0);
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding hmoplan by ID "+ exception.getMessage());
        }  
    }
         
     
          /**
     * adding hmo provider
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public Hmo createHmo(Hmo model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("add_medi_hmo");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to create a medi hmo "+ exception.getMessage());
        }  
    }
    
  /**
         * retrieving all medi registered hmo paginated
     * @param start
     * @param length
         * @return a assigned hmos
         * @throws CosmosDatabaseException 
         */
         public Page<ViewHmo> getAllMediHmos(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("all_hmo_paginated")
                            .returningResultSet("hmos", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> hmos = (List<ViewHmo>) result.get("hmos");         
            Page<ViewHmo> obj = new Page<>();
            obj.setContent(hmos);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all medi hmos "+exception.getMessage());
        }
       
      }
         
         /**
     * fetch all the previously added medi hmo
     * @return a List of all added hmos
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public List<ViewHmo> getAllMediHmo() throws CosmosDatabaseException{
    
       try{   
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource();
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_medi_hmo")
                            .returningResultSet("hmos", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> hmos = (List<ViewHmo>) result.get("hmos"); 
            
           return hmos;
                   
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while fetching medi hmos "+ exception.getMessage());
        }   
       
      }
    /**
     * finding a medi hmo using hmo ID
     * @param hmoId
     * @return an object of the Hmo Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public ViewHmo findHmoById(String hmoId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("hmoId",hmoId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_hmo_ById")
                             .returningResultSet("hmo", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> hmo = (List<ViewHmo>) result.get("hmo"); 
            if(hmo.size() > 0){
           return hmo.get(0);
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding hmo by ID "+ exception.getMessage());
        }  
    }
         
       /**
     * finding a medi hmo using country ID
     * @param countryId
     * @return an object of the Hmo Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public ViewHmo findHmoByCountryId(String countryId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("countryId",countryId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_medi_hmo_ByCountryId")
                             .returningResultSet("hmo", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> hmo = (List<ViewHmo>) result.get("hmo"); 
            if(hmo.size() > 0){
           return hmo.get(0);
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding hmo by country ID "+ exception.getMessage());
        }  
    }
        
       /**
     * assign hmo to plan
     * @param model
     * @return a replica of the model supplied if successful 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
    public AssignHmoPlan AssignHmoPlan(AssignHmoPlan model) throws CosmosDatabaseException {     
        
      try{
          SqlParameterSource in = new BeanPropertySqlParameterSource(model); 
           simpleJdbcCall = new SimpleJdbcCall(dataSource)
                                .withCatalogName("medipersona")
                                .withProcedureName("assign_hmo_to_plan");
            
            simpleJdbcCall.compile();
            
            Map result = simpleJdbcCall.execute(in);
                    
            return model;   
        
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("Unable to assign a medi hmo plan "+ exception.getMessage());
        }  
    }
    
     /**
     * finding a assigned plan using hmo ID
     * @param hmoId
     * @return an object of the HmoPlan Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public List<ViewHmo> findAssignedPlanByHmoId(String hmoId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("hmoId",hmoId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_assigned_hmo_ByHmoId")
                             .returningResultSet("assigned", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> assigned = (List<ViewHmo>) result.get("assigned"); 
            if(assigned.size() > 0){
           return assigned;
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding assigne plan by ID "+ exception.getMessage());
        }  
    }
         
        /**
     * finding a assigned plan using plan ID
     * @param planId
     * @return an object of the HmoPlan Class or null if not found 
     * @throws com.esl.cosmos.core.exceptions.CosmosDatabaseException 
     */
     public List<ViewHmo> findAssignedPlanByPlanId(String planId) throws CosmosDatabaseException {       
       try{
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("planId",planId);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                             .withProcedureName("find_assigned_hmo_ByPlanId")
                             .returningResultSet("assigned", BeanPropertyRowMapper.newInstance(ViewHmo.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewHmo> assigned = (List<ViewHmo>) result.get("assigned"); 
            if(assigned.size() > 0){
           return assigned;
                   }else{
               return null;
            }
        }
          catch(InvalidDataAccessApiUsageException exception)
        {
            throw new CosmosDatabaseException("An error occur while finding assigne plan by ID "+ exception.getMessage());
        }  
    }
        
       /**
         * retrieving all medi assigned hmo plan paginated
     * @param start
     * @param length
         * @return a paginated assigned hmo plan
         * @throws CosmosDatabaseException 
         */
         public Page<ViewAssignedPlan> getAllAssignedHmoPlan(int start,int length) throws CosmosDatabaseException{    
           try{ 
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("pageNum",start).addValue("pageSize", length);
         
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                            .withCatalogName("medipersona")
                            .withProcedureName("get_all_assigned_plan")
                            .returningResultSet("assigned", BeanPropertyRowMapper.newInstance(ViewAssignedPlan.class));
            
            simpleJdbcCall.compile();            
            Map result = simpleJdbcCall.execute(sqlParameterSource);
     
            List<ViewAssignedPlan> assigned = (List<ViewAssignedPlan>) result.get("assigned");         
            Page<ViewAssignedPlan> obj = new Page<>();
            obj.setContent(assigned);            
            obj.setCount( new Long(result.get("count").toString()));          
                return obj;
                   }
        catch(InvalidDataAccessApiUsageException exception){
            throw new CosmosDatabaseException("an error occur while getting all medi assigned plan "+exception.getMessage());
        }
       
      }
         
              /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
