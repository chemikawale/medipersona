/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.viewmodel;

/**
 *
 * @author Oyewale
 */
public class VerifyReviewCodeViewModel {
    private String reviewCode;

    public String getReviewCode() {
        return reviewCode;
    }

    public void setReviewCode(String reviewCode) {
        this.reviewCode = reviewCode;
    }
    
}
