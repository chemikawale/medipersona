/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.controller;


import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.Page;
import com.medi.cosmos.persona.dao.MedicalDataDao;
import com.medi.cosmos.persona.dao.UserDao;
import com.medi.cosmos.persona.dao.UtilitiesDao;
import com.medi.cosmos.persona.dataview.model.CustomHmoView;
import com.medi.cosmos.persona.dataview.model.ViewAllergy;
import com.medi.cosmos.persona.dataview.model.ViewAllergyCategory;
import com.medi.cosmos.persona.dataview.model.ViewAssignedPlan;
import com.medi.cosmos.persona.dataview.model.ViewCategory;
import com.medi.cosmos.persona.dataview.model.ViewCountry;
import com.medi.cosmos.persona.dataview.model.ViewCustomAllergy;
import com.medi.cosmos.persona.dataview.model.ViewCustomMediFacility;
import com.medi.cosmos.persona.dataview.model.ViewCustomState;
import com.medi.cosmos.persona.dataview.model.ViewFeedBack;
import com.medi.cosmos.persona.dataview.model.ViewHmo;
import com.medi.cosmos.persona.dataview.model.ViewHmoPlan;
import com.medi.cosmos.persona.dataview.model.ViewMediFacility;
import com.medi.cosmos.persona.dataview.model.ViewReviewRequest;
import com.medi.cosmos.persona.dataview.model.ViewState;
import com.medi.cosmos.persona.medical.viewmodel.AllergiesCategoryViewModel;
import com.medi.cosmos.persona.model.AllergyCategory;
import com.medi.cosmos.persona.model.AssignHmoPlan;
import com.medi.cosmos.persona.model.Category;
import com.medi.cosmos.persona.model.Country;
import com.medi.cosmos.persona.model.FeedBack;
import com.medi.cosmos.persona.model.Hmo;
import com.medi.cosmos.persona.model.HmoPlan;
import com.medi.cosmos.persona.model.MediFacility;
import com.medi.cosmos.persona.model.MediState;
import com.medi.cosmos.persona.service.UtilitiesService;
import com.medi.cosmos.persona.utilities.PageResult;
import com.medi.cosmos.persona.viewmodel.AssignHmoPlanViewModel;
import com.medi.cosmos.persona.viewmodel.CategoryViewModel;
import com.medi.cosmos.persona.viewmodel.CountryViewModel;
import com.medi.cosmos.persona.viewmodel.FeedBackViewModel;
import com.medi.cosmos.persona.viewmodel.HmoPlanViewModel;
import com.medi.cosmos.persona.viewmodel.HmoViewModel;
import com.medi.cosmos.persona.viewmodel.StateViewModel;
import com.medi.cosmos.persona.viewmodel.WebMediFacilityViewModel;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */

@Controller
@RequestMapping(value = "/uti")
public class UtilitiesController {
    
    @Autowired
    private UtilitiesService utiService;
    @Autowired
    private UtilitiesDao utiDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MedicalDataDao medicalDao;
 
    
    @RequestMapping(value = "/addcountry", method = RequestMethod.GET)
    public ModelAndView addCountry(Model model) {     
        ModelAndView mav = new ModelAndView("/secured/addcountry");
        mav.addObject("viewModel", new CountryViewModel());
         mav.addObject("utilities", "active");
        return mav ;
    }

    @RequestMapping(value = "/addcountry", method = RequestMethod.POST)
    public String addNewCountry(@ModelAttribute("viewModel") @Valid CountryViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new CountryViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addcountry";
        }             
        Country toCountry = viewModel.toCountry();
            boolean addCountry = utiService.addCountry(toCountry);
            if(!addCountry){
                model.addAttribute("viewModel", new CountryViewModel());
                model.addAttribute("error", "Unable to add country");
                return "/secured/addcountry";
            } 
     
            return "redirect:/uti/allcountries";

    }

       @RequestMapping(value = {"/allcountries"}, method = RequestMethod.GET)
    public ModelAndView AllMediCountries(Model model) throws CosmosDatabaseException  {

        List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
         ModelAndView mav = new ModelAndView("/views/uti/allcountries");
         mav.addObject("allCountries", allMediCountries);

        return mav;
    }

     @RequestMapping(value = "/addstate", method = RequestMethod.GET)
    public ModelAndView addState(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/addstate");
        mav.addObject("viewModel", new StateViewModel());
         List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
           mav.addObject("allCountries", allMediCountries);
         mav.addObject("utilities", "active");
        return mav ;
    }

    @RequestMapping(value = "/addstate", method = RequestMethod.POST)
    public String addNewState(@ModelAttribute("viewModel") @Valid StateViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new StateViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addstate";
        }             
        MediState toState = viewModel.toState();
            boolean addState = utiService.addState(toState);
            if(!addState){
                model.addAttribute("viewModel", new StateViewModel());
                model.addAttribute("error", "Unable to add state");
                return "/secured/addstate";
            } 
     
            return "redirect:/uti/allstates";

    }

       @RequestMapping(value = {"/allstates"}, method = RequestMethod.GET)
    public ModelAndView AllMediStates(Model model) throws CosmosDatabaseException{

        List<ViewState> allMediStates = utiDao.getAllMediStates();
        List<ViewCustomState> allStates = new ArrayList<>();
        if(allMediStates != null){
        allMediStates.stream().map((state) -> {
            ViewCustomState customState = new ViewCustomState();
            customState.setCountryId(state.getCountryId());
            customState.setCreatedOn(state.getCreatedOn());
            customState.setStateId(state.getStateId());
            customState.setStateName(state.getStateName());
            try {
                customState.setCountryName(utiDao.findCountryById(state.getCountryId()).getCountryName());
            } catch (CosmosDatabaseException ex) {
                Logger.getLogger(UtilitiesController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return customState;
        }).forEach((customState) -> {
            allStates.add(customState);
        });
        }
         ModelAndView mav = new ModelAndView("/views/uti/allstates");

        mav.addObject("allStates", allStates);

        return mav;
    }
     @PreAuthorize(value = "isAuthenticated()")
      @ResponseBody
    @RequestMapping(value = {"/states"}, method = RequestMethod.GET)    
    public List<ViewCustomState> CountryStates(@RequestParam("countryId") String countryId, Model model) throws CosmosDatabaseException  {

        List<ViewState> allMediStates = utiDao.findStatesByCountryId(countryId.trim());
        List<ViewCustomState> allStates = new ArrayList<>();
        if(allMediStates != null){
        allMediStates.stream().map((state) -> {
            ViewCustomState customState = new ViewCustomState();
            customState.setCountryId(state.getCountryId());
            customState.setCreatedOn(state.getCreatedOn());
            customState.setStateId(state.getStateId());
            customState.setStateName(state.getStateName());
            try {
                customState.setCountryName(utiDao.findCountryById(state.getCountryId()).getCountryName());
            } catch (CosmosDatabaseException ex) {
                Logger.getLogger(UtilitiesController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return customState;
        }).forEach((customState) -> {
            allStates.add(customState);
        });
        }
         

        return allStates;
    }
    
  @RequestMapping(value = "/addcategory", method = RequestMethod.GET)
    public ModelAndView addCategory(Model model) {     
        ModelAndView mav = new ModelAndView("/secured/addcategory");
        mav.addObject("viewModel", new CategoryViewModel());
         mav.addObject("utilities", "active");
        return mav ;
    }

    @RequestMapping(value = "/addcategory", method = RequestMethod.POST)
    public String addNewCategory(@ModelAttribute("viewModel") @Valid CategoryViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new CategoryViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addcategory";
        }             
        Category toCategory = viewModel.toCategory();
            boolean addCategory= utiService.addCategory(toCategory);
            if(!addCategory){
                model.addAttribute("viewModel", new CategoryViewModel());
                model.addAttribute("error", "Unable to add country");
                return "/secured/addcategory";
            } 
     
            return "redirect:/uti/allcategories";

    }

       @RequestMapping(value = {"/allcategories"}, method = RequestMethod.GET)
    public ModelAndView AllMediCategories(Model model) throws CosmosDatabaseException  {

        List<ViewCategory> allMediCategories = utiDao.getAllMediCategories();
         ModelAndView mav = new ModelAndView("/views/uti/allcategories");

        mav.addObject("allCategories", allMediCategories);

        return mav;
    }

     @RequestMapping(value = "/logfeedback", method = RequestMethod.GET)
    public ModelAndView logFeedBack(Model model) {     
        ModelAndView mav = new ModelAndView("/secured/addfeedback");
        mav.addObject("viewModel", new FeedBackViewModel());
         mav.addObject("utilities", "active");
        return mav ;
    }

    @RequestMapping(value = "/logfeedback", method = RequestMethod.POST)
    public String addNewFeedBack(@ModelAttribute("viewModel") @Valid FeedBackViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
         
        String cusername = (String)session.getAttribute("username");
        if (result.hasErrors()) {
            model.addAttribute("viewModel", new FeedBackViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addfeedback";
        }             
        FeedBack toFeedBack = viewModel.toFeedBack();
        toFeedBack.setUsername(cusername);
            boolean logFeedBack= utiService.logFeedBack(toFeedBack);
           
            if(!logFeedBack){
                model.addAttribute("viewModel", new FeedBackViewModel());
                model.addAttribute("error", "Unable to add country");
                return "/secured/addfeedback";
            } 
           model.addAttribute("success", "FeedBack logged successfully, some one will get back to you soon. Thanks");
           model.addAttribute("viewModel", new FeedBackViewModel());
           return "/secured/addfeedback";

    }
    
           @RequestMapping(value = {"/allfeedbacks"}, method = RequestMethod.GET)
    public String AllMediFeedBacks(Model model) throws CosmosDatabaseException {
      
        return "/views/uti/allfeedbacks";
    }
       @PreAuthorize(value = "isAuthenticated()")
     @RequestMapping(value = "/getallfeedbacks", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<ViewFeedBack> allFeedbacks(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {

         if(start > 0){
            start = start / length ;
        }
       
        try{
        PageResult<ViewFeedBack> result;
             Page<ViewFeedBack> allLoggedFeedBack = utiDao.getAllLoggedFeedBack(start, length);
             List<ViewFeedBack> content = allLoggedFeedBack.getContent();
    
            result = new PageResult<>(content, allLoggedFeedBack.getCount() , allLoggedFeedBack.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
    
     @RequestMapping(value = "/addfacility", method = RequestMethod.GET)
    public ModelAndView addMediFacility(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/addfacility");
        List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
        List<ViewCategory> allMediCategories = utiDao.getAllMediCategories();
        mav.addObject("allCountries",allMediCountries);
         mav.addObject("allCategories",allMediCategories);
        mav.addObject("viewModel", new WebMediFacilityViewModel());
         mav.addObject("utilities", "active");
        return mav ;
    }

    @RequestMapping(value = "/addfacility", method = RequestMethod.POST)
    public String addNewFacility(@ModelAttribute("viewModel") @Valid WebMediFacilityViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new WebMediFacilityViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addfacility";
        }             
        MediFacility toFacility = viewModel.toFacility();
            boolean addFacility = utiService.addFacility(toFacility);
            if(!addFacility){
                model.addAttribute("viewModel", new WebMediFacilityViewModel());
                model.addAttribute("error", "Unable to add medical provider");
                return "/secured/addfacility";
            } 
        model.addAttribute("success", "HealthCare Provider added successfully");
         return "redirect:/uti/allmedifacilities";

    }

    
      @RequestMapping(value = {"/allmedifacilities"}, method = RequestMethod.GET)
    public String AllMediFacilities(Model model) throws CosmosDatabaseException {
      
        return "/views/uti/allmedifacilities";
    }
       @PreAuthorize(value = "isAuthenticated()")
     @RequestMapping(value = "/getallmedifacilities", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<ViewCustomMediFacility> allMediFacilities(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {

         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomMediFacility> result;
             Page<ViewMediFacility> page = utiDao.getAllMediFacilities(start, length);
         List<ViewMediFacility> content = page.getContent();
             List<ViewCustomMediFacility> pageView = new ArrayList<>();
      
                 for (ViewMediFacility facility : content) {                    
                ViewCustomMediFacility pageObject = new ViewCustomMediFacility();
                    pageObject.setAddress(facility.getAddress());
                    pageObject.setCategoryId(facility.getCategoryId());
                    pageObject.setCategoryName(utiDao.findCategoryById(facility.getCategoryId()).getCategoryName());
                    pageObject.setContactPerson(facility.getContactPerson());
                    pageObject.setCountryId(facility.getCountryId());
                    pageObject.setCountryName(utiDao.findCountryById(facility.getCountryId()).getCountryName());
                    pageObject.setCreatedOn(facility.getCreatedOn());
                    pageObject.setFacilityId(facility.getFacilityId());
                    pageObject.setFacilityName(facility.getFacilityName());
                    pageObject.setLatitude(facility.getLatitude());
                    pageObject.setLongitude(facility.getLongitude());
                    pageObject.setServices(facility.getServices());
                    pageObject.setStateId(facility.getStateId());
                    pageObject.setStateName(utiDao.findStateById(facility.getStateId()).getStateName());
                    pageObject.setVerified(facility.isVerified());
                
                pageView.add(pageObject);
            }
            result = new PageResult<>(pageView, page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
      
     @RequestMapping(value = "/addhmo", method = RequestMethod.GET)
    public ModelAndView addMediHmo(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/addhmo");
        List<ViewCountry> allMediCountries = utiDao.getAllMediCountries();
        mav.addObject("allCountries",allMediCountries);        
        mav.addObject("viewModel", new HmoViewModel());
         mav.addObject("hmo", "active");
        return mav ;
    }
        
         @RequestMapping(value = "/addhmo", method = RequestMethod.POST)
    public String addNewHmo(@ModelAttribute("viewModel") @Valid HmoViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new HmoViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addhmo";
        }             
           Hmo toHmo = viewModel.toHmo();
            boolean addHmo = utiService.addHmo(toHmo);
            if(!addHmo){
                model.addAttribute("viewModel", new HmoViewModel());
                model.addAttribute("error", "Unable to add Hmo");
                return "/secured/addhmo";
            } 
        model.addAttribute("success", "HMO Provider added successfully");
         return "redirect:/uti/allhmos";

    }

    
      @RequestMapping(value = {"/allhmos"}, method = RequestMethod.GET)
    public String AllMediHmos(Model model) throws CosmosDatabaseException {
        model.addAttribute("hmo", "active");
        return "/views/uti/allhmos";
    }
       @PreAuthorize(value = "isAuthenticated()")
     @RequestMapping(value = "/getallhmos", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<CustomHmoView> allMediHmos(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {

         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<CustomHmoView> result;
             Page<ViewHmo> page = utiDao.getAllMediHmos(start, length);
         List<ViewHmo> content = page.getContent();
             List<CustomHmoView> pageView = new ArrayList<>();
      
                 for (ViewHmo hmo : content) {                    
                CustomHmoView pageObject = new CustomHmoView();
                    pageObject.setAddress(hmo.getAddress());
                    pageObject.setEmail(hmo.getEmail());
                    pageObject.setHmoId(hmo.getHmoId());
                    pageObject.setCountryId(hmo.getCountryId());
                    pageObject.setCountryName(utiDao.findCountryById(hmo.getCountryId()).getCountryName());
                    pageObject.setCreatedOn(hmo.getCreatedOn());
                    pageObject.setHmoName(hmo.getHmoName());
                  
                
                pageView.add(pageObject);
            }
            result = new PageResult<>(pageView, page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
     @RequestMapping(value = "/addhmoplan", method = RequestMethod.GET)
    public ModelAndView addMediHmoPlan(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/addhmoplan");          
        mav.addObject("viewModel", new HmoPlanViewModel());
         mav.addObject("hmo", "active");
        return mav ;
    }
        
         @RequestMapping(value = "/addhmoplan", method = RequestMethod.POST)
    public String addNewHmoPlan(@ModelAttribute("viewModel") @Valid HmoPlanViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new HmoPlanViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addhmoplan";
        }             
        HmoPlan toHmoPlan = viewModel.toHmoPlan();
            boolean addHmoPlan = utiService.addHmoPlan(toHmoPlan);
            if(!addHmoPlan){
                model.addAttribute("viewModel", new HmoPlanViewModel());
                model.addAttribute("error", "Unable to add Hmo plan");
                return "/secured/addhmoplan";
            } 
        model.addAttribute("success", "HMO Plan added successfully");
         return "redirect:/uti/allhmoplans";

    }

    
      @RequestMapping(value = {"/allhmoplans"}, method = RequestMethod.GET)
    public ModelAndView AllMediHmoPlans(Model model) throws CosmosDatabaseException {
      ModelAndView mav = new ModelAndView("/views/uti/allhmoplans");
        List<ViewHmoPlan> allMediHmoPlans = utiDao.getAllMediHmoPlans();
        mav.addObject("allHmoPlans",allMediHmoPlans);
          mav.addObject("hmo", "active");
        return mav;
    }
    
    
      @RequestMapping(value = "/assignplan", method = RequestMethod.GET)
    public ModelAndView assignMediHmoPlan(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/assignplan");     
        List<ViewHmoPlan> allMediHmoPlans = utiDao.getAllMediHmoPlans();
        List<ViewHmo> allMediHmo = utiDao.getAllMediHmo();
        mav.addObject("viewModel", new AssignHmoPlanViewModel());
         mav.addObject("allMediHmoPlans",allMediHmoPlans);
          mav.addObject("allMediHmo", allMediHmo);
         mav.addObject("hmo", "active");
        return mav ;
    }
        
         @RequestMapping(value = "/assignplan", method = RequestMethod.POST)
    public String assignNewHmoPlan(@ModelAttribute("viewModel") @Valid AssignHmoPlanViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws CosmosDatabaseException {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new AssignHmoPlanViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/assignplan";
        }           
          
        AssignHmoPlan toAssignPlan = viewModel.toAssignPlan();
        ViewHmo findHmoById = utiDao.findHmoById(toAssignPlan.getHmoId());
        ViewHmoPlan findHmoPlanById = utiDao.findHmoPlanById(toAssignPlan.getPlanId());
        toAssignPlan.setHmoName(findHmoById.getHmoName());
        toAssignPlan.setPlanName(findHmoPlanById.getPlanName());
            boolean assign = utiService.assignHmoPlan(toAssignPlan);
            if(!assign){
                model.addAttribute("viewModel", new AssignHmoPlanViewModel());
                model.addAttribute("error", "Unable to assign Hmo plan");
                return "/secured/assignplan";
            } 
        model.addAttribute("success", "HMO Plan assigned successfully");
         return "redirect:/uti/assignedplans";

    }

      @RequestMapping(value = {"/assignedplans"}, method = RequestMethod.GET)
    public String AllAssginedHmos(Model model) throws CosmosDatabaseException {
        model.addAttribute("hmo", "active");
        return "/views/uti/assignedplans";
    }
       @PreAuthorize(value = "isAuthenticated()")
     @RequestMapping(value = "/getassignedplans", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<ViewAssignedPlan> allAssignedPlans(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {

         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewAssignedPlan> result;
             Page<ViewAssignedPlan> page = utiDao.getAllAssignedHmoPlan(start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
        
        
      @RequestMapping(value = {"/reviewrequests"}, method = RequestMethod.GET)
    public String AllReviewRequest(Model model) throws CosmosDatabaseException {
        model.addAttribute("reviewrequest", "active");
        return "/views/uti/reviewrequest";
    }
       @PreAuthorize(value = "isAuthenticated()")
     @RequestMapping(value = "/getallreviewrequests", method = RequestMethod.GET)
    @ResponseBody
        public PageResult<ViewReviewRequest> allReviewRequest(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {

         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewReviewRequest> result;
             Page<ViewReviewRequest> page = userDao.getAllReviewRequest(start, length);  
            result = new PageResult<>( page.getContent(), page.getCount() , page.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
        
 @RequestMapping(value = "/addallergycategory", method = RequestMethod.GET)
    public ModelAndView addMediAllergyCategory(Model model) throws CosmosDatabaseException {     
        ModelAndView mav = new ModelAndView("/secured/addallergycategory");          
        mav.addObject("viewModel", new AllergiesCategoryViewModel());
         mav.addObject("allergy", "active");
        return mav ;
    }
        
         @RequestMapping(value = "/addallergycategory", method = RequestMethod.POST)
    public String addNewAllergyCategory(@ModelAttribute("viewModel") @Valid AllergiesCategoryViewModel viewModel,HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) {
          if (result.hasErrors()) {
            model.addAttribute("viewModel", new AllergiesCategoryViewModel());
            model.addAttribute("error", "Please correct invalid fields");
        return "/secured/addallergycategory";
        }             
        AllergyCategory toCategory = viewModel.toCategory();
            boolean addCategory = utiService.addAllergyCategory(toCategory);
            if(!addCategory){
                model.addAttribute("viewModel", new AllergiesCategoryViewModel());
                model.addAttribute("error", "Unable to add Allergy Category");
                return "/secured/addhmoplan";
            } 
        model.addAttribute("success", "Allergy Category added successfully");
         return "redirect:/uti/allergycategories";

    }

    
      @RequestMapping(value = {"/allergycategories"}, method = RequestMethod.GET)
    public ModelAndView AllAllergyCategories(Model model) throws CosmosDatabaseException {
      ModelAndView mav = new ModelAndView("/views/uti/allergycategories");
        List<ViewAllergyCategory> allAllergyCategories = medicalDao.getAllAllergyCategories();
        mav.addObject("allAllergyCategories",allAllergyCategories);
          mav.addObject("allergy", "active");
        return mav;
    }
    
     @RequestMapping(value = {"/allergies"}, method = RequestMethod.GET)
    public String AllAllergies(Model model) throws CosmosDatabaseException {
        model.addAttribute("allergy", "active");
        return "/admins/allergies";
    }
    
     
     @RequestMapping(value = "/getallergies", method = RequestMethod.GET)
     @ResponseBody
        public PageResult<ViewCustomAllergy> PaginatedAllUseraAllergies(@RequestParam(value = "start", required = false, defaultValue = "0") int start, 
            @RequestParam(value = "length", required = false, defaultValue = "10") int length, 
            @RequestParam(value = "draw", required = false, defaultValue = "1") int draw,  HttpSession session) throws CosmosDatabaseException {
     
         if(start > 0){
            start = start / length ;
        }
     
        try{
        PageResult<ViewCustomAllergy> result;
          Page<ViewAllergy> allUserAllergies = medicalDao.getAllAllergies(start, length);  
          List<ViewAllergy> allergies = allUserAllergies.getContent();
          List<ViewCustomAllergy> userAllergies = new ArrayList<>();
            if(allergies.size() > 0){
               for(ViewAllergy allergy : allergies){
                ViewCustomAllergy customAllergy = new ViewCustomAllergy();
                customAllergy.setActive(allergy.isActive());
                customAllergy.setAllergicTo(allergy.getAllergicTo());
                customAllergy.setCategoryId(allergy.getCategoryId());
                customAllergy.setCategoryName(medicalDao.findAllergyCategoryById(allergy.getCategoryId()).getCategoryName());
                customAllergy.setComment(allergy.getComment());
                customAllergy.setCreatedOn(allergy.getCreatedOn());
                customAllergy.setEntryId(allergy.getEntryId());
                customAllergy.setLocation(allergy.getLocation());
                customAllergy.setReaction(allergy.getReaction());
                customAllergy.setSeverity(allergy.getSeverity());
                customAllergy.setStarted(allergy.getStarted());
                customAllergy.setUsername(allergy.getUsername());
                        
                    userAllergies.add(customAllergy);
               
               
               }
            
            
            }
            result = new PageResult<>( userAllergies, allUserAllergies.getCount() , allUserAllergies.getCount());
          result.setDraw(draw);
            
            return result;
             }catch(CosmosDatabaseException cde){
            return null;
        }
    }
        
         @RequestMapping(value = {"/deleteallergy"}, method = RequestMethod.GET)    
    public String deleteAllergy(@RequestParam("entryId") String entryId, Model model) throws CosmosDatabaseException  {
       
        medicalDao.deleteAllergyByEntryId(entryId);
                
      return "redirect:/persona/allallergies";
    }
       
}
