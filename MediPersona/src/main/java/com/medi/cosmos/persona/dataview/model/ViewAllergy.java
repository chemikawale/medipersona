/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.dataview.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
public class ViewAllergy {
     private String categoryId;
    private String allergicTo;
    private String reaction;
    private String location;
    private String severity;
    private boolean active;
    private String started;
    private String comment;
    private String entryId;
    private String createdOn;
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getAllergicTo() {
        return allergicTo;
    }

    public void setAllergicTo(String allergicTo) {
        this.allergicTo = allergicTo;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getCreatedOn() {
              if( createdOn != null ){
         SimpleDateFormat formatObj = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date format = null;
        try {
            format = formatObj.parse(createdOn);
        } catch (ParseException ex) {
            Logger.getLogger(ViewAllergy.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            SimpleDateFormat formatObj2 = new SimpleDateFormat("E, dd-MMM-yyyy h:mm a");
            
        return formatObj2.format(format);}
         else 
             return createdOn; 
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
    
}
