/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.persona.utilities;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Oyewale
 * @param <T>
 */
public class PageResult<T> implements Serializable{
 //private List<T> data;
 private List<T> data;
 //private int draw;
 private int draw;
// private Long recordsTotal;
 private Long recordsTotal;
 //private Long recordsFiltered;
 private Long recordsFiltered;

    public PageResult() {
    }

    public PageResult(List<T> data,Long recordsTotal, Long recordsFiltered) {
        this.data = data;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
    }

   

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public Long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(Long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public Long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(Long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }
}