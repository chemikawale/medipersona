
CREATE DATABASE IF NOT EXISTS medipersona;

CREATE TABLE IF NOT EXISTS medipersona.db_migrations(
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    version VARCHAR(128) NOT NULL,
    moduleName VARCHAR(128) NOT NULL,
    description VARCHAR(128),
    createdOn DATETIME NULL
);


DROP PROCEDURE IF EXISTS medipersona.create_db_migration;

DELIMITER $$

CREATE PROCEDURE medipersona.create_db_migration(
	OUT id INTEGER,
	IN version VARCHAR(128),
        IN moduleName VARCHAR(128),
        IN description VARCHAR(128),
        IN createdOn DATETIME
)
BEGIN
	INSERT INTO db_migrations
        (
            version,
            moduleName,
            description,
            createdOn
        )
	VALUES
	(
            version,
            moduleName,
            description,
            createdOn
	);
        SET id = LAST_INSERT_ID();
       
        
END $$
DELIMITER ;



DROP PROCEDURE IF EXISTS medipersona.retrieve_last_migration;
DELIMITER $$
CREATE PROCEDURE medipersona.retrieve_last_migration
(
	IN moduleName VARCHAR(100)
)
BEGIN
  SELECT *  FROM db_migrations  WHERE db_migrations.moduleName = moduleName;
END$$

DELIMITER ;
