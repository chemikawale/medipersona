package com.medi.cosmos.core.dao;

import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.IHaveId;
import com.medi.cosmos.core.model.Page;
import java.util.List;

/**
 * @author tosin.eniolorunda
 *
 * Sep 4, 2013
 * @param <T> Model
 */
public interface Dao<T extends IHaveId> {

    public T create(T model) throws CosmosDatabaseException;

    public boolean update(T model) throws CosmosDatabaseException;

    public boolean delete(T model) throws CosmosDatabaseException;
 
    public T find(long id) throws CosmosDatabaseException;
    
    public T findByKey(String key) throws CosmosDatabaseException;

    public List<T> findAll() throws CosmosDatabaseException;

    public Page<T> findAll(int pageNum, int pageSize) throws CosmosDatabaseException;
    
    public DbEventHandle getEventsHandle();

}
