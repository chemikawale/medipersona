/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational.migration;

import com.medi.cosmos.core.exceptions.CosmosException;
import com.medi.cosmos.core.logging.LogManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 *
 * @author eniolorunda.tosin
 */
public class DBMigrationService {

    private static final String DB_MIGRATION_ROOT_FOLDER = "/db/";
    private static final String JAR_SEARCH_LOCATION = "classpath*:";
    private static final String MODULE_LOCATION_PATH = "/db/*_migration";
    private static final String INIT_VERSION_SCRIPTS_PATH = "classpath*:/db/cosmos-core_migration/migrationinitialisation.sql";
    /*
     script pattern
     Revision_FriendlyDescriotion_AlwaysRunFlag(_).sql
     */
    private static final String SCRIPT_REGEX = "/*_*_*.sql";
    private static final Logger errorLogger = LogManager.getLogger(DBMigrationService.class);
    private DataSource dataSource;

    private void apply(PspDBMigrationDao migrationDao, String currentModule, Resource[] versionScripts) throws Exception {

        DbMigration currentVersion = migrationDao.getCurrentDBVersion(currentModule);
        List<DbMigration> migrationFiles = new ArrayList<>();
        for (Resource versionScript : versionScripts) {
            if (DbMigration.isValidName(versionScript.getFilename())) {
                migrationFiles.add(new DbMigration(versionScript, currentModule));
            }
        }
        List<DbMigration> migrationList = DbMigration.getSortedRunList(migrationFiles, currentVersion);
        for (DbMigration migration : migrationList) {
            /*apply version script*/
            migrationDao.applyVersionScript(migration.getResource());
            /*create the migration entry*/
            if (currentVersion == null || currentVersion.getVersion().compareTo(migration.getVersion()) < 0) {
                migrationDao.create(migration);
            }
        }
    }

    private void setupMigrationTable(PspDBMigrationDao migrationDao, PathMatchingResourcePatternResolver resourceResolver) throws IOException, CosmosException, SQLException {
        Resource[] initMigrationResources = resourceResolver.getResources(INIT_VERSION_SCRIPTS_PATH);
        if (initMigrationResources.length < 0) {
            throw new CosmosException("Could not locate initial migration setup script");
        }
        Resource initMigrationScript = initMigrationResources[0];
        migrationDao.applyVersionScript(initMigrationScript);
    }

    public void migrate() throws Exception {
        try {
            PspDBMigrationDao migrationDao = new PspDBMigrationDao();
            migrationDao.setDataSource(dataSource);
            PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
            setupMigrationTable(migrationDao, resourceResolver);         
            Resource[] moduleFolders = resourceResolver.getResources(JAR_SEARCH_LOCATION + MODULE_LOCATION_PATH);
            int rootIndex;
            String currentModule;
            Resource[] versionScripts;
            for (Resource moduleFolder : moduleFolders) {
                rootIndex = moduleFolder.getURL().getFile().lastIndexOf(DB_MIGRATION_ROOT_FOLDER);
                currentModule = moduleFolder.getURL().getFile().substring(rootIndex + DB_MIGRATION_ROOT_FOLDER.length()).split("_")[0];
                versionScripts = resourceResolver.getResources(JAR_SEARCH_LOCATION + MODULE_LOCATION_PATH.replace("*", currentModule) + SCRIPT_REGEX);
                if (versionScripts != null && versionScripts.length > 0) {
                    apply(migrationDao, currentModule, versionScripts);

                }
            }

        } catch (Exception ex) {
            errorLogger.log(Level.SEVERE, "An error occured while migratiing DB", ex);
            throw ex;
        }
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
