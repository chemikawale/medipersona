/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao;

import com.medi.cosmos.core.async.AsyncOpertationCallback;
import com.medi.cosmos.core.async.AsyncWorkerPool;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.model.IHaveId;
import com.medi.cosmos.core.model.Page;
import java.util.List;

/**
 *
 * @author tosineniolorunda
 * @param <T> Model
 */
public class DaoAsyncWrapper<T extends IHaveId> implements Dao<T> {

    private final Dao<T> dao;
    private final AsyncWorkerPool asyncWorkerPool;

    public DaoAsyncWrapper(Dao<T> dao, AsyncWorkerPool asyncWorkerPool) {
        this.dao = dao;
        this.asyncWorkerPool = asyncWorkerPool;
    }

    public void asyncCreate(final T model, final AsyncOpertationCallback<T> cb) {
        asyncWorkerPool.queueJob(() -> create(model), cb);
    }

    public void asyncUpdate(final T model, final AsyncOpertationCallback<Boolean> cb) {
        asyncWorkerPool.queueJob(() -> update(model), cb);
    }

    public void asyncDelete(T model, final AsyncOpertationCallback<Boolean> cb) {
        asyncWorkerPool.queueJob(() -> update(model), cb);
    }

    @Override
    public T create(T model) throws CosmosDatabaseException {
        return dao.create(model);
    }

    @Override
    public boolean update(T model) throws CosmosDatabaseException {
        return dao.update(model);
    }

    @Override
    public boolean delete(T model) throws CosmosDatabaseException {
        return dao.delete(model);
    }

    @Override
    public T find(long id) throws CosmosDatabaseException {
        return dao.find(id);
    }

    @Override
    public T findByKey(String key) throws CosmosDatabaseException {
        return dao.findByKey(key);
    }

    @Override
    public List<T> findAll() throws CosmosDatabaseException {
        return dao.findAll();
    }

    @Override
    public Page<T> findAll(int pageNum, int pageSize) throws CosmosDatabaseException {
        return dao.findAll(pageNum, pageSize);
    }

    @Override
    public DbEventHandle getEventsHandle() {
        return dao.getEventsHandle();
    }
}
