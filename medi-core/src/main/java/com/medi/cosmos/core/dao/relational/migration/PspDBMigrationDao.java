/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational.migration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tosin.eniolorunda
 */
@Repository
public class PspDBMigrationDao {

    private SimpleJdbcCall pspFindCurrentDBVersion, pspCreate;
    private JdbcTemplate jdbcTemplate;
    private static final String OBJECT = "object";

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        pspCreate = new SimpleJdbcCall(jdbcTemplate).withProcedureName("create_db_migration").withCatalogName("medipersona");
        pspFindCurrentDBVersion = new SimpleJdbcCall(jdbcTemplate).withProcedureName("retrieve_last_migration").returningResultSet(OBJECT, new BeanPropertyRowMapper(DbMigration.class)).withCatalogName("medipersona");
    }

    public DbMigration create(DbMigration model) throws Exception {
        BeanPropertySqlParameterSource in = new BeanPropertySqlParameterSource(model);
        //SqlParameterSource in = new MapSqlParameterSource().addValue("moduleName", model.getModuleName()).addValue("version", model.getVersion()).addValue("description", model.getDescription()).addValue("createdOn", model.getCreatedOn());
        Map<String, Object> m = pspCreate.execute(in);
        int id = (Integer) m.get("id");
        model.setId(id);
        return model;
    }

    public List<DbMigration> create(List<DbMigration> migrations) throws Exception {
        List<DbMigration> createdList = new ArrayList<>();
        for (DbMigration migration : migrations) {
            createdList.add(create(migration));
        }
        return createdList;
    }

    public DbMigration getCurrentDBVersion(String moduleName) {
        SqlParameterSource in = new MapSqlParameterSource().addValue("moduleName", moduleName);//.addValue("max_count", maxCount);
        Map<String, Object> m = pspFindCurrentDBVersion.execute(in);
        List<DbMigration> data = null;
        if (m.containsKey(OBJECT)) {
            data = (List<DbMigration>) m.get(OBJECT);
        }
        DbMigration dbVersion = (data != null && data.size() > 0) ? data.get(0) : null;
        return dbVersion;
    }

    /**
     *@param resource 
     * @throws java.io.FileNotFoundException
     * @throws java.sql.SQLException
     */
    public void applyVersionScript(Resource resource) throws FileNotFoundException, IOException, SQLException {
        try {
            ScriptRunner sr = new ScriptRunner(jdbcTemplate.getDataSource().getConnection(), false, true);
            BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream()));
            sr.runScript(reader);

        } catch (DataAccessException ex) {
            throw ex;
        }
    }

}
