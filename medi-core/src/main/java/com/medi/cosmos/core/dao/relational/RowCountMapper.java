/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Eniolorunda.Tosin
 */
public class RowCountMapper implements RowMapper<Long> {

    @Override
    public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            return rs.getLong(1);
        } catch (IndexOutOfBoundsException ex) {
            return 0L;
        }
        
    }
}
