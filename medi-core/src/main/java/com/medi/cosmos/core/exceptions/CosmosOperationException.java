/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosOperationException extends CosmosException {

    public CosmosOperationException() {
        super();
    }

    public CosmosOperationException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosOperationException(String message) {
        super(message);
    }
}
