/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.core.dao.relational;

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author tosineniolorunda
 */
public class BeanPropertySqlParameterSourceFactory<T> implements SqlParameterSourceFactory<T>{

    @Override
    public SqlParameterSource getSqlParameterSource(T model) {
        return new BeanPropertySqlParameterSource(model);
    }
    
}
