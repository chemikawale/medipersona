/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosDatabaseException extends CosmosException {
    
    public CosmosDatabaseException(){
        super();
    }
    
    public CosmosDatabaseException(String message, Throwable inner){
        super(message, inner);
    }
    
    public CosmosDatabaseException(String message){
        super(message);
    }
}
