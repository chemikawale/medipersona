/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosCryptoException extends CosmosException {

    public CosmosCryptoException() {
        super();
    }

    public CosmosCryptoException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosCryptoException(String message) {
        super(message);
    }
}
