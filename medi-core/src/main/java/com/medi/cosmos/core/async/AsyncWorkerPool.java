/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.async;

import com.medi.cosmos.core.logging.LogManager;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author tosin.eniolorunda
 */
@Component
public class AsyncWorkerPool {

    private static final int MIN_POOL_SIZE = 10;
    private static final Logger logger = LogManager.getLogger(AsyncWorkerPool.class);
    private final ExecutorService asyncExecutor;

    @Autowired
    public AsyncWorkerPool(@Value(value =  "${core.asyncworker.poolsize:20}") int poolSize) {
        if (poolSize <  MIN_POOL_SIZE){
            throw new IllegalArgumentException(String.format("Async worker pool size cannot be less than %s", MIN_POOL_SIZE));
        }
        asyncExecutor = Executors.newFixedThreadPool(poolSize);
        logger.log(Level.INFO, String.format("Starting AsyncWorkerPool with pool size %d",poolSize));
    }

    public void queueJob(Callable<?> job, AsyncOpertationCallback<?> cb) {
        Future<?> result = asyncExecutor.submit(job);
        if (cb != null) {
            cb.submitFuture(result);
        }
    }

    public void queueJob(Callable<?> job) {
        asyncExecutor.submit(job);
    }
}
