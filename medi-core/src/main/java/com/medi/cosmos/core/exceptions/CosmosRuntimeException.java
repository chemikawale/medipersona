/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosineniolorunda
 */
public class CosmosRuntimeException extends RuntimeException {

    public CosmosRuntimeException() {
        super();
    }

    public CosmosRuntimeException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosRuntimeException(String message) {
        super(message);
    }
}
