/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tosin.eniolorunda
 */
public class Page<T> {

    private Long count;
    private List<T> content = new ArrayList<>();

    public Page() {
    }

    public Page(Long count, List<T> content) {
        this.count = count;
        this.content = content;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * @return the content
     */
    public List<T> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(List<T> content) {
        this.content = content;
    }
}
