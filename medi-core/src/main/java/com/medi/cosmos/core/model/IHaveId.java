package com.medi.cosmos.core.model;

/**
 * @author tosin.eniolorunda
 *
 * Sep 4, 2013
 */
/**
 * @author tosin.eniolorunda
 *
 * Sep 4, 2013
 */
public interface IHaveId {

    public long getId();

    public void setId(long id);
}
