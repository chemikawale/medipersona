/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational.migration;

import com.medi.cosmos.core.model.IHaveId;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.core.io.Resource;

/**
 *
 * @author tosin.eniolorunda
 */
public class DbMigration implements IHaveId {

    private long id;
    private String version;
    private String moduleName;
    private String description;
    private boolean alwaysRun;
    private Date createdOn;
    private Resource resource;

    public DbMigration() {

    }

    public DbMigration(Resource resource, String moduleName) throws IOException {
        this.resource = resource;
        String fileName = resource.getFilename();
        if (!isValidName(fileName)) {
            throw new IllegalArgumentException("Invalid migration sql file format");
        }
        String[] tokens = fileName.split("_");
        if (tokens.length >= 2) {
            version = tokens[0];
            description = tokens[1];
            if (tokens.length > 2) {
                alwaysRun = true;
            }
        }
        createdOn = new Date();
        this.moduleName = moduleName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAlwaysRun() {
        return alwaysRun;
    }

    public void setAlwaysRun(boolean alwaysRun) {
        this.alwaysRun = alwaysRun;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    
    public static boolean isValidName(String fileName) {
        if (fileName == null || fileName.isEmpty() || !fileName.toLowerCase().endsWith(".sql")) {
            return false;
        }
        return fileName.split("_").length >= 2;

    }

    public static List<DbMigration> getSortedRunList(List<DbMigration> migrationFiles, DbMigration currentVersion) {
        migrationFiles.sort((DbMigration o1, DbMigration o2) -> o1.getVersion().compareTo(o2.getVersion()));
        if (currentVersion == null) {
            return migrationFiles;
        }
        List<DbMigration> runList = new ArrayList<>();
        for (DbMigration migrationSqlFile : migrationFiles) {
            if (migrationSqlFile.isAlwaysRun()) {
                runList.add(migrationSqlFile);
            }
        }
        if (migrationFiles.size() > 0) {
            DbMigration lastMigration = migrationFiles.get(migrationFiles.size() - 1);
            if (lastMigration.getVersion().compareTo(currentVersion.getVersion()) < 0) {
                runList.add(lastMigration);
            }
        } else {
            runList.add(currentVersion);
        }
        runList.sort((DbMigration o1, DbMigration o2) -> o1.getVersion().compareTo(o2.getVersion()));
        return runList;
    }

}
