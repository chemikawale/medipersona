/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosServiceException extends CosmosException {

    public CosmosServiceException() {
        super();
    }

    public CosmosServiceException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosServiceException(String message) {
        super(message);
    }
}
