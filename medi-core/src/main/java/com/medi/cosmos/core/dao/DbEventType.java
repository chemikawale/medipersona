/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.core.dao;

/**
 *
 * @author tosineniolorunda
 */
public enum DbEventType {
    Create,
    Delete,
    Update
}
