/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational;

import com.medi.cosmos.core.async.AsyncWorkerPool;
import com.medi.cosmos.core.dao.Dao;
import com.medi.cosmos.core.dao.DbEventHandle;
import com.medi.cosmos.core.dao.DbEventType;
import com.medi.cosmos.core.exceptions.CosmosDatabaseException;
import com.medi.cosmos.core.exceptions.CosmosDuplicateRecordException;
import com.medi.cosmos.core.model.IHaveId;
import com.medi.cosmos.core.model.Page;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

/**
 *
 * @author tosineniolorunda
 * @param <T> Model
 */
public abstract class PspBaseDao<T extends IHaveId> implements Dao<T> {

    protected static final String DB_OP_RESULT_NAME = "result";
    protected static final String FIND_RESULT_NAME = "object";
    protected static final String FIND_RESULT_SET_NAME = "list";
    protected static final String RESULT_COUNT_NAME = "count";
    protected static final String ID_COLUMN_NAME = "id";
    protected static final String PAGE_SIZE = "pageSize";
    protected static final String PAGE_NUM = "pageNum";

    private DbEventHandle<T> eventsHandle;
    private SimpleJdbcCall pspCreate;
    private SimpleJdbcCall pspDelete;
    private SimpleJdbcCall pspFindById;
    private SimpleJdbcCall pspFindByKey;
    private SimpleJdbcCall pspFindAll;
    private SimpleJdbcCall pspUpdate;
    private SqlParameterSourceFactory sqlParameterSourceFactory;
    private String idColumnName;
    private String findResultName;
    private String findByKeyColumn;
    private String pageNumVarName;
    private String pageSizeVarName;
    private String resultSetCountVarName;
    private String findResultSetName;
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AsyncWorkerPool asyncWorkerPool;

    public void init(SimpleJdbcCall pspCreate,
            SimpleJdbcCall pspDelete, SimpleJdbcCall pspFindById, SimpleJdbcCall pspFindAll,
            SimpleJdbcCall pspFindByKey, SimpleJdbcCall pspUpdate,
            SqlParameterSourceFactory sqlParameterSourceFactory, JdbcTemplate jdbcTemplate) {
        init(pspCreate, pspDelete, pspFindById, pspFindAll, FIND_RESULT_NAME, pspFindByKey, findByKeyColumn,
                pspUpdate, sqlParameterSourceFactory, ID_COLUMN_NAME, PAGE_NUM, PAGE_SIZE,
                RESULT_COUNT_NAME, FIND_RESULT_SET_NAME, jdbcTemplate);
    }

    public abstract void setDataSource(DataSource dataSource);

    public void init(SimpleJdbcCall pspCreate,
            SimpleJdbcCall pspDelete, SimpleJdbcCall pspFindById,
            SimpleJdbcCall pspFindAll, String findResultName, SimpleJdbcCall pspFindByKey,
            String findByKeyColumn, SimpleJdbcCall pspUpdate,
            SqlParameterSourceFactory sqlParameterSourceFactory, String idColumnName,
            String pageNumVarName, String pageSizeVarName, String resultSetCountVarName,
            String findResultSetName, JdbcTemplate jdbcTemplate) {
        eventsHandle = new DbEventHandle<>(asyncWorkerPool);
        this.pspCreate = pspCreate;
        this.pspDelete = pspDelete;
        this.pspFindById = pspFindById;
        this.pspFindByKey = pspFindByKey;
        this.findByKeyColumn = findByKeyColumn;
        this.findResultName = findResultName;
        this.pspFindAll = pspFindAll;
        this.pspUpdate = pspUpdate;
        this.sqlParameterSourceFactory = sqlParameterSourceFactory;
        this.idColumnName = idColumnName;
        this.pageNumVarName = pageNumVarName;
        this.pageSizeVarName = pageSizeVarName;
        this.resultSetCountVarName = resultSetCountVarName;
        this.findResultSetName = findResultSetName;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public T create(T model) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = sqlParameterSourceFactory.getSqlParameterSource(model);
            Map<String, Object> m = pspCreate.execute(in);
            long id = (Long) m.get(idColumnName);
            model.setId(id);
            eventsHandle.publihEvent(model, DbEventType.Create);
            return model;
        } catch (DuplicateKeyException ex) {
            throw new CosmosDuplicateRecordException("there is a duplcate record based on db constraints", ex);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while creating model", ex);
        }

    }

    @Override
    public boolean update(T model) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = sqlParameterSourceFactory.getSqlParameterSource(model);
            Map<String, Object> m = pspUpdate.execute(in);
            String[] keys = m.keySet().toArray(new String[m.size()]);
            int ret = (Integer) m.get(keys[0]);
            if (ret > 0) {
                eventsHandle.publihEvent(model, DbEventType.Update);
                return true;
            }
            return false;
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while updating database", ex);
        }
    }

    @Override
    public boolean delete(T model) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue(idColumnName, model.getId());
            Map<String, Object> m = pspDelete.execute(in);
            if (m.size() <= 0) {
                return false;
            }
            String[] keys = m.keySet().toArray(new String[m.size()]);
            int ret = (Integer) m.get(keys[0]);
            if (ret > 0) {
                eventsHandle.publihEvent(model, DbEventType.Delete);
                return true;
            }
            return false;
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while delete model from database", ex);
        }
    }

    @Override
    public T find(long id) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue(idColumnName, id);
            Map<String, Object> m = pspFindById.execute(in);
            if (m == null) {
                return null;
            }
            List<T> list = (List<T>) m.get(findResultName);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by id failed", ex);
        }
    }

    @Override
    public T findByKey(String key) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue(findByKeyColumn, key);
            Map<String, Object> m = pspFindByKey.execute(in);
            if (m == null) {
                return null;
            }
            List<T> list = (List<T>) m.get(findResultName);
            if (list == null || list.isEmpty()) {
                return null;
            }
            return list.get(0);
        } catch (Exception ex) {
            throw new CosmosDatabaseException("Find model by key failed", ex);
        }
    }

    @Override
    public List<T> findAll() throws CosmosDatabaseException {
        return findAll(0, 0).getContent();
    }

    @Override
    public Page<T> findAll(int pageNum, int pageSize) throws CosmosDatabaseException {
        try {
            SqlParameterSource in = new MapSqlParameterSource().addValue(pageNumVarName, pageNum).addValue(pageSizeVarName, pageSize == 0 ? null : pageSize);
            Map<String, Object> m = pspFindAll.execute(in);
            List<T> content = (List<T>) m.get(findResultSetName);
            int count =   (int) m.get(resultSetCountVarName);
            Page<T> page = new Page<>(Long.parseLong(""+count), content);
            return page;
        } catch (Exception ex) {
            throw new CosmosDatabaseException("There was a problem while finding all data", ex);
        }
    }

    @Override
    public DbEventHandle getEventsHandle() {
        return eventsHandle;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

}
