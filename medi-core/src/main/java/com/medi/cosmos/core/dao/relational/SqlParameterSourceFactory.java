/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao.relational;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author tosin.eniolorunda
 */
public interface SqlParameterSourceFactory<T> {
    
    public SqlParameterSource getSqlParameterSource(T model);
}
