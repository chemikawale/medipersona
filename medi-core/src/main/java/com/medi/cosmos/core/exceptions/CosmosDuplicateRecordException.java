/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosineniolorunda
 */
public class CosmosDuplicateRecordException extends CosmosDatabaseException {

    public CosmosDuplicateRecordException() {
        super();
    }

    public CosmosDuplicateRecordException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosDuplicateRecordException(String message) {
        super(message);
    }
}
