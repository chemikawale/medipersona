package com.medi.cosmos.core.logging;

import java.util.logging.Logger;

public class LogManager {

    /*Use this to log critical errors and warnings*/
    public static Logger getLogger(Class<?> clazz) {
        return Logger.getLogger(clazz.getName());
    }
}
