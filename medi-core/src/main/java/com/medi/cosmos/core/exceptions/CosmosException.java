/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.exceptions;

/**
 *
 * @author tosin.eniolorunda
 */
public class CosmosException extends Exception {

    public CosmosException() {
        super();
    }

    public CosmosException(String message, Throwable inner) {
        super(message, inner);
    }

    public CosmosException(String message) {
        super(message);
    }
}
