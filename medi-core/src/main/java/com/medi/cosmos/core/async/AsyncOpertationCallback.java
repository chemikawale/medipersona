/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.async;


import com.medi.cosmos.core.logging.LogManager;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tosin.eniolorunda
 * @param <T> returning object
 */
public class AsyncOpertationCallback<T> {

    private final static Logger errorLogger = LogManager.getLogger(AsyncOpertationCallback.class);
    private T result;
    private final AtomicBoolean isCompleted = new AtomicBoolean();
    private final Function<T, Object> functionCallback;

    public AsyncOpertationCallback(Function<T, Object> functionCallback) {
        this.functionCallback = functionCallback;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isCompleted() {
        return isCompleted.get();
    }

    public void setCompleted(T result) {
        setResult(result);
        this.isCompleted.set(true);
        if (functionCallback != null) {
            try {
                functionCallback.apply(result);
            } catch (Exception ex) {
                errorLogger.log(Level.SEVERE, "set completion threw an exception", ex);
            }
        }
        synchronized (this.isCompleted) {
            this.isCompleted.notifyAll();
        }
    }

    public void submitFuture(final Future<?> future) {
        new Thread(() -> {
            T result1 = null;
            try {
                result1 = (T) future.get();
            }catch (InterruptedException | ExecutionException | CancellationException ex) {
                errorLogger.log(Level.SEVERE, "Could not get future result", ex);
            } finally {
                setCompleted(result1);
            }
        },
                "waiting-for-the-future").start();
    }

    public void waitCompletion() {
        try {
            while (!isCompleted.get()) {
                synchronized (this.isCompleted) {
                    isCompleted.wait(); /*releases lock on isCompleted while waiting*/
                }
            }
        } catch (InterruptedException ex) {
            errorLogger.log(Level.SEVERE, "AsyncOperationCallback InterruptedException", ex);
        }
    }

    public void waitCompletion(long ms) {
        try {
            while (!isCompleted.get()) {
                synchronized (this.isCompleted) {
                    isCompleted.wait(ms); /*releases lock on isCompleted while waiting*/
                }
            }
        } catch (InterruptedException ex) {
            errorLogger.log(Level.SEVERE, "AsyncOperationCallback InterruptedException", ex);
        }
    }
}
