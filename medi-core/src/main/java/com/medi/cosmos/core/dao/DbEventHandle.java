/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.dao;

import com.medi.cosmos.core.async.AsyncWorkerPool;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 *
 * @author tosineniolorunda
 * @param <T> Model
 */
public class DbEventHandle<T> {

    private final List<BiConsumer<T, DbEventType>> changeListeners = new ArrayList<>();
    private final AsyncWorkerPool asyncWorkerPool;

    public DbEventHandle(AsyncWorkerPool asyncWorkerPool) {
        this.asyncWorkerPool = asyncWorkerPool;
    }

    public void addChangeListener(BiConsumer<T, DbEventType> listener) {
        synchronized (changeListeners) {
            changeListeners.add(listener);
        }
    }

    public void removeChangeListener(BiConsumer<T, DbEventType> listener) {
        synchronized (changeListeners) {
            changeListeners.remove(listener);
        }
    }

    public void publihEvent(T model, DbEventType eventType) {
        asyncWorkerPool.queueJob(() -> {
            synchronized (changeListeners) {
                for (BiConsumer<T, DbEventType> listener : changeListeners) {
                    listener.accept(model, eventType);
                }
                return null;
            }
        });
    }

}
