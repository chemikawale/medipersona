use medipersona;
CREATE TABLE IF NOT EXISTS cosmos.core_institutions(
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL UNIQUE,
    description VARCHAR(128),
    defaultLocationId INTEGER
);

