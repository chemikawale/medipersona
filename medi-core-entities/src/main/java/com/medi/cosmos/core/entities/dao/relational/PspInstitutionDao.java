/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.entities.dao.relational;

import com.medi.cosmos.core.dao.Dao;
import com.medi.cosmos.core.dao.relational.BeanPropertySqlParameterSourceFactory;
import com.medi.cosmos.core.dao.relational.PspBaseDao;
import com.medi.cosmos.core.dao.relational.RowCountMapper;
import com.medi.cosmos.core.entities.model.Institution;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

/**
 *
 * @author tosineniolorunda
 */
@Component
public class PspInstitutionDao extends PspBaseDao<Institution> implements Dao<Institution> {

    @Autowired
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SimpleJdbcCall pspCreate = new SimpleJdbcCall(dataSource).withProcedureName("psp_create_core_institution").withReturnValue();
        SimpleJdbcCall pspUpdate = new SimpleJdbcCall(jdbcTemplate).withProcedureName("psp_update_core_institution").withReturnValue();
        SimpleJdbcCall pspDelete = new SimpleJdbcCall(jdbcTemplate).withProcedureName("psp_delete_core_institution").withReturnValue();
        BeanPropertyRowMapper rowMapper = new BeanPropertyRowMapper(Institution.class);
        rowMapper.setPrimitivesDefaultedForNullValue(true);
        SimpleJdbcCall pspFind = new SimpleJdbcCall(jdbcTemplate).withProcedureName("psp_retrieve_core_institution").returningResultSet(FIND_RESULT_NAME, rowMapper);
        SimpleJdbcCall pspFindAll = new SimpleJdbcCall(jdbcTemplate).withProcedureName("psp_retrieve_core_institutions").returningResultSet("count", new RowCountMapper()).returningResultSet(FIND_RESULT_SET_NAME, rowMapper);
        super.init(pspCreate, pspDelete, pspFind, pspFindAll, null, pspUpdate, new BeanPropertySqlParameterSourceFactory(), jdbcTemplate);

    }
}
