/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.core.entities.model;

import com.medi.cosmos.core.model.IHaveId;


/**
 *
 * @author tosin.eniolorunda
 * Institution is an entity that can directly map to say a Company
 * 
 */
public class Institution implements IHaveId {

    private long id;
    private String name;
    private String description;
    private Integer defaultLocationId;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDefaultLocationId() {
        return defaultLocationId;
    }

    public void setDefaultLocationId(Integer defaultLocationId) {
        this.defaultLocationId = defaultLocationId;
    }
}
