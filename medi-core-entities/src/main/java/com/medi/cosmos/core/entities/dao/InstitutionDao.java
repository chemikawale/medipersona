/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.core.entities.dao;

import com.medi.cosmos.core.dao.Dao;
import com.medi.cosmos.core.entities.model.Institution;

/**
 *
 * @author tosineniolorunda
 */
public interface InstitutionDao extends Dao<Institution>{
    
}
