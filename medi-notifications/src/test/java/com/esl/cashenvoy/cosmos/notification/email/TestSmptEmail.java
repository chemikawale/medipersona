/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.esl.cashenvoy.cosmos.notification.email;

import com.medi.cosmos.notification.email.SmtpMailSender;
import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import java.util.Iterator;
import javax.mail.MessagingException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author tosineniolorunda
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class TestSmptEmail {

    @Test
    public void testSmtpEmail() throws MessagingException {
        SimpleSmtpServer server = SimpleSmtpServer.start(3400);
        SmtpMailSender smtpMailSender = new SmtpMailSender(null, null, "localhost", 3400, false, false);
        smtpMailSender.sendTextMail("noreply@esl.com", "Test Email", "This is a test email", new String[]{"receiver@there.com"});
        server.stop();
        Assert.assertTrue(server.getReceivedEmailSize() == 1);
        Iterator emailIter = server.getReceivedEmail();
        SmtpMessage email = (SmtpMessage) emailIter.next();
        Assert.assertTrue(email.getHeaderValue("Subject").equals("Test Email"));
        Assert.assertTrue(email.getBody().equals("This is a test email"));
    }

}
