/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.notification.sms;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Adeyemi
 */
public class SmsMessage {
    
    private String senderName;
    private  List<SmsRecipient> recipients = new ArrayList<>();
    private String message;

    public SmsMessage(String senderName, SmsRecipient... recipient  ) {
        this.senderName = senderName;
        recipients  = Arrays.asList(recipient);
    }
    
    public SmsMessage(String senderName, String message, SmsRecipient... recipient  ) {
        this(senderName, recipient);
        this.message = message;
    }
    
    /**
     * @return the senderName
     */
    public String getSenderName() {
        return senderName;
    }

    /**
     * @param senderName the senderName to set
     */
    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    /**
     * @return the recipients
     */
    public List<SmsRecipient> getRecipients() {
        return recipients;
    }

    /**
     * 
     * @param recipient 
     */
    public void addRecipient(SmsRecipient recipient) {
        recipients.add(recipient);
    }
    
    public boolean removeRecipient(SmsRecipient recipient){
        
        
        
        for(int i = 0; i < recipients.size(); i++){
            if(recipients.get(i).getNationalSubscriberNumber().equals(recipient.getNationalSubscriberNumber())){
                recipients.remove(recipients.get(i));
                return true;
            }
        }
        
        return false;
    } 

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
