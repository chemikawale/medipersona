/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.notification.sms;

/**
 *
 * @author Adeyemi
 */
public abstract class SmsProvider {
    
    private String username;
    private String password;
    private String url;
    
    public SmsProvider(String username, String password, String url){
        this.password = password;
        this.username = username;
        this.url = url;
    }
    
    public abstract boolean send(SmsMessage smsMessage);

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
