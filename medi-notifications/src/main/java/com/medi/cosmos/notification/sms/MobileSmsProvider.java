/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.notification.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 * @author Adeyemi
 */
public class MobileSmsProvider extends SmsProvider {

    
    //static String allRecipients =  "";
    
    public MobileSmsProvider(String username, String password){
        super(username, password, "http://nuobjects.com/nusms/index.php?" );
    }
    
    public MobileSmsProvider(String username, String password, String url) {
        super(username, password, url);
    }
    
    @Override
    public boolean send(SmsMessage message) {
        
        if (message == null)
            return false;
        
        if (message.getRecipients().isEmpty() || message.getSenderName().isEmpty())
            return false;
        
        try{
            String buildMessage = build(message);
            
            //String encodedMessage = URLEncoder.encode(buildMessage, "UTF-8");
            
            URL url = new URL(buildMessage);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod("GET");
            
            int responseCode = connection.getResponseCode();
            
            if(responseCode != 200)
                return false;
            
            BufferedReader bis = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            String responseMessage;
            
            StringBuilder buffer = new StringBuilder();
            
            while((responseMessage = bis.readLine())!= null){
                buffer.append(responseMessage);
            }
            
            if (buffer.toString().equalsIgnoreCase("Sent"))
                return true;
                
        }
        catch(IOException exception){
            System.out.println("Error while sending message: "+ exception.getMessage());
        }
        
        return false;
        
    }
    
    private String build(SmsMessage message) throws UnsupportedEncodingException{
        
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getUrl());
        stringBuilder.append("user=");
        stringBuilder.append(getUsername());
        stringBuilder.append("&pass=");
        stringBuilder.append(getPassword());
        stringBuilder.append("&to=");
        String allRecipients = message.getRecipients().stream().map(x -> {
            return x.toString();
        }).reduce("", (acc,element) -> acc+""+element+",");
                
        allRecipients = allRecipients.substring(0,allRecipients.length() - 1);
        stringBuilder.append(allRecipients);
        stringBuilder.append("&from=");
        stringBuilder.append(message.getSenderName());
        stringBuilder.append("&msg=");
        stringBuilder.append(URLEncoder.encode(message.getMessage(), "UTF-8"));
        
        return stringBuilder.toString();
    }
    
}
