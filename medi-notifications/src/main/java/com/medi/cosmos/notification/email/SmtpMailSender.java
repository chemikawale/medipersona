/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.medi.cosmos.notification.email;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

/**
 *
 * @author tosineniolorunda
 */
public class SmtpMailSender {

    private final Properties properties;
    private final Session session;
    @Autowired
    private JavaMailSender mailSender;

    public SmtpMailSender(final String smtpUsername, final String smtpPassword, String smtpHost, int smtpPort, boolean isSmtpAuth, boolean isSmtpTls) {
        properties = new Properties();
        properties.put("mail.smtp.auth", Boolean.toString(isSmtpAuth));
        properties.put("mail.smtp.starttls.enable", Boolean.toString(isSmtpTls));
        properties.put("mail.smtp.host", smtpHost.trim());
        properties.put("mail.smtp.port", Integer.toString(smtpPort));
        properties.put("mail.smtp.socketFactory.port", "465"); //SSL Port
        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        Authenticator authenticator = null;
        if (isSmtpAuth) {
            authenticator = new javax.mail.Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpUsername.trim(), smtpPassword.trim());
                }
            };
        }
        session = Session.getInstance(properties, authenticator);
    }

    public void sendTextMail(String from, String subject, String messageBody, String[] recipients) throws MessagingException {

        InternetAddress[] recipientAddress = new InternetAddress[recipients.length];
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        int count = 0;
        for (String recipent : recipients) {
            recipientAddress[count] = new InternetAddress(recipent.trim());
            count++;
        }
        message.setRecipients(Message.RecipientType.TO, recipientAddress);
        message.setSubject(subject);
        message.setText(messageBody);
        Transport.send(message);

    }
     public void sendHtmlMail(String from, String title, String subject, String messageBody, String[] recipients) throws MessagingException{
        
        InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];        
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        
        try{
            message.setFrom(new InternetAddress(from, title));
        }catch(UnsupportedEncodingException ex){
            message.setFrom(new InternetAddress(from));
        }
            
        int count = 0;
        for (String recipent : recipients) {
            recipientAddresses[count] = new InternetAddress(recipent.trim());
            count++;
        }
        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        message.setSubject(subject);
        
        message.setContent(messageBody, "text/html");
        
        Transport.send(message);
    }
    
    public void sendHtmlMailWithCc(String from, String title, String subject, String messageBody, String[] recipients, String[] bcc, String[] cc) throws MessagingException{
     
        
        MimeMessage message  = new MimeMessage(session);
       
        //message.setFrom(new InternetAddress(from));
        
        try{
            message.setFrom(new InternetAddress(from, title));
        }catch(UnsupportedEncodingException ex){
            message.setFrom(new InternetAddress(from));
        }
            //setting the recipients
        int count = 0;
        
         if(recipients != null){
            if(recipients.length > 0){
                count = 0;
             InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];
                for (String recipient : recipients) {
                         if((recipient != null) || (recipient.isEmpty())){
                    recipientAddresses[count] = new InternetAddress(recipient.trim());
                    count++;
                        }
                }

                message.setRecipients(Message.RecipientType.CC, recipientAddresses); 
            }
        }
        
        
        
         if(cc != null){
            if(cc.length > 0){
                count = 0;
              InternetAddress[] carbonaddresses = new InternetAddress[cc.length];
                for (String cC : cc) {
                         if((cC != null) || (cC.isEmpty())){
                    carbonaddresses[count] = new InternetAddress(cC.trim());
                    count++;
                        }
                }

                message.setRecipients(Message.RecipientType.CC, carbonaddresses); 
            }
        }
        
        if(bcc != null)
            if(bcc.length > 0){
                count = 0;
               InternetAddress[] blindaddresses = new InternetAddress[bcc.length];
                for (String recipent : bcc) {
                    if((recipent != null) || (recipent.isEmpty())){
                    blindaddresses[count] = new InternetAddress(recipent.trim());
                    count++;
                    }
                }

                message.setRecipients(Message.RecipientType.BCC, blindaddresses); 
            }
     
        
       
        message.setSubject(subject);
        
        message.setContent(messageBody, "text/html");
        
        Transport.send(message);
        //message.setContent(messageBody, "text/html");
        
       
    }
    
    
    public void sendHtmlMail(String from, String subject, String messageBody, String[] recipients) throws MessagingException{
        
        InternetAddress[] recipientAddresses = new InternetAddress[recipients.length];
        
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
            
        int count = 0;
        for (String recipent : recipients) {
            recipientAddresses[count] = new InternetAddress(recipent.trim());
            count++;
        }
        message.setRecipients(Message.RecipientType.TO, recipientAddresses);
        message.setSubject(subject);
        
        message.setContent(messageBody, "text/html");
        
        Transport.send(message);
    }
    
    public void sendHtmlMailWithLogo(String from, String subject, String message,String[] recipients){
        
    }

}
