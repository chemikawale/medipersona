/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.medi.cosmos.notification.sms;

/**
 *
 * @author Adeyemi
 */
public class SmsRecipient {
    
    private String countryCode;
    private String nationalSubscriberNumber;

    private SmsRecipient() {
    }
    
    public SmsRecipient(String countryCode, String nationalSubscriberNumber) {
        this.countryCode = Character.isDigit(countryCode.charAt(0)) ? countryCode : countryCode.substring(1) ;
        this.nationalSubscriberNumber = nationalSubscriberNumber;
    }
    
    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * This is the country code of the recipient e.g 234 for Nigeria
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the nationalSubscriberNumber
     */
    public String getNationalSubscriberNumber() {
        return nationalSubscriberNumber;
    }

    /**
     * @param nationalSubscriberNumber the nationalSubscriberNumber to set
     */
    public void setNationalSubscriberNumber(String nationalSubscriberNumber) {
        this.nationalSubscriberNumber = nationalSubscriberNumber;
    }
    
    public static SmsRecipient toSmsRecipient(String recipient, int countryCodeLength){
        
        boolean hasSign = false;
        
        SmsRecipient smsRecipient = new SmsRecipient();
        
        Character checkSign = recipient.charAt(0);
        
        if(!Character.isDigit(checkSign)){
            hasSign = true;
        }
        
        int ccLength = countryCodeLength, start = 0;
        
        if(hasSign){
            start = 1;
            ccLength = countryCodeLength+1;
        }
        
        smsRecipient.setCountryCode(recipient.substring(start,ccLength));
        smsRecipient.setNationalSubscriberNumber(recipient.substring(ccLength));
        
        return smsRecipient;
    }
    
    @Override
    public String toString(){
        return countryCode+""+nationalSubscriberNumber;
    }
}
